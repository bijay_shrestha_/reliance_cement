<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!empty(getallheaders()['Accesstoken']))
        {
            $access_token = getallheaders()['Accesstoken'];        
            $this->db->where('access_token',$access_token);
            $check_token = $this->db->get('aauth_users')->result_array();

            if(empty($check_token))
            {
                echo json_encode('Access Denied');
                exit;
            }
        }
        else
        {
            echo json_encode('Access Token not sent on header');
            exit();
        }

        $this->load->model('payments/payment_model');
    }

    public function payment_get()
    {
        $this->db->where('user_id',getallheaders()['Userid']);
        $agent = $this->db->get('mst_agents')->row();

        $this->payment_model->_table = "view_payment";

        
        $where = "(agent_id = {$agent->id} AND is_published = 1)";

        $this->db->where($where);
        $rows=$this->payment_model->findAll();
        
        echo json_encode(array('rows'=>$rows));
        exit;
    }

    public function payment_post()
    {

    }

    public function payment_delete()
    {

    }


}