<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Payments
 *
 * Extends the Project_Controller class
 * 
 */

class AdminPayments extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Payments');

		$this->load->model('payments/payment_model');
		$this->load->model('agents/agent_model');
		$this->load->model('parties/party_model');
		$this->load->model('nepali_months/nepali_month_model');
		$this->load->model('payment_temporaries/payment_temporary_model');
		$this->load->library('sales/sale');
		
		$this->lang->load('payments/payment');
	}

	public function index()
	{
		list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
		if ($fiscal_year_id == null) {
			flashMsg('warning', 'Current Fiscal Year is not recorded yet. Please save the FISCAL YEAR.');
			redirect('admin/fiscal_years');
		}
		// Display Page
		if(is_agent())
		{
			$view_page = "agent_index";
		}
		else
		{
			$view_page = "index";
		}
		$data['header'] = lang('payments');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'payments';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->db->where('user_id',$this->session->userdata('id'));
		$agent = $this->db->get('mst_agents')->row();

		$this->payment_model->_table = "view_payment";

		$where = '1=1';	

		if(is_agent())
		{
			$where = "(agent_id = {$agent->id} AND is_published = 1)";
		}

		search_params();
		$this->db->where($where);
		$total=$this->payment_model->find_count();
		
		paging('id');
		
		search_params();
		$this->db->where($where);
		$rows=$this->payment_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}


	public function temporary_json()
	{
		$this->payment_model->_table = "view_payment_temporary";

		search_params();
		
		$total=$this->payment_model->find_count(array('is_processed'=>0));
		
		paging('id');
		
		search_params();
		
		$rows=$this->payment_model->findAll(array('is_processed'=>0));
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function date_only_json()
	{
		$where = '1=1';	
		if(is_agent())
		{
			$where = "('agent_id' = {$this->session->userdata('id')})";
		}
		$fields= array('payment_date','is_published');
		
		paging('id');
		
		search_params();
		
		$this->db->where($where);

		$this->db->group_by($fields);
		$rows=$this->payment_model->findAll(NULL,$fields);
		$total = count($rows);
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	/*public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->payment_model->insert($data);
        }
        else
        {
        	$success=$this->payment_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['created_by'] = $this->input->post('created_by');
    	$data['updated_by'] = $this->input->post('updated_by');
    	$data['deleted_by'] = $this->input->post('deleted_by');
    	$data['created_at'] = $this->input->post('created_at');
    	$data['updated_at'] = $this->input->post('updated_at');
    	$data['deleted_at'] = $this->input->post('deleted_at');
    	$data['payment_date'] = $this->input->post('payment_date');
    	$data['payment_date_np'] = $this->input->post('payment_date_np');
    	$data['nepali_month_id'] = $this->input->post('nepali_month_id');
    	$data['bank_name'] = $this->input->post('bank_name');
    	$data['agent_id'] = $this->input->post('agent_id');
    	$data['party_id'] = $this->input->post('party_id');
    	$data['amount'] = $this->input->post('amount');
    	$data['is_published'] = $this->input->post('is_published');

    	return $data;
    }*/

    public function upload_payment()
    {
    	$target_dir = "uploads/payment/";

    	if (!file_exists('./uploads/payment')) {
    		mkdir('./uploads/payment', 0777, true);
    	}
    	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

    	$uploadOk = 1;
    	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    		//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    	} else {
    		echo "Sorry, there was an error uploading your file.";
    	}    	
    	$this->excel_payments($_FILES["fileToUpload"]["name"]);
    }

    public function excel_payments($filename = NULL)
    {
    	list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
    	
    	if($this->input->post('filename'))
    	{
    		$filename = $this->input->post('filename');
    	}

    	if($this->input->post('new_agents'))
    	{
    		$new_agents = json_decode($this->input->post('new_agents'));
    		foreach ($new_agents as $key => $value) 
    		{
    			$agent['name'] = $value;
    			$this->agent_model->insert($agent);
    			
    			// create user as well
    			$username = strtolower(substr(str_replace('-','',str_replace(' ', '.', $value)),0,20));
    			$email = strtolower(substr(str_replace('-','',str_replace(' ', '.', $value)),0,20)).'@reliance.com';
    			$fullname = $value;
    			$password = 'password123';
    			$this->sale->register_user($email, $password, $username, $fullname);
    			
    		}
    	}
    	if($this->input->post('new_parties'))
    	{
    		$new_parties = json_decode($this->input->post('new_parties'));
    		foreach ($new_parties as $key => $value) 
    		{
    			$party['name'] = $value;
    			$this->party_model->insert($party);
    		}
    	}

    	$file = FCPATH . 'uploads/payment/'.$filename; 
    	$this->load->library('Excel');
    	$objPHPExcel = PHPExcel_IOFactory::load($file);
    	$objReader = PHPExcel_IOFactory::createReader('Excel2007');        
    	$objReader->setReadDataOnly(false);

    	$index = array('date','nepali_date', 'nepali_month', 'party', 'bank_name', 'payment_amount', 'agent');
    	$raw_data = array();
    	$view_data = array();
    	foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
    		if ($key == 0) {
    			$worksheetTitle = $worksheet->getTitle();
				$highestRow = $worksheet->getHighestRow(); // e.g. 10
				$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				$nrColumns = ord($highestColumn) - 64;

				for ($row = 2; $row <= $highestRow; ++$row) {
					for ($col = 0; $col < $highestColumnIndex; ++$col) {
						$cell = $worksheet->getCellByColumnAndRow($col, $row);
						$val = $cell->getValue();
						$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
						$raw_data[$row][$index[$col]] = $val;
					}
				}
			}
		}
		if(!$this->input->post('create_new'))
		{
			$new_agent = array();
			$new_party = array();
			foreach($raw_data as $key => $val) 
			{
				$get_agent = $this->agent_model->find(array('name'=>$val['agent']));
				if(!$get_agent)
				{
					if(!(in_array($val['agent'], $new_agent)))
					{
						$new_agent[] = $val['agent'];
					}
				}
				$get_party = $this->party_model->find(array('name'=>$val['party']));
				if(!$get_party)
				{
					if(!(in_array($val['party'],$new_party)))
					{
						$new_party[] = $val['party'];
					}
				}
			}

			if(!empty($new_agent) || !empty($new_party))
			{
				echo json_encode(array('new_agent'=>$new_agent,'new_party'=>$new_party));
				exit;
			}
		}
		
		foreach ($raw_data as $key => $value) 
		{
			$get_month = $this->nepali_month_model->find(array('name'=>$value['nepali_month']));
			$get_agent = $this->agent_model->find(array('name'=>$value['agent']));
			$get_party = $this->party_model->find(array('name'=>$value['party']));
			
			if(!$get_agent)
			{
				echo json_encode("Unknown Agent ".$value['agent']);
				exit;
			}
			
			if(!$get_party)
			{
				echo json_encode("Party Unknown ".$value['party']);
				exit;
			}

			$payment[$key]['payment_date'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($value['date']));
			$payment[$key]['payment_date_np'] = get_nepali_date(date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($value['date'])),'nep');
			$payment[$key]['nepali_month_id'] = $get_month->id;
			$payment[$key]['agent_id'] = $get_agent->id;
			$payment[$key]['party_id'] = $get_party->id;
			$payment[$key]['bank_name'] = $value['bank_name'];
			$payment[$key]['amount'] = $value['payment_amount'];
			$payment[$key]['fiscal_year_id'] = $fiscal_year_id;
		} 

		$this->db->trans_start();
		$success = $this->payment_temporary_model->insert_many($payment);
		if ($this->db->trans_status() === FALSE) 
		{
			$this->db->trans_rollback();
		} 
		else 
		{
			$this->db->trans_commit();
		} 
		$this->db->trans_complete();

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function mass_delete()
	{
		$date = $this->input->post('payment_date');
		$rows = $this->payment_model->findAll(array('payment_date'=>$date));
		foreach ($rows as $key => $value) 
		{
			$data['id']	= $value->id;
			$success = $this->payment_model->delete($data['id']);
		}

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function publish_data()
	{
		$date = $this->input->post('payment_date');
		$rows = $this->payment_model->findAll(array('payment_date'=>$date));
		foreach ($rows as $key => $value) 
		{
			$data['id']	= $value->id;
			$data['is_published']	= 1;
			$success = $this->payment_model->update($data['id'],$data);
		}

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function invoice_delete()
	{
		$data['id'] = $this->input->post('id');
		$success = $this->payment_model->delete($data['id']);
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function save()
	{
		$updated = $this->update_temporary_table();

		if($updated)
		{
			$data = $this->input->post('rows');
			foreach ($data as $key => $value) 
			{
				$payments[$key]['payment_date'] = $value['payment_date'];
				$payments[$key]['payment_date_np'] = $value['payment_date_np'];
				$payments[$key]['nepali_month_id'] =$value['nepali_month_id'];
				$payments[$key]['agent_id'] = $value['agent_id'];
				$payments[$key]['party_id'] = $value['party_id'];
				$payments[$key]['bank_name'] = $value['bank_name'];
				$payments[$key]['amount'] = $value['amount'];
				$payments[$key]['fiscal_year_id'] = $value['fiscal_year_id'];
			} 
			
			$this->db->trans_start();
			$success = $this->payment_model->insert_many($payments);
			if ($this->db->trans_status() === FALSE) 
			{
				$this->db->trans_rollback();
			} 
			else 
			{
				$this->db->trans_commit();
			} 
			$this->db->trans_complete();

			if($success)
			{
				$success = TRUE;
				$msg=lang('general_success');
			}
			else
			{
				$success = FALSE;
				$msg=lang('general_failure');
			}

			echo json_encode(array('msg'=>$msg,'success'=>$success));
			exit;	
		}
	}

	public function update_data()
	{
		$data['id'] = $this->input->post('id');
		$data[$this->input->post('column')] = $this->input->post('newvalue');
		$success = $this->payment_temporary_model->update($data['id'],$data);
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function update_data_payments()
	{
		$data['id'] = $this->input->post('id');
		$data[$this->input->post('column')] = $this->input->post('newvalue');
		$success = $this->payment_model->update($data['id'],$data);
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function update_temporary_table()
	{
		$update_data = array('is_processed'=>1);
		$updated = $this->db->update('tbl_payment_temporary',$update_data);
		return $updated;
	}

	public function publishAll()
	{
		$data['is_published'] = 1;
		$this->db->set($data);
		$success = $this->db->update('tbl_payment');
		// $success = $this->sale_model->update_all('tbl_sales',$data);
		echo json_encode (array('success' =>$success));
	}
}