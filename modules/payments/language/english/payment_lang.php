<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['payment_date'] = 'Payment Date';
$lang['payment_date_np'] = 'Payment Date Np';
$lang['nepali_month_id'] = 'Nepali Month';
$lang['bank_name'] = 'Bank Name';
$lang['agent_id'] = 'Agent';
$lang['party_id'] = 'Party';
$lang['amount'] = 'Amount';
$lang['is_published'] = 'Is Published';

$lang['payments']='Payments';