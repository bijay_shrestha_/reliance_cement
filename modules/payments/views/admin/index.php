<style type="text/css">
/* CSS REQUIRED */
.state-icon {
	left: -5px;
}
.list-group-item-primary {
	color: rgb(255, 255, 255);
	background-color: rgb(66, 139, 202);
}

/* DEMO ONLY - REMOVES UNWANTED MARGIN */
.well .list-group {
	margin-bottom: 0px;
}

.modal-xl{
	width: 1200px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('payments'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('payments'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridPaymentToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Paymentmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridPaymentFilterClear"><?php echo lang('general_clear'); ?></button>
					<button type="button" class="btn btn-success btn-flat btn-xs" id="publishAll"><?php echo lang('general_publish_all')?></button>
					<!-- <button type="button" class="btn btn-warning btn-flat btn-xs" data-toggle="modal" data-target="#massDelete">Mass Delete Invoice</button> -->
				</div>
				<div id="jqxGridPayment"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="messageNotification">
	<div>
		Successfully Completed Request.
	</div>
</div>

<div id="Paymentmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('payments'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4"><label>Choose File</label></div>
					<div class="col-md-8"><div id="payment_import"></div></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="agentaddModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<input type="hidden" name="agents[]">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New Agent and Party</h4>
			</div>
			<div class="modal-body">
				<div class="well" style="max-height: 300px;overflow: auto;">
					<h4>New Agents</h4>
					<ul id="check-list-box-agent" class="list-group checked-list-box">
						
					</ul>
					<h4>New Party</h4>
					<ul id="check-list-box-party" class="list-group checked-list-box">
						
					</ul>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="get-checked-data">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="deleteConfirmation" class="modal fade" role="dialog" style="z-index: 9999">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Payment</h4>
			</div>
			<div class="modal-body">
				<div class="well" style="max-height: 300px;overflow: auto;">
					<input type="hidden" id="child_grid_id">
					<input type="hidden" id="payment_id">
					<h1>Party Name :<div id="payment_partyname"></div></h1>
					<h1>Agent Name :<div id="payment_agentname"></div></h1>
					<h1>Bank Name :<div id="payment_bankname"></div></h1>
					<h1>Amount :<div id="payment_amount"></div></h1>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="delete_invoice">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div id="massDelete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mass Delete Payment</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><label>Payment Date: </label></div>
					<div class="col-md-9"><input type="text" class="text_input" id="payment_date" readonly="readonly"></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="deletemass">Delete</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<div id="Publishdata_payment" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Publish All Data</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><label>Payment Date: </label></div>
					<div class="col-md-9"><input type="text" class="text_input" id="payment_date_publish" readonly="readonly"></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="publish_all_data">Publish</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<div id="Preshow_Data" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Excel Data</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12"><div id="excelGrid"></div></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="saveExceldata">Approve</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="close_preshow">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(function(){
		$("#messageNotification").jqxNotification({
			width: 300, position: "top-right", opacity: 0.9,
			autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 4000, template: "success"
		});

		$('#payment_import').jqxFileUpload({ width: 300, uploadUrl: '<?php echo site_url('admin/Payments/upload_payment') ?>', fileInputName: 'fileToUpload' });
/*		$('#payment_import').on('uploadEnd', function (event) {
			var args = event.args;
			var fileName = args.file;
			var serverResponce = args.response;

			$('#Paymentmodal').modal('hide');
			open_preshow_modal();
		});
		*/
		$('#payment_import').on('uploadEnd', function (event) {
			$('#Paymentmodal').modal('hide');
			var args = event.args;
			var fileName = args.file;
			var serverResponce = args.response;

			var result = $.parseJSON(serverResponce);

			if(result.success)
			{
				open_preshow_modal();
			}
			else
			{				
				$('#agentaddModal').modal('show');
				$.each($.parseJSON(serverResponce),function(key,val){
					if(key == 'new_agent')
					{
						$.each(val,function(k,v)
						{
							$('#check-list-box-agent').append('<li class="list-group-item" data-color="success">'+v+'</li>');
						});
					}
					if(key == 'new_party')
					{
						$.each(val,function(i,va)
						{
							$('#check-list-box-party').append('<li class="list-group-item" data-color="success">'+va+'</li>');
						});
					}
				});

				$('.list-group.checked-list-box .list-group-item').each(function () {

					var $widget = $(this),
					$checkbox = $('<input type="checkbox" class="hidden" name="agent[]" />'),
					color = ($widget.data('color') ? $widget.data('color') : "primary"),
					style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
					settings = {
						on: {
							icon: 'glyphicon glyphicon-check'
						},
						off: {
							icon: 'glyphicon glyphicon-unchecked'
						}
					};

					$widget.css('cursor', 'pointer')
					$widget.append($checkbox);

					$widget.on('click', function () {
						$checkbox.prop('checked', !$checkbox.is(':checked'));
						$checkbox.triggerHandler('change');
						updateDisplay();
					});
					$checkbox.on('change', function () {
						updateDisplay();
					});


					function updateDisplay() {
						var isChecked = $checkbox.is(':checked');

						$widget.data('state', (isChecked) ? "on" : "off");

						$widget.find('.state-icon')
						.removeClass()
						.addClass('state-icon ' + settings[$widget.data('state')].icon);

						if (isChecked) {
							$widget.addClass(style + color + ' active');
						} else {
							$widget.removeClass(style + color + ' active');
						}
					}

					function init() {

						if ($widget.data('checked') == true) {
							$checkbox.prop('checked', !$checkbox.is(':checked'));
						}

						updateDisplay();

						if ($widget.find('.state-icon').length == 0) {
							$widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
						}
					}
					init();
				});
				$('#get-checked-data').on('click', function(event) {
					event.preventDefault(); 
					var AgentcheckedItems = {}, PartycheckedItems = {}, counter = 0;
					$("#check-list-box-agent li.active").each(function(idx, li) {
						AgentcheckedItems[counter] = $(li).text();
						counter++;
					});
					$("#check-list-box-party li.active").each(function(idx, li) {
						PartycheckedItems[counter] = $(li).text();
						counter++;
					});
					var new_agents = (JSON.stringify(AgentcheckedItems, null));
					var new_parties = (JSON.stringify(PartycheckedItems, null));

					$.post("<?php echo site_url('admin/Payments/excel_payments')?>",{new_agents : new_agents,new_parties : new_parties, filename:fileName, create_new : '1' }, function(result)
					{	
						if(result.success)
						{
							open_preshow_modal();
							$('#agentaddModal').modal('hide');
						}
					},'json');
				}); } 
			});

		var date_only_source =
		{
			datafields: [
			{ name: 'payment_date' , type :'string'},
			{ name: 'is_published' , type :'bool'}
			],
			root: "rows",
			id: 'payment_date',
			datatype: "json",
			async: false,
			url: '<?php echo site_url('admin/Payments/date_only_json') ?>'
		};

		var ordersSource =
		{
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'payment_date', type: 'string' },
			{ name: 'payment_date_np', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'party_id', type: 'number' },
			{ name: 'bank_name', type: 'string' },
			{ name: 'amount', type: 'number' },
			{ name: 'is_published', type: 'bool' },
			{ name: 'agent_name', type: 'string' },
			{ name: 'party_name', type: 'string' },
			{ name: 'nepali_month', type: 'string' },
			],
			root: "rows",              
			datatype: "json",
			url: '<?php echo site_url('admin/Payments/json') ?>',
			async: false
		};
		var ordersDataAdapter = new $.jqx.dataAdapter(ordersSource, { autoBind: true });
		orders = ordersDataAdapter.records;
		var nestedGrids = new Array();
            // create nested grid.
            var initrowdetails = function (index, parentElement, gridElement, record) {
            	var id = record.uid.toString();
            	var grid = $($(parentElement).children()[0]);
            	nestedGrids[index] = grid;
            	var filtergroup = new $.jqx.filter();
            	var filter_or_operator = 1;
            	var filtervalue = id;
            	var filtercondition = 'equal';
            	var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // fill the orders depending on the id.
                var ordersbyid = [];
                for (var m = 0; m < orders.length; m++) {
                	var result = filter.evaluate(orders[m]["payment_date"]);
                	if (result)
                		ordersbyid.push(orders[m]);
                }
                var orderssource = { datafields: [
                	{ name: 'id', type: 'number' },
                	{ name: 'created_by', type: 'number' },
                	{ name: 'updated_by', type: 'number' },
                	{ name: 'deleted_by', type: 'number' },
                	{ name: 'created_at', type: 'date' },
                	{ name: 'updated_at', type: 'date' },
                	{ name: 'deleted_at', type: 'date' },
                	{ name: 'payment_date', type: 'date' },
                	{ name: 'payment_date_np', type: 'string' },
                	{ name: 'nepali_month_id', type: 'number' },
                	{ name: 'agent_id', type: 'number' },
                	{ name: 'party_id', type: 'number' },
                	{ name: 'bank_name', type: 'string' },
                	{ name: 'amount', type: 'number' },
                	{ name: 'is_published', type: 'bool' },
                	{ name: 'agent_name', type: 'string' },
                	{ name: 'party_name', type: 'string' },
                	{ name: 'nepali_month', type: 'string' },
                	],
                	id: 'id',
                	localdata: ordersbyid
                }
                var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                if (grid != null) {
                	grid.jqxGrid({
                		source: nestedGridAdapter, 
                		width: '95%', 
                		height: 275,
                		sortable: true,
                		showfilterrow: true,
                		filterable: true,
                		columnsresize: true,
                		editable: true,
                		autoshowfiltericon: true,
                		columnsreorder: true,
                		selectionmode: 'multiplecellsadvanced',
                		showstatusbar: true,
                		showaggregates: true,
                		statusbarheight: 25,
                		columns: [
                		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer,editable: false, cellsrenderer: rownumberRenderer , filterable: false},
                		{
                			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' ,editable: false, cellsalign: 'center', cellclassname: 'grid-column-center', 
                			cellsrenderer: function (index) {
                				var row =  $(grid).jqxGrid('getrowdata', index);
                				// if(row.is_published == 0)
                				// {
                					var e = '<a href="javascript:void(0)" onclick="editSaleRecord(' + index + ','+ grid[0].id +' ); return false;" title="Delete Invoice"><i class="fa fa-trash"></i></a>';
                					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
                				// }
                			}
                		},
                		{ text: '<?php echo lang("payment_date"); ?>',datafield: 'payment_date',width: 120,filterable: false,editable: false,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
                		{ text: '<?php echo lang("payment_date_np"); ?>',datafield: 'payment_date_np',width: 140,filterable: true, editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 120,filterable: true, editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 250,filterable: true, editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_name',width: 250,filterable: true, editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("bank_name"); ?>',datafield: 'bank_name',width: 200,filterable: true, editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 250,filterable: true, editable: true,renderer: gridColumnsRenderer,
                		aggregates: ["sum"],cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', {  minimumFractionDigits: 2, maximumFractionDigits:2 }) + '</div>';
                		},
                		aggregatesrenderer: function (aggregates, column, element) {
                			var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + aggregates.sum.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits:2 }) + '</div>';
                			return renderstring;
                		}
                	},
                	]
                });
                	$(grid).on('cellvaluechanged', function (event) {
                		var rowBoundIndex = event.args.rowindex;
                		var column = event.args.datafield;
                		var newvalue = event.args.newvalue;
                		var rowdata = $(grid).jqxGrid('getrowdata', rowBoundIndex);

                		$.post('<?php echo site_url('admin/Payments/update_data_payments') ?>',{id : rowdata.id,column : column, newvalue:newvalue },function(result)
                		{
                			if(result.success)
                			{
                				$('#notification_message').html(result.msg);
                				$("#messageNotification").jqxNotification("open"); 
                				location.reload();
                			}

                		},'json');

                	});
                }
            }
            var renderer = function (row, column, value) {
            	return '<span style="margin-left: 1px; margin-top: 9px; float: left;">' + value + '</span>';
            }
            // creage grid
            $("#jqxGridPayment").jqxGrid(
            {
            	width: '100%',
            	height: gridHeight,
            	source: date_only_source,
            	rowdetails: true,
            	rowsheight: 35,
            	sortable: true,
            	showfilterrow: true,
            	filterable: true,
            	columnsresize: true,
            	autoshowfiltericon: true,
            	columnsreorder: true,
            	initrowdetails: initrowdetails,
            	rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 290, rowdetailshidden: true },
            	showtoolbar: true,
            	rendertoolbar: function (toolbar) {
            		var container = $("<div style='margin: 5px; height:50px'></div>");
            		container.append($('#jqxGridPaymentToolbar').html());
            		toolbar.append(container);
            	},
            	// ready: function () {
            	// 	$("#jqxGridPayment").jqxGrid('showrowdetails', 1);
            	// },

            	columns: [ 
            	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
            	{ text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
            	cellsrenderer: function (index) {
            		var e = '';
            		var row =  $('#jqxGridPayment').jqxGrid('getrowdata', index);
            		// if(row.is_published == 0)
            		// {
            			e += '<a href="javascript:void(0)" onclick="delete_all(' + index + ' ); return false;" title="Delete All"><i class="fa fa-trash"></i></a> &nbsp';
            			e += '<a href="javascript:void(0)" onclick="publish_data(' + index + ' ); return false;" title="Publish Data"><i class="fa fa-edit"></i></a>';
            			return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
            		// }
            	}
            },
            { text: '<?php echo lang("payment_date"); ?>',datafield: 'payment_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
            { text: 'Published', datafield: 'is_published', width: 170, columntype:'checkbox', filtertype: 'bool', renderer: gridColumnsRenderer }
            ]
        });

            $(document).on('click','#jqxGridPaymentFilterClear', function () { 
            	$('#jqxGridPayment').jqxGrid('clearfilters');
            });

            $("#excelGrid").jqxGrid({
            	theme: theme,
            	width: '100%',
            	height: gridHeight,
            	// source: excelDataSource,
            	altrows: true,
            	pageable: true,
            	sortable: true,
            	rowsheight: 30,
            	columnsheight:30,
            	showfilterrow: true,
            	filterable: true,
            	columnsresize: true,
            	autoshowfiltericon: true,
            	editable: true,
            	columnsreorder: true,
            	selectionmode: 'none',
            	virtualmode: true,
            	enableanimations: false,
            	showstatusbar: true,
            	showaggregates: true,
            	statusbarheight: 25,
            	pagesizeoptions: pagesizeoptions,
            	showtoolbar: true,
            	rendertoolbar: function (toolbar) {
            		var container = $("<div style='margin: 5px; height:50px'></div>");
            		container.append($('#excelGridToolbar').html());
            		toolbar.append(container);
            	},
            	columns: [
            	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', editable:false,cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
            	{ text: '<?php echo lang("payment_date"); ?>',datafield: 'payment_date',width: 120,filterable: false,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
            	{ text: '<?php echo lang("payment_date_np"); ?>',datafield: 'payment_date_np',width: 140,filterable: true,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 120,filterable: true,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 250,filterable: true,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_name',width: 250,filterable: true,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("bank_name"); ?>',datafield: 'bank_name',width: 200,filterable: true,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 250,filterable: true,renderer: gridColumnsRenderer,
            	aggregates: ["sum"],cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, style: 'currency', currency: 'NPR' }) + '</div>';
            	},
            	aggregatesrenderer: function (aggregates, column, element) {
            		var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, style: 'currency', currency: 'NPR' }) + '</div>';
            		return renderstring;
            	}
            },
            ],
            rendergridrows: function (result) {
            	return result.data;
            } });

            $("#delete_invoice").on('click', function () {
            	deleteInvoice();
            });
            $("#deletemass").on('click', function () {
            	deleteMass();
            });
            $("#publish_all_data").on('click', function () {
            	publish_Data();
            });
            $("#saveExceldata").on('click', function () {
            	saveGriddata();
            });
        });

$('#excelGrid').on('cellvaluechanged', function (event) {

	var rowBoundIndex = event.args.rowindex;
	var column = event.args.datafield;
	var newvalue = event.args.newvalue;
	var rowdata = $('#excelGrid').jqxGrid('getrowdata', rowBoundIndex);

	$.post('<?php echo site_url('admin/Payments/update_data') ?>',{id : rowdata.id,column : column, newvalue:newvalue},function(result)
	{
		if(result.success)
		{
			$('#notification_message').html(result.msg);
			$('#excelGrid').jqxGrid('updatebounddata');
			$("#messageNotification").jqxNotification("open"); 
		}

	},'json');

});
function editSaleRecord(index,grid_name){
	var row =  $(grid_name).jqxGrid('getrowdata', index);
	if (row) {
		$('#deleteConfirmation').modal('show');
	}
}

function delete_all(index)
{
	var row =  $('#jqxGridPayment').jqxGrid('getrowdata', index);
	if(row)
	{
		$('#payment_date').val(row.payment_date);
		$('#massDelete').modal('show');
	}
}
function publish_data(index)
{
	var row =  $('#jqxGridPayment').jqxGrid('getrowdata', index);
	if(row)
	{
		$('#payment_date_publish').val(row.payment_date);
		$('#Publishdata_payment').modal('show');
	}
}
function deleteInvoice(){
	
	$('#deleteConfirmation').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Payments/payment_delete"); ?>',
		data: {id : id},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$("#messageNotification").jqxNotification("open");
				$('#deleteConfirmation').modal('hide');
				location.reload();
			}
			$('#deleteConfirmation').unblock();
		}
	});
}

function deleteMass(){
	var payment_date = $("#payment_date").val();

	$('#massDelete').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Payments/mass_delete"); ?>',
		data: {payment_date : payment_date},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$("#messageNotification").jqxNotification("open");
				$('#jqxGridPayment').jqxGrid('updatebounddata');
				$('#massDelete').modal('hide');
			}
			$('#massDelete').unblock();
		}
	});
}
function publish_Data(){
	var payment_date = $("#payment_date_publish").val();

	$('#Publishdata_payment').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Payments/publish_data"); ?>',
		data: {payment_date : payment_date},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$("#messageNotification").jqxNotification("open");
				$('#jqxGridPayment').jqxGrid('updatebounddata');
				$('#Publishdata_payment').modal('hide');
				location.reload();
			}
			$('#Publishdata_payment').unblock();
		}
	});
}
function saveGriddata(){
	var rows = $('#excelGrid').jqxGrid('getrows');

	$('#Preshow_Data').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Payments/save"); ?>',
		data: {rows:rows},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$("#messageNotification").jqxNotification("open"); 
				$('#Preshow_Data').modal('hide');
				location.reload();
			}
			$('#Preshow_Data').unblock();
		}
	});
}

$('#close_preshow').click(function()
{
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Payments/update_temporary_table"); ?>',		
		success: function (result) {			
			$('#jqxGridPayment').jqxGrid('updatebounddata');
			$('#Preshow_Data').modal('hide');
		}
	});
});
function open_preshow_modal()
{
	var excelpaymentsDataSource =
	{
		datatype: "json",
		datafields: [
		{ name: 'id', type: 'number' },
		{ name: 'created_by', type: 'number' },
		{ name: 'updated_by', type: 'number' },
		{ name: 'deleted_by', type: 'number' },
		{ name: 'created_at', type: 'date' },
		{ name: 'updated_at', type: 'date' },
		{ name: 'deleted_at', type: 'date' },
		{ name: 'payment_date', type: 'string' },
		{ name: 'payment_date_np', type: 'string' },
		{ name: 'nepali_month_id', type: 'number' },
		{ name: 'agent_id', type: 'number' },
		{ name: 'party_id', type: 'number' },
		{ name: 'bank_name', type: 'string' },
		{ name: 'amount', type: 'number' },
		{ name: 'is_published', type: 'number' },
		{ name: 'fiscal_year_id', type: 'number' },
		{ name: 'agent_name', type: 'string' },
		{ name: 'party_name', type: 'string' },
		{ name: 'nepali_month', type: 'string' },
		],
		url: '<?php echo site_url("admin/Payments/temporary_json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
		},
		beforeprocessing: function (data) {
			excelpaymentsDataSource.totalrecords = data.total;
		},
		filter: function () {
			$("#excelGrid").jqxGrid('updatebounddata', 'filter');
		},
		sort: function () {
			$("#excelGrid").jqxGrid('updatebounddata', 'sort');
		},
		processdata: function(data) {
		}
	};
	var excelpaymentsDataAdapter = new $.jqx.dataAdapter(excelpaymentsDataSource);

	$('#excelGrid').jqxGrid({ source : excelpaymentsDataAdapter });
	$('#Preshow_Data').modal('show');
}
</script>

<script type="text/javascript">
	$(document).on('click','#publishAll', function () { 
		$.post('<?php echo site_url('admin/Payments/publishAll')?>',{},function(data) {
			if(data.success){
				location.reload();
				$('#notification_message').html('Updated successfully.');
				$("#messageNotification").jqxNotification("open"); 
			}else{
				$('#errornotification_message').html('Error Occured.');
				$("#errorNotification").jqxNotification("open"); 
			}
		},'json');
	});
</script>