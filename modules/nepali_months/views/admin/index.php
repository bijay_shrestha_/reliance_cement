<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('nepali_months'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('nepali_months'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridNepali_monthToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Nepali_monthmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridNepali_monthFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridNepali_month"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Nepali_monthmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('nepali_months'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-nepali_months', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "nepali_months_id"/>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_months_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='nepali_months_name' class=' form-control' name='name'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_months_rank'><?php echo lang('rank')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='nepali_months_rank' class=' form-control' name='rank'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxNepali_monthSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var nepali_monthsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			{ name: 'rank', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/nepali_months/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	nepali_monthsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridNepali_month").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridNepali_month").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridNepali_month").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: nepali_monthsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridNepali_monthToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editNepali_monthRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("rank"); ?>',datafield: 'rank',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridNepali_month").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridNepali_monthFilterClear', function () { 
		$('#jqxGridNepali_month').jqxGrid('clearfilters');
	});

	
    /*$('#form-nepali_months').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nepali_months_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_months_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nepali_months_rank', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_months_rank').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxNepali_monthSubmitButton").on('click', function () {
    	saveNepali_monthRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveNepali_monthRecord();
                }
            };
        $('#form-nepali_months').jqxValidator('validate', validationResult);
        */
    });
});

function editNepali_monthRecord(index){
	var row =  $("#jqxGridNepali_month").jqxGrid('getrowdata', index);
	if (row) {
		$('#nepali_months_id').val(row.id);
		$('#nepali_months_name').val(row.name);
		$('#nepali_months_rank').val(row.rank);
		
		$('#Nepali_monthmodal').modal('show');
	}
}

function saveNepali_monthRecord(){
	var data = $("#form-nepali_months").serialize();

	$('#Nepali_monthmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/nepali_months/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_nepali_months();
				$('#jqxGridNepali_month').jqxGrid('updatebounddata');
				$('#Nepali_monthmodal').modal('hide');
			}
			$('#Nepali_monthmodal').unblock();
		}
	});
}

function reset_form_nepali_months(){
	$('#nepali_months_id').val('');
	$('#form-nepali_months')[0].reset();
}
</script>