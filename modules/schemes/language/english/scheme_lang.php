<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['fiscal_year_id'] = 'Fiscal Year';
$lang['agent_id'] = 'Agent';
$lang['amount'] = 'Amount';
$lang['bag_count'] = 'No. of Bags';
$lang['price'] = 'Price';
$lang['nepali_month_id'] = 'Month';
$lang['scheme_type'] = 'Scheme Type';

$lang['schemes']='Schemes';