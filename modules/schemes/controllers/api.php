<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{

    var $user_id;

	public function __construct()
    {
    	parent::__construct();
        $this->load->model('schemes/scheme_model');

        $this->user_id  = getallheaders()['Userid'];
        $this->load->helper('project');

        if(!empty(getallheaders()['Accesstoken']))
        {
            $access_token = getallheaders()['Accesstoken'];
            $this->db->where('id',$this->user_id);
            $this->db->where('access_token',$access_token);
            $check_token = $this->db->get('aauth_users')->result_array();

            if(empty($check_token))
            {
                echo json_encode('Access Denied');
                exit;
            }
        }
        else
        {
            echo json_encode('Access Token not sent on header');
            exit();
        }
    }

    public function scheme_get()
    {
    	$this->db->where('user_id',$this->user_id);
        $agent = $this->db->get('mst_agents')->row();

        $this->db->where('agent_id',$agent->id);
        $overview = $this->db->get('view_schemes')->row();

        echo json_encode($overview);
    }

    public function scheme_post()
    {
	    //TODO
    }

    public function scheme_delete()
    {
	    //TODO
    }


}