<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Schemes
 *
 * Extends the Project_Controller class
 * 
 */

class AdminSchemes extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Schemes');

        $this->load->model('schemes/scheme_model');
        $this->load->model('agents/agent_model');
		$this->load->model('parties/party_model');
		$this->load->model('brands/brand_model');
		$this->load->model('brand_types/brand_type_model');
		$this->load->model('nepali_months/nepali_month_model');
        $this->load->model('schemes_temporaries/schemes_temporary_model');
        $this->lang->load('schemes/scheme');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('schemes');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'schemes';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->scheme_model->_table = "view_schemes";
		search_params();
		
		$total=$this->scheme_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->scheme_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function temporary_json()
	{
		$this->scheme_model->_table = "view_schemes_temporary";

		search_params();
		
		$total=$this->scheme_model->find_count(array('is_processed'=>0));
		
		paging('id');
		
		search_params();
		
		$rows=$this->scheme_model->findAll(array('is_processed'=>0));
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->scheme_model->insert($data);
        }
        else
        {
            $success=$this->scheme_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   	private function _get_posted_data()
   	{
		list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();

   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['agent_id'] = $this->input->post('agent_id');
		$data['amount'] = $this->input->post('amount');
		$data['scheme_type'] = $this->input->post('scheme_type');
		$data['nepali_month_id'] = $this->input->post('nepali_month_id');
		$data['bag_count'] = ($this->input->post('bag_count') ? $this->input->post('bag_count') : 0);
		$data['price'] = ($this->input->post('price') ? $this->input->post('price') : 0);
		$data['amount'] = $this->input->post('amount');
		$data['fiscal_year_id'] = $fiscal_year_id;

        return $data;
   	}

   	public function saveGridValue()
	{
		$updated = $this->update_temporary_table();

		if($updated)
		{
			$data = $this->input->post('rows');
		
			foreach ($data as $key => $value) 
			{
				$scheme[$key]['fiscal_year_id'] = $value['fiscal_year_id'];
				$scheme[$key]['agent_id'] = $value['agent_id'];
				$scheme[$key]['bag_count'] =$value['bag_count'];
				$scheme[$key]['price'] = $value['price'];
				$scheme[$key]['amount'] = $value['amount'];
				$scheme[$key]['nepali_month_id'] = $value['nepali_month_id'];
				$scheme[$key]['scheme_type'] = $value['scheme_type'];
			} 
			
			$this->db->trans_start();
			$success = $this->scheme_model->insert_many($scheme);
			if ($this->db->trans_status() === FALSE) 
			{
				$this->db->trans_rollback();
			} 
			else 
			{
				$this->db->trans_commit();
			} 
			$this->db->trans_complete();

			if($success)
			{
				$success = TRUE;
				$msg=lang('general_success');
			}
			else
			{
				$success = FALSE;
				$msg=lang('general_failure');
			}

			echo json_encode(array('msg'=>$msg,'success'=>$success));
			exit;	
		}
	}

   	public function update_temporary_table()
	{
		$update_data = array('is_processed'=>1);
		$updated = $this->db->update('temp_schemes_temporary',$update_data);
		return $updated;
	}

   	public function upload_schemes()
   	{
   		$this->update_temporary_table();

    	$target_dir = "uploads/schemes/";

    	if (!file_exists('./uploads/schemes')) {
    		mkdir('./uploads/schemes', 0777, true);
    	}
    	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

    	$uploadOk = 1;
    	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    		//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    	} else {
    		echo "Sorry, there was an error uploading your file.";
    	}    	
    	$this->excel_schemes($_FILES["fileToUpload"]["name"]);
   	}

	public function excel_schemes($filename = NULL)
    {
    	list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
    	
    	if($this->input->post('filename'))
    	{
    		$filename = $this->input->post('filename');
    	}

    	if($this->input->post('new_agents'))
    	{
    		$new_agents = json_decode($this->input->post('new_agents'));
    		foreach ($new_agents as $key => $value) 
    		{
    			$agent['name'] = $value;
    			$agent_id = $this->agent_model->insert($agent);
    			
    			// create user as well
    			$username = strtolower(substr(str_replace('-','',str_replace(' ', '.', $value)),0,20));
    			$email = strtolower(substr(str_replace('-','',str_replace(' ', '.', $value)),0,20)).'@reliance.com';
    			$fullname = $value;
    			$password = 'password123';
    			$this->sale->register_user($email, $password, $username, $fullname,$agent_id);
    			
    		}
    	}

    	$file = FCPATH . 'uploads/schemes/'.$filename; 
    	$this->load->library('Excel');
    	$objPHPExcel = PHPExcel_IOFactory::load($file);
    	$objReader = PHPExcel_IOFactory::createReader('Excel2007');        
    	$objReader->setReadDataOnly(false);

    	$index = array('agent_name', 'month', 'bag_count', 'price', 'amount');
    	$raw_data = array();
    	$view_data = array();
    	foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
    		if ($key == 0) {
    			$worksheetTitle = $worksheet->getTitle();
				$highestRow = $worksheet->getHighestRow(); // e.g. 10
				$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				$nrColumns = ord($highestColumn) - 64;

				for ($row = 2; $row <= $highestRow; ++$row) {
					for ($col = 0; $col < $highestColumnIndex; ++$col) {
						$cell = $worksheet->getCellByColumnAndRow($col, $row);
						$val = $cell->getValue();
						$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);

						$raw_data[$row][$index[$col]] = $val;
					}
				}
			}
		}

		if(!$this->input->post('create_new'))
		{
			$new_agent = array();
			$new_party = array();
			foreach($raw_data as $key => $val) 
			{
				$get_agent = $this->agent_model->find(array('name'=>$val['agent_name']));
				if(!$get_agent)
				{
					if(!(in_array($val['agent_name'], $new_agent)))
					{
						$new_agent[] = $val['agent_name'];
					}
				}
				// $get_party = $this->party_model->find(array('name'=>$val['party']));
				// if(!$get_party)
				// {
				// 	if(!(in_array($val['party'],$new_party)))
				// 	{
				// 		$new_party[] = $val['party'];
				// 	}
				// }
			}

			if(!empty($new_agent) || !empty($new_party))
			{
				echo json_encode(array('new_agent'=>$new_agent,'new_party'=>$new_party));
				exit;
			}
		}
		
		foreach ($raw_data as $key => $value) 
		{
			// $get_brand = $this->brand_model->find(array('name'=>$value['brand']));
			// $get_brand_type = $this->brand_type_model->find(array('name'=>$value['type']));
			$get_agent = $this->agent_model->find(array('name'=>$value['agent_name']));
			$get_month = $this->nepali_month_model->find(array('name'=>$value['month']));
			// $get_party = $this->party_model->find(array('name'=>$value['party']));
			/*$this->db->select_max('batch_no');
			$get_batch_no = $this->db->get('tbl_schemes')->row();*/
			

			$schemes[$key]['fiscal_year_id'] = $fiscal_year_id;
			$schemes[$key]['nepali_month_id'] = $get_month->id;
			
			$schemes[$key]['agent_id'] = ($get_agent?$get_agent->id:'');
			$schemes[$key]['bag_count'] = ($value['bag_count'])?$value['bag_count']:0;
			$schemes[$key]['price'] = ($value['price'])?$value['price']:0;
			$schemes[$key]['amount'] = $value['amount'];
			$schemes[$key]['scheme_type'] = SCHEME_TYPE_LUMP_SUM;
			if($value['bag_count']){
				$schemes[$key]['amount'] = $schemes[$key]['bag_count'] * $schemes[$key]['price'];
				$schemes[$key]['scheme_type'] = SCHEME_TYPE_BAG;
			}
			//$schemes[$key]['batch_no'] = ($get_batch_no ? ($get_batch_no->batch_no + 1) : 1);
		} 
		
		$this->db->trans_start();
		$success = $this->schemes_temporary_model->insert_many($schemes);
		if ($this->db->trans_status() === FALSE) 
		{
			$this->db->trans_rollback();
		} 
		else 
		{
			$this->db->trans_commit();
		} 
		$this->db->trans_complete();

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}
}