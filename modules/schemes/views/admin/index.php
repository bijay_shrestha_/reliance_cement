<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('schemes'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('schemes'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSchemeToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Schememodal">Create</button>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#excelUploadmodal">Upload</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSchemeFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridScheme"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Schememodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('schemes'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-schemes', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "schemes_id"/>

					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='agent_id'><?php echo lang('agent_id')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='agent_id' class=' form-control' name='agent_id'></div></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_month_id'><?php echo lang('nepali_month_id')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='nepali_month_id' class=' form-control' name='nepali_month_id'></div></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='scheme_type'><?php echo lang('scheme_type')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='scheme_type' class=' form-control' name='scheme_type'></div></div>
					</div>
					<div class='row form-group bag_number' style="display: none;">
						<div class='col-md-3 col-sm-6 col-xs-6'><label for='bag_count'><?php echo lang('bag_count')?></label></div>
						<div class='col-md-3 col-sm-6 col-xs-6'><input type="text" id='bag_count' class='form-control' name='bag_count'></div>
						<div class='col-md-2 col-sm-12 col-xs-12'></div>
						<div class='col-md-1 col-sm-6 col-xs-6'><label for='price'><?php echo lang('price')?></label></div>
						<div class='col-md-3 col-sm-6 col-xs-6'><input type="text" id='price' class=' form-control' name='price'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='amount'><?php echo lang('amount')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='amount' class=' form-control' name='amount'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxSchemeSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<!-- for excel uploads -->
<div id="excelUploadmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('sales'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4"><label>Choose File</label></div>
					<div class="col-md-8"><div id="sales_import"></div></div>
				</div>
			</div>
			<div class="modal-footer">
				<!-- <button type="button" class="btn btn-success" id="jqxSaleSubmitButton"><?php echo lang('general_save'); ?></button> -->
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<div id="Preshow_Data" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Excel Data</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12"><div id="excelGrid"></div></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="saveExceldata">Approve</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="close_preshow">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){
		// file upload
		$('#sales_import').jqxFileUpload({ width: 300, uploadUrl: '<?php echo site_url('admin/schemes/upload_schemes') ?>', fileInputName: 'fileToUpload' });

		$('#sales_import').on('uploadEnd', function (event) {
			$('#excelUploadmodal').modal('hide');
			var args = event.args;
			var fileName = args.file;
			var serverResponce = args.response;

			var result = $.parseJSON(serverResponce);

			if(result.success)
			{
				open_preshow_modal();
			}
			else
			{				
				$('#agentaddModal').modal('show');
				$.each($.parseJSON(serverResponce),function(key,val){
					if(key == 'new_agent')
					{
						$.each(val,function(k,v)
						{
							$('#check-list-box-agent').append('<li class="list-group-item" data-color="success">'+v+'</li>');
						});
					}
					if(key == 'new_party')
					{
						$.each(val,function(i,va)
						{
							$('#check-list-box-party').append('<li class="list-group-item" data-color="success">'+va+'</li>');
						});
					}
				});

				$('.list-group.checked-list-box .list-group-item').each(function () {

					var $widget = $(this),
					$checkbox = $('<input type="checkbox" class="hidden" name="agent[]" />'),
					color = ($widget.data('color') ? $widget.data('color') : "primary"),
					style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
					settings = {
						on: {
							icon: 'glyphicon glyphicon-check'
						},
						off: {
							icon: 'glyphicon glyphicon-unchecked'
						}
					};

					$widget.css('cursor', 'pointer')
					$widget.append($checkbox);

					$widget.on('click', function () {
						$checkbox.prop('checked', !$checkbox.is(':checked'));
						$checkbox.triggerHandler('change');
						updateDisplay();
					});
					$checkbox.on('change', function () {
						updateDisplay();
					});


					function updateDisplay() {
						var isChecked = $checkbox.is(':checked');

						$widget.data('state', (isChecked) ? "on" : "off");

						$widget.find('.state-icon')
						.removeClass()
						.addClass('state-icon ' + settings[$widget.data('state')].icon);

						if (isChecked) {
							$widget.addClass(style + color + ' active');
						} else {
							$widget.removeClass(style + color + ' active');
						}
					}

					function init() {

						if ($widget.data('checked') == true) {
							$checkbox.prop('checked', !$checkbox.is(':checked'));
						}

						updateDisplay();

						if ($widget.find('.state-icon').length == 0) {
							$widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
						}
					}
					init();
				});
				$('#get-checked-data').on('click', function(event) {
					event.preventDefault(); 
					var AgentcheckedItems = {}, PartycheckedItems = {}, counter = 0;
					$("#check-list-box-agent li.active").each(function(idx, li) {
						AgentcheckedItems[counter] = $(li).text();
						counter++;
					});
					$("#check-list-box-party li.active").each(function(idx, li) {
						PartycheckedItems[counter] = $(li).text();
						counter++;
					});
					var new_agents = (JSON.stringify(AgentcheckedItems, null));
					var new_parties = (JSON.stringify(PartycheckedItems, null));

					$.post("<?php echo site_url('admin/Sales/excel_sales')?>",{new_agents : new_agents,new_parties : new_parties, filename:fileName, create_new : '1' }, function(result)
					{	
						if(result.success)
						{
							open_preshow_modal();
							$('#agentaddModal').modal('hide');
						}
					},'json');
				}); } 
			});
		// end of file upload

		var data = new Array();
		var scheme_types = ["Lump Sum","Bag Number"];
		var k = 0;
		for (var i = 0; i < scheme_types.length; i++) {
			var row = {};
			row["scheme_type"] = scheme_types[k];
			data[i] = row;
			k++;
		}
		var Schemesource =
		{
			localdata: data,
			datatype: "array"
		};
		var SchemedataAdapter = new $.jqx.dataAdapter(Schemesource);
		$('#scheme_type').jqxComboBox({ selectedIndex: -1,  source: SchemedataAdapter, displayMember: "scheme_type", height: 25, width: '95%',placeHolder:'Choose a option' });

		$("#scheme_type").select('bind', function (event) {
			scheme_type = $("#scheme_type").jqxComboBox('val');
			if(scheme_type == 'Bag Number')
			{
				$('.bag_number').show();
				$('#price').val('');
				$('#bag_count').val('');
				$('#amount').val('');
				$('#amount').attr('readonly',true);
			}
			else
			{
				$('.bag_number').hide();
				$('#price').val('');
				$('#bag_count').val('');
				$('#amount').val('');
				$('#amount').attr('readonly',false);
			}

		});

		var agentDataSource = {
			url : '<?php echo site_url("admin/Agents/get_agents_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		agentDataAdapter = new $.jqx.dataAdapter(agentDataSource);

		$("#agent_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: agentDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var nepaliMonthDataSource = {
			url : '<?php echo site_url("admin/Agents/get_nepali_month_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		nepaliMonthDataAdapter = new $.jqx.dataAdapter(nepaliMonthDataSource);

		$("#nepali_month_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: nepaliMonthDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var schemesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'fiscal_year_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'amount', type: 'string' },
			{ name: 'fiscal_year', type: 'string' },
			{ name: 'agent_name', type: 'string' },
			{ name: 'nepali_month', type: 'string' },
			{ name: 'price', type: 'number' },
			{ name: 'bag_count', type: 'number' },
			{ name: 'scheme_type', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/Schemes/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	schemesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridScheme").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridScheme").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridScheme").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: schemesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSchemeToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editSchemeRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		// { text: '<?php echo lang("fiscal_year_id"); ?>',datafield: 'fiscal_year',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("scheme_type"); ?>',datafield: 'scheme_type',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 200,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("bag_count"); ?>',datafield: 'bag_count',width: 200,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 200,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 150,filterable: true,renderer: gridColumnsRenderer,
			cellsrenderer: 
    		function (row, columnfield, value, defaulthtml, columnproperties) {
    			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + parseFloat(value).toLocaleString('en-US', {  minimumFractionDigits: 2, maximumFractionDigits:2 }) + '</div>';
    		},
		 },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridScheme").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSchemeFilterClear', function () { 
		$('#jqxGridScheme').jqxGrid('clearfilters');
	});

	
	$('#form-schemes').jqxValidator({
		hintType: 'label',
		animationDuration: 500,
		rules: [		
		{ input: '#agent_id', message: 'Agents Scheme already exists', action: 'blur', 
		rule: function(input, commit) {

			val = $("#agent_id").jqxComboBox('val');
			nepali_month_id = $('#nepali_month_id').jqxComboBox('val');
			$.ajax({
				url: "<?php echo site_url('admin/Schemes/check_duplicate_scheme'); ?>",
				type: 'POST',
				data: {model: 'schemes/scheme_model', field: 'agent_id', value: val, id:$('input#schemes_id').val() , nepali_month_id: nepali_month_id},
				success: function (result) {
					var result = eval('('+result+')');
					return commit(result.success);
				},
				error: function(result) {
					return commit(false);
				}
			});
		}
	},
	{ input: '#agent_id', message: 'Required', action: 'blur', 
	rule: function(input) {
		val = $('#agent_id').jqxComboBox('val');
		return (val == '' || val == null || val == 0) ? false: true;
	}},
	{ input: '#nepali_month_id', message: 'Required', action: 'blur', 
	rule: function(input) {
		val = $('#nepali_month_id').jqxComboBox('val');
		return (val == '' || val == null || val == 0) ? false: true;
	}},	{ input: '#scheme_type', message: 'Required', action: 'blur', 
	rule: function(input) {
		val = $('#scheme_type').jqxComboBox('val');
		return (val == '' || val == null || val == 0) ? false: true;
	}},

	{ input: '#bag_count', message: 'Required', action: 'blur', 
	rule: function(input) {
		scheme_type = $('#scheme_type').jqxComboBox('val');
		if(scheme_type == 'Bag Number'){
			val = $('#bag_count').val();
			return (val == '' || val == null || val == 0) ? false: true;
		}
		else
		{
			return true;
		}
	}},
	{ input: '#price', message: 'Required', action: 'blur', 
	rule: function(input) {
		scheme_type = $('#scheme_type').jqxComboBox('val');
		if(scheme_type == 'Bag Number'){
			val = $('#price').val();
			return (val == '' || val == null || val == 0) ? false: true;
		}
		else
		{
			return true;
		}
	}},
	{ input: '#amount', message: 'Required', action: 'blur', 
	rule: function(input) {
		val = $('#amount').val();
		return (val == '' || val == null || val == 0) ? false: true;
	}},

	]
});

	$("#jqxSchemeSubmitButton").on('click', function () {

		var validationResult = function (isValid) {
			if (isValid) {
				saveSchemeRecord();
			}
		};
		$('#form-schemes').jqxValidator('validate', validationResult);

	});

	$("#excelGrid").jqxGrid({
				theme: theme,
				width: '100%',
				height: gridHeight,
            	// source: excelDataSource,
            	altrows: true,
            	pageable: true,
            	sortable: true,
            	rowsheight: 30,
            	columnsheight:30,
            	showfilterrow: true,
            	filterable: true,
            	columnsresize: true,
            	autoshowfiltericon: true,
            	editable: true,
            	columnsreorder: true,
            	selectionmode: 'none',
            	virtualmode: true,
            	enableanimations: false,
            	showstatusbar: true,
            	showaggregates: true,
            	statusbarheight: 25,
            	pagesizeoptions: pagesizeoptions,
            	showtoolbar: true,
            	rendertoolbar: function (toolbar) {
            		var container = $("<div style='margin: 5px; height:50px'></div>");
            		container.append($('#excelGridToolbar').html());
            		toolbar.append(container);
            	},
            	columns: [
            	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', editable:false,cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
            	{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 150,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'month_name',width: 90,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("bag_count"); ?>',datafield: 'bag_count',width: 110,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 110,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 110,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	],
           
            rendergridrows: function (result) {
            	return result.data;
            } });
});

$('#bag_count').on('keyup',function()
{
	bag_count = $('#bag_count').val();
	price = $('#price').val();
	$('#amount').val(Math.ceil(parseFloat((bag_count?bag_count:0)) * parseFloat((price?price:0))));
});

$('#price').on('keyup',function()
{
	bag_count = $('#bag_count').val();
	price = $('#price').val();
	$('#amount').val(Math.ceil(parseFloat((bag_count?bag_count:0)) * parseFloat(price?price:0)));
});

function editSchemeRecord(index){
	var row =  $("#jqxGridScheme").jqxGrid('getrowdata', index);
	if (row) {
		$('#schemes_id').val(row.id);		
		$('#agent_id').jqxComboBox('val', row.agent_id);
		$('#nepali_month_id').jqxComboBox('val', row.nepali_month_id);
		$('#scheme_type').jqxComboBox('val', row.scheme_type);
		$('#bag_count').val(row.bag_count);
		$('#price').val(row.price);
		$('#amount').val(row.amount);
		
		$('#Schememodal').modal('show');
	}
}

function saveSchemeRecord(){
	var data = $("#form-schemes").serialize();

	$('#Schememodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Schemes/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_schemes();
				$('#jqxGridScheme').jqxGrid('updatebounddata');
				$('#Schememodal').modal('hide');
			}
			$('#Schememodal').unblock();
		}
	});
}

function reset_form_schemes(){
	$('#schemes_id').val('');
	$('#form-schemes')[0].reset();
	$('#agent_id').jqxComboBox('clearSelection');
	$('#nepali_month_id').jqxComboBox('clearSelection');
	$('#scheme_type').jqxComboBox('clearSelection');
}

// after file upload
function open_preshow_modal()
{
	var excelsalesDataSource =
	{
		datatype: "json",
		datafields: [
		{ name: 'id', type: 'number' },
		{ name: 'created_by', type: 'number' },
		{ name: 'updated_by', type: 'number' },
		{ name: 'deleted_by', type: 'number' },
		{ name: 'created_at', type: 'date' },
		{ name: 'updated_at', type: 'date' },
		{ name: 'deleted_at', type: 'date' },
		{ name: 'nepali_month_id', type: 'number' },
		{ name: 'agent_id', type: 'number' },
		{ name: 'fiscal_year_id', type: 'number' },
		{ name: 'price', type: 'number' },
		{ name: 'amount', type: 'number' },
		{ name: 'agent_name', type: 'string' },
		{ name: 'month_name', type: 'string' },
		{ name: 'bag_count', type: 'number' },
		{ name: 'scheme_type', type: 'string' },
		],
		url: '<?php echo site_url("admin/Schemes/temporary_json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
		},
		beforeprocessing: function (data) {
			excelsalesDataSource.totalrecords = data.total;
		},
		filter: function () {
			$("#jqxGridParty").jqxGrid('updatebounddata', 'filter');
		},
		sort: function () {
			$("#jqxGridParty").jqxGrid('updatebounddata', 'sort');
		},
		processdata: function(data) {
		}
	};
	var excelsalesDataAdapter = new $.jqx.dataAdapter(excelsalesDataSource);

	$('#excelGrid').jqxGrid({ source : excelsalesDataAdapter });
	$('#Preshow_Data').modal('show');
}

	$("#saveExceldata").on('click', function () {
		saveGriddata();
	});

	function saveGriddata(){
	var rows = $('#excelGrid').jqxGrid('getrows');

	$('#Preshow_Data').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Schemes/saveGridValue"); ?>',
		data: {rows:rows},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$('#notification_message').html('successfully Completed Request');
				$('#jqxGridScheme').jqxGrid('updatebounddata');
				$('#Preshow_Data').modal('hide');
				location.reload();
			}
			$('#Preshow_Data').unblock();
		}
	});
}
</script>