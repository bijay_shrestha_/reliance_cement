<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Brand_types
 *
 * Extends the Public_Controller class
 * 
 */

class Brand_types extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Brand Types');

        $this->load->model('brand_types/brand_type_model');
        $this->lang->load('brand_types/brand_type');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('brand_types');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'brand_types';
		$this->load->view($this->_container,$data);
	}
}