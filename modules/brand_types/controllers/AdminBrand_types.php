<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Brand_types
 *
 * Extends the Project_Controller class
 * 
 */

class AdminBrand_types extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Brand Types');

        $this->load->model('brand_types/brand_type_model');
        $this->lang->load('brand_types/brand_type');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('brand_types');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'brand_types';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->brand_type_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->brand_type_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->brand_type_model->insert($data);
        }
        else
        {
            $success=$this->brand_type_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['name'] = $this->input->post('name');
		$data['code'] = $this->input->post('code');
		$data['rank'] = $this->input->post('rank');

        return $data;
   }
}