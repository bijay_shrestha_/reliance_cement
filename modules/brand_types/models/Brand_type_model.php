<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Brand_type_model extends MY_Model
{

    protected $_table = 'mst_brand_types';

    protected $blamable = TRUE;

}