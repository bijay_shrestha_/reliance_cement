<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3><?php echo lang('brand_types'); ?></h3>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url()?>">Home</a></li>
            <li class="active"><?php echo lang('brand_types'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">
                <?php echo displayStatus(); ?>
                <div id='jqxGridBrand_typeToolbar' class='grid-toolbar'>
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridBrand_typeInsert"><?php echo lang('general_create'); ?></button>
                    <button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridBrand_typeFilterClear"><?php echo lang('general_clear'); ?></button>
                </div>
                <div id="jqxGridBrand_type"></div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowBrand_type">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-brand_types', 'onsubmit' => 'return false')); ?>
            <input type = "hidden" name = "id" id = "brand_types_id"/>
            <table class="form-table">
                
                <tr>
                    <td><label for='brand_types_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
                    <td><input id='brand_types_name' class='text_input' name='name'></td>
                </tr>
                <!-- <tr>
                    <td><label for='code'><?php echo lang('code')?></label></td>
                    <td><input id='code' class='text_input' name='code'></td>
                </tr>
                <tr>
                    <td><label for='brand_types_rank'><?php echo lang('rank')?><span class='mandatory'>*</span></label></td>
                    <td><div id='brand_types_rank' class='number_general' name='rank'></div></td>
                </tr> -->
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxBrand_typeSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxBrand_typeCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

    var brand_typesDataSource =
    {
        datatype: "json",
        datafields: [
            { name: 'id', type: 'number' },
            { name: 'created_by', type: 'number' },
            { name: 'updated_by', type: 'number' },
            { name: 'deleted_by', type: 'number' },
            { name: 'created_at', type: 'date' },
            { name: 'updated_at', type: 'date' },
            { name: 'deleted_at', type: 'date' },
            { name: 'name', type: 'string' },
            { name: 'code', type: 'string' },
            { name: 'rank', type: 'number' },
            
        ],
        url: '<?php echo site_url("admin/Brand_types/json"); ?>',
        pagesize: defaultPageSize,
        root: 'rows',
        id : 'id',
        cache: true,
        pager: function (pagenum, pagesize, oldpagenum) {
            //callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
            brand_typesDataSource.totalrecords = data.total;
        },
        // update the grid and send a request to the server.
        filter: function () {
            $("#jqxGridBrand_type").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxGridBrand_type").jqxGrid('updatebounddata', 'sort');
        },
        processdata: function(data) {
        }
    };
    
    $("#jqxGridBrand_type").jqxGrid({
        theme: theme,
        width: '100%',
        height: gridHeight,
        source: brand_typesDataSource,
        altrows: true,
        pageable: true,
        sortable: true,
        rowsheight: 30,
        columnsheight:30,
        showfilterrow: true,
        filterable: true,
        columnsresize: true,
        autoshowfiltericon: true,
        columnsreorder: true,
        selectionmode: 'none',
        virtualmode: true,
        enableanimations: false,
        pagesizeoptions: pagesizeoptions,
        showtoolbar: true,
        rendertoolbar: function (toolbar) {
            var container = $("<div style='margin: 5px; height:50px'></div>");
            container.append($('#jqxGridBrand_typeToolbar').html());
            toolbar.append(container);
        },
        columns: [
            { text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
            {
                text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
                cellsrenderer: function (index) {
                    var e = '<a href="javascript:void(0)" onclick="editBrand_typeRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
                    return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
                }
            },
            // { text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
            
            { text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
            // { text: '<?php echo lang("code"); ?>',datafield: 'code',width: 150,filterable: true,renderer: gridColumnsRenderer },
            // { text: '<?php echo lang("rank"); ?>',datafield: 'rank',width: 150,filterable: true,renderer: gridColumnsRenderer },
            
        ],
        rendergridrows: function (result) {
            return result.data;
        }
    });

    $("[data-toggle='offcanvas']").click(function(e) {
        e.preventDefault();
        setTimeout(function() {$("#jqxGridBrand_type").jqxGrid('refresh');}, 500);
    });

    $(document).on('click','#jqxGridBrand_typeFilterClear', function () { 
        $('#jqxGridBrand_type').jqxGrid('clearfilters');
    });

    $(document).on('click','#jqxGridBrand_typeInsert', function () { 
        openPopupWindow('jqxPopupWindowBrand_type', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

    // initialize the popup window
    $("#jqxPopupWindowBrand_type").jqxWindow({ 
        theme: theme,
        width: '500px',
        maxWidth: '500px',
        height: '120px',  
        maxHeight: '120px',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowBrand_type").on('close', function () {
        reset_form_brand_types();
    });

    $("#jqxBrand_typeCancelButton").on('click', function () {
        reset_form_brand_types();
        $('#jqxPopupWindowBrand_type').jqxWindow('close');
    });

    $('#form-brand_types').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
            

            { input: '#brand_types_name', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#brand_types_name').val();
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            // { input: '#code', message: 'Required', action: 'blur', 
            //  rule: function(input) {
            //      val = $('#code').val();
            //      return (val == '' || val == null || val == 0) ? false: true;
            //  }
            // },

            // { input: '#brand_types_rank', message: 'Required', action: 'blur', 
            //  rule: function(input) {
            //      val = $('#brand_types_rank').jqxNumberInput('val');
            //      return (val == '' || val == null || val == 0) ? false: true;
            //  }
            // },

        ]
    });

    $("#jqxBrand_typeSubmitButton").on('click', function () {
        // saveBrand_typeRecord();
        
        var validationResult = function (isValid) {
                if (isValid) {
                   saveBrand_typeRecord();
                }
            };
        $('#form-brand_types').jqxValidator('validate', validationResult);
        
    });
});

function editBrand_typeRecord(index){
    var row =  $("#jqxGridBrand_type").jqxGrid('getrowdata', index);
    if (row) {
        $('#brand_types_id').val(row.id);
        
        $('#brand_types_name').val(row.name);
        // $('#code').val(row.code);
        // $('#brand_types_rank').jqxNumberInput('val', row.rank);
        
        openPopupWindow('jqxPopupWindowBrand_type', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveBrand_typeRecord(){
    var data = $("#form-brand_types").serialize();
    
    $('#jqxPopupWindowBrand_type').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/Brand_types/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_brand_types();
                $('#jqxGridBrand_type').jqxGrid('updatebounddata');
                $('#jqxPopupWindowBrand_type').jqxWindow('close');
            }
            $('#jqxPopupWindowBrand_type').unblock();
        }
    });
}

function reset_form_brand_types(){
    $('#brand_types_id').val('');
    $('#form-brand_types')[0].reset();
}
</script>