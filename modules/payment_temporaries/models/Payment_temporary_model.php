<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Payment_temporary_model extends MY_Model
{

    protected $_table = 'tbl_payment_temporary';

    protected $blamable = TRUE;

}