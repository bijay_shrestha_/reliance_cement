<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('payment_temporaries'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('payment_temporaries'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridPayment_temporaryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Payment_temporarymodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridPayment_temporaryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridPayment_temporary"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Payment_temporarymodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('payment_temporaries'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-payment_temporaries', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "payment_temporaries_id"/>
					
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_by'><?php echo lang('created_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='created_by' class=' form-control' name='created_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_by'><?php echo lang('updated_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='updated_by' class=' form-control' name='updated_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_by'><?php echo lang('deleted_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='deleted_by' class=' form-control' name='deleted_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_at'><?php echo lang('created_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='created_at' class=' form-control' name='created_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_at'><?php echo lang('updated_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='updated_at' class=' form-control' name='updated_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_at'><?php echo lang('deleted_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='deleted_at' class=' form-control' name='deleted_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='fiscal_year_id'><?php echo lang('fiscal_year_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='fiscal_year_id' class=' form-control' name='fiscal_year_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='payment_date'><?php echo lang('payment_date')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='payment_date' class=' form-control' name='payment_date'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='payment_date_np'><?php echo lang('payment_date_np')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='payment_date_np' class=' form-control' name='payment_date_np'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_month_id'><?php echo lang('nepali_month_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='nepali_month_id' class=' form-control' name='nepali_month_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='bank_name'><?php echo lang('bank_name')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='bank_name' class=' form-control' name='bank_name'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='agent_id'><?php echo lang('agent_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='agent_id' class=' form-control' name='agent_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='party_id'><?php echo lang('party_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='party_id' class=' form-control' name='party_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='amount'><?php echo lang('amount')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='amount' class=' form-control' name='amount'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='is_processed'><?php echo lang('is_processed')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='is_processed' class=' form-control' name='is_processed'></div></div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxPayment_temporarySubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var payment_temporariesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'fiscal_year_id', type: 'number' },
			{ name: 'payment_date', type: 'date' },
			{ name: 'payment_date_np', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'bank_name', type: 'string' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'party_id', type: 'number' },
			{ name: 'amount', type: 'string' },
			{ name: 'is_processed', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/payment_temporaries/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	payment_temporariesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridPayment_temporary").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridPayment_temporary").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridPayment_temporary").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: payment_temporariesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPayment_temporaryToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editPayment_temporaryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("fiscal_year_id"); ?>',datafield: 'fiscal_year_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("payment_date"); ?>',datafield: 'payment_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("payment_date_np"); ?>',datafield: 'payment_date_np',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("bank_name"); ?>',datafield: 'bank_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("is_processed"); ?>',datafield: 'is_processed',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridPayment_temporary").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPayment_temporaryFilterClear', function () { 
		$('#jqxGridPayment_temporary').jqxGrid('clearfilters');
	});

	
    /*$('#form-payment_temporaries').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#fiscal_year_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#fiscal_year_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#payment_date_np', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#payment_date_np').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nepali_month_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_month_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#bank_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#bank_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#agent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#agent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#party_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#party_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#amount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#amount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#is_processed', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#is_processed').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxPayment_temporarySubmitButton").on('click', function () {
    	savePayment_temporaryRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   savePayment_temporaryRecord();
                }
            };
        $('#form-payment_temporaries').jqxValidator('validate', validationResult);
        */
    });
});

	function editPayment_temporaryRecord(index){
		var row =  $("#jqxGridPayment_temporary").jqxGrid('getrowdata', index);
		if (row) {
			$('#payment_temporaries_id').val(row.id);
			$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#fiscal_year_id').jqxNumberInput('val', row.fiscal_year_id);
		$('#payment_date').jqxDateTimeInput('setDate', row.payment_date);
		$('#payment_date_np').val(row.payment_date_np);
		$('#nepali_month_id').jqxNumberInput('val', row.nepali_month_id);
		$('#bank_name').val(row.bank_name);
		$('#agent_id').jqxNumberInput('val', row.agent_id);
		$('#party_id').jqxNumberInput('val', row.party_id);
		$('#amount').val(row.amount);
		$('#is_processed').jqxNumberInput('val', row.is_processed);
		
			$('#Payment_temporarymodal').modal('show');
		}
	}

	function savePayment_temporaryRecord(){
		var data = $("#form-payment_temporaries").serialize();

		$('#Payment_temporarymodal').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/payment_temporaries/save"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_payment_temporaries();
					$('#jqxGridPayment_temporary').jqxGrid('updatebounddata');
					$('#Payment_temporarymodal').modal('hide');
				}
				$('#Payment_temporarymodal').unblock();
			}
		});
	}

	function reset_form_payment_temporaries(){
		$('#payment_temporaries_id').val('');
		$('#form-payment_temporaries')[0].reset();
	}
</script>