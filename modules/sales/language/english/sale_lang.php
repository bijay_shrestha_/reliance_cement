<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['invoice_no'] = 'Invoice No';
$lang['invoice_date'] = 'Invoice Date';
$lang['invoice_date_np'] = 'Invoice Date Np';
$lang['nepali_month_id'] = 'Nepali Month';
$lang['agent_id'] = 'Agent';
$lang['party_id'] = 'Party';
$lang['truck_number'] = 'Truck Number';
$lang['type_id'] = 'Type';
$lang['brand_id'] = 'Brand';
$lang['quantity'] = 'Quantity';
$lang['rate'] = 'Rate';
$lang['discount'] = 'Discount';
$lang['gross_total'] = 'Gross Total';
$lang['is_published'] = 'Is Published';
$lang['total_pre_discount'] = 'Total Pre Discount';
$lang['total_discount'] = 'Total Discount';
$lang['total_excise'] = 'Total Excise';
$lang['total_vat'] = 'Total Vat';
$lang['total'] = 'Total';

$lang['sales']='Sales';