<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('sales'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSales_agentToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSales_temporaryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridSales_agent"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script language="javascript" type="text/javascript">

	$(function(){

		var salesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'invoice_no', type: 'string' },
			{ name: 'invoice_date', type: 'string' },
			{ name: 'invoice_date_np', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'party_id', type: 'number' },
			{ name: 'truck_number', type: 'string' },
			{ name: 'type_id', type: 'number' },
			{ name: 'brand_id', type: 'number' },
			{ name: 'quantity', type: 'number' },
			{ name: 'rate', type: 'number' },
			{ name: 'discount', type: 'number' },
			{ name: 'gross_total', type: 'number' },
			{ name: 'is_published', type: 'number' },
			{ name: 'agent_name', type: 'string' },
			{ name: 'party_name', type: 'string' },
			{ name: 'nepali_month', type: 'string' },
			{ name: 'brand_type', type: 'string' },
			{ name: 'brand_name', type: 'string' },
			],
			url: '<?php echo site_url("admin/Sales/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	salesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSales_agent").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSales_agent").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSales_agent").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: salesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSales_temporaryToolbar').html());
			toolbar.append(container);
		},
		columns: [
		
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{ text: '<?php echo lang("invoice_no"); ?>',datafield: 'invoice_no',width: 110,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("invoice_date"); ?>',datafield: 'invoice_date',width: 100,filterable: false,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("invoice_date_np"); ?>',datafield: 'invoice_date_np',width: 120,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 100,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 180,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_name',width: 270,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("truck_number"); ?>',datafield: 'truck_number',width: 100,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("type_id"); ?>',datafield: 'brand_type',width: 80,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("brand_id"); ?>',datafield: 'brand_name',width: 100,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("quantity"); ?>',datafield: 'quantity',cellsalign:'right',width: 120,filterable: true,renderer: gridColumnsRenderer,aggregates: ["sum"]},
		{ text: '<?php echo lang("rate"); ?>',datafield: 'rate',width: 150,filterable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
		function (row, columnfield, value, defaulthtml, columnproperties) {
			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
		}   
	},
	{ text: '<?php echo lang("discount"); ?>',datafield: 'discount',width: 90,filterable: true,cellsalign:'right',renderer: gridColumnsRenderer },
	{ text: '<?php echo lang("gross_total"); ?>',datafield: 'gross_total',width: 250,filterable: true,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
	function (row, columnfield, value, defaulthtml, columnproperties) {
		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
	},
	aggregatesrenderer: function (aggregates, column, element) {
		var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
		return renderstring;
	} },

	],
	rendergridrows: function (result) {
		return result.data;
	}
});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSales_agent").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSales_temporaryFilterClear', function () { 
		$('#jqxGridSales_agent').jqxGrid('clearfilters');
	});


});


</script>