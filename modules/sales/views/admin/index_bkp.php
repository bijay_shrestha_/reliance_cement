<style type="text/css">
/* CSS REQUIRED */
.state-icon {
	left: -5px;
}
.list-group-item-primary {
	color: rgb(255, 255, 255);
	background-color: rgb(66, 139, 202);
}

/* DEMO ONLY - REMOVES UNWANTED MARGIN */
.well .list-group {
	margin-bottom: 0px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('sales'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSaleToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Salemodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSaleFilterClear"><?php echo lang('general_clear'); ?></button>
					<button type="button" class="btn btn-warning btn-flat btn-xs" data-toggle="modal" data-target="#massDelete">Mass Delete Invoice</button>
				</div>
				<div id="jqxGridSale"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="errorMessage"></div>

<div id="Salemodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('sales'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4"><label>Choose File</label></div>
					<div class="col-md-8"><div id="sales_import"></div></div>
				</div>
			</div>
			<div class="modal-footer">
				<!-- <button type="button" class="btn btn-success" id="jqxSaleSubmitButton"><?php echo lang('general_save'); ?></button> -->
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="agentaddModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<input type="hidden" name="agents[]">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New Agent and Party</h4>
			</div>
			<div class="modal-body">
				<div class="well" style="max-height: 300px;overflow: auto;">
					<h4>New Agents</h4>
					<ul id="check-list-box-agent" class="list-group checked-list-box">
						
					</ul>
					<h4>New Party</h4>
					<ul id="check-list-box-party" class="list-group checked-list-box">
						
					</ul>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="get-checked-data">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="deleteConfirmation" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Invoice</h4>
			</div>
			<div class="modal-body">
				<div class="well" style="max-height: 300px;overflow: auto;">
					<input type="hidden" id="sales_id">
					<h1>Delete Invoice No. :<div id="invoice_no"></div></h1>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="delete_invoice">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div id="massDelete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mass Delete Invoice</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><label>Invoice Date: </label></div>
					<div class="col-md-9"><div id="invoice_date_dropdown"></div></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="deletemass">Delete</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){
		$("#errorMessage").jqxNotification({ width: "auto", position: "top-left",
			opacity: 0.9, autoOpen: true, autoClose: false, template: "info"
		});

		var invoiceDateDataSource = {
			url : '<?php echo site_url("admin/sales/get_invoice_dates"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'invoice_date', type: 'string' },
			],
			async: false,
			cache: true
		}

		invoiceDateDataAdapter = new $.jqx.dataAdapter(invoiceDateDataSource);

		$("#invoice_date_dropdown").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			placeHolder: 'Choose Invoice Date',
			searchMode: 'containsignorecase',
			source: invoiceDateDataAdapter,
			displayMember: "invoice_date",
		});

		$('#sales_import').jqxFileUpload({ width: 300, uploadUrl: '<?php echo site_url('admin/Sales/upload_sales') ?>', fileInputName: 'fileToUpload' });
		$('#sales_import').on('uploadEnd', function (event) {
			$('#Salemodal').modal('hide');
			var args = event.args;
			var fileName = args.file;
			var serverResponce = args.response;

			$('#agentaddModal').modal('show');
			$.each($.parseJSON(serverResponce),function(key,val){
				if(key == 'new_agent')
				{
					$.each(val,function(k,v)
					{
						$('#check-list-box-agent').append('<li class="list-group-item" data-color="success">'+v+'</li>');
					});
				}
				if(key == 'new_party')
				{
					$.each(val,function(i,va)
					{
						$('#check-list-box-party').append('<li class="list-group-item" data-color="success">'+va+'</li>');
					});
				}
			});

			$('.list-group.checked-list-box .list-group-item').each(function () {

				var $widget = $(this),
				$checkbox = $('<input type="checkbox" class="hidden" name="agent[]" />'),
				color = ($widget.data('color') ? $widget.data('color') : "primary"),
				style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
				settings = {
					on: {
						icon: 'glyphicon glyphicon-check'
					},
					off: {
						icon: 'glyphicon glyphicon-unchecked'
					}
				};

				$widget.css('cursor', 'pointer')
				$widget.append($checkbox);

				$widget.on('click', function () {
					$checkbox.prop('checked', !$checkbox.is(':checked'));
					$checkbox.triggerHandler('change');
					updateDisplay();
				});
				$checkbox.on('change', function () {
					updateDisplay();
				});


				function updateDisplay() {
					var isChecked = $checkbox.is(':checked');

					$widget.data('state', (isChecked) ? "on" : "off");

					$widget.find('.state-icon')
					.removeClass()
					.addClass('state-icon ' + settings[$widget.data('state')].icon);

					if (isChecked) {
						$widget.addClass(style + color + ' active');
					} else {
						$widget.removeClass(style + color + ' active');
					}
				}

				function init() {

					if ($widget.data('checked') == true) {
						$checkbox.prop('checked', !$checkbox.is(':checked'));
					}

					updateDisplay();

					if ($widget.find('.state-icon').length == 0) {
						$widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
					}
				}
				init();
			});
			$('#get-checked-data').on('click', function(event) {
				event.preventDefault(); 
				var AgentcheckedItems = {}, PartycheckedItems = {}, counter = 0;
				$("#check-list-box-agent li.active").each(function(idx, li) {
					AgentcheckedItems[counter] = $(li).text();
					counter++;
				});
				$("#check-list-box-party li.active").each(function(idx, li) {
					PartycheckedItems[counter] = $(li).text();
					counter++;
				});
				var new_agents = (JSON.stringify(AgentcheckedItems, null));
				var new_parties = (JSON.stringify(PartycheckedItems, null));
				
				$.post("<?php echo site_url('admin/sales/excel_sales')?>",{new_agents : new_agents,new_parties : new_parties, filename:fileName, create_new : '1' }, function(result)
				{	
					if(result.success)
					{
						$('#jqxGridSale').jqxGrid('updatebounddata');
						$('#agentaddModal').modal('hide');
					}
				},'json');
			});
		});

		var salesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'invoice_no', type: 'string' },
			{ name: 'invoice_date', type: 'date' },
			{ name: 'invoice_date_np', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'party_id', type: 'number' },
			{ name: 'truck_number', type: 'string' },
			{ name: 'type_id', type: 'number' },
			{ name: 'brand_id', type: 'number' },
			{ name: 'quantity', type: 'number' },
			{ name: 'rate', type: 'string' },
			{ name: 'discount', type: 'string' },
			{ name: 'gross_total', type: 'string' },
			{ name: 'is_published', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/sales/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	salesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSale").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSale").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSale").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: salesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSaleToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editSaleRecord(' + index + '); return false;" title="Delete Invoice"><i class="fa fa-trash"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("invoice_no"); ?>',datafield: 'invoice_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("invoice_date"); ?>',datafield: 'invoice_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("invoice_date_np"); ?>',datafield: 'invoice_date_np',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("truck_number"); ?>',datafield: 'truck_number',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("type_id"); ?>',datafield: 'type_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("brand_id"); ?>',datafield: 'brand_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("quantity"); ?>',datafield: 'quantity',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("rate"); ?>',datafield: 'rate',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("discount"); ?>',datafield: 'discount',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("gross_total"); ?>',datafield: 'gross_total',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("is_published"); ?>',datafield: 'is_published',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSale").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSaleFilterClear', function () { 
		$('#jqxGridSale').jqxGrid('clearfilters');
	});


    /*$('#form-sales').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#invoice_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#invoice_no').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#invoice_date_np', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#invoice_date_np').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nepali_month_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_month_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#agent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#agent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#party_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#party_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#truck_number', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#truck_number').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#type_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#type_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#brand_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#brand_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#quantity', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#quantity').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#rate', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#rate').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#discount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#discount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#gross_total', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#gross_total').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#is_published', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#is_published').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#delete_invoice").on('click', function () {
    	deleteInvoice();
    });
    $("#deletemass").on('click', function () {
    	deleteMass();
    });
});

function editSaleRecord(index){
	var row =  $("#jqxGridSale").jqxGrid('getrowdata', index);
	if (row) {
		$('#sales_id').val(row.id);	
		$('#invoice_no').html(row.invoice_no);	
		$('#deleteConfirmation').modal('show');
	}
}

function deleteInvoice(){
	var id = $("#sales_id").val();

	$('#deleteConfirmation').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/sales/invoice_delete"); ?>',
		data: {id : id},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$('#jqxGridSale').jqxGrid('updatebounddata');
				$('#deleteConfirmation').modal('hide');
			}
			$('#deleteConfirmation').unblock();
		}
	});
}

function deleteMass(){
	var invoice_date = $("#invoice_date_dropdown").val();

	$('#massDelete').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/sales/mass_delete"); ?>',
		data: {invoice_date : invoice_date},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$('#jqxGridSale').jqxGrid('updatebounddata');
				$('#massDelete').modal('hide');
			}
			$('#massDelete').unblock();
		}
	});
}
</script>