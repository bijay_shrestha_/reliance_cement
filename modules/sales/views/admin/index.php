<style type="text/css">
/* CSS REQUIRED */
.state-icon {
	left: -5px;
}
.list-group-item-primary {
	color: rgb(255, 255, 255);
	background-color: rgb(66, 139, 202);
}

/* DEMO ONLY - REMOVES UNWANTED MARGIN */
.well .list-group {
	margin-bottom: 0px;
}

.modal-xl{
	width: 1200px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('sales'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSaleToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Salemodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSaleFilterClear"><?php echo lang('general_clear'); ?></button>
					<button type="button" class="btn btn-success btn-flat btn-xs" id="publishAll"><?php echo lang('general_publish_all')?></button>
					<!-- <button type="button" class="btn btn-warning btn-flat btn-xs" data-toggle="modal" data-target="#massDelete">Mass Delete Invoice</button> -->
				</div>
				<div id="jqxGridSale"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="messageNotification">
	<div id="notification_message"> </div>
</div>

<div id="errorNotification">
	<div id="errornotification_message"> </div>
</div>

<div id="Salemodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('sales'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4"><label>Choose File</label></div>
					<div class="col-md-8"><div id="sales_import"></div></div>
				</div>
			</div>
			<div class="modal-footer">
				<!-- <button type="button" class="btn btn-success" id="jqxSaleSubmitButton"><?php echo lang('general_save'); ?></button> -->
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="agentaddModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<input type="hidden" name="agents[]">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New Agent and Party</h4>
			</div>
			<div class="modal-body">
				<div class="well" style="max-height: 300px;overflow: auto;">
					<h4>New Agents</h4>
					<ul id="check-list-box-agent" class="list-group checked-list-box">
						
					</ul>
					<h4>New Party</h4>
					<ul id="check-list-box-party" class="list-group checked-list-box">
						
					</ul>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="get-checked-data">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="deleteConfirmation" class="modal fade" role="dialog" style="z-index: 9999">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Invoice</h4>
			</div>
			<div class="modal-body">
				<div class="well" style="max-height: 300px;overflow: auto;">
					<input type="hidden" id="child_grid_id">
					<input type="hidden" id="sales_id">
					<h1>Delete Invoice No. :<div id="invoice_no"></div></h1>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="delete_invoice">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div id="massDelete" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mass Delete Invoice</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><label>Invoice Date: </label></div>
					<div class="col-md-9"><input type="text" class="text_input" id="invoice_date_dropdown" readonly="readonly"></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="deletemass">Delete</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<div id="Publishdata_sales" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Publish All Data</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><label>Invoice Date: </label></div>
					<div class="col-md-9"><input type="text" class="text_input" id="invoice_date_publish" readonly="readonly"></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="publish_all_data">Publish</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div id="Preshow_Data" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Excel Data</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12"><div id="excelGrid"></div></div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-primary" id="saveExceldata">Approve</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="close_preshow">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){


		$("#messageNotification").jqxNotification({
			width: 300, position: "top-right", opacity: 0.9,
			autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 4000, template: "success"
		});
		$("#errorNotification").jqxNotification({
			width: 300, position: "top-right", opacity: 0.9,
			autoOpen: false, animationOpenDelay: 800, autoClose: true, autoCloseDelay: 4000, template: "error"
		});
		
		$('#sales_import').jqxFileUpload({ width: 300, uploadUrl: '<?php echo site_url('admin/Sales/upload_sales') ?>', fileInputName: 'fileToUpload' });
		$('#sales_import').on('uploadEnd', function (event) {
			$('#Salemodal').modal('hide');
			var args = event.args;
			var fileName = args.file;
			var serverResponce = args.response;

			var result = $.parseJSON(serverResponce);

			if(result.success)
			{
				open_preshow_modal();
			}
			else
			{				
				$('#agentaddModal').modal('show');
				$.each($.parseJSON(serverResponce),function(key,val){
					if(key == 'new_agent')
					{
						$.each(val,function(k,v)
						{
							$('#check-list-box-agent').append('<li class="list-group-item" data-color="success">'+v+'</li>');
						});
					}
					if(key == 'new_party')
					{
						$.each(val,function(i,va)
						{
							$('#check-list-box-party').append('<li class="list-group-item" data-color="success">'+va+'</li>');
						});
					}
				});

				$('.list-group.checked-list-box .list-group-item').each(function () {

					var $widget = $(this),
					$checkbox = $('<input type="checkbox" class="hidden" name="agent[]" />'),
					color = ($widget.data('color') ? $widget.data('color') : "primary"),
					style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
					settings = {
						on: {
							icon: 'glyphicon glyphicon-check'
						},
						off: {
							icon: 'glyphicon glyphicon-unchecked'
						}
					};

					$widget.css('cursor', 'pointer')
					$widget.append($checkbox);

					$widget.on('click', function () {
						$checkbox.prop('checked', !$checkbox.is(':checked'));
						$checkbox.triggerHandler('change');
						updateDisplay();
					});
					$checkbox.on('change', function () {
						updateDisplay();
					});


					function updateDisplay() {
						var isChecked = $checkbox.is(':checked');

						$widget.data('state', (isChecked) ? "on" : "off");

						$widget.find('.state-icon')
						.removeClass()
						.addClass('state-icon ' + settings[$widget.data('state')].icon);

						if (isChecked) {
							$widget.addClass(style + color + ' active');
						} else {
							$widget.removeClass(style + color + ' active');
						}
					}

					function init() {

						if ($widget.data('checked') == true) {
							$checkbox.prop('checked', !$checkbox.is(':checked'));
						}

						updateDisplay();

						if ($widget.find('.state-icon').length == 0) {
							$widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
						}
					}
					init();
				});
				$('#get-checked-data').on('click', function(event) {
					event.preventDefault(); 
					var AgentcheckedItems = {}, PartycheckedItems = {}, counter = 0;
					$("#check-list-box-agent li.active").each(function(idx, li) {
						AgentcheckedItems[counter] = $(li).text();
						counter++;
					});
					$("#check-list-box-party li.active").each(function(idx, li) {
						PartycheckedItems[counter] = $(li).text();
						counter++;
					});
					var new_agents = (JSON.stringify(AgentcheckedItems, null));
					var new_parties = (JSON.stringify(PartycheckedItems, null));

					$.post("<?php echo site_url('admin/Sales/excel_sales')?>",{new_agents : new_agents,new_parties : new_parties, filename:fileName, create_new : '1' }, function(result)
					{	
						if(result.success)
						{
							open_preshow_modal();
							$('#agentaddModal').modal('hide');
						}
					},'json');
				}); } 
			});

		var date_only_source =
		{
			datafields: [
			{ name: 'invoice_date' , type :'string'},
			{ name: 'is_published' , type :'bool'}
			],
			root: "rows",
			id: 'invoice_date',
			datatype: "json",
			async: false,
			url: '<?php echo site_url('admin/Sales/date_only_json') ?>'
		};

		var sales_Source =
		{
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'invoice_no', type: 'string' },
			{ name: 'invoice_date', type: 'string' },
			{ name: 'invoice_date_np', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'party_id', type: 'number' },
			{ name: 'truck_number', type: 'string' },
			{ name: 'type_id', type: 'number' },
			{ name: 'brand_id', type: 'number' },
			{ name: 'quantity', type: 'number' },
			{ name: 'rate', type: 'number' },
			{ name: 'discount', type: 'number' },
			{ name: 'gross_total', type: 'number' },
			{ name: 'is_published', type: 'bool' },
			{ name: 'agent_name', type: 'string' },
			{ name: 'party_name', type: 'string' },
			{ name: 'nepali_month', type: 'string' },
			{ name: 'brand_type', type: 'string' },
			{ name: 'brand_name', type: 'string' },
			{ name: 'total_pre_discount', type: 'number' },
			{ name: 'total_discount', type: 'number' },
			{ name: 'total_excise', type: 'number' },
			{ name: 'total_vat', type: 'number' },
			{ name: 'total', type: 'number' },
			],
			root: "rows",              
			datatype: "json",
			url: '<?php echo site_url('admin/Sales/json') ?>',
			async: false
		};
		var salesDataAdapter = new $.jqx.dataAdapter(sales_Source, { autoBind: true });
		sales = salesDataAdapter.records;
		var nestedGrids = new Array();
            // create nested grid.
            var initrowdetails = function (index, parentElement, gridElement, record) {
            	var id = record.uid.toString();
            	var grid = $($(parentElement).children()[0]);
            	nestedGrids[index] = grid;
            	var filtergroup = new $.jqx.filter();
            	var filter_or_operator = 1;
            	var filtervalue = id;
            	var filtercondition = 'equal';
            	var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // fill the sales depending on the id.
                var salesbyid = [];
                for (var m = 0; m < sales.length; m++) {
                	var result = filter.evaluate(sales[m]["invoice_date"]);
                	if (result)
                		salesbyid.push(sales[m]);
                }
                var sales_Source = { datafields: [
                	{ name: 'id', type: 'number' },
                	{ name: 'created_by', type: 'number' },
                	{ name: 'updated_by', type: 'number' },
                	{ name: 'deleted_by', type: 'number' },
                	{ name: 'created_at', type: 'date' },
                	{ name: 'updated_at', type: 'date' },
                	{ name: 'deleted_at', type: 'date' },
                	{ name: 'invoice_no', type: 'string' },
                	{ name: 'invoice_date', type: 'date' },
                	{ name: 'invoice_date_np', type: 'string' },
                	{ name: 'nepali_month_id', type: 'number' },
                	{ name: 'agent_id', type: 'number' },
                	{ name: 'party_id', type: 'number' },
                	{ name: 'truck_number', type: 'string' },
                	{ name: 'type_id', type: 'number' },
                	{ name: 'brand_id', type: 'number' },
                	{ name: 'quantity', type: 'number' },
                	{ name: 'rate', type: 'number' },
                	{ name: 'discount', type: 'number' },
                	{ name: 'gross_total', type: 'number' },
                	{ name: 'is_published', type: 'bool' },
                	{ name: 'agent_name', type: 'string' },
                	{ name: 'party_name', type: 'string' },
                	{ name: 'nepali_month', type: 'string' },
                	{ name: 'brand_type', type: 'string' },
                	{ name: 'brand_name', type: 'string' },
                	{ name: 'total_pre_discount', type: 'number' },
                	{ name: 'total_discount', type: 'number' },
                	{ name: 'total_excise', type: 'number' },
                	{ name: 'total_vat', type: 'number' },
                	{ name: 'total', type: 'number' },
                	],
                	id: 'id',
                	localdata: salesbyid
                }
                var nestedGridAdapter = new $.jqx.dataAdapter(sales_Source);
                if (grid != null) {
                	grid.jqxGrid({
                		source: nestedGridAdapter, 
                		width: '95%', 
                		height: 275,
                		sortable: true,
                		showfilterrow: true,
                		filterable: true,
                		editable: true,
                		columnsresize: true,
                		autoshowfiltericon: true,
                		selectionmode: 'multiplecellsadvanced',
                		columnsreorder: true,
                		showstatusbar: true,
                		showaggregates: true,
                		statusbarheight: 25,
                		columns: [
                		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', editable: false,renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
                		{
                			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center',editable: false, cellclassname: 'grid-column-center', 
                			cellsrenderer: function (index) {
                				var row =  $(grid).jqxGrid('getrowdata', index);
                				// if(row.is_published == 0)
                				// {
                					var e = '<a href="javascript:void(0)" onclick="deleteRecord(' + index + ','+ grid[0].id +' ); return false;" title="Delete Invoice"><i class="fa fa-trash"></i></a>';
                					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
                				// }
                			}
                		},
                		{ text: '<?php echo lang("invoice_no"); ?>',datafield: 'invoice_no',width: 110,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("invoice_date"); ?>',datafield: 'invoice_date',width: 100,filterable: false, editable: false,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
                		{ text: '<?php echo lang("invoice_date_np"); ?>',datafield: 'invoice_date_np',width: 120,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 100,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 180,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_name',width: 270,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("truck_number"); ?>',datafield: 'truck_number',width: 100,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("type_id"); ?>',datafield: 'brand_type',width: 80,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("brand_id"); ?>',datafield: 'brand_name',width: 100,filterable: true,editable: false,renderer: gridColumnsRenderer },
                		{ text: '<?php echo lang("quantity"); ?>',datafield: 'quantity',cellsalign:'right',width: 120,filterable: true,editable: true,renderer: gridColumnsRenderer,aggregates: ["sum"]},
                		{ text: '<?php echo lang("rate"); ?>',datafield: 'rate',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}},
                		{ text: '<?php echo lang("discount"); ?>',datafield: 'discount',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}},
                		{ text: '<?php echo lang("total_pre_discount"); ?>',datafield: 'total_pre_discount',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}},
                		{ text: '<?php echo lang("total_discount"); ?>',datafield: 'total_discount',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}},
                		{ text: '<?php echo lang("total_excise"); ?>',datafield: 'total_excise',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}},
                		{ text: '<?php echo lang("total_vat"); ?>',datafield: 'total_vat',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}},
                		{ text: '<?php echo lang("gross_total"); ?>',datafield: 'gross_total',width: 250,filterable: true,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			/*var row = $(grid).jqxGrid('getrowdata', index);
                			var e = row.quantity * row.rate;
                			*/
                			return '<div style="text-align: right; margin-top: 8px;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		},
                		aggregatesrenderer: function (aggregates, column, element) {
                			var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                			return renderstring;
                		}   
                	},
                	]
                }); 
$(grid).on('cellvaluechanged', function (event) {
	var rowBoundIndex = event.args.rowindex;
	var column = event.args.datafield;
	var newvalue = event.args.newvalue;
	var rowdata = $(grid).jqxGrid('getrowdata', rowBoundIndex);

	var support_value;
	if(column == 'rate')
	{
		support_value = rowdata.quantity;
	}
	if(column == 'quantity')
	{
		support_value = rowdata.rate;
	}

	$.post('<?php echo site_url('admin/Sales/update_data_sales') ?>',{id : rowdata.id,column : column, newvalue:newvalue,support_value:support_value },function(result)
	{
		if(result.success)
		{
			$('#notification_message').html(result.msg);
			$("#messageNotification").jqxNotification("open"); 
			location.reload();
		}

	},'json');

});
}}
var renderer = function (row, column, value) {
	return '<span style="margin-left: 1px; margin-top: 9px; float: left;">' + value + '</span>';
}
			// creage grid
			$("#jqxGridSale").jqxGrid(
			{
				width: '100%',
				height: gridHeight,
				source: date_only_source,
				rowdetails: true,
				rowsheight: 35,
				sortable: true,
				showfilterrow: true,
				filterable: true,
				columnsresize: true,
				autoshowfiltericon: true,
				columnsreorder: true,
				initrowdetails: initrowdetails,
				rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 290, rowdetailshidden: true },
				showtoolbar: true,
				rendertoolbar: function (toolbar) {
					var container = $("<div style='margin: 5px; height:50px'></div>");
					container.append($('#jqxGridSaleToolbar').html());
					toolbar.append(container);
				},
				// ready: function () {
				// 	$("#jqxGridSale").jqxGrid('showrowdetails', 1);
				// },

				columns: [ 
				{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
				{ text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '';
					var row =  $('#jqxGridSale').jqxGrid('getrowdata', index);
					// if(row.is_published == 0)
					// {
						e += '<a href="javascript:void(0)" onclick="delete_all(' + index + ' ); return false;" title="Delete All"><i class="fa fa-trash"></i></a> &nbsp';
						e += '<a href="javascript:void(0)" onclick="publish_data(' + index + ' ); return false;" title="Publish Data"><i class="fa fa-edit"></i></a>';
						return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
					// }
				}
			},
			{ text: '<?php echo lang("invoice_date"); ?>',datafield: 'invoice_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: 'Published', datafield: 'is_published', columntype: 'checkbox',filterable: true, filtertype: 'bool', width: 170, renderer: gridColumnsRenderer }
			]
		});

			$(document).on('click','#jqxGridSaleFilterClear', function () { 
				$('#jqxGridSale').jqxGrid('clearfilters');
			});

			$("#excelGrid").jqxGrid({
				theme: theme,
				width: '100%',
				height: gridHeight,
            	// source: excelDataSource,
            	altrows: true,
            	pageable: true,
            	sortable: true,
            	rowsheight: 30,
            	columnsheight:30,
            	showfilterrow: true,
            	filterable: true,
            	columnsresize: true,
            	autoshowfiltericon: true,
            	editable: true,
            	columnsreorder: true,
            	selectionmode: 'none',
            	virtualmode: true,
            	enableanimations: false,
            	showstatusbar: true,
            	showaggregates: true,
            	statusbarheight: 25,
            	pagesizeoptions: pagesizeoptions,
            	showtoolbar: true,
            	rendertoolbar: function (toolbar) {
            		var container = $("<div style='margin: 5px; height:50px'></div>");
            		container.append($('#excelGridToolbar').html());
            		toolbar.append(container);
            	},
            	columns: [
            	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', editable:false,cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
            	{ text: '<?php echo lang("invoice_no"); ?>',datafield: 'invoice_no',width: 110,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("invoice_date"); ?>',datafield: 'invoice_date',width: 90,filterable: true,editable:false,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
            	{ text: '<?php echo lang("invoice_date_np"); ?>',datafield: 'invoice_date_np',width: 110,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 90,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 150,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_name',width: 250,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("truck_number"); ?>',datafield: 'truck_number',width: 100,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("type_id"); ?>',datafield: 'brand_type',width: 50,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("brand_id"); ?>',datafield: 'brand_name',width: 100,filterable: true,editable:false,renderer: gridColumnsRenderer },
            	{ text: '<?php echo lang("quantity"); ?>',datafield: 'quantity',cellsalign:'right',width: 90,filterable: true,editable:true,renderer: gridColumnsRenderer,aggregates: ["sum"]},
            	{ text: '<?php echo lang("rate"); ?>',datafield: 'rate',width: 150,filterable: true,editable:true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value:0).toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	} },
            	{ text: '<?php echo lang("discount"); ?>',datafield: 'discount',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	}},
            	{ text: '<?php echo lang("total_pre_discount"); ?>',datafield: 'total_pre_discount',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	}},
            	{ text: '<?php echo lang("total_discount"); ?>',datafield: 'total_discount',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	}},
            	{ text: '<?php echo lang("total_excise"); ?>',datafield: 'total_excise',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	}},
            	{ text: '<?php echo lang("total_vat"); ?>',datafield: 'total_vat',width: 150,filterable: true,editable: true, cellsformat:'F2',cellsalign:'right',renderer: gridColumnsRenderer,cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	}},
            	{ text: '<?php echo lang("gross_total"); ?>',datafield: 'gross_total',width: 250,filterable: true,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer:
            	function (index) { 
            		var row = $("#excelGrid").jqxGrid('getrowdata', index);
            		var e = row.quantity * row.rate;
            		return '<div style="text-align: right; margin-top: 8px;">' + e.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            	},
            	aggregatesrenderer: function (aggregates, column, element) {
            		var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum:0).toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
            		return renderstring;
            	}   
            }],
            rendergridrows: function (result) {
            	return result.data;
            } });

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#excelGrid").jqxGrid('refresh');}, 500);
	});

	$("#delete_invoice").on('click', function () {
		deleteInvoice();
	});
	$("#deletemass").on('click', function () {
		deleteMass();
	});
	$("#publish_all_data").on('click', function () {
		publish_Data();
	});
	$("#saveExceldata").on('click', function () {
		saveGriddata();
	});
});
$('#excelGrid').on('cellvaluechanged', function (event) {

	var rowBoundIndex = event.args.rowindex;
	var column = event.args.datafield;
	var newvalue = event.args.newvalue;
	var rowdata = $('#excelGrid').jqxGrid('getrowdata', rowBoundIndex);

	$.post('<?php echo site_url('admin/Sales/update_data') ?>',{id : rowdata.id,column : column, newvalue:newvalue},function(result)
	{
		if(result.success)
		{
			$('#notification_message').html(result.msg);
			$('#excelGrid').jqxGrid('updatebounddata');
			$("#messageNotification").jqxNotification("open"); 
		}

	},'json');

});
function deleteRecord(index,grid_name){
	var row =  $(grid_name).jqxGrid('getrowdata', index);
	if (row) {
		$('#sales_id').val(row.id);	
		$('#invoice_no').html(row.invoice_no);	
		$('#deleteConfirmation').modal('show');
	}
}

function delete_all(index)
{
	var row =  $('#jqxGridSale').jqxGrid('getrowdata', index);
	if(row)
	{
		$('#invoice_date_dropdown').val(row.invoice_date);
		$('#massDelete').modal('show');
	}
}

function publish_data(index)
{
	var row =  $('#jqxGridSale').jqxGrid('getrowdata', index);
	if(row)
	{
		$('#invoice_date_publish').val(row.invoice_date);
		$('#Publishdata_sales').modal('show');
	}
}

function publish_data(index)
{
	var row =  $('#jqxGridSale').jqxGrid('getrowdata', index);
	if(row)
	{
		$('#invoice_date_publish').val(row.invoice_date);
		$('#Publishdata_sales').modal('show');
	}
}

function deleteInvoice(){
	var id = $("#sales_id").val();

	$('#deleteConfirmation').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Sales/invoice_delete"); ?>',
		data: {id : id},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				//$(grid_name).jqxGrid('updatebounddata');
				$('#notification_message').html('successfully Completed Request');
				$('#deleteConfirmation').modal('hide');
				location.reload();
			}
			$('#deleteConfirmation').unblock();
		}
	});
}

function deleteMass(){
	var invoice_date = $("#invoice_date_dropdown").val();

	$('#massDelete').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Sales/mass_delete"); ?>',
		data: {invoice_date : invoice_date},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$('#notification_message').html('successfully Completed Request');
				$('#jqxGridSale').jqxGrid('updatebounddata');
				$('#massDelete').modal('hide');
				// location.reload();
			}
			$('#massDelete').unblock();
		}
	});
}
function publish_Data(){
	var invoice_date = $("#invoice_date_publish").val();

	$('#Publishdata_sales').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Sales/publish_data"); ?>',
		data: {invoice_date : invoice_date},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$('#notification_message').html('successfully Completed Request');
				$('#jqxGridSale').jqxGrid('updatebounddata');
				$('#Publishdata_sales').modal('hide');
				location.reload();
			}
			$('#Publishdata_sales').unblock();
		}
	});
}

function saveGriddata(){
	var rows = $('#excelGrid').jqxGrid('getrows');

	$('#Preshow_Data').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Sales/save"); ?>',
		data: {rows:rows},
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				$('#notification_message').html('successfully Completed Request');
				$('#jqxGridSale').jqxGrid('updatebounddata');
				$('#Preshow_Data').modal('hide');
				location.reload();
			}
			$('#Preshow_Data').unblock();
		}
	});
}

$('#close_preshow').click(function()
{
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Sales/update_temporary_table"); ?>',		
		success: function (result) {			
			$('#jqxGridSale').jqxGrid('updatebounddata');
			$('#Preshow_Data').modal('hide');
		}
	});
});

function open_preshow_modal()
{
	var excelsalesDataSource =
	{
		datatype: "json",
		datafields: [
		{ name: 'id', type: 'number' },
		{ name: 'created_by', type: 'number' },
		{ name: 'updated_by', type: 'number' },
		{ name: 'deleted_by', type: 'number' },
		{ name: 'created_at', type: 'date' },
		{ name: 'updated_at', type: 'date' },
		{ name: 'deleted_at', type: 'date' },
		{ name: 'invoice_no', type: 'string' },
		{ name: 'invoice_date', type: 'string' },
		{ name: 'invoice_date_np', type: 'string' },
		{ name: 'nepali_month_id', type: 'number' },
		{ name: 'agent_id', type: 'number' },
		{ name: 'party_id', type: 'number' },
		{ name: 'truck_number', type: 'string' },
		{ name: 'type_id', type: 'number' },
		{ name: 'brand_id', type: 'number' },
		{ name: 'quantity', type: 'number' },
		{ name: 'rate', type: 'number' },
		{ name: 'discount', type: 'number' },
		{ name: 'gross_total', type: 'number' },
		{ name: 'is_published', type: 'number' },
		{ name: 'fiscal_year_id', type: 'number' },
		{ name: 'agent_name', type: 'string' },
		{ name: 'party_name', type: 'string' },
		{ name: 'nepali_month', type: 'string' },
		{ name: 'brand_type', type: 'string' },
		{ name: 'brand_name', type: 'string' },
		{ name: 'total_pre_discount', type: 'number' },
		{ name: 'total_discount', type: 'number' },
		{ name: 'total_excise', type: 'number' },
		{ name: 'total_vat', type: 'number' },
		{ name: 'total', type: 'number' },
		],
		url: '<?php echo site_url("admin/Sales/temporary_json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
		},
		beforeprocessing: function (data) {
			excelsalesDataSource.totalrecords = data.total;
		},
		filter: function () {
			$("#jqxGridParty").jqxGrid('updatebounddata', 'filter');
		},
		sort: function () {
			$("#jqxGridParty").jqxGrid('updatebounddata', 'sort');
		},
		processdata: function(data) {
		}
	};
	var excelsalesDataAdapter = new $.jqx.dataAdapter(excelsalesDataSource);

	$('#excelGrid').jqxGrid({ source : excelsalesDataAdapter });
	$('#Preshow_Data').modal('show');
}
</script>

<script type="text/javascript">
	$(document).on('click','#publishAll', function () { 
		$.post('<?php echo site_url('admin/Sales/publishAll')?>',{},function(data) {
			if(data.success){
				location.reload();
				$('#notification_message').html('Updated successfully.');
				$("#messageNotification").jqxNotification("open"); 
			}else{
				$('#errornotification_message').html('Error Occured.');
				$("#errorNotification").jqxNotification("open"); 
			}
		},'json');
	});
</script>