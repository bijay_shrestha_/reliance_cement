<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/*
 * Rename the file to Sale.php
 * and Define Module Library Function (if any)
 */


/* End of file Sale.php */
/* Location: ./modules/Sale/libraries/Sale.php */
/**
* 
*/
class Sale
{
	
	public $CI;

	public function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->library('auth/Aauth');
	}


	public function register_user($email, $password, $username, $fullname,$agent_id)
	{
		$user_id = $this->CI->aauth->create_user($email, $password , $username, $fullname);

		$this->CI->db->trans_begin();

		if($user_id != 0 && $user_id){
			$temp = array();
			$temp['user_id'] = $user_id;
			$temp['group_id'] = AGENT_GROUP_ID;

			$this->CI->db->insert('aauth_user_groups',$temp);

			$agent['id'] = $agent_id;
            $agent['user_id'] = $user_id;

            $this->agent_model->update($agent['id'],$agent);
		}

		if ($this->CI->db->trans_status() === FALSE)
		{
			$this->CI->db->trans_rollback();
		}
		else
		{
			$this->CI->db->trans_commit();          
		}

		return $user_id;

	}

	public function get_sales_monthwise_typewise()
	{
		$query = "SELECT view_sales.brand_type,view_sales.nepali_month,sum(view_sales.gross_total) as total_sales FROM view_sales GROUP BY view_sales.brand_type,view_sales.nepali_month";

		$rows = $this->CI->db->query($query)->result_array();

		return $rows;
	}
}
