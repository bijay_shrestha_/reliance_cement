<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Sales
 *
 * Extends the Project_Controller class
 * 
 */

class AdminSales extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Sales');

		$this->load->model('sales/sale_model');
		$this->load->model('agents/agent_model');
		$this->load->model('parties/party_model');
		$this->load->model('brands/brand_model');
		$this->load->model('brand_types/brand_type_model');
		$this->load->model('nepali_months/nepali_month_model');
		$this->load->model('sales_temporaries/sales_temporary_model');

		$this->load->library('sales/sale');
		$this->lang->load('sales/sale');
	}

	public function index()
	{
		list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
		if ($fiscal_year_id == null) {
			flashMsg('warning', 'Current Fiscal Year is not recorded yet. Please save the FISCAL YEAR.');
			redirect('admin/fiscal_years');
		}
		// Display Page
		if(is_agent())
		{
			$view_page = "index_agent";
		}
		else
		{
			$view_page = "index";
		}
		$data['header'] = lang('sales');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'sales';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->sale_model->_table = "view_sales";

		$this->db->where('user_id',$this->session->userdata('id'));
		$agent = $this->db->get('mst_agents')->row();

		$where = '1=1';	
		if(is_agent())
		{
			$where = "(agent_id = {$agent->id} AND is_published = 1)";
		}

		search_params();
		$this->db->where($where);
		$total=$this->sale_model->find_count();
		
		paging('id');
		
		search_params();
		$this->db->where($where);
		$rows=$this->sale_model->findAll();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function temporary_json()
	{
		$this->sale_model->_table = "view_sales_temporary";

		search_params();
		
		$total=$this->sale_model->find_count(array('is_processed'=>0));
		
		paging('id');
		
		search_params();
		
		$rows=$this->sale_model->findAll(array('is_processed'=>0));
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function date_only_json()
	{
		$where = '1=1';	
		if(is_agent())
		{
			$where = "('agent_id' = {$this->session->userdata('id')})";
		}
		
		$fields= array('invoice_date','is_published');
		paging('id');
		
		search_params();
		$this->db->group_by($fields);
		$this->db->where($where);
		$rows=$this->sale_model->findAll(NULL,$fields);
		// echo $this->db->last_query();
		$total = count($rows);
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	/*public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->sale_model->insert($data);
        }
        else
        {
            $success=$this->sale_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['invoice_no'] = $this->input->post('invoice_no');
		$data['invoice_date'] = $this->input->post('invoice_date');
		$data['invoice_date_np'] = $this->input->post('invoice_date_np');
		$data['nepali_month_id'] = $this->input->post('nepali_month_id');
		$data['agent_id'] = $this->input->post('agent_id');
		$data['party_id'] = $this->input->post('party_id');
		$data['truck_number'] = $this->input->post('truck_number');
		$data['type_id'] = $this->input->post('type_id');
		$data['brand_id'] = $this->input->post('brand_id');
		$data['quantity'] = $this->input->post('quantity');
		$data['rate'] = $this->input->post('rate');
		$data['discount'] = $this->input->post('discount');
		$data['gross_total'] = $this->input->post('gross_total');
		$data['is_published'] = $this->input->post('is_published');

        return $data;
    }*/

    public function upload_sales()
    {
    	$this->update_temporary_table();

    	$target_dir = "uploads/sales/";

    	if (!file_exists('./uploads/sales')) {
    		mkdir('./uploads/sales', 0777, true);
    	}
    	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

    	$uploadOk = 1;
    	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    		//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    	} else {
    		echo "Sorry, there was an error uploading your file.";
    	}    	
    	$this->excel_sales($_FILES["fileToUpload"]["name"]);
    }

    public function excel_sales($filename = NULL)
    {
    	list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
    	
    	if($this->input->post('filename'))
    	{
    		$filename = $this->input->post('filename');
    	}

    	if($this->input->post('new_agents'))
    	{
    		$new_agents = json_decode($this->input->post('new_agents'));
    		foreach ($new_agents as $key => $value) 
    		{
    			$agent['name'] = $value;
    			$agent_id = $this->agent_model->insert($agent);
    			
    			// create user as well
    			$username = strtolower(substr(str_replace('-','',str_replace(' ', '.', $value)),0,20));
    			$email = strtolower(substr(str_replace('-','',str_replace(' ', '.', $value)),0,20)).'@reliance.com';
    			$fullname = $value;
    			$password = 'password123';
    			$this->sale->register_user($email, $password, $username, $fullname,$agent_id);
    			
    		}
    	}
    	if($this->input->post('new_parties'))
    	{
    		$new_parties = json_decode($this->input->post('new_parties'));
    		foreach ($new_parties as $key => $value) 
    		{
    			$party['name'] = $value;
    			$this->party_model->insert($party);
    		}
    	}

    	$file = FCPATH . 'uploads/sales/'.$filename; 
    	$this->load->library('Excel');
    	$objPHPExcel = PHPExcel_IOFactory::load($file);
    	$objReader = PHPExcel_IOFactory::createReader('Excel2007');        
    	$objReader->setReadDataOnly(false);

    	$index = array('voucher_no', 'date','nep_date', 'nepali_month', 'agent', 'party', 'truck_number', 'type', 'brand', 'quantity', 'original_rate', 'discount','total_pre_discount','total_discount','total_excise','total_vat', 'gross_total','total','agent_main');
    	$raw_data = array();
    	$view_data = array();
    	foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
    		if ($key == 0) {
    			$worksheetTitle = $worksheet->getTitle();
				$highestRow = $worksheet->getHighestRow(); // e.g. 10
				$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				$nrColumns = ord($highestColumn) - 64;

				for ($row = 2; $row <= $highestRow; ++$row) {
					for ($col = 0; $col < $highestColumnIndex; ++$col) {
						$cell = $worksheet->getCellByColumnAndRow($col, $row);
						$val = $cell->getValue();
						$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);

						$raw_data[$row][$index[$col]] = $val;
					}
				}
			}
		}

		if(!$this->input->post('create_new'))
		{
			$new_agent = array();
			$new_party = array();
			foreach($raw_data as $key => $val) 
			{
				$get_agent = $this->agent_model->find(array('name'=>$val['agent_main']));
				if(!$get_agent)
				{
					if(!(in_array($val['agent_main'], $new_agent)))
					{
						$new_agent[] = $val['agent_main'];
					}
				}
				$get_party = $this->party_model->find(array('name'=>$val['party']));
				if(!$get_party)
				{
					if(!(in_array($val['party'],$new_party)))
					{
						$new_party[] = $val['party'];
					}
				}
			}

			if(!empty($new_agent) || !empty($new_party))
			{
				echo json_encode(array('new_agent'=>$new_agent,'new_party'=>$new_party));
				exit;
			}
		}
		
		foreach ($raw_data as $key => $value) 
		{
			$get_brand = $this->brand_model->find(array('name'=>$value['brand']));
			$get_brand_type = $this->brand_type_model->find(array('name'=>$value['type']));
			$get_agent = $this->agent_model->find(array('name'=>$value['agent_main']));
			$get_month = $this->nepali_month_model->find(array('name'=>$value['nepali_month']));
			$get_party = $this->party_model->find(array('name'=>$value['party']));
			/*$this->db->select_max('batch_no');
			$get_batch_no = $this->db->get('tbl_sales')->row();*/
			
			if(!$get_brand)
			{
				echo json_encode("Brand error ".$value['brand']);
				exit;
			}
			if(!$get_brand_type)
			{
				echo json_encode("Brand Type error ".$value['type']);
				exit;
			}
			if(!$get_party)
			{
				echo json_encode("Party Unknown ".$value['party']);
				exit;
			}

			$sales[$key]['invoice_no'] = $value['voucher_no'];
			$sales[$key]['invoice_date'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($value['date']));
			$sales[$key]['invoice_date_np'] = get_nepali_date(date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($value['date'])),'nep');
			$sales[$key]['nepali_month_id'] = $get_month->id;
			$sales[$key]['truck_number'] = ucfirst($value['truck_number']);
			$sales[$key]['quantity'] = $value['quantity'];
			$sales[$key]['type_id'] = $get_brand_type->id;
			$sales[$key]['brand_id'] = $get_brand->id;
			$sales[$key]['agent_id'] = ($get_agent?$get_agent->id:'');
			$sales[$key]['party_id'] = $get_party->id;
			$sales[$key]['agent_name'] = ($value['agent']);
			$sales[$key]['rate'] = $value['original_rate'];
			$sales[$key]['discount'] = $value['discount'];
			$sales[$key]['gross_total'] = $value['gross_total'];
			$sales[$key]['fiscal_year_id'] = $fiscal_year_id;
			$sales[$key]['total_pre_discount'] = $value['total_pre_discount'];
			$sales[$key]['total_discount'] = $value['total_discount'];
			$sales[$key]['total_excise'] = $value['total_excise'];
			$sales[$key]['total_vat'] = $value['total_vat'];
			$sales[$key]['total'] = $value['total'];
			//$sales[$key]['batch_no'] = ($get_batch_no ? ($get_batch_no->batch_no + 1) : 1);
		} 
		
		$this->db->trans_start();
		$success = $this->sales_temporary_model->insert_many($sales);
		if ($this->db->trans_status() === FALSE) 
		{
			$this->db->trans_rollback();
		} 
		else 
		{
			$this->db->trans_commit();
		} 
		$this->db->trans_complete();

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function mass_delete()
	{
		$date = $this->input->post('invoice_date');
		$rows = $this->sale_model->findAll(array('invoice_date'=>$date));
		foreach ($rows as $key => $value) 
		{
			$data['id']	= $value->id;
			$success = $this->sale_model->delete($data['id']);
		}
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function publish_data()
	{
		$date = $this->input->post('invoice_date');
		$rows = $this->sale_model->findAll(array('invoice_date'=>$date));
		foreach ($rows as $key => $value) 
		{
			$data['id']	= $value->id;
			$data['is_published']	= 1;
			$success = $this->sale_model->update($data['id'],$data);
		}

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function invoice_delete()
	{
		$data['id'] = $this->input->post('id');
		$success = $this->sale_model->delete($data['id']);
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function get_invoice_dates()
	{
		$this->db->group_by('invoice_date');
		$rows = $this->sale_model->findAll(NULL,'invoice_date');
		echo json_encode($rows);
	}

	public function save()
	{

		$updated = $this->update_temporary_table();

		if($updated)
		{
			$data = $this->input->post('rows');
		
			foreach ($data as $key => $value) 
			{
				$sales[$key]['invoice_no'] = $value['invoice_no'];
				$sales[$key]['invoice_date'] = $value['invoice_date'];
				$sales[$key]['invoice_date_np'] =$value['invoice_date_np'];
				$sales[$key]['nepali_month_id'] = $value['nepali_month_id'];
				$sales[$key]['truck_number'] = $value['truck_number'];
				$sales[$key]['quantity'] = $value['quantity'];
				$sales[$key]['type_id'] = $value['type_id'];
				$sales[$key]['brand_id'] = $value['brand_id'];
				$sales[$key]['agent_id'] = $value['agent_id'];
				$sales[$key]['party_id'] = $value['party_id'];
				$sales[$key]['agent_name'] = $value['agent_name'];
				$sales[$key]['rate'] = $value['rate'];
				$sales[$key]['discount'] = $value['discount'];
				$sales[$key]['gross_total'] = $value['quantity'] * $value['rate'];
				$sales[$key]['fiscal_year_id'] = $value['fiscal_year_id'];
				$sales[$key]['total_pre_discount'] = $value['total_pre_discount'];
				$sales[$key]['total_discount'] = $value['total_discount'];
				$sales[$key]['total_excise'] = $value['total_excise'];
				$sales[$key]['total_vat'] = $value['total_vat'];
				$sales[$key]['total'] = $value['total'];
			} 
			
			$this->db->trans_start();
			$success = $this->sale_model->insert_many($sales);
			if ($this->db->trans_status() === FALSE) 
			{
				$this->db->trans_rollback();
			} 
			else 
			{
				$this->db->trans_commit();
			} 
			$this->db->trans_complete();

			if($success)
			{
				$success = TRUE;
				$msg=lang('general_success');
			}
			else
			{
				$success = FALSE;
				$msg=lang('general_failure');
			}

			echo json_encode(array('msg'=>$msg,'success'=>$success));
			exit;	
		}
	}

	public function update_data()
	{
		$data['id'] = $this->input->post('id');
		$data[$this->input->post('column')] = $this->input->post('newvalue');
		$success = $this->sales_temporary_model->update($data['id'],$data);
		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function update_data_sales()
	{
		$data['id'] = $this->input->post('id');
		$data[$this->input->post('column')] = $this->input->post('newvalue');
		if($this->input->post('support_value'))
		{
			$data['gross_total'] = $this->input->post('newvalue') * $this->input->post('support_value');
		}
		$success = $this->sale_model->update($data['id'],$data);

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
		exit;
	}

	public function update_temporary_table()
	{
		$update_data = array('is_processed'=>1);
		$updated = $this->db->update('tbl_sales_temporary',$update_data);
		return $updated;
	}

	public function get_typewise_report()
	{		
		$nep_month = get_nepali_date(date('Y-m-d'),'nep');

		$month_explode = explode('-',$nep_month);

		$rows = $this->sale->get_sales_monthwise_typewise();

		echo json_encode($data);
	}

	public function dashboard_json()
	{
		$this->sale_model->_table = "view_overview_detail";
		// $this->sale_model->_table = "view_partywise_sales";

		$this->db->where('user_id',$this->session->userdata('id'));
		$agent = $this->db->get('mst_agents')->row();
		$agent_id = $agent->id;


		$where = '1=1';	
		if(is_agent())
		{
			$where = "(agent_id = {$agent_id})";
		}

		search_params();
		$this->db->where($where);
		$total=$this->sale_model->find_count();
		
		paging('nepali_month_id','asc');
		
		search_params();
		$this->db->where($where);
		$rows=$this->sale_model->findAll();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function publishAll()
	{
		$data['is_published'] = 1;
		$this->db->set($data);
		$success = $this->db->update('tbl_sales');
		// $success = $this->sale_model->update_all('tbl_sales',$data);
		echo json_encode (array('success' =>$success));
	}
}