<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	public function __construct()
    {
        parent::__construct();
        
        if(!empty(getallheaders()['Accesstoken']))
        {
            $access_token = getallheaders()['Accesstoken'];        
            $this->db->where('access_token',$access_token);
            $check_token = $this->db->get('aauth_users')->result_array();

            if(empty($check_token))
            {
                echo json_encode('Access Denied');
                exit;
            }
        }
        else
        {
            echo json_encode('Access Token not sent on header');
            exit();
        }

        $this->load->model('sales/sale_model');
    }

    public function sale_get()
    {
        $this->sale_model->_table = "view_sales";

        $this->db->where('user_id',getallheaders()['Userid']);
        $agent = $this->db->get('mst_agents')->row();

        
        $where = "(agent_id = {$agent->id} AND is_published = 1)";
        
        $this->db->where($where);
        $rows=$this->sale_model->findAll();

        echo json_encode(array('rows'=>$rows));
        exit;
    }

    public function sale_post()
    {

    }

    public function sale_delete()
    {

    }


}