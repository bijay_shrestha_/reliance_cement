<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Brands
 *
 * Extends the Project_Controller class
 * 
 */

class AdminBrands extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Brands');

        $this->load->model('brands/brand_model');
        $this->lang->load('brands/brand');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('brands');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'brands';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->brand_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->brand_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->brand_model->insert($data);
        }
        else
        {
            $success=$this->brand_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		
		$data['name'] = $this->input->post('name');
		$data['type_id'] = $this->input->post('type_id');
		$data['code'] = $this->input->post('code');
		$data['rank'] = $this->input->post('rank');

        return $data;
   }
}