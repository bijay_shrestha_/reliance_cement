<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3><?php echo lang('brands'); ?></h3>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>">Home</a></li>
            <li class="active"><?php echo lang('brands'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">
                <?php echo displayStatus(); ?>
                <div id='jqxGridBrandToolbar' class='grid-toolbar'>
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridBrandInsert"><?php echo lang('general_create'); ?></button>
                    <button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridBrandFilterClear"><?php echo lang('general_clear'); ?></button>
                </div>
                <div id="jqxGridBrand"></div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowBrand">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-brands', 'onsubmit' => 'return false')); ?>
            <input type = "hidden" name = "id" id = "brands_id"/>
            <table class="form-table">
                
                <tr>
                    <td><label for='brands_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
                    <td><input id='brands_name' class='text_input' name='name'></td>
                </tr>
                <tr>
                    <td><label for='type_id'><?php echo lang('type_id')?></label></td>
                    <td><div id='type_id' class='number_general' name='type_id'></div></td>
                </tr>
                <!-- <tr>
                    <td><label for='code'><?php echo lang('code')?></label></td>
                    <td><input id='code' class='text_input' name='code'></td>
                </tr>
                <tr>
                    <td><label for='brands_rank'><?php echo lang('rank')?><span class='mandatory'>*</span></label></td>
                    <td><div id='brands_rank' class='number_general' name='rank'></div></td>
                </tr> -->
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxBrandSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxBrandCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

    var brandsDataSource =
    {
        datatype: "json",
        datafields: [
            { name: 'id', type: 'number' },
            { name: 'created_by', type: 'number' },
            { name: 'updated_by', type: 'number' },
            { name: 'deleted_by', type: 'number' },
            { name: 'created_at', type: 'date' },
            { name: 'updated_at', type: 'date' },
            { name: 'deleted_at', type: 'date' },
            { name: 'name', type: 'string' },
            { name: 'type_id', type: 'number' },
            { name: 'code', type: 'string' },
            { name: 'rank', type: 'number' },
            
        ],
        url: '<?php echo site_url("admin/Brands/json"); ?>',
        pagesize: defaultPageSize,
        root: 'rows',
        id : 'id',
        cache: true,
        pager: function (pagenum, pagesize, oldpagenum) {
            //callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
            brandsDataSource.totalrecords = data.total;
        },
        // update the grid and send a request to the server.
        filter: function () {
            $("#jqxGridBrand").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxGridBrand").jqxGrid('updatebounddata', 'sort');
        },
        processdata: function(data) {
        }
    };
    
    $("#jqxGridBrand").jqxGrid({
        theme: theme,
        width: '100%',
        height: gridHeight,
        source: brandsDataSource,
        altrows: true,
        pageable: true,
        sortable: true,
        rowsheight: 30,
        columnsheight:30,
        showfilterrow: true,
        filterable: true,
        columnsresize: true,
        autoshowfiltericon: true,
        columnsreorder: true,
        selectionmode: 'none',
        virtualmode: true,
        enableanimations: false,
        pagesizeoptions: pagesizeoptions,
        showtoolbar: true,
        rendertoolbar: function (toolbar) {
            var container = $("<div style='margin: 5px; height:50px'></div>");
            container.append($('#jqxGridBrandToolbar').html());
            toolbar.append(container);
        },
        columns: [
            { text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
            {
                text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
                cellsrenderer: function (index) {
                    var e = '<a href="javascript:void(0)" onclick="editBrandRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
                    return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
                }
            },
            
            { text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
            { text: '<?php echo lang("type_id"); ?>',datafield: 'type_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
            // { text: '<?php echo lang("code"); ?>',datafield: 'code',width: 150,filterable: true,renderer: gridColumnsRenderer },
            // { text: '<?php echo lang("rank"); ?>',datafield: 'rank',width: 150,filterable: true,renderer: gridColumnsRenderer },
            
        ],
        rendergridrows: function (result) {
            return result.data;
        }
    });

    $("[data-toggle='offcanvas']").click(function(e) {
        e.preventDefault();
        setTimeout(function() {$("#jqxGridBrand").jqxGrid('refresh');}, 500);
    });

    $(document).on('click','#jqxGridBrandFilterClear', function () { 
        $('#jqxGridBrand').jqxGrid('clearfilters');
    });

    $(document).on('click','#jqxGridBrandInsert', function () { 
        openPopupWindow('jqxPopupWindowBrand', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

    // initialize the popup window
    $("#jqxPopupWindowBrand").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowBrand").on('close', function () {
        reset_form_brands();
    });

    $("#jqxBrandCancelButton").on('click', function () {
        reset_form_brands();
        $('#jqxPopupWindowBrand').jqxWindow('close');
    });

    /*$('#form-brands').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
            { input: '#created_by', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#created_by').jqxNumberInput('val');
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#updated_by', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#updated_by').jqxNumberInput('val');
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#deleted_by', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#deleted_by').jqxNumberInput('val');
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#created_at', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#created_at').val();
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#updated_at', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#updated_at').val();
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#deleted_at', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#deleted_at').val();
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#brands_name', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#brands_name').val();
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#type_id', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#type_id').jqxNumberInput('val');
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#code', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#code').val();
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

            { input: '#brands_rank', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#brands_rank').jqxNumberInput('val');
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            },

        ]
    });*/

    $("#jqxBrandSubmitButton").on('click', function () {
        saveBrandRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveBrandRecord();
                }
            };
        $('#form-brands').jqxValidator('validate', validationResult);
        */
    });
});

function editBrandRecord(index){
    var row =  $("#jqxGridBrand").jqxGrid('getrowdata', index);
    if (row) {
        $('#brands_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
        // $('#updated_by').jqxNumberInput('val', row.updated_by);
        // $('#deleted_by').jqxNumberInput('val', row.deleted_by);
        // $('#created_at').val(row.created_at);
        // $('#updated_at').val(row.updated_at);
        // $('#deleted_at').val(row.deleted_at);
        $('#brands_name').val(row.name);
        $('#type_id').jqxNumberInput('val', row.type_id);
        // $('#code').val(row.code);
        // $('#brands_rank').jqxNumberInput('val', row.rank);
        
        openPopupWindow('jqxPopupWindowBrand', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveBrandRecord(){
    var data = $("#form-brands").serialize();
    
    $('#jqxPopupWindowBrand').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/Brands/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_brands();
                $('#jqxGridBrand').jqxGrid('updatebounddata');
                $('#jqxPopupWindowBrand').jqxWindow('close');
            }
            $('#jqxPopupWindowBrand').unblock();
        }
    });
}

function reset_form_brands(){
    $('#brands_id').val('');
    $('#form-brands')[0].reset();
}
</script>