<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
    var $user_id;

    public function __construct()
    {
        parent::__construct();

        if(!empty(getallheaders()['Accesstoken']))
        {
            $this->user_id  = getallheaders()['Userid'];
        
            $access_token = getallheaders()['Accesstoken'];
            $this->db->where('id',$this->user_id);
            $this->db->where('access_token',$access_token);
            $check_token = $this->db->get('aauth_users')->result_array();

            if(empty($check_token))
            {
                echo json_encode('Access Denied');
                exit;
            }
        }
        else
        {
            echo json_encode('Access Token not sent on header');
            exit();
        }

        $this->load->model('visit_summaries/visit_summary_model');

        $this->load->helper('project');
    }

    public function visit_summary_get()
    {
        $where = array(
            'created_by' => $this->user_id,
        );
        $this->db->select('id, agent_name, date_en, date_np,agent_id');

        $this->db->where($where);
        $rows = $this->db->get('view_field_visit_summary')->result();

        echo json_encode(array('rows'=>$rows));
        exit;
    }

    public function visit_summary_detail_post()
    {
        $where = array(
            'id' => $this->input->post('id'),
        );
        $this->db->where($where);
        $rows = $this->db->get('view_field_visit_summary')->result();

        echo json_encode(array('rows' => $rows));
        exit;
    }

    public function visit_summary_post()
    {
        $data['created_by']             = getallheaders()['Userid'];
        $data['created_at']             = date('Y-m-d H:i:s');
        $data['agent_id']               = $this->input->post('agent_id');
        $data['date_en']                = $this->input->post('date_en');
        $data['date_np']                = $this->input->post('date_np');
        $data['latitude']               = $this->input->post('latitude');
        $data['longitude']              = $this->input->post('longitude');
        $data['city']                   = $this->input->post('city');
        $data['province']               = $this->input->post('province');
        $nepali_date_array              = explode('-', $data['date_np']);
        $data['nepali_month_id']        = (int)$nepali_date_array[1];
        $data['summary']                = $this->input->post('summary');
        $data['opc_price']              = $this->input->post('opc_price');
        $data['ppc_price']              = $this->input->post('ppc_price');

        // echo '<pre>';print_r($data);
        $success = $this->db->insert('field_visit_summary',$data);

        if($success)
        {
            $success = TRUE;
        }
        else
        {
            $success = FALSE;
        }

        echo json_encode(array('success'=>$success));
        exit;
    }

    public function visit_summary_delete()
    {
        //TODO
    }
    
    public function form_data_get()
    {
        $where['sales_person_id'] = $this->user_id;

        $this->db->where($where);
        $data['agents'] = $this->db->get('mst_agents')->result();

        echo json_encode($data);

    }


}