<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Visit_summaries
 *
 * Extends the Project_Controller class
 * 
 */

class AdminVisit_summaries extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Visit Summaries');

        $this->load->model('visit_summaries/visit_summary_model');
        $this->lang->load('visit_summaries/visit_summary');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('visit_summaries');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'visit_summaries';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->visit_summary_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->visit_summary_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->visit_summary_model->insert($data);
        }
        else
        {
            $success=$this->visit_summary_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['agent_id'] = $this->input->post('agent_id');
		$data['date_en'] = $this->input->post('date_en');
		$data['date_np'] = $this->input->post('date_np');
		$data['latitude'] = $this->input->post('latitude');
		$data['longitude'] = $this->input->post('longitude');
		$data['city'] = $this->input->post('city');
		$data['province'] = $this->input->post('province');
		$data['nepali_month_id'] = $this->input->post('nepali_month_id');
		$data['summary'] = $this->input->post('summary');
		$data['opc_price'] = $this->input->post('opc_price');
		$data['ppc_price'] = $this->input->post('ppc_price');

        return $data;
   }
}