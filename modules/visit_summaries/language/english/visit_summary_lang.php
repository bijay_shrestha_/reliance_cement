<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['agent_id'] = 'Agent Id';
$lang['date_en'] = 'Date En';
$lang['date_np'] = 'Date Np';
$lang['latitude'] = 'Latitude';
$lang['longitude'] = 'Longitude';
$lang['city'] = 'City';
$lang['province'] = 'Province';
$lang['nepali_month_id'] = 'Nepali Month Id';
$lang['summary'] = 'Summary';
$lang['opc_price'] = 'Opc Price';
$lang['ppc_price'] = 'Ppc Price';

$lang['visit_summaries']='Visit Summaries';