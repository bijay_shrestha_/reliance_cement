<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Visit_summary_model extends MY_Model
{

    protected $_table = 'field_visit_summary';

    protected $blamable = TRUE;

}