<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('visit_summaries'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('visit_summaries'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridVisit_summaryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Visit_summarymodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridVisit_summaryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridVisit_summary"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Visit_summarymodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('visit_summaries'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-visit_summaries', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "visit_summaries_id"/>
					
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_by'><?php echo lang('created_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='created_by' class=' form-control' name='created_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_by'><?php echo lang('updated_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='updated_by' class=' form-control' name='updated_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_by'><?php echo lang('deleted_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='deleted_by' class=' form-control' name='deleted_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_at'><?php echo lang('created_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='created_at' class=' form-control' name='created_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_at'><?php echo lang('updated_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='updated_at' class=' form-control' name='updated_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_at'><?php echo lang('deleted_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='deleted_at' class=' form-control' name='deleted_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='agent_id'><?php echo lang('agent_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='agent_id' class=' form-control' name='agent_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='date_en'><?php echo lang('date_en')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='date_en' class=' form-control' name='date_en'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='date_np'><?php echo lang('date_np')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='date_np' class=' form-control' name='date_np'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='latitude'><?php echo lang('latitude')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='latitude' class=' form-control' name='latitude'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='longitude'><?php echo lang('longitude')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='longitude' class=' form-control' name='longitude'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='city'><?php echo lang('city')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='city' class=' form-control' name='city'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='province'><?php echo lang('province')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='province' class=' form-control' name='province'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_month_id'><?php echo lang('nepali_month_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='nepali_month_id' class=' form-control' name='nepali_month_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='summary'><?php echo lang('summary')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='summary' class=' form-control' name='summary'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='opc_price'><?php echo lang('opc_price')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='opc_price' class=' form-control' name='opc_price'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='ppc_price'><?php echo lang('ppc_price')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='ppc_price' class=' form-control' name='ppc_price'></div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxVisit_summarySubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var visit_summariesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'date_en', type: 'date' },
			{ name: 'date_np', type: 'string' },
			{ name: 'latitude', type: 'string' },
			{ name: 'longitude', type: 'string' },
			{ name: 'city', type: 'number' },
			{ name: 'province', type: 'number' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'summary', type: 'string' },
			{ name: 'opc_price', type: 'string' },
			{ name: 'ppc_price', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/visit_summaries/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	visit_summariesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridVisit_summary").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridVisit_summary").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridVisit_summary").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: visit_summariesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridVisit_summaryToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editVisit_summaryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date_en"); ?>',datafield: 'date_en',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("date_np"); ?>',datafield: 'date_np',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("latitude"); ?>',datafield: 'latitude',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("longitude"); ?>',datafield: 'longitude',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("city"); ?>',datafield: 'city',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("province"); ?>',datafield: 'province',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("summary"); ?>',datafield: 'summary',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("opc_price"); ?>',datafield: 'opc_price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("ppc_price"); ?>',datafield: 'ppc_price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridVisit_summary").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridVisit_summaryFilterClear', function () { 
		$('#jqxGridVisit_summary').jqxGrid('clearfilters');
	});

	
    /*$('#form-visit_summaries').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#agent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#agent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#date_np', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#date_np').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#latitude', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#latitude').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#longitude', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#longitude').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#city', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#city').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#province', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#province').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nepali_month_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_month_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#summary', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#summary').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#opc_price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#opc_price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#ppc_price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#ppc_price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxVisit_summarySubmitButton").on('click', function () {
    	saveVisit_summaryRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveVisit_summaryRecord();
                }
            };
        $('#form-visit_summaries').jqxValidator('validate', validationResult);
        */
    });
});

	function editVisit_summaryRecord(index){
		var row =  $("#jqxGridVisit_summary").jqxGrid('getrowdata', index);
		if (row) {
			$('#visit_summaries_id').val(row.id);
			$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#agent_id').jqxNumberInput('val', row.agent_id);
		$('#date_en').jqxDateTimeInput('setDate', row.date_en);
		$('#date_np').val(row.date_np);
		$('#latitude').val(row.latitude);
		$('#longitude').val(row.longitude);
		$('#city').jqxNumberInput('val', row.city);
		$('#province').jqxNumberInput('val', row.province);
		$('#nepali_month_id').jqxNumberInput('val', row.nepali_month_id);
		$('#summary').val(row.summary);
		$('#opc_price').val(row.opc_price);
		$('#ppc_price').val(row.ppc_price);
		
			$('#Visit_summarymodal').modal('show');
		}
	}

	function saveVisit_summaryRecord(){
		var data = $("#form-visit_summaries").serialize();

		$('#Visit_summarymodal').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/visit_summaries/save"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_visit_summaries();
					$('#jqxGridVisit_summary').jqxGrid('updatebounddata');
					$('#Visit_summarymodal').modal('hide');
				}
				$('#Visit_summarymodal').unblock();
			}
		});
	}

	function reset_form_visit_summaries(){
		$('#visit_summaries_id').val('');
		$('#form-visit_summaries')[0].reset();
	}
</script>