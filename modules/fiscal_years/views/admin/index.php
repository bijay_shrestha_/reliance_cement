<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('fiscal_years'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('fiscal_years'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridFiscal_yearToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Fiscal_yearmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridFiscal_yearFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridFiscal_year"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Fiscal_yearmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('fiscal_years'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-fiscal_years', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "fiscal_years_id"/>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='english_start_date'><?php echo lang('english_start_date')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='english_start_date' class=' form-control' name='english_start_date'></div></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='english_end_date'><?php echo lang('english_end_date')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='english_end_date' class=' form-control' name='english_end_date'></div></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxFiscal_yearSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){
		$("#english_start_date").jqxDateTimeInput({ formatString: formatString_yyyy_MM_dd, showTimeButton: false, width: '300px', height: '25px' });
		$("#english_end_date").jqxDateTimeInput({ formatString: formatString_yyyy_MM_dd, showTimeButton: false, width: '300px', height: '25px' });

		var fiscal_yearsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'nepali_start_date', type: 'string' },
			{ name: 'nepali_end_date', type: 'string' },
			{ name: 'english_start_date', type: 'date' },
			{ name: 'english_end_date', type: 'date' },
			{ name: 'active', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/Fiscal_years/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	fiscal_yearsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridFiscal_year").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridFiscal_year").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridFiscal_year").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: fiscal_yearsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridFiscal_yearToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editFiscal_yearRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("english_start_date"); ?>',datafield: 'english_start_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("english_end_date"); ?>',datafield: 'english_end_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("nepali_start_date"); ?>',datafield: 'nepali_start_date',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("nepali_end_date"); ?>',datafield: 'nepali_end_date',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("active"); ?>',datafield: 'active', width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'checkbox', filtertype: 'bool' },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridFiscal_year").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridFiscal_yearFilterClear', function () { 
		$('#jqxGridFiscal_year').jqxGrid('clearfilters');
	});

	
    $('#form-fiscal_years').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			
			{ input: '#english_start_date', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#english_start_date').jqxDateTimeInput('value');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#english_end_date', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#english_end_date').jqxDateTimeInput('value');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
 		]
    });

    $("#jqxFiscal_yearSubmitButton").on('click', function () {
    	// saveFiscal_yearRecord();
        
        var validationResult = function (isValid) {
                if (isValid) {
                   saveFiscal_yearRecord();
                }
            };
        $('#form-fiscal_years').jqxValidator('validate', validationResult);
        
    });
});

function editFiscal_yearRecord(index){
	var row =  $("#jqxGridFiscal_year").jqxGrid('getrowdata', index);
	if (row) {
		$('#fiscal_years_id').val(row.id);
		$('#nepali_start_date').val(row.nepali_start_date);
		$('#nepali_end_date').val(row.nepali_end_date);
		$('#english_start_date').jqxDateTimeInput('setDate', row.english_start_date);
		$('#english_end_date').jqxDateTimeInput('setDate', row.english_end_date);
		$('#active').jqxNumberInput('val', row.active);
		
		$('#Fiscal_yearmodal').modal('show');
	}
}

function saveFiscal_yearRecord(){
	var data = $("#form-fiscal_years").serialize();

	$('#Fiscal_yearmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Fiscal_years/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_fiscal_years();
				$('#jqxGridFiscal_year').jqxGrid('updatebounddata');
				$('#Fiscal_yearmodal').modal('hide');
			}
			$('#Fiscal_yearmodal').unblock();
		}
	});
}

function reset_form_fiscal_years(){
	$('#fiscal_years_id').val('');
	$('#form-fiscal_years')[0].reset();
}
</script>