<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{

    // var $user_id;

    public function __construct()
    {
    	parent::__construct();

        $this->user_id  = getallheaders()['Userid'];
        $this->load->helper('project');
        $this->load->model('sales/sale_model');

        if(!empty(getallheaders()['Accesstoken']))
        {
            $access_token = getallheaders()['Accesstoken'];
            $this->db->where('id',$this->user_id);
            $this->db->where('access_token',$access_token);
            $check_token = $this->db->get('aauth_users')->result_array();

            if(empty($check_token))
            {
                echo json_encode('Access Denied');
                exit;
            }
        }
        else
        {
            echo json_encode('Access Token not sent on header');
            exit();
        }

    }

    public function overview_get()
    {
        $this->db->where('user_id',$this->user_id);
        $agent = $this->db->get('mst_agents')->row();

        $this->db->where('id',$agent->id);
        $overview = $this->db->get('view_report_overview')->row();

        echo json_encode($overview);
    }

    public function current_month_sales_get()
    {
        $rows['current_month_target'] = $this->current_month_target($this->user_id);
        $rows['current_opc_sales'] = $this->monthly_summary_report($this->user_id,'OPC');
        $rows['current_psc_sales'] = $this->monthly_summary_report($this->user_id,'PSC');
        $rows['current_total_sales'] = $this->monthly_summary_report($this->user_id,NULL);
        $rows['difference'] = ($rows['current_month_target'] - $rows['current_total_sales']);
        $rows['previous_opc_sales'] = $this->previous_month_sales($this->user_id,'OPC');
        $rows['previous_psc_sales'] = $this->previous_month_sales($this->user_id,'PSC');
        $rows['previous_total_sales'] = $this->previous_month_sales($this->user_id,NULL);

        echo json_encode($rows);
    }
    
    // for yearly sales report
    public function yearly_sales_get()
    {
        $rows['target'] = $this->target($this->user_id);
        $rows['sales'] = $this->summary_report($this->user_id,'OPC','quantity');
        $rows['sales'] = $this->summary_report($this->user_id,'PSC','quantity');
        $rows['sales'] = $this->summary_report($this->user_id,NULL, 'quantity');
        $rows['difference'] = ($rows['current_month_target'] - $rows['current_total_sales']);

        echo json_encode($rows);
    }

    public function agent_post()
    {
	    //TODO
    }

    public function agent_delete()
    {
	    //TODO
    }

    private function monthly_summary_report($user_id,$brand_type,$requested_value = 'gross_total')
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$user_id);
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        $whereCondition[] = " ( agent_id = " . $agent_id. ")";

        if ($brand_type)
        {
            $whereCondition[] = "brand_type = '{$brand_type}' ";    
        }

        $current_date = explode('-',get_nepali_date(date('Y-m-d'),'nep'));

        $whereCondition[] = " ( nepali_month_id = " . $current_date[1]. " AND is_published = 1)";

        $fields = array();

        $fields[] = 'COALESCE(SUM($requested_value),0) AS total';

        $CI->db->select($fields);

        $CI->db->from('view_sales');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
    }
    
    private function summary_report($user_id,$brand_type)
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$user_id);
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        $whereCondition[] = " ( agent_id = " . $agent_id. ")";

        if ($brand_type)
        {
            $whereCondition[] = "brand_type = '{$brand_type}' ";    
        }

        $fields = array();

        $fields[] = 'COALESCE(SUM(gross_total),0) AS total';

        $CI->db->select($fields);

        $CI->db->from('view_sales');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
    }

    private function previous_month_sales($user_id,$brand_type)
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$user_id);
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        $whereCondition[] = " ( agent_id = " . $agent_id. ")";

        if ($brand_type)
        {
            $whereCondition[] = "brand_type = '{$brand_type}' ";    
        }

        $current_date = explode('-',get_nepali_date(date('Y-m-d'),'nep'));

        if($current_date[1] == 1)
        {
            $previous_month = 12;   
        }
        else
        {
            $previous_month = $current_date[1] - 1;
        }


        $whereCondition[] = " ( nepali_month_id = " . $previous_month. " AND is_published = 1 )";

        $fields = array();

        $fields[] = 'COALESCE(SUM(gross_total),0) AS total';

        $CI->db->select($fields);

        $CI->db->from('view_sales');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
    }

    private function current_month_target()
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$this->user_id);
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        $whereCondition[] = " ( agent_id = " . $agent_id. ")";

        $current_date = explode('-',get_nepali_date(date('Y-m-d'),'nep'));

        $whereCondition[] = " ( nepali_month_id = " . $current_date[1]. ")";

        $fields = array();

        $fields[] = 'amount';

        $CI->db->select($fields);

        $CI->db->from('mst_monthly_target');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['amount'];
    }
    
    private function target()
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$this->user_id);
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        $whereCondition[] = " ( agent_id = " . $agent_id. ")";

        $fields = array();

        $fields[] = 'SUM(amount) as amount';

        $CI->db->select($fields);

        $CI->db->from('mst_monthly_target');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['amount'];
    }

    public function partywise_report_get()
    {
        $this->sale_model->_table = "view_partywise_sales";

        $this->db->where('user_id',$this->user_id);
        $agent = $this->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $where = "(agent_id = {$agent_id})";
        $this->db->where($where);
        $rows=$this->sale_model->findAll();

        echo json_encode($rows);
        exit;
    }
}