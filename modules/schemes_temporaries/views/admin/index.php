<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('schemes_temporaries'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('schemes_temporaries'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSchemes_temporaryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Schemes_temporarymodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSchemes_temporaryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridSchemes_temporary"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Schemes_temporarymodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('schemes_temporaries'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-schemes_temporaries', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "schemes_temporaries_id"/>
					
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_by'><?php echo lang('created_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='created_by' class=' form-control' name='created_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_by'><?php echo lang('updated_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='updated_by' class=' form-control' name='updated_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_by'><?php echo lang('deleted_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='deleted_by' class=' form-control' name='deleted_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_at'><?php echo lang('created_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='created_at' class=' form-control' name='created_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_at'><?php echo lang('updated_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='updated_at' class=' form-control' name='updated_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_at'><?php echo lang('deleted_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='deleted_at' class=' form-control' name='deleted_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='fiscal_year_id'><?php echo lang('fiscal_year_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='fiscal_year_id' class=' form-control' name='fiscal_year_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='agent_id'><?php echo lang('agent_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='agent_id' class=' form-control' name='agent_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='bag_count'><?php echo lang('bag_count')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='bag_count' class=' form-control' name='bag_count'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='price'><?php echo lang('price')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='price' class=' form-control' name='price'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='amount'><?php echo lang('amount')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='amount' class=' form-control' name='amount'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='is_processed'><?php echo lang('is_processed')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='is_processed' class=' form-control' name='is_processed'></div></div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxSchemes_temporarySubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var schemes_temporariesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'fiscal_year_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'bag_count', type: 'number' },
			{ name: 'price', type: 'string' },
			{ name: 'amount', type: 'string' },
			{ name: 'is_processed', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/schemes_temporaries/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	schemes_temporariesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSchemes_temporary").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSchemes_temporary").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSchemes_temporary").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: schemes_temporariesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSchemes_temporaryToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editSchemes_temporaryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("fiscal_year_id"); ?>',datafield: 'fiscal_year_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("bag_count"); ?>',datafield: 'bag_count',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("is_processed"); ?>',datafield: 'is_processed',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSchemes_temporary").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSchemes_temporaryFilterClear', function () { 
		$('#jqxGridSchemes_temporary').jqxGrid('clearfilters');
	});

	
    /*$('#form-schemes_temporaries').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#fiscal_year_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#fiscal_year_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#agent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#agent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#bag_count', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#bag_count').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#amount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#amount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#is_processed', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#is_processed').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxSchemes_temporarySubmitButton").on('click', function () {
    	saveSchemes_temporaryRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveSchemes_temporaryRecord();
                }
            };
        $('#form-schemes_temporaries').jqxValidator('validate', validationResult);
        */
    });
});

	function editSchemes_temporaryRecord(index){
		var row =  $("#jqxGridSchemes_temporary").jqxGrid('getrowdata', index);
		if (row) {
			$('#schemes_temporaries_id').val(row.id);
			$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#fiscal_year_id').jqxNumberInput('val', row.fiscal_year_id);
		$('#agent_id').jqxNumberInput('val', row.agent_id);
		$('#bag_count').jqxNumberInput('val', row.bag_count);
		$('#price').val(row.price);
		$('#amount').val(row.amount);
		$('#is_processed').jqxNumberInput('val', row.is_processed);
		
			$('#Schemes_temporarymodal').modal('show');
		}
	}

	function saveSchemes_temporaryRecord(){
		var data = $("#form-schemes_temporaries").serialize();

		$('#Schemes_temporarymodal').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/schemes_temporaries/save"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_schemes_temporaries();
					$('#jqxGridSchemes_temporary').jqxGrid('updatebounddata');
					$('#Schemes_temporarymodal').modal('hide');
				}
				$('#Schemes_temporarymodal').unblock();
			}
		});
	}

	function reset_form_schemes_temporaries(){
		$('#schemes_temporaries_id').val('');
		$('#form-schemes_temporaries')[0].reset();
	}
</script>