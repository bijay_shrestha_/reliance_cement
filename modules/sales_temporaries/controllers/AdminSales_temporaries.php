<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Sales_temporaries
 *
 * Extends the Project_Controller class
 * 
 */

class AdminSales_temporaries extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Sales Temporaries');

        $this->load->model('sales_temporaries/sales_temporary_model');
        $this->lang->load('sales_temporaries/sales_temporary');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('sales_temporaries');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'sales_temporaries';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->sales_temporary_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->sales_temporary_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->sales_temporary_model->insert($data);
        }
        else
        {
            $success=$this->sales_temporary_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['fiscal_year_id'] = $this->input->post('fiscal_year_id');
		$data['invoice_no'] = $this->input->post('invoice_no');
		$data['invoice_date'] = $this->input->post('invoice_date');
		$data['invoice_date_np'] = $this->input->post('invoice_date_np');
		$data['nepali_month_id'] = $this->input->post('nepali_month_id');
		$data['agent_id'] = $this->input->post('agent_id');
		$data['party_id'] = $this->input->post('party_id');
		$data['truck_number'] = $this->input->post('truck_number');
		$data['type_id'] = $this->input->post('type_id');
		$data['brand_id'] = $this->input->post('brand_id');
		$data['quantity'] = $this->input->post('quantity');
		$data['rate'] = $this->input->post('rate');
		$data['discount'] = $this->input->post('discount');
		$data['gross_total'] = $this->input->post('gross_total');
		$data['is_processed'] = $this->input->post('is_processed');

        return $data;
   }
}