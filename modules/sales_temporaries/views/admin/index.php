<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('sales_temporaries'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('sales_temporaries'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridSales_temporaryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Sales_temporarymodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridSales_temporaryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridSales_temporary"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Sales_temporarymodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('sales_temporaries'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-sales_temporaries', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "sales_temporaries_id"/>
					
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_by'><?php echo lang('created_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='created_by' class=' form-control' name='created_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_by'><?php echo lang('updated_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='updated_by' class=' form-control' name='updated_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_by'><?php echo lang('deleted_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='deleted_by' class=' form-control' name='deleted_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_at'><?php echo lang('created_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='created_at' class=' form-control' name='created_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_at'><?php echo lang('updated_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='updated_at' class=' form-control' name='updated_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_at'><?php echo lang('deleted_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='deleted_at' class=' form-control' name='deleted_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='fiscal_year_id'><?php echo lang('fiscal_year_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='fiscal_year_id' class=' form-control' name='fiscal_year_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='invoice_no'><?php echo lang('invoice_no')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='invoice_no' class=' form-control' name='invoice_no'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='invoice_date'><?php echo lang('invoice_date')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='invoice_date' class=' form-control' name='invoice_date'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='invoice_date_np'><?php echo lang('invoice_date_np')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='invoice_date_np' class=' form-control' name='invoice_date_np'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_month_id'><?php echo lang('nepali_month_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='nepali_month_id' class=' form-control' name='nepali_month_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='agent_id'><?php echo lang('agent_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='agent_id' class=' form-control' name='agent_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='party_id'><?php echo lang('party_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='party_id' class=' form-control' name='party_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='truck_number'><?php echo lang('truck_number')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='truck_number' class=' form-control' name='truck_number'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='type_id'><?php echo lang('type_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='type_id' class=' form-control' name='type_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='brand_id'><?php echo lang('brand_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='brand_id' class=' form-control' name='brand_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='quantity'><?php echo lang('quantity')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='quantity' class=' form-control' name='quantity'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='rate'><?php echo lang('rate')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='rate' class=' form-control' name='rate'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='discount'><?php echo lang('discount')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='discount' class=' form-control' name='discount'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='gross_total'><?php echo lang('gross_total')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='gross_total' class=' form-control' name='gross_total'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='is_processed'><?php echo lang('is_processed')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='is_processed' class=' form-control' name='is_processed'></div></div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxSales_temporarySubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var sales_temporariesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'fiscal_year_id', type: 'number' },
			{ name: 'invoice_no', type: 'string' },
			{ name: 'invoice_date', type: 'date' },
			{ name: 'invoice_date_np', type: 'string' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'party_id', type: 'number' },
			{ name: 'truck_number', type: 'string' },
			{ name: 'type_id', type: 'number' },
			{ name: 'brand_id', type: 'number' },
			{ name: 'quantity', type: 'number' },
			{ name: 'rate', type: 'string' },
			{ name: 'discount', type: 'string' },
			{ name: 'gross_total', type: 'string' },
			{ name: 'is_processed', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/sales_temporaries/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	sales_temporariesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSales_temporary").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSales_temporary").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSales_temporary").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: sales_temporariesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSales_temporaryToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editSales_temporaryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("fiscal_year_id"); ?>',datafield: 'fiscal_year_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("invoice_no"); ?>',datafield: 'invoice_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("invoice_date"); ?>',datafield: 'invoice_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("invoice_date_np"); ?>',datafield: 'invoice_date_np',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("party_id"); ?>',datafield: 'party_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("truck_number"); ?>',datafield: 'truck_number',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("type_id"); ?>',datafield: 'type_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("brand_id"); ?>',datafield: 'brand_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("quantity"); ?>',datafield: 'quantity',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("rate"); ?>',datafield: 'rate',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("discount"); ?>',datafield: 'discount',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("gross_total"); ?>',datafield: 'gross_total',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("is_processed"); ?>',datafield: 'is_processed',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSales_temporary").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSales_temporaryFilterClear', function () { 
		$('#jqxGridSales_temporary').jqxGrid('clearfilters');
	});

	
    /*$('#form-sales_temporaries').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#fiscal_year_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#fiscal_year_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#invoice_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#invoice_no').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#invoice_date_np', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#invoice_date_np').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#nepali_month_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_month_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#agent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#agent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#party_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#party_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#truck_number', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#truck_number').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#type_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#type_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#brand_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#brand_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#quantity', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#quantity').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#rate', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#rate').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#discount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#discount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#gross_total', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#gross_total').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#is_processed', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#is_processed').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxSales_temporarySubmitButton").on('click', function () {
    	saveSales_temporaryRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveSales_temporaryRecord();
                }
            };
        $('#form-sales_temporaries').jqxValidator('validate', validationResult);
        */
    });
});

	function editSales_temporaryRecord(index){
		var row =  $("#jqxGridSales_temporary").jqxGrid('getrowdata', index);
		if (row) {
			$('#sales_temporaries_id').val(row.id);
			$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#fiscal_year_id').jqxNumberInput('val', row.fiscal_year_id);
		$('#invoice_no').val(row.invoice_no);
		$('#invoice_date').jqxDateTimeInput('setDate', row.invoice_date);
		$('#invoice_date_np').val(row.invoice_date_np);
		$('#nepali_month_id').jqxNumberInput('val', row.nepali_month_id);
		$('#agent_id').jqxNumberInput('val', row.agent_id);
		$('#party_id').jqxNumberInput('val', row.party_id);
		$('#truck_number').val(row.truck_number);
		$('#type_id').jqxNumberInput('val', row.type_id);
		$('#brand_id').jqxNumberInput('val', row.brand_id);
		$('#quantity').jqxNumberInput('val', row.quantity);
		$('#rate').val(row.rate);
		$('#discount').val(row.discount);
		$('#gross_total').val(row.gross_total);
		$('#is_processed').jqxNumberInput('val', row.is_processed);
		
			$('#Sales_temporarymodal').modal('show');
		}
	}

	function saveSales_temporaryRecord(){
		var data = $("#form-sales_temporaries").serialize();

		$('#Sales_temporarymodal').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sales_temporaries/save"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_sales_temporaries();
					$('#jqxGridSales_temporary').jqxGrid('updatebounddata');
					$('#Sales_temporarymodal').modal('hide');
				}
				$('#Sales_temporarymodal').unblock();
			}
		});
	}

	function reset_form_sales_temporaries(){
		$('#sales_temporaries_id').val('');
		$('#form-sales_temporaries')[0].reset();
	}
</script>