<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['fiscal_year_id'] = 'Fiscal Year Id';
$lang['invoice_no'] = 'Invoice No';
$lang['invoice_date'] = 'Invoice Date';
$lang['invoice_date_np'] = 'Invoice Date Np';
$lang['nepali_month_id'] = 'Nepali Month Id';
$lang['agent_id'] = 'Agent Id';
$lang['party_id'] = 'Party Id';
$lang['truck_number'] = 'Truck Number';
$lang['type_id'] = 'Type Id';
$lang['brand_id'] = 'Brand Id';
$lang['quantity'] = 'Quantity';
$lang['rate'] = 'Rate';
$lang['discount'] = 'Discount';
$lang['gross_total'] = 'Gross Total';
$lang['is_processed'] = 'Is Processed';

$lang['sales_temporaries']='Sales Temporaries';