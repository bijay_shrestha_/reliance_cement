
application/x-httpd-php AdminReports.php ( PHP script text )
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Payments
 *
 * Extends the Project_Controller class
 * 
 */

class AdminReports extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		//control('Payments');

		$this->load->model('payments/payment_model');
		$this->load->model('agents/agent_model');
		$this->load->model('parties/party_model');
		$this->load->model('nepali_months/nepali_month_model');
		$this->load->model('payment_temporaries/payment_temporary_model');
		$this->load->library('reports/report');
		
		$this->lang->load('reports/report');
	}

	public function index()
	{
		# code...
	}

	public function monthly_sales()
	{
		$view_page = 'monthly_sales';

		$data['header'] = lang('reports');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'reports';
		$this->load->view($this->_container,$data);
	}

	public function monthly_sales_json()
	{
		/*search_params();
		
		$total=$this->report->monthly_sales_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->party_model->findAll();*/

		$data = $this->report->monthly_sales();
		
		echo json_encode($data);
		// echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	
	public function sales_by_party()
	{
		$view_page = 'party_month_sales';

		$data['header'] = lang('reports');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'reports';
		$this->load->view($this->_container,$data);
	}

	public function sales_by_party_json()
	{
		$where = array();
		if(is_agent()){
			$where = array(
				'user_id' => $this->_user_id,
			);
			$agent = $this->report_model->find($where);
			$where = array(
				'agent_id' => $agent->id,
			);
		}

		if(is_sales_person()){
			$where = array(
				'sales_person_id'=>$this->_user_id,
			);
		}

		search_params();
		$data['total'] = $this->report->party_month_sales($where,'total');

		search_params();
		paging('id');

		$data['rows'] = $this->report->party_month_sales($where);

		echo json_encode($data);

	}

	public function getAgentsJson()
	{
		$this->report_model->_table = 'view_party_sales_detail';

		$where = array();

		if(is_agent()){
			$where = array(
				'user_id' => $this->_user_id,
			);
			$agent = $this->report_model->find($where);
			$where = array(
				'agent_id' => $agent->id,
			);
		}

		if(is_sales_person()){
			$where = array(
				'sales_person_id'=>$this->_user_id,
			);
		}
		$where['is_published'] = 1;
		$this->db->group_by('agent_id');

		search_params();
		$this->db->where($where);
		$data['total'] = $this->report_model->find_count();

		$this->db->group_by('agent_id');
		$fields = 'id,agent_id, agent_name, SUM(quantity) as total';
		search_params();
		paging('id');
		$data['rows'] = $this->report_model->findAll($where,$fields);
// 		echo $this->db->last_query().'<br>';
// echo '<pre>';
// 		print_r($data);

		echo json_encode($data);

	}

	public function partyMonthJson()
	{
		$where = array();

		if(is_agent()){
			$where = array(
				'user_id' => $this->_user_id,
			);
			$agent = $this->report_model->find($where);
			$where = array(
				'agent_id' => $agent->id,
			);
		}elseif(is_sales_person()){
			$where = array(
				'sales_person_id'=>$this->_user_id,
			);
		}

		search_params();
		$data['total'] = $this->report->party_month_sales($where,1);

		search_params();
		paging('id');
		$data['rows'] = $this->report->party_month_sales($where);

		echo json_encode($data);

	}

	public function brandtype_sales()
	{
		$view_page = 'brandtype_sales';

		$data['header'] = lang('reports');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'reports';
		$this->load->view($this->_container,$data);
	}

	public function brandtypeJson()
	{
		$where = array();

		if(is_agent()){
			$where = array(
				'user_id' => $this->_user_id,
			);
			$agent = $this->agent_model->find($where);
			$where = array(
				'agent_id' => $agent->id,
			);
		}elseif(is_sales_person()){
			$where = array(
				'sales_person_id'=>$this->_user_id,
			);
		}		

		search_params();
		paging('agent_id','asc');
		$data['rows'] = $this->report->sales_brandtype($where);

		echo json_encode($data);
	}

	public function target_sales_monthly()
	{
		$view_page = 'target_sales_monthly';

		$data['header'] = lang('reports');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'reports';
		$this->load->view($this->_container,$data);
	}

	public function target_salesmonthly_Json()
	{
		$where = array();

		if(is_agent()){
			$where = array(
				'user_id' => $this->_user_id,
			);
		}elseif(is_sales_person()){
			$where = array(
				'sales_person_id'=>$this->_user_id,
			);
		}		

		search_params();
		paging('nepali_month_rank','asc');
		$data['rows'] = $this->report->monthly_sales_target($where);

		echo json_encode($data);
	}

	public function opening_closing_report()
	{
		$view_page = 'opening_closing';

		$data['header'] = lang('reports');
		$data['page'] = $this->config->item('template_admin') . $view_page;
		$data['module'] = 'reports';
		$this->load->view($this->_container,$data);
	}

	public function opening_closingJson()
	{
		$where = array();

		if(is_agent()){
			$where = array(
				'user_id' => $this->_user_id,
			);
		}elseif(is_sales_person()){
			$where = array(
				'sales_person_id'=>$this->_user_id,
			);
		}		

		search_params();
		paging('agent_id','asc');
		$data['rows'] = $this->report->opening_closing($where);

		echo json_encode($data);
	}
}