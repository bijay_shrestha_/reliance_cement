<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
    var $user_id;

    public function __construct()
    {
        parent::__construct();

        // if(!empty(getallheaders()['Accesstoken']))
        // {
        //     $access_token = getallheaders()['Accesstoken'];        
        //     $this->db->where('access_token',$access_token);
        //     $check_token = $this->db->get('aauth_users')->result_array();

        //     if(empty($check_token))
        //     {
        //         echo json_encode('Access Denied');
        //         exit;
        //     }
        // }
        // else
        // {
        //     echo json_encode('Access Token not sent on header');
        //     exit();
        // }

        // $this->load->model('party/party_model');
        $this->load->library('reports/report');
            $this->load->model('agents/agent_model');

        $this->user_id  = getallheaders()['Userid'];
    }

    public function report_get()
    {
        //TODO
    }

    public function report_post()
    {
        //TODO
    }

    public function report_delet()
    {
        //TODO
    }

    public function monthly_sales_get($type = NULL)
    {
        if(!$type){
            $this->db->where('user_id',getallheaders()['Userid']);
            $agent = $this->db->get('mst_agents')->row();

            $agent_id = $agent->id;
        }else{
            $agent_id = getallheaders()['Userid'];
        };

        $rows = $this->report->monthly_sales($agent_id, $type);
        
        $total_opc = 0;
        $total_ppc = 0;
        $total = 0;
        foreach ($rows['rows'] as $key => $value) {
            $total_opc += $value->OPC;
            $total_ppc += $value->PPC;
            $total += $value->total_bags;
        };

        $rows['total'] = array(
            'total_opc' => $total_opc,
            'total_ppc' => $total_ppc,
            'total' => $total,
        );
        if($total >0){
            $rows['percentage'] = array(
                'opc' => $total_opc/$total * 100,
                'ppc' => $total_ppc/$total * 100,
                'total' => $total/$total * 100,
            );
        }else{
            $rows['percentage'] = array(
                'opc' => 0,
                'ppc' => 0,
                'total' => 0,
            );
        }

        echo json_encode($rows);
    }
    
        public function getAgents_get($type = 'admin')
    {
        $where = array();
        if($type == 'sales_person'){
            $where = array(
                'agent_id' => getallheaders()['Userid'],
            );
        }
        $where['is_published'] = 1;
        $this->db->group_by('agent_id');
        $this->db->where($where);
        $fields = 'id,agent_id, agent_name, SUM(quantity) as total';
        $this->db->select($fields);

        $data['agents'] = $this->db->get('view_party_sales_detail')->result();
        $data['total'] = count($data['agents']);

        echo json_encode($data);

    }

    public function party_month_sales_get($group = 'admin')
    {
       $where = array();

       if($group == 'agent'){
        $where = array(
            'user_id' =>  $this->user_id ,
        );
        $agent = $this->agent_model->find($where);
        $where = array(
            'agent_id' => $agent->id,
        );
    }elseif($group == 'sales_person'){
        $where = array(
            'sales_person_id'=> $this->user_id ,
        );
    }  

    $data['rows'] = $this->report->party_month_sales($where);
    $data['count'] = count($data['rows']);

    echo json_encode($data);
}
    
    public function brandtype_sales_get($group = 'admin')
{
    $where = array();

    if($group == 'agent'){
        $where = array(
            'user_id' =>  $this->user_id ,
        );
        $agent = $this->agent_model->find($where);
        $where = array(
            'agent_id' => $agent->id
        );
    }elseif($group == 'sales_person'){
        $where = array(
            'sales_person_id'=> $this->user_id ,
        );
    }       

    $data['rows'] = $this->report->sales_brandtype($where);

    $total_opc = 0;
    $total_ppc = 0;
    $total = 0;
    foreach ($data['rows'] as $key => $value) {
        $total_opc += $value->total_opc;
        $total_ppc += $value->total_psc;
        $total += $value->total_sales;
    };

    $data['total'] = array(
        'total_opc' => $total_opc,
        'total_ppc' => $total_ppc,
        'total' => $total,
    );

    echo json_encode($data);
}


}