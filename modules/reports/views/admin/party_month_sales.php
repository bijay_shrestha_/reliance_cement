<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('monthly_sales'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('monthly_sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id="jqxGridParty"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script language="javascript" type="text/javascript">

	$(function(){

		var url = "<?php echo site_url('admin/reports/getAgentsJson')?>";
        var source =
        {
            datatype: "json",
	        datafields: [
	            { name: 'id', type: 'number' },
	            { name: 'agent_name', type: 'string' },
	            { name: 'agent_id', type: 'number' },
	            { name: 'total', type: 'number' },
	            
	        ],
	        url: '<?php echo site_url("admin/reports/getAgentsJson"); ?>',
	        pagesize: defaultPageSize,
	        root: 'rows',
	        id : 'id',
	        cache: true,
	        pager: function (pagenum, pagesize, oldpagenum) {
	            //callback called when a page or page size is changed.
	        },
	        beforeprocessing: function (data) {
	            source.totalrecords = data.total;
	        },
	        // update the grid and send a request to the server.
	        filter: function () {
	            $("#jqxGridBrand").jqxGrid('updatebounddata', 'filter');
	        },
	        // update the grid and send a request to the server.
	        sort: function () {
	            $("#jqxGridBrand").jqxGrid('updatebounddata', 'sort');
	        },
	        processdata: function(data) {
	        }

        };
        var agentAdapter = new $.jqx.dataAdapter(source);

        var partydetailsurl = "<?php echo site_url('admin/reports/partyMonthJson')?>";
        var partysSource =
        {
            datafields: [
                { name: 'id', type: 'number' },
				{ name: 'sales_person', type: 'string' },
				{ name: 'agent_name', type: 'string' },
				{ name: 'agent_id', type: 'string' },
				{ name: 'party_name', type: 'string' },
				{ name: 'Shrawan', type: 'number' },
				{ name: 'Bhadra', type: 'number' },
				{ name: 'Ashwin', type: 'number' },
				{ name: 'Karthik', type: 'number' },
				{ name: 'Mangshir', type: 'number' },
				{ name: 'Poush', type: 'number' },
				{ name: 'Magh', type: 'number' },
				{ name: 'Falgun', type: 'number' },
				{ name: 'Chaitra', type: 'number' },
				{ name: 'Baishak', type: 'number' },
				{ name: 'Jestha', type: 'number' },
				{ name: 'Ashad', type: 'number' },
				{ name: 'total', type: 'number' },
            ],
            // root: "Partys",
            // record: "Party",
            datatype: "json",
            url: partydetailsurl,
            async: false
        };

        var partysDataAdapter = new $.jqx.dataAdapter(partysSource, { autoBind: true });
        partys = partysDataAdapter.records;
        var nestedGrids = new Array();
        // create nested grid.
        var initrowdetails = function (index, parentElement, gridElement, record) {
        	console.log(record);

            var id = record.uid.toString();
            var grid = $($(parentElement).children()[0]);
            nestedGrids[index] = grid;
            var filtergroup = new $.jqx.filter();
            var filter_or_operator = 1;
            var filtervalue = record.id;
            var filtercondition = 'equal';
            var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
            // fill the partys depending on the id.
            var partysbyid = [];
            for (var m = 0; m < partys.length; m++) {
                var result = filter.evaluate(partys[m]["agent_id"]);
        		result = partys[m]["agent_id"] == record.agent_id;
                if (result)
                    partysbyid.push(partys[m]);
            }
            var partyssource = { datafields: [
                { name: 'id', type: 'number' },
				{ name: 'sales_person', type: 'string' },
				{ name: 'agent_name', type: 'string' },
				{ name: 'agent_id', type: 'string' },
				{ name: 'party_name', type: 'string' },
				{ name: 'Shrawan', type: 'number' },
				{ name: 'Bhadra', type: 'number' },
				{ name: 'Ashwin', type: 'number' },
				{ name: 'Karthik', type: 'number' },
				{ name: 'Mangshir', type: 'number' },
				{ name: 'Poush', type: 'number' },
				{ name: 'Magh', type: 'number' },
				{ name: 'Falgun', type: 'number' },
				{ name: 'Chaitra', type: 'number' },
				{ name: 'Baishak', type: 'number' },
				{ name: 'Jestha', type: 'number' },
				{ name: 'Ashad', type: 'number' },
				{ name: 'total', type: 'number' },
            ],
                id: 'partyID',
                localdata: partysbyid
            }
            var nestedGridAdapter = new $.jqx.dataAdapter(partyssource);
            if (grid != null) {
                grid.jqxGrid({
                    source: nestedGridAdapter,
                    width: '95%', 
            		height: '95%',
            		sortable: true,
            		showfilterrow: true,
            		filterable: true,
            		columnsresize: true,
            		editable: true,
            		autoshowfiltericon: true,
            		columnsreorder: true,
            		selectionmode: 'multiplecellsadvanced',
            		showstatusbar: true,
            		showaggregates: true,
            		statusbarheight: 25,
                    columns: [
                    	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
                      { text: 'Party', datafield: 'party_name', width: 400 },
                      // { text: 'Ship Address', datafield: 'ShipAddress', width: 200 },
                      { text: 'Shrawan', datafield: 'Shrawan', width: 100, aggregates: ['sum'] },
                      { text: 'Bhadra', datafield: 'Bhadra', width: 100, aggregates: ['sum'] },
                      { text: 'Ashwin', datafield: 'Ashwin', width: 100, aggregates: ['sum'] },
                      { text: 'Karthik', datafield: 'Karthik', width: 100, aggregates: ['sum'] },
                      { text: 'Mangshir', datafield: 'Mangshir', width: 100, aggregates: ['sum'] },
                      { text: 'Poush', datafield: 'Poush', width: 100, aggregates: ['sum'] },
                      { text: 'Magh', datafield: 'Magh', width: 100, aggregates: ['sum'] },
                      { text: 'Falgun', datafield: 'Falgun', width: 100, aggregates: ['sum'] },
                      { text: 'Chaitra', datafield: 'Chaitra', width: 100, aggregates: ['sum'] },
                      { text: 'Baishak', datafield: 'Baishak', width: 100, aggregates: ['sum'] },
                      { text: 'Jestha', datafield: 'Jestha', width: 100, aggregates: ['sum'] },
                      { text: 'Ashad', datafield: 'Ashad', width: 100, aggregates: ['sum']},
                      { text: 'Total', datafield: 'total', width: 100, aggregates: ['sum']},
                   ]
                });
            }
        }
        var renderer = function (row, column, value) {
            return '<span style="margin-left: 4px; margin-top: 9px; float: left;">' + value + '</span>';
        }
        // creage grid
        $("#jqxGridParty").jqxGrid(
        {
        	theme: theme,
	        width: '100%',
	        height: gridHeight,
	        source: source,
	        altrows: true,
	        pageable: true,
	        sortable: true,
	        rowsheight: 30,
	        columnsheight:30,
	        showfilterrow: true,
	        filterable: true,
	        columnsresize: true,
	        autoshowfiltericon: true,
	        columnsreorder: true,
	        selectionmode: 'none',
	        // virtualmode: true,
	        enableanimations: false,
	        pagesizeoptions: pagesizeoptions,
	        showtoolbar: true,
            rowdetails: true,
            initrowdetails: initrowdetails,
            showstatusbar: true,
        	statusbarheight: 50,
        	showaggregates: true,
            rendertoolbar: function (toolbar) {
	            var container = $("<div style='margin: 5px; height:50px'></div>");
	            container.append($('#jqxGridBrandToolbar').html());
	            toolbar.append(container);
	        },
            rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 500, rowdetailshidden: true },
            columns: [
        		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
                  { text: 'Agent', datafield: 'agent_name', width: 'auto', cellsrenderer: renderer },
                  { text: 'Total', datafield: 'total', width: 'auto', cellsrenderer: renderer, aggregates: ['sum'] },
              ]
        });



		$("[data-toggle='offcanvas']").click(function(e) {
			e.preventDefault();
			setTimeout(function() {$("#jqxGridParty").jqxGrid('refresh');}, 500);
		});

		$(document).on('click','#jqxGridPartyFilterClear', function () { 
			$('#jqxGridParty').jqxGrid('clearfilters');
		});

	});




</script>