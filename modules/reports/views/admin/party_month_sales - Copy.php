<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('monthly_sales'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('monthly_sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id="jqxGridParty"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script language="javascript" type="text/javascript">

	$(function(){

		var partiesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'sales_person', type: 'string' },
			{ name: 'agent_name', type: 'string' },
			{ name: 'party_name', type: 'string' },
			{ name: 'Shrawan', type: 'number' },
			{ name: 'Bhadra', type: 'number' },
			{ name: 'Ashwin', type: 'number' },
			{ name: 'Karthik', type: 'number' },
			{ name: 'Mangshir', type: 'number' },
			{ name: 'Poush', type: 'number' },
			{ name: 'Magh', type: 'number' },
			{ name: 'Falgun', type: 'number' },
			{ name: 'Chaitra', type: 'number' },
			{ name: 'Baishak', type: 'number' },
			{ name: 'Jestha', type: 'number' },
			{ name: 'Ashad', type: 'number' },
			],
			url: '<?php echo site_url("admin/reports/sales_by_party_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			// pager: function (pagenum, pagesize, oldpagenum) {
   //      	//callback called when a page or page size is changed.
	  //       },
	        beforeprocessing: function (data) {
	        	partiesDataSource.totalrecords = data.total;
	        },
		    // update the grid and send a request to the server.
		    filter: function () {
		    	$("#jqxGridParty").jqxGrid('updatebounddata', 'filter');
		    },
		    // update the grid and send a request to the server.
		    sort: function () {
		    	$("#jqxGridParty").jqxGrid('updatebounddata', 'sort');
		    },
		    processdata: function(data) {
		    }
		};
	
	$("#jqxGridParty").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: partiesDataSource,
		altrows: true,
		autoheight:true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		autorowheight: true,
		columnsresize: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		showtoolbar: false,
		showstatusbar: true,
		statusbarheight: 50,
		showaggregates: true,
		filterable:true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPartyToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{ text: '<?php echo lang("month"); ?>',datafield: 'party_name',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Shrawan',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Bhadra',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Ashwin',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Karthik',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Mangshir',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Poush',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Magh',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Falgun',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Chaitra',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Baishak',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Jestha',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("month"); ?>',datafield: 'Ashad',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridParty").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPartyFilterClear', function () { 
		$('#jqxGridParty').jqxGrid('clearfilters');
	});

});




</script>