<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('monthly_sales'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('monthly_sales'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id="jqxGridParty"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script language="javascript" type="text/javascript">

	$(function(){

		var partiesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'month_name', type: 'string' },
			{ name: 'OPC', type: 'number' },
			{ name: 'PPC', type: 'number' },
			{ name: 'total_bags', type: 'number' },
			],
			url: '<?php echo site_url("admin/reports/monthly_sales_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			// pager: function (pagenum, pagesize, oldpagenum) {
   //      	//callback called when a page or page size is changed.
	  //       },
	        beforeprocessing: function (data) {
	        	partiesDataSource.totalrecords = data.total;
	        },
		    // update the grid and send a request to the server.
		    filter: function () {
		    	$("#jqxGridParty").jqxGrid('updatebounddata', 'filter');
		    },
		    // update the grid and send a request to the server.
		    sort: function () {
		    	$("#jqxGridParty").jqxGrid('updatebounddata', 'sort');
		    },
		    processdata: function(data) {
		    }
		};
	
	$("#jqxGridParty").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: partiesDataSource,
		altrows: true,
		autoheight:true,
		pageable: false,
		sortable: false,
		rowsheight: 30,
		columnsheight:30,
		autorowheight: true,
		columnsresize: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		showtoolbar: false,
		showstatusbar: true,
		statusbarheight: 50,
		showaggregates: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPartyToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{ text: '<?php echo lang("month"); ?>',datafield: 'month_name',width: 'auto',filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("opc"); ?>',datafield: 'OPC',width: 'auto',filterable: true,renderer: gridColumnsRenderer, aggregates: ['sum', 
				{ 
					'Percentage':function (aggregatedValue, currentValue, column, record) {
						opc = (!isNaN(parseInt(record['OPC'])))?parseInt(record['OPC']):0;
						total_bags_sum = $("#jqxGridParty").jqxGrid('getcolumnaggregateddata','total_bags',['sum']);
						opc_bags_sum = $("#jqxGridParty").jqxGrid('getcolumnaggregateddata','OPC',['sum']);
						total = (parseInt(opc_bags_sum.sum) * 100) / parseInt(total_bags_sum.sum);
		                return total.toFixed(2);
					}
				}
			],
			aggregatesrenderer: function (aggregates) {
        		var renderstring = "";
        		$.each(aggregates, function (key, value) {
            		renderstring += '<div style="text-align: center; margin-top: 8px;">' + key.toUpperCase() + ': ' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits:2 }) +'</div>';
	        	});
		        return renderstring;
		    },
			cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits:2 }) + '</div>';
            	},
			
		},
		{ text: '<?php echo lang("psc"); ?>',datafield: 'PPC',width: 'auto',filterable: true,renderer: gridColumnsRenderer, aggregates: ['sum', 
				{ 
					'Percentage':function (aggregatedValue, currentValue, column, record) {
						opc = (!isNaN(parseInt(record['OPC'])))?parseInt(record['OPC']):0;
						total_bags_sum = $("#jqxGridParty").jqxGrid('getcolumnaggregateddata','total_bags',['sum']);
						ppc_bags_sum = $("#jqxGridParty").jqxGrid('getcolumnaggregateddata','PPC',['sum']);
						total = (parseInt(ppc_bags_sum.sum) * 100) / parseInt(total_bags_sum.sum);
		                return total.toFixed(2);
					}
				}
			],
			aggregatesrenderer: function (aggregates) {
        		var renderstring = "";
        		$.each(aggregates, function (key, value) {
            		renderstring += '<div style="text-align: center; margin-top: 8px;">' + key.toUpperCase() + ': ' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits:2 }) +'</div>';
	        	});
		        return renderstring;
		    },
			cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits:2 }) + '</div>';
            	},
		},
		{ text: '<?php echo lang("total"); ?>',datafield: 'total_bags',width: 'auto',filterable: true,renderer: gridColumnsRenderer, aggregates: ['sum', 
			{ 'Percentage':function (aggregatedValue, currentValue, column, record) {
				total_bags = (!isNaN(parseInt(record['total_bags'])))?parseInt(record['total_bags']):0;
				total = aggregatedValue;
				if(total_bags > 0){
					var total = (total_bags * 100) / total_bags;
				}
				// console.log((total));
                return total;
			}

			}],

			aggregatesrenderer: function (aggregates) {
        		var renderstring = "";
        		$.each(aggregates, function (key, value) {
            		renderstring += '<div style="text-align: center; margin-top: 8px;">' + key.toUpperCase() + ': ' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits:2 }) +'</div>';
	        	});
		        return renderstring;
		    },
			cellsrenderer: 
            	function (row, columnfield, value, defaulthtml, columnproperties) {
            		return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits:2 }) + '</div>';
            	},

		},
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridParty").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPartyFilterClear', function () { 
		$('#jqxGridParty').jqxGrid('clearfilters');
	});

});




</script>