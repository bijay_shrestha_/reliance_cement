<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/*
 * Rename the file to Sale.php
 * and Define Module Library Function (if any)
 */


/* End of file Sale.php */
/* Location: ./modules/Sale/libraries/Sale.php */
/**
* 
*/
class Report
{
	
	public $CI;

	public function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->library('auth/Aauth');
	}

	public function monthly_sales($agent_id = NULL, $type = NULL)
	{
		if($agent_id){
			if($type == NULL){
				$this->CI->db->group_by('nepali_month_id, agent_id');
				$this->CI->db->where('agent_id',$agent_id);
			}else{
				$this->CI->db->group_by('nepali_month_id, sales_person_id');
				$this->CI->db->where('sales_person_id',$agent_id);
			}
		}else{
			if(is_agent()){
				$this->db->where('user_id',$this->_user_id);
				$agent = $this->db->get('mst_agent')->result();

				$this->db->where('agent_id',$agent->id);
				$this->CI->db->group_by('nepali_month_id, agent_id');
			}else{
				$this->CI->db->group_by('nepali_month_id');
			}
		}

		$this->CI->db->order_by('rank');
		$this->CI->db->select('agent_name, month_name, SUM(total_OPC_bags) AS OPC, SUM(total_PPC_bags) AS PPC, (SUM(total_OPC_bags) + SUM(total_PPC_bags)) AS total_bags');
		$data['rows'] = $this->CI->db->get('view_sales_detail')->result();
		// echo $this->CI->db->last_query();

		$data['count'] = count($data['rows']);

		return($data);
	}
	public function party_month_sales($where,$count = NULL)
	{
		$this->CI->db->where($where);
		$data = $this->CI->db->get('view_party_month_sales')->result();

		if($count == NULL){
			return $data;
		}else{
			$count = count($data);
			return $count;
		}
	}

	public function sales_brandtype($where)
	{
		$this->CI->db->where($where);
		$rows = $this->CI->db->get('view_brandtype_report')->result();
		return $rows;	
	}

	public function monthly_sales_target($where)
	{
		$this->CI->db->where($where);
		$rows = $this->CI->db->get('view_monthly_target_sales_pivot')->result();
		return $rows;	
	}
	public function opening_closing($where)
	{
		$this->CI->db->where($where);
		$rows = $this->CI->db->get('view_opening_closing_report')->result();
		return $rows;	
	}
}