<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('parties'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('parties'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridPartyToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Partymodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridPartyFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridParty"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Partymodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('parties'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-parties', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "parties_id"/>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='parties_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='parties_name' class=' form-control' name='name'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='phone'><?php echo lang('phone')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='phone' class=' form-control' name='phone'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='mobile'><?php echo lang('mobile')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='mobile' class=' form-control' name='mobile'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxPartySubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var partiesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			{ name: 'phone', type: 'string' },
			{ name: 'mobile', type: 'string' },
			],
			url: '<?php echo site_url("admin/parties/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	partiesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridParty").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridParty").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridParty").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: partiesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPartyToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editPartyRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("phone"); ?>',datafield: 'phone',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("mobile"); ?>',datafield: 'mobile',width: 150,filterable: true,renderer: gridColumnsRenderer },
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridParty").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPartyFilterClear', function () { 
		$('#jqxGridParty').jqxGrid('clearfilters');
	});

	
	$('#form-parties').jqxValidator({
		hintType: 'label',
		animationDuration: 500,
		rules: [
		{ input: '#parties_name', message: 'Required', action: 'blur', 
		rule: function(input) {
			val = $('#parties_name').val();
			return (val == '' || val == null || val == 0) ? false: true;
		} },


		{ input: '#mobile', message: 'Required', action: 'blur', 
		rule: function(input) {
			val = $('#mobile').val();
			return (val == '' || val == null || val == 0) ? false: true;
		} },

		]
	});

	$("#jqxPartySubmitButton").on('click', function () {
    	// savePartyRecord();

    	var validationResult = function (isValid) {
    		if (isValid) {
    			savePartyRecord();
    		}
    	};
    	$('#form-parties').jqxValidator('validate', validationResult);

    });
});

function editPartyRecord(index){
	var row =  $("#jqxGridParty").jqxGrid('getrowdata', index);
	if (row) {
		$('#parties_id').val(row.id);
		$('#parties_name').val(row.name);
		$('#phone').val(row.phone);
		$('#mobile').val(row.mobile);

		$('#Partymodal').modal('show');
	}
}

function savePartyRecord(){
	var data = $("#form-parties").serialize();

	$('#Partymodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/parties/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_parties();
				$('#jqxGridParty').jqxGrid('updatebounddata');
				$('#Partymodal').modal('hide');
			}
			$('#Partymodal').unblock();
		}
	});
}

function reset_form_parties(){
	$('#parties_id').val('');
	$('#form-parties')[0].reset();
}
</script>