<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('munvdcs'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('munvdcs'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridMunvdcToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Munvdcmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridMunvdcFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridMunvdc"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Munvdcmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('munvdcs'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-munvdcs', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "munvdcs_id"/>
					
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_by'><?php echo lang('created_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='created_by' class=' form-control' name='created_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_by'><?php echo lang('updated_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='updated_by' class=' form-control' name='updated_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_by'><?php echo lang('deleted_by')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='deleted_by' class=' form-control' name='deleted_by'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='created_at'><?php echo lang('created_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='created_at' class=' form-control' name='created_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='updated_at'><?php echo lang('updated_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='updated_at' class=' form-control' name='updated_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='deleted_at'><?php echo lang('deleted_at')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='deleted_at' class=' form-control' name='deleted_at'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='munvdcs_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='munvdcs_name' class=' form-control' name='name'></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='parent_id'><?php echo lang('parent_id')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><div id='parent_id' class=' form-control' name='parent_id'></div></div>
				</div>
				<div class='row form-group'>
					<div class='col-md-3 col-sm-6 col-xs-12'><label for='type'><?php echo lang('type')?></label></div>
					<div class='col-md-9 col-sm-6 col-xs-12'><input id='type' class=' form-control' name='type'></div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxMunvdcSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var munvdcsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			{ name: 'parent_id', type: 'number' },
			{ name: 'type', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/munvdcs/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	munvdcsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridMunvdc").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridMunvdc").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridMunvdc").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: munvdcsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridMunvdcToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editMunvdcRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("parent_id"); ?>',datafield: 'parent_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("type"); ?>',datafield: 'type',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridMunvdc").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridMunvdcFilterClear', function () { 
		$('#jqxGridMunvdc').jqxGrid('clearfilters');
	});

	
    /*$('#form-munvdcs').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#munvdcs_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#munvdcs_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#parent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#parent_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#type', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#type').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxMunvdcSubmitButton").on('click', function () {
    	saveMunvdcRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveMunvdcRecord();
                }
            };
        $('#form-munvdcs').jqxValidator('validate', validationResult);
        */
    });
});

	function editMunvdcRecord(index){
		var row =  $("#jqxGridMunvdc").jqxGrid('getrowdata', index);
		if (row) {
			$('#munvdcs_id').val(row.id);
			$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#munvdcs_name').val(row.name);
		$('#parent_id').jqxNumberInput('val', row.parent_id);
		$('#type').val(row.type);
		
			$('#Munvdcmodal').modal('show');
		}
	}

	function saveMunvdcRecord(){
		var data = $("#form-munvdcs").serialize();

		$('#Munvdcmodal').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/munvdcs/save"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_munvdcs();
					$('#jqxGridMunvdc').jqxGrid('updatebounddata');
					$('#Munvdcmodal').modal('hide');
				}
				$('#Munvdcmodal').unblock();
			}
		});
	}

	function reset_form_munvdcs(){
		$('#munvdcs_id').val('');
		$('#form-munvdcs')[0].reset();
	}
</script>