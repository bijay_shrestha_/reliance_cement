<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Monthly_targets
 *
 * Extends the Project_Controller class
 * 
 */

class AdminMonthly_targets extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Monthly Targets');

        $this->load->model('monthly_targets/monthly_target_model');
        $this->lang->load('monthly_targets/monthly_target');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('monthly_targets');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'monthly_targets';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->monthly_target_model->_table = "view_mst_monthly_target";
		
		search_params();
		
		$total=$this->monthly_target_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->monthly_target_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->monthly_target_model->insert($data);
        }
        else
        {
            $success=$this->monthly_target_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['agent_id'] = $this->input->post('agent_id');
		$data['amount'] = $this->input->post('amount');
		$data['nepali_month_id'] = $this->input->post('nepali_month_id');

        return $data;
   }
}