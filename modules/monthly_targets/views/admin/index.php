<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('monthly_targets'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('monthly_targets'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridMonthly_targetToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Monthly_targetmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridMonthly_targetFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridMonthly_target"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Monthly_targetmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('monthly_targets'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-monthly_targets', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "monthly_targets_id"/>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='agent_id'><?php echo lang('agent_id')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='agent_id' class=' form-control' name='agent_id'></div></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='nepali_month_id'><?php echo lang('nepali_month_id')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='nepali_month_id' class=' form-control' name='nepali_month_id'></div></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='amount'><?php echo lang('amount')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='amount' class=' form-control' name='amount'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxMonthly_targetSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var agentDataSource = {
			url : '<?php echo site_url("admin/Monthly_targets/get_agents_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		agentDataAdapter = new $.jqx.dataAdapter(agentDataSource);

		$("#agent_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: agentDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var nepaliMonthDataSource = {
			url : '<?php echo site_url("admin/Monthly_targets/get_nepali_month_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		nepaliMonthDataAdapter = new $.jqx.dataAdapter(nepaliMonthDataSource);

		$("#nepali_month_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: nepaliMonthDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var monthly_targetsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'agent_id', type: 'number' },
			{ name: 'amount', type: 'number' },
			{ name: 'nepali_month_id', type: 'number' },
			{ name: 'agent_name', type: 'string' },
			{ name: 'nepali_month', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/Monthly_targets/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	monthly_targetsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridMonthly_target").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridMonthly_target").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridMonthly_target").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: monthly_targetsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridMonthly_targetToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editMonthly_targetRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("agent_id"); ?>',datafield: 'agent_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("nepali_month_id"); ?>',datafield: 'nepali_month',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 150,filterable: true,renderer: gridColumnsRenderer,cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		} },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridMonthly_target").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridMonthly_targetFilterClear', function () { 
		$('#jqxGridMonthly_target').jqxGrid('clearfilters');
	});

	
    $('#form-monthly_targets').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [

			{ input: '#agent_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#agent_id').jqxComboBox('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#agent_id', message: 'Agents monthly target already exists', action: 'blur', 
				rule: function(input, commit) {

				val = $("#agent_id").jqxComboBox('val');
				nepali_month_id = $('#nepali_month_id').jqxComboBox('val');
				$.ajax({
					url: "<?php echo site_url('admin/schemes/check_duplicate_monthly_target'); ?>",
					type: 'POST',
					data: {model: 'monthly_targets/monthly_target_model', field: 'agent_id', value: val, id:$('input#monthly_targets_id').val() , nepali_month_id: nepali_month_id},
					success: function (result) {
						var result = eval('('+result+')');
						return commit(result.success);
					},
					error: function(result) {
						return commit(false);
					} }); 
				} 
			},
			{ input: '#nepali_month_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#nepali_month_id').jqxComboBox('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#amount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#amount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });

    $("#jqxMonthly_targetSubmitButton").on('click', function () {
        var validationResult = function (isValid) {
                if (isValid) {
                   saveMonthly_targetRecord();
                }
            };
        $('#form-monthly_targets').jqxValidator('validate', validationResult);
        
    });
});

function editMonthly_targetRecord(index){
	var row =  $("#jqxGridMonthly_target").jqxGrid('getrowdata', index);
	if (row) {
		$('#monthly_targets_id').val(row.id);
		$('#agent_id').jqxComboBox('val', row.agent_id);
		$('#nepali_month_id').jqxComboBox('val', row.nepali_month_id);
		$('#amount').val(row.amount);
		
		$('#Monthly_targetmodal').modal('show');
	}
}

function saveMonthly_targetRecord(){
	var data = $("#form-monthly_targets").serialize();

	$('#Monthly_targetmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Monthly_targets/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_monthly_targets();
				$('#jqxGridMonthly_target').jqxGrid('updatebounddata');
				$('#Monthly_targetmodal').modal('hide');
			}
			$('#Monthly_targetmodal').unblock();
		}
	});
}

function reset_form_monthly_targets(){
	$('#monthly_targets_id').val('');
	$('#form-monthly_targets')[0].reset();
	$('#agent_id').jqxComboBox('clearSelection');
	$('#nepali_month_id').jqxComboBox('clearSelection');
}
</script>