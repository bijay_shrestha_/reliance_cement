<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Monthly_target_model extends MY_Model
{

    public $_table = 'mst_monthly_target';

    protected $blamable = TRUE;

}