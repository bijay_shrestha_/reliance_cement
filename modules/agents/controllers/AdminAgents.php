<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Agents
 *
 * Extends the Project_Controller class
 * 
 */

class AdminAgents extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Agents');

		$this->load->model('agents/agent_model');
		$this->load->model('users/user_group_model');
		$this->load->library('form_validation');
		$this->load->library('auth/Aauth');

		$this->lang->load('agents/agent');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('agents');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'agents';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
        $this->agent_model->_table = "view_agents";
        
		search_params();
		
		$total=$this->agent_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->agent_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->agent_model->insert($data);

            if($success)
            {
                $username = strtolower(str_replace(' ', '.', $data['name']));
                $email = strtolower(str_replace(' ', '.', $data['name'])).'@reliance.com';
                $fullname = strtoupper($data['name']);
                $password = 'password123';
                $this->register_user($email, $password, $username, $fullname,$success);
            }
        }
        else
        {
        	$success=$this->agent_model->update($data['id'],$data);
        }
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['name'] = $this->input->post('name');
    	$data['phone'] = $this->input->post('phone');
        $data['mobile'] = $this->input->post('mobile');
        $data['sales_person_id'] = $this->input->post('sales_person_id');
        $data['opening_amount'] = $this->input->post('opening_amount');
        return $data;
    }

    private function register_user($email, $password, $username, $fullname,$agent_id)
    {
        $user_id = $this->aauth->create_user($email, $password , $username, $fullname);

        $this->db->trans_begin();

        if($user_id != 0 && $user_id){
            $temp = array();
            $temp['user_id'] = $user_id;
            $temp['group_id'] = AGENT_GROUP_ID;

            $this->db->insert('aauth_user_groups',$temp);

            $agent['id'] = $agent_id;
            $agent['user_id'] = $user_id;

            $this->agent_model->update($agent['id'],$agent);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();          
        }

        return $user_id;

    }
}