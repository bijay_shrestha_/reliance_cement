<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
    var $user_id;

	public function __construct()
    {
    	parent::__construct();
        if(!empty(getallheaders()['Accesstoken']))
        {
            $access_token = getallheaders()['Accesstoken'];        
            $this->db->where('access_token',$access_token);
            $check_token = $this->db->get('aauth_users')->result_array();

            if(empty($check_token))
            {
                echo json_encode('Access Denied');
                exit;
            }
        }
        else
        {
            echo json_encode('Access Token not sent on header');
            exit();
        }
        
        $this->user_id  = getallheaders()['Userid'];

        $this->load->model('agents/agent_model');
        $this->load->model('users/user_group_model');
        $this->load->library('form_validation');
        $this->load->library('auth/Aauth');
    }

    public function new_agent_post()
    {
        $data['created_by'] = getallheaders()['Userid'];
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['name'] = $this->input->post('name');
        $data['phone'] = $this->input->post('phone');
        $data['mobile'] = $this->input->post('mobile');
        $data['sales_person_id'] = $this->user_id;

        $success = $this->db->insert('mst_agents',$data);
        $agent_id = $this->db->insert_id();

        if($success)
        {
            $username = strtolower(str_replace(' ', '.', $data['name']));
            $email = strtolower(str_replace(' ', '.', $data['name'])).'@reliance.com';
            $fullname = strtoupper($data['name']);
            $password = 'password123';
            $this->_register_user($email, $password, $username, $fullname, $agent_id);
        }

        if($success)
        {
            $success = TRUE;
        }
        else
        {
            $success = FALSE;
        }

        echo json_encode(array('success'=>$success));
        exit;
    }

    private function _register_user($email, $password, $username, $fullname,$agent_id)
    {
        $user_id = $this->aauth->create_user($email, $password , $username, $fullname);

        $this->db->trans_begin();

        if($user_id != 0 && $user_id){
            $temp = array();
            $temp['user_id'] = $user_id;
            $temp['group_id'] = 1500;

            $this->db->insert('aauth_user_groups',$temp);

            $agent['user_id'] = $user_id;

            $this->db->where('id',$agent_id);
            $this->db->update('mst_agents',$agent);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();          
        }

        return $user_id;

    }
}