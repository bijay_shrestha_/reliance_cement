<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['name'] = 'Name';
$lang['district_id'] = 'District Id';
$lang['mun_vdc_id'] = 'Mun Vdc Id';
$lang['city'] = 'City';
$lang['address'] = 'Address';
$lang['phone'] = 'Phone';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['province_id'] = 'Province Id';
$lang['image'] = 'Image';

$lang['agents']='Agents';
$lang['opening_amount']='Opening Amount';
$lang['sales_person_id'] 	= 'Sales Person';
