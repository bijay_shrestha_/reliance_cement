<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('agents'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('agents'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridAgentToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Agentmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridAgentFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridAgent"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="Agentmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('agents'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-agents', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "agents_id"/>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='agents_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='agents_name' class=' form-control' name='name'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='phone'><?php echo lang('phone')?></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='phone' class=' form-control' name='phone'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='mobile'><?php echo lang('mobile')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='mobile' class=' form-control' name='mobile'></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='sales_person_id'><?php echo lang('sales_person_id')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><div id='sales_person_id' class=' form-control' name='sales_person_id'></div></div>
					</div>
					<div class='row form-group'>
						<div class='col-md-3 col-sm-6 col-xs-12'><label for='opening_amount'><?php echo lang('opening_amount')?><span class='mandatory'>*</span></label></div>
						<div class='col-md-9 col-sm-6 col-xs-12'><input id='opening_amount' class=' form-control' name='opening_amount'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxAgentSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		//salespersons
		var salespersonDataSource = {
			url : '<?php echo site_url("admin/Agents/get_sales_person_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'user_id', type: 'number' },
			{ name: 'fullname', type: 'string' },
			],
			async: false,
			cache: true
		}

		salespersonDataAdapter = new $.jqx.dataAdapter(salespersonDataSource);

		$("#sales_person_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: salespersonDataAdapter,
			displayMember: "fullname",
			valueMember: "user_id",
		});


		var agentsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			{ name: 'phone', type: 'string' },
			{ name: 'mobile', type: 'string' },
			{ name: 'salesperson_name', type: 'string' },
			{ name: 'sales_person_id', type: 'number' },
			{ name: 'opening_amount', type: 'number' },
			],
			url: '<?php echo site_url("admin/Agents/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	agentsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridAgent").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridAgent").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridAgent").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: agentsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridAgentToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editAgentRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("phone"); ?>',datafield: 'phone',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("mobile"); ?>',datafield: 'mobile',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("sales_person_id"); ?>',datafield: 'salesperson_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("opening_amount"); ?>',datafield: 'opening_amount',width: 150,filterable: true,renderer: gridColumnsRenderer,
			cellsrenderer: 
                		function (row, columnfield, value, defaulthtml, columnproperties) {
                			return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + value.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 }) + '</div>';
                		}
		 },
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridAgent").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridAgentFilterClear', function () { 
		$('#jqxGridAgent').jqxGrid('clearfilters');
	});

	
	$('#form-agents').jqxValidator({
		hintType: 'label',
		animationDuration: 500,
		rules: [
		{ input: '#agents_name', message: 'Required', action: 'blur', 
		rule: function(input) {
			val = $('#agents_name').val();
			return (val == '' || val == null || val == 0) ? false: true;
		}
	},
	/*{ input: '#mobile', message: 'Required', action: 'blur', 
	rule: function(input) {
		val = $('#mobile').val();
		return (val == '' || val == null || val == 0) ? false: true;
	} },
	{ input: '#sales_person_id', message: 'Required', action: 'blur', 
	rule: function(input) {
		val = $('#sales_person_id').jqxComboBox('val');
		return (val == '' || val == null || val == 0) ? false: true;
	} },*/
	]
});

	$("#jqxAgentSubmitButton").on('click', function () {
    	// saveAgentRecord();

    	var validationResult = function (isValid) {
    		if (isValid) {
    			saveAgentRecord();
    		}
    	};
    	$('#form-agents').jqxValidator('validate', validationResult);

    });
});

function editAgentRecord(index){
	var row =  $("#jqxGridAgent").jqxGrid('getrowdata', index);
	if (row) {
		$('#agents_id').val(row.id);
		$('#agents_name').val(row.name);
		$('#phone').val(row.phone);
		$('#mobile').val(row.mobile);
		$('#sales_person_id').jqxComboBox('val',row.sales_person_id);
		$('#opening_amount').val(row.opening_amount);

		$('#Agentmodal').modal('show');
	}
}

function saveAgentRecord(){
	var data = $("#form-agents").serialize();

	$('#Agentmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/agents/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_agents();
				$('#jqxGridAgent').jqxGrid('updatebounddata');
				$('#Agentmodal').modal('hide');
			}
			$('#Agentmodal').unblock();
		}
	});
}

function reset_form_agents(){
	$('#agents_id').val('');
	$('#form-agents')[0].reset();
}
</script>