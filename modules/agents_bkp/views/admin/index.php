<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3><?php echo lang('agents'); ?></h3>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url(); ?>">Home</a></li>
			<li class="active"><?php echo lang('agents'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridAgentToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#agentModal"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridAgentFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridAgent"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="agentModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('agents'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-agents', 'class'=>'form-horizontal', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "agents_id"/>
					<input type="hidden" name="user_id">
					<input type="hidden" name="group_id" value="<?php echo AGENT_GROUP_ID?>">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3"><label for='agents_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></div>
							<div class="col-md-9"><input id='agents_name' class=' form-control' name='name'></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='user_id'><?php echo lang('username')?></label></div>
							<div class="col-md-9"><input type="text" class=" form-control" name="username" id="username"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='district'><?php echo lang('district')?></label></div>
							<div class="col-md-9"><div id='district' class="form-control" name='district'></div></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='mun_vdc'><?php echo lang('mun_vdc')?></label></div>
							<div class="col-md-9"><div id='mun_vdc' class="form-control" name='mun_vdc'></div></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='city'><?php echo lang('city')?></label></div>
							<div class="col-md-9"><input id='city' name='city' class=" form-control"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='address'><?php echo lang('address')?></label></div>
							<div class="col-md-9"><input id='address' class=' form-control' name='address'></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='phone'><?php echo lang('phone')?></label></div>
							<div class="col-md-9"><input id='phone' class=' form-control' name='phone'></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='mobile'><?php echo lang('mobile')?></label></div>
							<div class="col-md-9"><input id='mobile' class=' form-control' name='mobile'></div>
						</div>
						<div class="row">
							<div class="col-md-3"><label for='email'><?php echo lang('email')?></label></div>
							<div class="col-md-9"><input id='email' class=' form-control' name='email'></div>
						</div>						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxAgentSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close(); ?>
		</div>

	</div>
</div>
<script language="javascript" type="text/javascript">

	$(function(){

	//districts
	var districtDataSource = {
		url : '<?php echo site_url("admin/agents/get_districts_combo_json"); ?>',
		datatype: 'json',
		datafields: [
		{ name: 'id', type: 'number' },
		{ name: 'name', type: 'string' },
		],
		async: false,
		cache: true
	}

	districtDataAdapter = new $.jqx.dataAdapter(districtDataSource);

	$("#district").jqxComboBox({
		theme: theme,
		width: '96%',
		height: 30,
		selectionMode: 'dropDownList',
		autoComplete: true,
		searchMode: 'containsignorecase',
		source: districtDataAdapter,
		displayMember: "name",
		valueMember: "id",
	});

	$("#district").select('bind', function (event) {
		val = $("#district").jqxComboBox('val');
	    //districts
	    munVdcDataSource  = {
	    	url : '<?php echo site_url("admin/agents/get_mun_vdcs_combo_json"); ?>',
	    	datatype: 'json',
	    	datafields: [
	    	{ name: 'id', type: 'number' },
	    	{ name: 'name', type: 'string' },
	    	],
	    	data: {
	    		parent_id: val
	    	},
	    	async: false,
	    	cache: true
	    }

	    munVdcDataAdapter = new $.jqx.dataAdapter(munVdcDataSource);
	    $("#mun_vdc").jqxComboBox({
	    	theme: theme,
	    	width: '96%',
	    	height: 30,
	    	selectionMode: 'dropDownList',
	    	autoComplete: true,
	    	searchMode: 'containsignorecase',
	    	source: munVdcDataAdapter,
	    	displayMember: "name",
	    	valueMember: "id",
	    });
	});

	var agentsDataSource =
	{
		datatype: "json",
		datafields: [
		{ name: 'id', type: 'number' },
		{ name: 'created_by', type: 'number' },
		{ name: 'updated_by', type: 'number' },
		{ name: 'deleted_by', type: 'number' },
		{ name: 'created_at', type: 'date' },
		{ name: 'updated_at', type: 'date' },
		{ name: 'deleted_at', type: 'date' },
		{ name: 'name', type: 'string' },
		{ name: 'user_id', type: 'number' },
		{ name: 'district', type: 'string' },
		{ name: 'mun_vdc', type: 'string' },
		{ name: 'city', type: 'string' },
		{ name: 'address', type: 'string' },
		{ name: 'phone', type: 'string' },
		{ name: 'mobile', type: 'string' },
		{ name: 'email', type: 'string' },
		{ name: 'username', type: 'string' },

		],
		url: '<?php echo site_url("admin/agents/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	agentsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridAgent").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridAgent").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridAgent").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: agentsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridAgentToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editAgentRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},

		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("username"); ?>',datafield: 'username',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("district"); ?>',datafield: 'district',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("mun_vdc"); ?>',datafield: 'mun_vdc',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("city"); ?>',datafield: 'city',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("address"); ?>',datafield: 'address',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("phone"); ?>',datafield: 'phone',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("mobile"); ?>',datafield: 'mobile',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("email"); ?>',datafield: 'email',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridAgent").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridAgentFilterClear', function () { 
		$('#jqxGridAgent').jqxGrid('clearfilters');
	});

	$('#form-agents').jqxValidator({
		hintType: 'label',
		animationDuration: 500,
		rules: [
		{ input: '#agents_name', message: 'Required', action: 'blur', 
		rule: function(input) {
			val = $('#agents_name').val();
			return (val == '' || val == null || val == 0) ? false: true;
		}
	},

			// { input: '#user_id', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#user_id').jqxNumberInput('val');
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#district', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#district').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#mun_vdc', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#mun_vdc').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#city', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#city').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#address', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#address').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#phone', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#phone').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#mobile', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#mobile').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },

			// { input: '#email', message: 'Required', action: 'blur', 
			// 	rule: function(input) {
			// 		val = $('#email').val();
			// 		return (val == '' || val == null || val == 0) ? false: true;
			// 	}
			// },
			{ input: '#username', message: 'Username already exists', action: 'blur', 
			rule: function(input, commit) {
				val = $("#username").val();
				$.ajax({
					url: "<?php echo site_url('admin/agents/check_duplicate'); ?>",
					type: 'POST',
					data: {model: 'users/user_model', field: 'username', value: val, id:$('input#user_id').val()},
					success: function (result) {
						var result = eval('('+result+')');
						return commit(result.success);
					},
					error: function(result) {
						return commit(false);
					}
				});
			}
		},

		]
	});

	$("#jqxAgentSubmitButton").on('click', function () {
        // saveAgentRecord();
        
        var validationResult = function (isValid) {
        	if (isValid) {
        		saveAgentRecord();
        	}
        };
        $('#form-agents').jqxValidator('validate', validationResult);
        
    });
});

function editAgentRecord(index){
	var row =  $("#jqxGridAgent").jqxGrid('getrowdata', index);
	if (row) {
		$('#agents_id').val(row.id);
		$('#agents_name').val(row.name);
		$('#district').val(row.district);
		$('#mun_vdc').val(row.mun_vdc);
		$('#city').val(row.city);
		$('#address').val(row.address);
		$('#phone').val(row.phone);
		$('#mobile').val(row.mobile);
		$('#email').val(row.email);

		$('#agentModal').modal('show');
	}
}

function saveAgentRecord(){
	var data = $("#form-agents").serialize();
	
	$('#agentModal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/agents/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_agents();
				$('#jqxGridAgent').jqxGrid('updatebounddata');
				$('#agentModal').modal('hide');
			}
			$('#agentModal').unblock();
		}
	});
}

function reset_form_agents(){
	$('#agents_id').val('');
	$('#form-agents')[0].reset();
}
</script>