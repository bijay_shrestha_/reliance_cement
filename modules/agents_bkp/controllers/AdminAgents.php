<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Agents
 *
 * Extends the Project_Controller class
 * 
 */

class AdminAgents extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Agents');

        $this->load->model('agents/agent_model');
        $this->lang->load('agents/agent');

        $this->load->library('agents/agent');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('agents');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'agents';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->agent_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->agent_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->agent_model->insert($data);
        }
        else
        {
            $success=$this->agent_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		//list($msg, $success) = $result = $this->employee->save_employee($data);

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}

		// $data['username'] = $this->input->post('username');
        // $data['group_id'] = $this->input->post('group_id');
		$data['name'] = $this->input->post('name');
		$data['user_id'] = $this->input->post('user_id');
		$data['district'] = $this->input->post('district');
		$data['mun_vdc'] = $this->input->post('mun_vdc');
		$data['city'] = $this->input->post('city');
		$data['address'] = $this->input->post('address');
		$data['phone'] = $this->input->post('phone');
		$data['mobile'] = $this->input->post('mobile');
		$data['email'] = $this->input->post('email');

        return $data;
   }
}