<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['name'] 		= 'Name';
$lang['user_id'] 	= 'User Id';
$lang['district'] 	= 'District';
$lang['mun_vdc'] 	= 'Mun Vdc';
$lang['city'] 		= 'City';
$lang['address'] 	= 'Address';
$lang['phone'] 		= 'Phone';
$lang['mobile'] 	= 'Mobile';
$lang['email'] 		= 'Email';
$lang['fax'] 		= 'Fax';
$lang['latitude'] 	= 'Latitude';
$lang['longitude'] 	= 'Longitude';
$lang['remarks'] 	= 'Remarks';
$lang['rank'] 		= 'Rank';

$lang['agents']		= 'Agents';
$lang['username'] 	= 'Username';
$lang['sales_person_id'] 	= 'Sales Person';