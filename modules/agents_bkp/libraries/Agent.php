<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/*
 * Rename the file to Agent.php
 * and Define Module Library Function (if any)
 */

/**
* 
*/
class Agent
{
	public $CI;
	
	function __construct()
	{
		$this->CI =& get_instance();

       	$this->CI->load->model('agents/agent_model');

        $this->CI->load->helper(array('project'));
	}

	public function save_agent()
	{
		 $this->CI->db->trans_begin();

		 if (!$data['user_id']) {

            $username = ($data['username'])?$data['username']:(str_replace(' ', '_', $data['name']));
            $email = $data['email'] = ($data['email'])?$data['mail']:$data['username'].'@rgn.com.np';
            $group_id = $data['group_id'];

            //create user
            $user_id = $this->CI->aauth->create_user($email, DEFAULT_PASSWORD, $username,$fullname);
            //create group
            $success = $this->CI->aauth->add_member($user_id, $group_id);


            $data['user_id'] = $user_id;
           
        
        }

        unset($data['username']);
        unset($data['group_id']);

        if(!array_key_exists('id', $data))
        {
            $success=$this->CI->employee_model->insert($data);
            
        }
        else
        {
            $success=$this->CI->employee_model->update($data['id'],$data);
        
        }

        if ($this->CI->db->trans_status() === FALSE)
        {
            $this->CI->db->trans_rollback();
            $success = FALSE;
            $msg=lang('failure_message');
        }
        else
        {
        	$this->CI->db->trans_commit();
            $success = TRUE;
            $msg=lang('success_message');
    	}

        return array($msg, $success);

	}
}


/* End of file Agent.php */
/* Location: ./modules/Agent/libraries/Agent.php */

