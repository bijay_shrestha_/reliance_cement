/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance_cement

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2018-04-10 18:02:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for aauth_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_groups`;
CREATE TABLE `aauth_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_groups
-- ----------------------------
INSERT INTO `aauth_groups` VALUES ('1', 'Member', 'Member Default Access Group');
INSERT INTO `aauth_groups` VALUES ('2', 'Super Administrator', 'Super Administrator Access Group');
INSERT INTO `aauth_groups` VALUES ('100', 'Administrator', 'Administrator Access Group');
INSERT INTO `aauth_groups` VALUES ('500', 'Factory Staff', 'Login for factory staff');
INSERT INTO `aauth_groups` VALUES ('1000', 'Sales Person', 'Login for sales person\r\n');
INSERT INTO `aauth_groups` VALUES ('1500', 'Agent', 'Login for agent person');
INSERT INTO `aauth_groups` VALUES ('2000', 'Office Staff', 'Login for Office Staff');

-- ----------------------------
-- Table structure for aauth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_group_permissions`;
CREATE TABLE `aauth_group_permissions` (
  `perm_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_group_permissions
-- ----------------------------
INSERT INTO `aauth_group_permissions` VALUES ('1', '1');
INSERT INTO `aauth_group_permissions` VALUES ('7', '100');
INSERT INTO `aauth_group_permissions` VALUES ('8', '100');
INSERT INTO `aauth_group_permissions` VALUES ('13', '1500');
INSERT INTO `aauth_group_permissions` VALUES ('14', '1500');
INSERT INTO `aauth_group_permissions` VALUES ('15', '1500');

-- ----------------------------
-- Table structure for aauth_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `aauth_login_attempts`;
CREATE TABLE `aauth_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_permissions`;
CREATE TABLE `aauth_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_permissions
-- ----------------------------
INSERT INTO `aauth_permissions` VALUES ('1', 'Control Panel', 'Control Panel');
INSERT INTO `aauth_permissions` VALUES ('2', 'System', 'System');
INSERT INTO `aauth_permissions` VALUES ('3', 'Users', 'Users');
INSERT INTO `aauth_permissions` VALUES ('4', 'Groups', 'Groups');
INSERT INTO `aauth_permissions` VALUES ('5', 'Permissions', 'Permissions');
INSERT INTO `aauth_permissions` VALUES ('6', 'Brand Types', 'Brand Types');
INSERT INTO `aauth_permissions` VALUES ('7', 'Master', 'Permission for master data');
INSERT INTO `aauth_permissions` VALUES ('8', 'Brands', 'Brands');
INSERT INTO `aauth_permissions` VALUES ('9', 'Parties', 'Parties');
INSERT INTO `aauth_permissions` VALUES ('10', 'Nepali Months', 'Nepali Months');
INSERT INTO `aauth_permissions` VALUES ('11', 'Schemes', 'Schemes');
INSERT INTO `aauth_permissions` VALUES ('12', 'Fiscal Year', 'Fiscal Year');
INSERT INTO `aauth_permissions` VALUES ('13', 'Payments', 'Payments');
INSERT INTO `aauth_permissions` VALUES ('14', 'Sales', 'Sales');
INSERT INTO `aauth_permissions` VALUES ('15', 'Transactions', 'Transactions');

-- ----------------------------
-- Table structure for aauth_pms
-- ----------------------------
DROP TABLE IF EXISTS `aauth_pms`;
CREATE TABLE `aauth_pms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(11) NOT NULL DEFAULT '0',
  `pm_deleted_receiver` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_sender_id_receiver_id_date_read` (`id`,`sender_id`,`receiver_id`,`date_read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_pms
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_sub_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_sub_groups`;
CREATE TABLE `aauth_sub_groups` (
  `group_id` int(11) unsigned NOT NULL,
  `subgroup_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_sub_groups
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_users
-- ----------------------------
DROP TABLE IF EXISTS `aauth_users`;
CREATE TABLE `aauth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `banned` int(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_users
-- ----------------------------
INSERT INTO `aauth_users` VALUES ('1', 'superadmin@gmail.com', '8b235284a9f7a82364468e52dab386f33844421b481113794e0b4d634c86d0f3', 'superadmin', 'Super Administrator', '0', '2018-04-10 16:52:46', '2018-04-10 16:57:39', '2018-03-15 09:35:46', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('2', 'admin@no.com', '52b3a93aac36bd14b3a1c9e7118f79981d14d39c6fd5118884d7544e58232a8d', 'admin', 'Administrator', '0', '2018-03-27 10:03:43', '2018-03-27 10:04:43', '2018-03-15 09:35:46', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('101', 'prashantji@reliance.com', '952ef9d74b98887d07852fc684e03d3d2f0f4019aa1323ee59b21d2335fe90ff', 'prashantji', 'Prashantji', '0', '2018-04-10 16:58:07', '2018-04-10 18:01:24', '2018-04-10 12:13:49', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('102', 'vishwanathji@reliance.com', '7223dbedffce450dfb58b9b6a927cb526bdae24d1d94eebb23c203253c468d05', 'vishwanathji', 'Vishwanathji', '0', null, null, '2018-04-10 12:13:49', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('103', 'dineshji@reliance.com', 'e7b1e80595e19d65af764cf3d5f7d3e95a3c7b489c3ad4c124f2af9e66d9671f', 'dineshji', 'Dineshji', '0', null, null, '2018-04-10 12:13:50', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('104', 'chandanji@reliance.com', 'e585b1dfa09d5e19f934f117c8215ff9c365c2826ff93ab80114c89e21c67907', 'chandanji', 'Chandanji', '0', null, null, '2018-04-10 12:13:50', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('105', 'hari.chandraji@reliance.com', '2fd7473d4ca229fdebf1e036245b4712c3191c97817dfa8753a53272b3fadf6c', 'hari.chandraji', 'Hari Chandraji', '0', null, null, '2018-04-10 12:15:48', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('106', 'nagendraji@reliance.com', '415c03e797d84e4bbe247f3933e752518fdab9f9d7ec4d34c14ee117153dafe0', 'nagendraji', 'Nagendraji', '0', null, null, '2018-04-10 12:15:49', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('107', 'chudamanji@reliance.com', '2d615f152bcf93a984c1c17916cc5398bf33f1f8df11e3f424b82a2903f8ae48', 'chudamanji', 'Chudamanji', '0', null, null, '2018-04-10 12:15:50', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for aauth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_groups`;
CREATE TABLE `aauth_user_groups` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_groups
-- ----------------------------
INSERT INTO `aauth_user_groups` VALUES ('1', '1');
INSERT INTO `aauth_user_groups` VALUES ('1', '2');
INSERT INTO `aauth_user_groups` VALUES ('2', '1');
INSERT INTO `aauth_user_groups` VALUES ('2', '100');
INSERT INTO `aauth_user_groups` VALUES ('101', '1');
INSERT INTO `aauth_user_groups` VALUES ('101', '1500');
INSERT INTO `aauth_user_groups` VALUES ('102', '1');
INSERT INTO `aauth_user_groups` VALUES ('102', '1500');
INSERT INTO `aauth_user_groups` VALUES ('103', '1');
INSERT INTO `aauth_user_groups` VALUES ('103', '1500');
INSERT INTO `aauth_user_groups` VALUES ('104', '1');
INSERT INTO `aauth_user_groups` VALUES ('104', '1500');
INSERT INTO `aauth_user_groups` VALUES ('105', '1');
INSERT INTO `aauth_user_groups` VALUES ('105', '1500');
INSERT INTO `aauth_user_groups` VALUES ('106', '1');
INSERT INTO `aauth_user_groups` VALUES ('106', '1500');
INSERT INTO `aauth_user_groups` VALUES ('107', '1');
INSERT INTO `aauth_user_groups` VALUES ('107', '1500');

-- ----------------------------
-- Table structure for aauth_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_permissions`;
CREATE TABLE `aauth_user_permissions` (
  `perm_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_user_variables
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_variables`;
CREATE TABLE `aauth_user_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_variables
-- ----------------------------

-- ----------------------------
-- Table structure for mst_agents
-- ----------------------------
DROP TABLE IF EXISTS `mst_agents`;
CREATE TABLE `mst_agents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `sales_person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `opening_amount` float(32,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_agents
-- ----------------------------
INSERT INTO `mst_agents` VALUES ('1', '1', '1', null, '2018-04-10 12:13:49', '2018-04-10 12:13:49', null, 'Prashantji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('2', '1', '1', null, '2018-04-10 12:13:49', '2018-04-10 12:13:49', null, 'Vishwanathji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('3', '1', '1', null, '2018-04-10 12:13:50', '2018-04-10 12:13:50', null, 'Dineshji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('4', '1', '1', null, '2018-04-10 12:13:50', '2018-04-10 12:13:50', null, 'Others - Birgunj', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('5', '1', '1', null, '2018-04-10 12:13:50', '2018-04-10 12:13:50', null, 'Chandanji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('6', '1', '1', null, '2018-04-10 12:15:48', '2018-04-10 12:15:48', null, 'Hari Chandraji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('7', '1', '1', null, '2018-04-10 12:15:49', '2018-04-10 12:15:49', null, 'Nagendraji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('8', '1', '1', null, '2018-04-10 12:15:49', '2018-04-10 12:15:49', null, 'Chudamanji', null, null, '0', null, '2000000.00');
INSERT INTO `mst_agents` VALUES ('9', '1', '1', null, '2018-04-10 12:15:50', '2018-04-10 12:15:50', null, 'Hari Shrestha, Gorkha', null, null, '0', null, '2000000.00');

-- ----------------------------
-- Table structure for mst_brands
-- ----------------------------
DROP TABLE IF EXISTS `mst_brands`;
CREATE TABLE `mst_brands` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_brands
-- ----------------------------
INSERT INTO `mst_brands` VALUES ('1', '1', '1', null, '2018-03-30 09:28:49', '2018-03-30 09:28:49', null, 'Dunlop', '1', null, null);
INSERT INTO `mst_brands` VALUES ('2', '1', '1', null, '2018-04-05 15:04:50', '2018-04-05 15:04:50', null, 'Dragon', '2', null, null);
INSERT INTO `mst_brands` VALUES ('3', '1', '1', null, '2018-04-05 15:05:03', '2018-04-05 15:05:03', null, 'Shivalik', '3', null, null);
INSERT INTO `mst_brands` VALUES ('4', '1', '1', null, '2018-04-10 11:27:26', '2018-04-10 11:27:26', null, 'Chrome', '4', null, null);

-- ----------------------------
-- Table structure for mst_brand_types
-- ----------------------------
DROP TABLE IF EXISTS `mst_brand_types`;
CREATE TABLE `mst_brand_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_brand_types
-- ----------------------------
INSERT INTO `mst_brand_types` VALUES ('1', '1', '1', null, '2018-03-16 10:11:03', '2018-03-16 10:11:03', null, 'OPC', null, null);
INSERT INTO `mst_brand_types` VALUES ('2', '1', '1', null, '2018-03-16 10:11:10', '2018-03-16 10:11:10', null, 'PSC', null, null);

-- ----------------------------
-- Table structure for mst_fiscal_years
-- ----------------------------
DROP TABLE IF EXISTS `mst_fiscal_years`;
CREATE TABLE `mst_fiscal_years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nepali_start_date` varchar(255) DEFAULT NULL,
  `nepali_end_date` varchar(255) DEFAULT NULL,
  `english_start_date` date DEFAULT NULL,
  `english_end_date` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_fiscal_years
-- ----------------------------
INSERT INTO `mst_fiscal_years` VALUES ('1', '1', '1', null, '2018-04-09 15:16:24', '2018-04-09 15:16:24', null, '2074-04-01', '2075-03-32', '2017-07-16', '2018-07-16', '1');

-- ----------------------------
-- Table structure for mst_monthly_target
-- ----------------------------
DROP TABLE IF EXISTS `mst_monthly_target`;
CREATE TABLE `mst_monthly_target` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_monthly_target
-- ----------------------------
INSERT INTO `mst_monthly_target` VALUES ('1', '1', '1', null, '2018-04-10 12:18:16', '2018-04-10 12:18:16', null, '5', '100000', '1');
INSERT INTO `mst_monthly_target` VALUES ('2', '1', '1', null, '2018-04-10 12:18:29', '2018-04-10 12:18:29', null, '1', '150000', '1');
INSERT INTO `mst_monthly_target` VALUES ('3', '1', '1', null, '2018-04-10 12:18:41', '2018-04-10 12:18:41', null, '2', '200000', '1');
INSERT INTO `mst_monthly_target` VALUES ('4', '1', '1', null, '2018-04-10 12:23:36', '2018-04-10 12:23:36', null, '9', '85000', '3');

-- ----------------------------
-- Table structure for mst_munvdc
-- ----------------------------
DROP TABLE IF EXISTS `mst_munvdc`;
CREATE TABLE `mst_munvdc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_munvdc
-- ----------------------------

-- ----------------------------
-- Table structure for mst_nepali_month
-- ----------------------------
DROP TABLE IF EXISTS `mst_nepali_month`;
CREATE TABLE `mst_nepali_month` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `rank` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_nepali_month
-- ----------------------------
INSERT INTO `mst_nepali_month` VALUES ('1', '1', '1', null, '2018-04-09 15:38:05', '2018-04-09 15:38:05', null, 'Shrawan', '1');
INSERT INTO `mst_nepali_month` VALUES ('2', '1', '1', null, '2018-04-09 15:40:57', '2018-04-09 15:40:57', null, 'Bhadra', '2');
INSERT INTO `mst_nepali_month` VALUES ('3', '1', '1', null, '2018-04-09 16:09:12', '2018-04-10 11:28:27', null, 'Ashwin', '3');

-- ----------------------------
-- Table structure for mst_parties
-- ----------------------------
DROP TABLE IF EXISTS `mst_parties`;
CREATE TABLE `mst_parties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_parties
-- ----------------------------
INSERT INTO `mst_parties` VALUES ('1', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'Advance Marketing Pvt.Ltd.-Jeetpur', null, null);
INSERT INTO `mst_parties` VALUES ('2', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'New Sagarmatha Enterprises-Arughat Bazar', null, null);
INSERT INTO `mst_parties` VALUES ('3', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'Shiv Kripa Traders-Mirchaiya', null, null);
INSERT INTO `mst_parties` VALUES ('4', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'Jagdamba International-Birganj', null, null);
INSERT INTO `mst_parties` VALUES ('5', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'Kushwaha Enterprises-Mahuwan', null, null);
INSERT INTO `mst_parties` VALUES ('6', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'Chandan Iron Store-Janakpur', null, null);
INSERT INTO `mst_parties` VALUES ('7', '1', '1', null, '2018-04-10 12:13:51', '2018-04-10 12:13:51', null, 'Ramesh Sah-Imlipatti', null, null);
INSERT INTO `mst_parties` VALUES ('8', '1', '1', null, '2018-04-10 12:15:50', '2018-04-10 12:15:50', null, 'Jai Shiv Traders-Balchandpur', null, null);
INSERT INTO `mst_parties` VALUES ('9', '1', '1', null, '2018-04-10 12:15:50', '2018-04-10 12:15:50', null, 'Siddhartha Hardware-Haripur', null, null);
INSERT INTO `mst_parties` VALUES ('10', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, 'Saraswati Hardware-Suknaha', null, null);
INSERT INTO `mst_parties` VALUES ('11', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, 'Unisha Multi Traders-Karmaiya', null, null);
INSERT INTO `mst_parties` VALUES ('12', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, 'Dhaulagiri Traders-Ratnagar', null, null);
INSERT INTO `mst_parties` VALUES ('13', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, 'Adhikari Nirman Sewa-Hanspur', null, null);
INSERT INTO `mst_parties` VALUES ('14', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, 'Nagarjun Baisdhara J.V.-Tarkeshwar', null, null);

-- ----------------------------
-- Table structure for mst_schemes
-- ----------------------------
DROP TABLE IF EXISTS `mst_schemes`;
CREATE TABLE `mst_schemes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `bag_count` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `amount` double(53,2) DEFAULT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `scheme_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_schemes
-- ----------------------------
INSERT INTO `mst_schemes` VALUES ('1', '1', '1', null, '2018-04-10 12:18:59', '2018-04-10 12:18:59', null, '1', '1', '0', '0', '150000.00', '1', 'Lump Sum');
INSERT INTO `mst_schemes` VALUES ('2', '1', '1', null, '2018-04-10 12:19:21', '2018-04-10 12:19:21', null, '1', '2', '200', '400', '80000.00', '1', 'Bag Number');
INSERT INTO `mst_schemes` VALUES ('3', '1', '1', null, '2018-04-10 12:19:34', '2018-04-10 12:19:34', null, '1', '8', '0', '0', '200000.00', '1', 'Lump Sum');
INSERT INTO `mst_schemes` VALUES ('4', '1', '1', null, '2018-04-10 12:23:03', '2018-04-10 12:23:03', null, '1', '6', '250', '1500', '375000.00', '2', 'Bag Number');

-- ----------------------------
-- Table structure for project_activity_logs
-- ----------------------------
DROP TABLE IF EXISTS `project_activity_logs`;
CREATE TABLE `project_activity_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `table_pk` int(11) unsigned NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_dttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_activity_logs
-- ----------------------------
INSERT INTO `project_activity_logs` VALUES ('1', '1', 'mst_brand_types', '1', 'insert', '2018-03-16 10:11:03');
INSERT INTO `project_activity_logs` VALUES ('2', '1', 'mst_brand_types', '2', 'insert', '2018-03-16 10:11:10');
INSERT INTO `project_activity_logs` VALUES ('3', '1', 'mst_agents', '1', 'insert', '2018-03-26 11:55:19');
INSERT INTO `project_activity_logs` VALUES ('4', '1', 'mst_agents', '1', 'update', '2018-03-26 11:56:52');
INSERT INTO `project_activity_logs` VALUES ('5', '1', 'mst_agents', '1', 'update', '2018-03-26 11:57:07');
INSERT INTO `project_activity_logs` VALUES ('6', '1', 'mst_agents', '1', 'update', '2018-03-26 11:57:30');
INSERT INTO `project_activity_logs` VALUES ('7', '1', 'mst_agents', '1', 'update', '2018-03-26 11:58:17');
INSERT INTO `project_activity_logs` VALUES ('8', '1', 'mst_agents', '1', 'update', '2018-03-26 14:14:21');
INSERT INTO `project_activity_logs` VALUES ('9', '1', 'mst_agents', '1', 'update', '2018-03-26 14:39:20');
INSERT INTO `project_activity_logs` VALUES ('10', '1', 'mst_agents', '1', 'update', '2018-03-26 15:57:15');
INSERT INTO `project_activity_logs` VALUES ('11', '1', 'mst_agents', '2', 'insert', '2018-03-26 17:33:29');
INSERT INTO `project_activity_logs` VALUES ('12', '1', 'mst_agents', '3', 'insert', '2018-03-27 11:22:16');
INSERT INTO `project_activity_logs` VALUES ('13', '1', 'mst_agents', '4', 'insert', '2018-03-27 11:23:45');
INSERT INTO `project_activity_logs` VALUES ('14', '1', 'mst_agents', '5', 'insert', '2018-03-27 11:24:08');
INSERT INTO `project_activity_logs` VALUES ('15', '1', 'mst_parties', '1', 'insert', '2018-03-27 12:09:23');
INSERT INTO `project_activity_logs` VALUES ('16', '1', 'mst_agents', '6', 'insert', '2018-03-27 12:13:05');
INSERT INTO `project_activity_logs` VALUES ('17', '1', 'mst_agents', '7', 'insert', '2018-03-27 12:30:57');
INSERT INTO `project_activity_logs` VALUES ('18', '1', 'mst_nepali_month', '1', 'insert', '2018-03-27 17:40:52');
INSERT INTO `project_activity_logs` VALUES ('19', '1', 'mst_nepali_month', '1', 'update', '2018-03-27 17:41:10');
INSERT INTO `project_activity_logs` VALUES ('20', '1', 'mst_nepali_month', '1', 'update', '2018-03-27 17:41:14');
INSERT INTO `project_activity_logs` VALUES ('21', '1', 'mst_agents', '8', 'insert', '2018-03-29 18:05:35');
INSERT INTO `project_activity_logs` VALUES ('22', '1', 'mst_agents', '9', 'insert', '2018-03-29 18:05:36');
INSERT INTO `project_activity_logs` VALUES ('23', '1', 'mst_agents', '1', 'insert', '2018-03-30 09:23:36');
INSERT INTO `project_activity_logs` VALUES ('24', '1', 'mst_agents', '2', 'insert', '2018-03-30 09:23:36');
INSERT INTO `project_activity_logs` VALUES ('25', '1', 'mst_agents', '3', 'insert', '2018-03-30 09:23:36');
INSERT INTO `project_activity_logs` VALUES ('26', '1', 'mst_agents', '4', 'insert', '2018-03-30 09:23:53');
INSERT INTO `project_activity_logs` VALUES ('27', '1', 'mst_agents', '5', 'insert', '2018-03-30 09:23:53');
INSERT INTO `project_activity_logs` VALUES ('28', '1', 'mst_agents', '6', 'insert', '2018-03-30 09:23:54');
INSERT INTO `project_activity_logs` VALUES ('29', '1', 'mst_agents', '7', 'insert', '2018-03-30 09:24:47');
INSERT INTO `project_activity_logs` VALUES ('30', '1', 'mst_agents', '8', 'insert', '2018-03-30 09:24:47');
INSERT INTO `project_activity_logs` VALUES ('31', '1', 'mst_agents', '9', 'insert', '2018-03-30 09:24:47');
INSERT INTO `project_activity_logs` VALUES ('32', '1', 'mst_agents', '10', 'insert', '2018-03-30 09:25:32');
INSERT INTO `project_activity_logs` VALUES ('33', '1', 'mst_agents', '11', 'insert', '2018-03-30 09:25:33');
INSERT INTO `project_activity_logs` VALUES ('34', '1', 'mst_agents', '12', 'insert', '2018-03-30 09:25:33');
INSERT INTO `project_activity_logs` VALUES ('35', '1', 'mst_brands', '1', 'insert', '2018-03-30 09:28:49');
INSERT INTO `project_activity_logs` VALUES ('36', '1', 'mst_agents', '13', 'insert', '2018-03-30 09:36:49');
INSERT INTO `project_activity_logs` VALUES ('37', '1', 'mst_agents', '14', 'insert', '2018-03-30 09:36:49');
INSERT INTO `project_activity_logs` VALUES ('38', '1', 'mst_agents', '15', 'insert', '2018-03-30 09:36:50');
INSERT INTO `project_activity_logs` VALUES ('39', '1', 'mst_agents', '16', 'insert', '2018-03-30 09:37:56');
INSERT INTO `project_activity_logs` VALUES ('40', '1', 'mst_agents', '17', 'insert', '2018-03-30 09:37:56');
INSERT INTO `project_activity_logs` VALUES ('41', '1', 'mst_agents', '18', 'insert', '2018-03-30 09:37:56');
INSERT INTO `project_activity_logs` VALUES ('42', '1', 'mst_agents', '19', 'insert', '2018-03-30 09:38:20');
INSERT INTO `project_activity_logs` VALUES ('43', '1', 'mst_agents', '20', 'insert', '2018-03-30 09:38:20');
INSERT INTO `project_activity_logs` VALUES ('44', '1', 'mst_agents', '21', 'insert', '2018-03-30 09:38:20');
INSERT INTO `project_activity_logs` VALUES ('45', '1', 'mst_agents', '22', 'insert', '2018-03-30 09:38:45');
INSERT INTO `project_activity_logs` VALUES ('46', '1', 'mst_agents', '23', 'insert', '2018-03-30 09:38:46');
INSERT INTO `project_activity_logs` VALUES ('47', '1', 'mst_agents', '24', 'insert', '2018-03-30 09:38:46');
INSERT INTO `project_activity_logs` VALUES ('48', '1', 'mst_agents', '25', 'insert', '2018-03-30 09:39:26');
INSERT INTO `project_activity_logs` VALUES ('49', '1', 'mst_agents', '26', 'insert', '2018-03-30 09:39:26');
INSERT INTO `project_activity_logs` VALUES ('50', '1', 'mst_agents', '27', 'insert', '2018-03-30 09:39:26');
INSERT INTO `project_activity_logs` VALUES ('51', '1', 'mst_agents', '28', 'insert', '2018-03-30 09:40:54');
INSERT INTO `project_activity_logs` VALUES ('52', '1', 'mst_agents', '29', 'insert', '2018-03-30 09:40:54');
INSERT INTO `project_activity_logs` VALUES ('53', '1', 'mst_agents', '30', 'insert', '2018-03-30 09:40:55');
INSERT INTO `project_activity_logs` VALUES ('54', '1', 'mst_agents', '31', 'insert', '2018-03-30 09:42:07');
INSERT INTO `project_activity_logs` VALUES ('55', '1', 'mst_agents', '32', 'insert', '2018-03-30 09:42:08');
INSERT INTO `project_activity_logs` VALUES ('56', '1', 'mst_agents', '33', 'insert', '2018-03-30 09:42:08');
INSERT INTO `project_activity_logs` VALUES ('57', '1', 'mst_agents', '34', 'insert', '2018-03-30 09:51:34');
INSERT INTO `project_activity_logs` VALUES ('58', '1', 'mst_agents', '35', 'insert', '2018-03-30 09:51:35');
INSERT INTO `project_activity_logs` VALUES ('59', '1', 'mst_agents', '36', 'insert', '2018-03-30 09:53:38');
INSERT INTO `project_activity_logs` VALUES ('60', '1', 'mst_agents', '37', 'insert', '2018-03-30 09:53:38');
INSERT INTO `project_activity_logs` VALUES ('61', '1', 'mst_agents', '38', 'insert', '2018-03-30 09:53:39');
INSERT INTO `project_activity_logs` VALUES ('62', '1', 'mst_agents', '39', 'insert', '2018-03-30 09:53:39');
INSERT INTO `project_activity_logs` VALUES ('63', '1', 'mst_agents', '40', 'insert', '2018-03-30 09:54:31');
INSERT INTO `project_activity_logs` VALUES ('64', '1', 'mst_agents', '41', 'insert', '2018-03-30 09:54:31');
INSERT INTO `project_activity_logs` VALUES ('65', '1', 'mst_agents', '42', 'insert', '2018-03-30 09:56:06');
INSERT INTO `project_activity_logs` VALUES ('66', '1', 'mst_agents', '43', 'insert', '2018-03-30 09:56:06');
INSERT INTO `project_activity_logs` VALUES ('67', '1', 'mst_agents', '44', 'insert', '2018-03-30 09:58:21');
INSERT INTO `project_activity_logs` VALUES ('68', '1', 'mst_agents', '44', 'update', '2018-03-30 11:06:43');
INSERT INTO `project_activity_logs` VALUES ('69', '1', 'mst_agents', '45', 'insert', '2018-03-30 12:56:40');
INSERT INTO `project_activity_logs` VALUES ('70', '1', 'mst_agents', '46', 'insert', '2018-03-30 12:56:40');
INSERT INTO `project_activity_logs` VALUES ('71', '1', 'mst_agents', '47', 'insert', '2018-03-30 12:56:40');
INSERT INTO `project_activity_logs` VALUES ('72', '1', 'mst_agents', '48', 'insert', '2018-03-30 12:58:43');
INSERT INTO `project_activity_logs` VALUES ('73', '1', 'mst_agents', '49', 'insert', '2018-03-30 12:58:43');
INSERT INTO `project_activity_logs` VALUES ('74', '1', 'mst_agents', '50', 'insert', '2018-03-30 12:58:44');
INSERT INTO `project_activity_logs` VALUES ('75', '1', 'mst_agents', '51', 'insert', '2018-03-30 13:08:47');
INSERT INTO `project_activity_logs` VALUES ('76', '1', 'mst_agents', '52', 'insert', '2018-03-30 13:08:47');
INSERT INTO `project_activity_logs` VALUES ('77', '1', 'mst_agents', '53', 'insert', '2018-03-30 13:08:47');
INSERT INTO `project_activity_logs` VALUES ('78', '1', 'mst_parties', '2', 'insert', '2018-03-30 13:08:48');
INSERT INTO `project_activity_logs` VALUES ('79', '1', 'mst_parties', '3', 'insert', '2018-03-30 13:08:48');
INSERT INTO `project_activity_logs` VALUES ('80', '1', 'mst_agents', '1', 'insert', '2018-03-30 14:32:43');
INSERT INTO `project_activity_logs` VALUES ('81', '1', 'mst_agents', '2', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('82', '1', 'mst_agents', '3', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('83', '1', 'mst_agents', '4', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('84', '1', 'mst_agents', '5', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('85', '1', 'mst_agents', '6', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('86', '1', 'mst_agents', '7', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('87', '1', 'mst_agents', '8', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('88', '1', 'mst_agents', '9', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('89', '1', 'mst_agents', '10', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('90', '1', 'mst_agents', '11', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('91', '1', 'mst_agents', '12', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('92', '1', 'mst_agents', '13', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('93', '1', 'mst_agents', '14', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('94', '1', 'mst_agents', '15', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('95', '1', 'mst_agents', '16', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('96', '1', 'mst_agents', '17', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('97', '1', 'mst_agents', '18', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('98', '1', 'mst_agents', '19', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('99', '1', 'mst_agents', '20', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('100', '1', 'mst_agents', '21', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('101', '1', 'mst_agents', '22', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('102', '1', 'mst_agents', '23', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('103', '1', 'mst_agents', '24', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('104', '1', 'mst_parties', '4', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('105', '1', 'mst_parties', '5', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('106', '1', 'mst_parties', '6', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('107', '1', 'mst_parties', '7', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('108', '1', 'mst_parties', '8', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('109', '1', 'mst_parties', '9', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('110', '1', 'mst_parties', '10', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('111', '1', 'mst_parties', '11', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('112', '1', 'mst_parties', '12', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('113', '1', 'mst_parties', '13', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('114', '1', 'mst_parties', '14', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('115', '1', 'mst_parties', '15', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('116', '1', 'mst_parties', '16', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('117', '1', 'mst_parties', '17', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('118', '1', 'mst_parties', '18', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('119', '1', 'mst_parties', '19', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('120', '1', 'mst_parties', '20', 'insert', '2018-03-30 14:32:49');
INSERT INTO `project_activity_logs` VALUES ('121', '1', 'mst_parties', '21', 'insert', '2018-03-30 14:32:49');
INSERT INTO `project_activity_logs` VALUES ('122', '1', 'mst_parties', '22', 'insert', '2018-03-30 14:32:49');
INSERT INTO `project_activity_logs` VALUES ('123', '1', 'mst_agents', '25', 'insert', '2018-04-02 15:09:09');
INSERT INTO `project_activity_logs` VALUES ('124', '1', 'tbl_sales', '12', 'update', '2018-04-02 16:54:51');
INSERT INTO `project_activity_logs` VALUES ('125', '1', 'tbl_sales', '9', 'update', '2018-04-02 16:55:18');
INSERT INTO `project_activity_logs` VALUES ('126', '1', 'tbl_sales', '10', 'update', '2018-04-02 16:56:54');
INSERT INTO `project_activity_logs` VALUES ('127', '1', 'tbl_sales', '11', 'update', '2018-04-02 16:56:54');
INSERT INTO `project_activity_logs` VALUES ('128', '1', 'tbl_sales', '12', 'update', '2018-04-02 16:56:54');
INSERT INTO `project_activity_logs` VALUES ('129', '1', 'tbl_sales', '7', 'update', '2018-04-02 16:57:31');
INSERT INTO `project_activity_logs` VALUES ('130', '1', 'tbl_sales', '8', 'update', '2018-04-02 16:57:31');
INSERT INTO `project_activity_logs` VALUES ('131', '1', 'tbl_sales', '9', 'update', '2018-04-02 16:57:31');
INSERT INTO `project_activity_logs` VALUES ('132', '1', 'tbl_sales', '10', 'update', '2018-04-02 16:57:54');
INSERT INTO `project_activity_logs` VALUES ('133', '1', 'tbl_sales', '11', 'update', '2018-04-02 16:57:54');
INSERT INTO `project_activity_logs` VALUES ('134', '1', 'tbl_sales', '12', 'update', '2018-04-02 16:57:55');
INSERT INTO `project_activity_logs` VALUES ('135', '1', 'tbl_sales', '7', 'update', '2018-04-02 16:58:36');
INSERT INTO `project_activity_logs` VALUES ('136', '1', 'tbl_sales', '8', 'update', '2018-04-02 16:58:36');
INSERT INTO `project_activity_logs` VALUES ('137', '1', 'tbl_sales', '9', 'update', '2018-04-02 16:58:36');
INSERT INTO `project_activity_logs` VALUES ('138', '1', 'tbl_payment', '1', 'update', '2018-04-03 12:06:42');
INSERT INTO `project_activity_logs` VALUES ('139', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:51:45');
INSERT INTO `project_activity_logs` VALUES ('140', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:53:54');
INSERT INTO `project_activity_logs` VALUES ('141', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:56:33');
INSERT INTO `project_activity_logs` VALUES ('142', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:57:40');
INSERT INTO `project_activity_logs` VALUES ('143', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:59:03');
INSERT INTO `project_activity_logs` VALUES ('144', '1', 'tbl_payment', '1', 'update', '2018-04-03 15:00:02');
INSERT INTO `project_activity_logs` VALUES ('145', '1', 'tbl_payment', '1', 'update', '2018-04-03 15:01:40');
INSERT INTO `project_activity_logs` VALUES ('146', '1', 'mst_agents', '1', 'insert', '2018-04-04 14:57:29');
INSERT INTO `project_activity_logs` VALUES ('147', '1', 'mst_agents', '2', 'insert', '2018-04-04 14:58:07');
INSERT INTO `project_activity_logs` VALUES ('148', '1', 'mst_agents', '3', 'insert', '2018-04-04 14:59:37');
INSERT INTO `project_activity_logs` VALUES ('149', '1', 'mst_agents', '4', 'insert', '2018-04-04 14:59:37');
INSERT INTO `project_activity_logs` VALUES ('150', '1', 'mst_agents', '5', 'insert', '2018-04-04 14:59:38');
INSERT INTO `project_activity_logs` VALUES ('151', '1', 'mst_parties', '1', 'insert', '2018-04-04 14:59:38');
INSERT INTO `project_activity_logs` VALUES ('152', '1', 'mst_parties', '2', 'insert', '2018-04-04 14:59:38');
INSERT INTO `project_activity_logs` VALUES ('153', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:01:31');
INSERT INTO `project_activity_logs` VALUES ('154', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:01:32');
INSERT INTO `project_activity_logs` VALUES ('155', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:01:32');
INSERT INTO `project_activity_logs` VALUES ('156', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:03:19');
INSERT INTO `project_activity_logs` VALUES ('157', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:03:20');
INSERT INTO `project_activity_logs` VALUES ('158', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:03:20');
INSERT INTO `project_activity_logs` VALUES ('159', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:04:17');
INSERT INTO `project_activity_logs` VALUES ('160', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:04:18');
INSERT INTO `project_activity_logs` VALUES ('161', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:04:19');
INSERT INTO `project_activity_logs` VALUES ('162', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:07:08');
INSERT INTO `project_activity_logs` VALUES ('163', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:07:08');
INSERT INTO `project_activity_logs` VALUES ('164', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:07:08');
INSERT INTO `project_activity_logs` VALUES ('165', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:08:15');
INSERT INTO `project_activity_logs` VALUES ('166', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:08:16');
INSERT INTO `project_activity_logs` VALUES ('167', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:08:16');
INSERT INTO `project_activity_logs` VALUES ('168', '1', 'tbl_sales', '1', 'update', '2018-04-04 15:19:51');
INSERT INTO `project_activity_logs` VALUES ('169', '1', 'tbl_sales', '2', 'update', '2018-04-04 15:19:51');
INSERT INTO `project_activity_logs` VALUES ('170', '1', 'tbl_sales', '1', 'update', '2018-04-04 15:20:53');
INSERT INTO `project_activity_logs` VALUES ('171', '1', 'tbl_sales', '2', 'update', '2018-04-04 15:20:53');
INSERT INTO `project_activity_logs` VALUES ('172', '1', 'mst_fiscal_years', '1', 'insert', '2018-04-04 16:32:00');
INSERT INTO `project_activity_logs` VALUES ('173', '1', 'mst_fiscal_years', '2', 'insert', '2018-04-04 16:32:14');
INSERT INTO `project_activity_logs` VALUES ('174', '1', 'mst_fiscal_years', '3', 'insert', '2018-04-04 16:49:45');
INSERT INTO `project_activity_logs` VALUES ('175', '1', 'mst_fiscal_years', '4', 'insert', '2018-04-04 16:51:59');
INSERT INTO `project_activity_logs` VALUES ('176', '1', 'mst_fiscal_years', '5', 'insert', '2018-04-04 16:53:46');
INSERT INTO `project_activity_logs` VALUES ('177', '1', 'mst_fiscal_years', '6', 'insert', '2018-04-04 16:54:20');
INSERT INTO `project_activity_logs` VALUES ('178', '1', 'mst_fiscal_years', '1', 'insert', '2018-04-04 17:02:47');
INSERT INTO `project_activity_logs` VALUES ('179', '1', 'mst_fiscal_years', '2', 'insert', '2018-04-04 17:18:26');
INSERT INTO `project_activity_logs` VALUES ('180', '1', 'mst_schemes', '1', 'insert', '2018-04-04 17:26:01');
INSERT INTO `project_activity_logs` VALUES ('181', '1', 'mst_schemes', '2', 'insert', '2018-04-04 17:37:49');
INSERT INTO `project_activity_logs` VALUES ('182', '1', 'mst_schemes', '3', 'insert', '2018-04-04 17:38:23');
INSERT INTO `project_activity_logs` VALUES ('183', '1', 'mst_schemes', '4', 'insert', '2018-04-04 17:38:54');
INSERT INTO `project_activity_logs` VALUES ('184', '1', 'mst_schemes', '5', 'insert', '2018-04-04 17:41:43');
INSERT INTO `project_activity_logs` VALUES ('185', '1', 'tbl_sales', '1', 'update', '2018-04-04 17:54:35');
INSERT INTO `project_activity_logs` VALUES ('186', '1', 'tbl_sales', '2', 'update', '2018-04-04 17:54:36');
INSERT INTO `project_activity_logs` VALUES ('187', '1', 'tbl_sales', '3', 'update', '2018-04-04 17:54:36');
INSERT INTO `project_activity_logs` VALUES ('188', '1', 'tbl_sales', '1', 'update', '2018-04-04 17:55:50');
INSERT INTO `project_activity_logs` VALUES ('189', '1', 'tbl_sales', '2', 'update', '2018-04-04 17:55:50');
INSERT INTO `project_activity_logs` VALUES ('190', '1', 'tbl_sales', '3', 'update', '2018-04-04 17:55:50');
INSERT INTO `project_activity_logs` VALUES ('191', '1', 'mst_agents', '1', 'insert', '2018-04-04 18:08:55');
INSERT INTO `project_activity_logs` VALUES ('192', '1', 'mst_agents', '2', 'insert', '2018-04-04 18:09:26');
INSERT INTO `project_activity_logs` VALUES ('193', '1', 'mst_agents', '3', 'insert', '2018-04-04 18:09:26');
INSERT INTO `project_activity_logs` VALUES ('194', '1', 'mst_agents', '4', 'insert', '2018-04-04 18:09:27');
INSERT INTO `project_activity_logs` VALUES ('195', '1', 'mst_parties', '1', 'insert', '2018-04-04 18:09:27');
INSERT INTO `project_activity_logs` VALUES ('196', '1', 'mst_parties', '2', 'insert', '2018-04-04 18:09:27');
INSERT INTO `project_activity_logs` VALUES ('197', '1', 'tbl_sales_temporary', '46', 'update', '2018-04-05 12:23:07');
INSERT INTO `project_activity_logs` VALUES ('198', '1', 'tbl_sales_temporary', '47', 'update', '2018-04-05 12:23:57');
INSERT INTO `project_activity_logs` VALUES ('199', '1', 'tbl_sales_temporary', '48', 'update', '2018-04-05 12:24:05');
INSERT INTO `project_activity_logs` VALUES ('200', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 12:26:08');
INSERT INTO `project_activity_logs` VALUES ('201', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 12:28:12');
INSERT INTO `project_activity_logs` VALUES ('202', '1', 'tbl_sales_temporary', '12', 'update', '2018-04-05 14:32:35');
INSERT INTO `project_activity_logs` VALUES ('203', '1', 'tbl_sales_temporary', '11', 'update', '2018-04-05 14:32:40');
INSERT INTO `project_activity_logs` VALUES ('204', '1', 'tbl_sales_temporary', '13', 'update', '2018-04-05 14:34:16');
INSERT INTO `project_activity_logs` VALUES ('205', '1', 'tbl_sales_temporary', '14', 'update', '2018-04-05 14:34:19');
INSERT INTO `project_activity_logs` VALUES ('206', '1', 'tbl_sales_temporary', '15', 'update', '2018-04-05 14:34:23');
INSERT INTO `project_activity_logs` VALUES ('207', '1', 'tbl_sales_temporary', '16', 'update', '2018-04-05 14:34:26');
INSERT INTO `project_activity_logs` VALUES ('208', '1', 'tbl_sales_temporary', '17', 'update', '2018-04-05 14:34:30');
INSERT INTO `project_activity_logs` VALUES ('209', '1', 'tbl_sales_temporary', '18', 'update', '2018-04-05 14:34:34');
INSERT INTO `project_activity_logs` VALUES ('210', '1', 'tbl_sales_temporary', '21', 'update', '2018-04-05 14:36:24');
INSERT INTO `project_activity_logs` VALUES ('211', '1', 'tbl_sales_temporary', '20', 'update', '2018-04-05 14:36:27');
INSERT INTO `project_activity_logs` VALUES ('212', '1', 'tbl_sales_temporary', '19', 'update', '2018-04-05 14:36:30');
INSERT INTO `project_activity_logs` VALUES ('213', '1', 'tbl_sales_temporary', '24', 'update', '2018-04-05 14:37:24');
INSERT INTO `project_activity_logs` VALUES ('214', '1', 'tbl_sales_temporary', '23', 'update', '2018-04-05 14:37:29');
INSERT INTO `project_activity_logs` VALUES ('215', '1', 'tbl_sales_temporary', '22', 'update', '2018-04-05 14:37:31');
INSERT INTO `project_activity_logs` VALUES ('216', '1', 'tbl_sales_temporary', '27', 'update', '2018-04-05 14:38:22');
INSERT INTO `project_activity_logs` VALUES ('217', '1', 'tbl_sales_temporary', '27', 'update', '2018-04-05 14:38:28');
INSERT INTO `project_activity_logs` VALUES ('218', '1', 'tbl_sales_temporary', '30', 'update', '2018-04-05 14:39:49');
INSERT INTO `project_activity_logs` VALUES ('219', '1', 'tbl_sales_temporary', '33', 'update', '2018-04-05 14:40:53');
INSERT INTO `project_activity_logs` VALUES ('220', '1', 'tbl_sales_temporary', '36', 'update', '2018-04-05 14:41:25');
INSERT INTO `project_activity_logs` VALUES ('221', '1', 'tbl_sales_temporary', '39', 'update', '2018-04-05 14:42:29');
INSERT INTO `project_activity_logs` VALUES ('222', '1', 'tbl_sales_temporary', '38', 'update', '2018-04-05 14:42:32');
INSERT INTO `project_activity_logs` VALUES ('223', '1', 'tbl_sales_temporary', '37', 'update', '2018-04-05 14:42:36');
INSERT INTO `project_activity_logs` VALUES ('224', '1', 'tbl_sales', '43', 'update', '2018-04-05 14:45:26');
INSERT INTO `project_activity_logs` VALUES ('225', '1', 'tbl_sales', '44', 'update', '2018-04-05 14:45:26');
INSERT INTO `project_activity_logs` VALUES ('226', '1', 'tbl_sales', '45', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('227', '1', 'tbl_sales', '46', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('228', '1', 'tbl_sales', '47', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('229', '1', 'tbl_sales', '48', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('230', '1', 'tbl_sales', '49', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('231', '1', 'tbl_sales', '50', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('232', '1', 'tbl_sales', '51', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('233', '1', 'tbl_sales', '52', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('234', '1', 'tbl_sales', '53', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('235', '1', 'tbl_sales', '54', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('236', '1', 'tbl_sales', '55', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('237', '1', 'tbl_sales', '56', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('238', '1', 'tbl_sales', '57', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('239', '1', 'tbl_sales', '58', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('240', '1', 'tbl_sales', '59', 'update', '2018-04-05 14:45:30');
INSERT INTO `project_activity_logs` VALUES ('241', '1', 'tbl_sales', '60', 'update', '2018-04-05 14:45:30');
INSERT INTO `project_activity_logs` VALUES ('242', '1', 'tbl_sales_temporary', '3', 'update', '2018-04-05 14:54:34');
INSERT INTO `project_activity_logs` VALUES ('243', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 14:54:40');
INSERT INTO `project_activity_logs` VALUES ('244', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 14:54:45');
INSERT INTO `project_activity_logs` VALUES ('245', '1', 'tbl_sales_temporary', '12', 'update', '2018-04-05 14:57:52');
INSERT INTO `project_activity_logs` VALUES ('246', '1', 'tbl_sales_temporary', '11', 'update', '2018-04-05 14:57:56');
INSERT INTO `project_activity_logs` VALUES ('247', '1', 'tbl_sales_temporary', '10', 'update', '2018-04-05 14:57:59');
INSERT INTO `project_activity_logs` VALUES ('248', '1', 'mst_agents', '5', 'insert', '2018-04-05 15:04:22');
INSERT INTO `project_activity_logs` VALUES ('249', '1', 'mst_parties', '3', 'insert', '2018-04-05 15:04:23');
INSERT INTO `project_activity_logs` VALUES ('250', '1', 'mst_parties', '4', 'insert', '2018-04-05 15:04:23');
INSERT INTO `project_activity_logs` VALUES ('251', '1', 'mst_brands', '2', 'insert', '2018-04-05 15:04:50');
INSERT INTO `project_activity_logs` VALUES ('252', '1', 'mst_brands', '3', 'insert', '2018-04-05 15:05:03');
INSERT INTO `project_activity_logs` VALUES ('253', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 15:05:30');
INSERT INTO `project_activity_logs` VALUES ('254', '1', 'mst_agents', '6', 'insert', '2018-04-05 15:06:25');
INSERT INTO `project_activity_logs` VALUES ('255', '1', 'mst_parties', '5', 'insert', '2018-04-05 15:06:26');
INSERT INTO `project_activity_logs` VALUES ('256', '1', 'mst_agents', '7', 'insert', '2018-04-05 15:07:06');
INSERT INTO `project_activity_logs` VALUES ('257', '1', 'mst_agents', '8', 'insert', '2018-04-05 15:09:23');
INSERT INTO `project_activity_logs` VALUES ('258', '1', 'mst_agents', '9', 'insert', '2018-04-05 15:09:24');
INSERT INTO `project_activity_logs` VALUES ('259', '1', 'mst_parties', '6', 'insert', '2018-04-05 15:09:24');
INSERT INTO `project_activity_logs` VALUES ('260', '1', 'mst_parties', '7', 'insert', '2018-04-05 15:09:25');
INSERT INTO `project_activity_logs` VALUES ('261', '1', 'mst_agents', '10', 'insert', '2018-04-05 15:10:59');
INSERT INTO `project_activity_logs` VALUES ('262', '1', 'mst_agents', '11', 'insert', '2018-04-05 15:10:59');
INSERT INTO `project_activity_logs` VALUES ('263', '1', 'mst_parties', '8', 'insert', '2018-04-05 15:11:00');
INSERT INTO `project_activity_logs` VALUES ('264', '1', 'mst_parties', '9', 'insert', '2018-04-05 15:11:00');
INSERT INTO `project_activity_logs` VALUES ('265', '1', 'mst_agents', '12', 'insert', '2018-04-05 15:13:36');
INSERT INTO `project_activity_logs` VALUES ('266', '1', 'mst_parties', '10', 'insert', '2018-04-05 15:13:37');
INSERT INTO `project_activity_logs` VALUES ('267', '1', 'mst_parties', '11', 'insert', '2018-04-05 15:13:37');
INSERT INTO `project_activity_logs` VALUES ('268', '1', 'mst_agents', '13', 'insert', '2018-04-05 15:15:17');
INSERT INTO `project_activity_logs` VALUES ('269', '1', 'mst_parties', '12', 'insert', '2018-04-05 15:15:17');
INSERT INTO `project_activity_logs` VALUES ('270', '1', 'tbl_sales_temporary', '14', 'update', '2018-04-05 15:15:26');
INSERT INTO `project_activity_logs` VALUES ('271', '1', 'mst_agents', '14', 'insert', '2018-04-05 15:17:39');
INSERT INTO `project_activity_logs` VALUES ('272', '1', 'mst_parties', '13', 'insert', '2018-04-05 15:17:40');
INSERT INTO `project_activity_logs` VALUES ('273', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 15:17:50');
INSERT INTO `project_activity_logs` VALUES ('274', '1', 'tbl_sales_temporary', '2', 'update', '2018-04-05 15:18:38');
INSERT INTO `project_activity_logs` VALUES ('275', '1', 'tbl_sales', '2', 'update', '2018-04-05 15:18:59');
INSERT INTO `project_activity_logs` VALUES ('276', '1', 'tbl_payment_temporary', '5', 'update', '2018-04-05 15:35:12');
INSERT INTO `project_activity_logs` VALUES ('277', '1', 'tbl_payment_temporary', '8', 'update', '2018-04-05 15:38:47');
INSERT INTO `project_activity_logs` VALUES ('278', '1', 'mst_agents', '15', 'insert', '2018-04-09 14:28:24');
INSERT INTO `project_activity_logs` VALUES ('279', '1', 'mst_agents', '1', 'insert', '2018-04-09 15:14:42');
INSERT INTO `project_activity_logs` VALUES ('280', '1', 'mst_schemes', '1', 'insert', '2018-04-09 15:15:20');
INSERT INTO `project_activity_logs` VALUES ('281', '1', 'mst_fiscal_years', '1', 'insert', '2018-04-09 15:16:24');
INSERT INTO `project_activity_logs` VALUES ('282', '1', 'mst_schemes', '2', 'insert', '2018-04-09 15:17:49');
INSERT INTO `project_activity_logs` VALUES ('283', '1', 'mst_schemes', '3', 'insert', '2018-04-09 15:19:14');
INSERT INTO `project_activity_logs` VALUES ('284', '1', 'mst_nepali_month', '1', 'insert', '2018-04-09 15:38:05');
INSERT INTO `project_activity_logs` VALUES ('285', '1', 'mst_nepali_month', '2', 'insert', '2018-04-09 15:40:57');
INSERT INTO `project_activity_logs` VALUES ('286', '1', 'mst_schemes', '4', 'insert', '2018-04-09 15:43:45');
INSERT INTO `project_activity_logs` VALUES ('287', '1', 'mst_schemes', '1', 'insert', '2018-04-09 15:49:00');
INSERT INTO `project_activity_logs` VALUES ('288', '1', 'mst_schemes', '1', 'update', '2018-04-09 15:52:54');
INSERT INTO `project_activity_logs` VALUES ('289', '1', 'mst_schemes', '2', 'insert', '2018-04-09 15:53:28');
INSERT INTO `project_activity_logs` VALUES ('290', '1', 'mst_schemes', '1', 'update', '2018-04-09 16:01:40');
INSERT INTO `project_activity_logs` VALUES ('291', '1', 'mst_nepali_month', '3', 'insert', '2018-04-09 16:09:12');
INSERT INTO `project_activity_logs` VALUES ('292', '1', 'mst_schemes', '3', 'insert', '2018-04-09 16:09:33');
INSERT INTO `project_activity_logs` VALUES ('293', '1', 'mst_schemes', '2', 'update', '2018-04-09 16:10:38');
INSERT INTO `project_activity_logs` VALUES ('294', '1', 'mst_monthly_target', '1', 'insert', '2018-04-09 16:17:34');
INSERT INTO `project_activity_logs` VALUES ('295', '1', 'mst_monthly_target', '2', 'insert', '2018-04-09 16:22:00');
INSERT INTO `project_activity_logs` VALUES ('296', '1', 'mst_monthly_target', '3', 'insert', '2018-04-09 16:32:35');
INSERT INTO `project_activity_logs` VALUES ('297', '1', 'mst_monthly_target', '4', 'insert', '2018-04-09 16:32:36');
INSERT INTO `project_activity_logs` VALUES ('298', '1', 'mst_agents', '2', 'insert', '2018-04-09 17:05:17');
INSERT INTO `project_activity_logs` VALUES ('299', '1', 'mst_parties', '1', 'insert', '2018-04-09 17:05:17');
INSERT INTO `project_activity_logs` VALUES ('300', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-09 17:05:27');
INSERT INTO `project_activity_logs` VALUES ('301', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-09 17:20:40');
INSERT INTO `project_activity_logs` VALUES ('302', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-09 17:20:55');
INSERT INTO `project_activity_logs` VALUES ('303', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-09 17:21:21');
INSERT INTO `project_activity_logs` VALUES ('304', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:22:51');
INSERT INTO `project_activity_logs` VALUES ('305', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:25:00');
INSERT INTO `project_activity_logs` VALUES ('306', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:25:10');
INSERT INTO `project_activity_logs` VALUES ('307', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:27:24');
INSERT INTO `project_activity_logs` VALUES ('308', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:35:23');
INSERT INTO `project_activity_logs` VALUES ('309', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:36:01');
INSERT INTO `project_activity_logs` VALUES ('310', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:36:24');
INSERT INTO `project_activity_logs` VALUES ('311', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:37:03');
INSERT INTO `project_activity_logs` VALUES ('312', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:37:43');
INSERT INTO `project_activity_logs` VALUES ('313', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:38:16');
INSERT INTO `project_activity_logs` VALUES ('314', '1', 'tbl_sales', '1', 'update', '2018-04-09 17:38:26');
INSERT INTO `project_activity_logs` VALUES ('315', '1', 'mst_agents', '3', 'insert', '2018-04-09 17:47:53');
INSERT INTO `project_activity_logs` VALUES ('316', '1', 'mst_agents', '4', 'insert', '2018-04-09 17:48:20');
INSERT INTO `project_activity_logs` VALUES ('317', '1', 'mst_parties', '2', 'insert', '2018-04-09 17:48:20');
INSERT INTO `project_activity_logs` VALUES ('318', '1', 'tbl_payment_temporary', '1', 'update', '2018-04-09 17:48:29');
INSERT INTO `project_activity_logs` VALUES ('319', '1', 'tbl_payment', '1', 'update', '2018-04-09 17:52:59');
INSERT INTO `project_activity_logs` VALUES ('320', '1', 'tbl_payment', '1', 'update', '2018-04-09 18:18:21');
INSERT INTO `project_activity_logs` VALUES ('321', '1', 'mst_agents', '5', 'insert', '2018-04-10 09:47:18');
INSERT INTO `project_activity_logs` VALUES ('322', '1', 'mst_agents', '6', 'insert', '2018-04-10 09:49:15');
INSERT INTO `project_activity_logs` VALUES ('323', '1', 'mst_agents', '7', 'insert', '2018-04-10 09:50:11');
INSERT INTO `project_activity_logs` VALUES ('324', '1', 'mst_agents', '8', 'insert', '2018-04-10 09:56:09');
INSERT INTO `project_activity_logs` VALUES ('325', '1', 'mst_agents', '9', 'insert', '2018-04-10 09:56:54');
INSERT INTO `project_activity_logs` VALUES ('326', '1', 'mst_agents', '10', 'insert', '2018-04-10 09:57:06');
INSERT INTO `project_activity_logs` VALUES ('327', '1', 'mst_agents', '11', 'insert', '2018-04-10 09:58:22');
INSERT INTO `project_activity_logs` VALUES ('328', '1', 'mst_agents', '12', 'insert', '2018-04-10 09:59:27');
INSERT INTO `project_activity_logs` VALUES ('329', '1', 'mst_agents', '13', 'insert', '2018-04-10 10:00:11');
INSERT INTO `project_activity_logs` VALUES ('330', '1', 'mst_agents', '14', 'insert', '2018-04-10 10:03:04');
INSERT INTO `project_activity_logs` VALUES ('331', '1', 'mst_agents', '15', 'insert', '2018-04-10 10:03:14');
INSERT INTO `project_activity_logs` VALUES ('332', '1', 'mst_agents', '16', 'insert', '2018-04-10 10:04:04');
INSERT INTO `project_activity_logs` VALUES ('333', '1', 'mst_agents', '17', 'insert', '2018-04-10 10:04:24');
INSERT INTO `project_activity_logs` VALUES ('334', '1', 'mst_agents', '18', 'insert', '2018-04-10 10:04:35');
INSERT INTO `project_activity_logs` VALUES ('335', '1', 'mst_agents', '19', 'insert', '2018-04-10 10:04:47');
INSERT INTO `project_activity_logs` VALUES ('336', '1', 'mst_agents', '20', 'insert', '2018-04-10 10:04:53');
INSERT INTO `project_activity_logs` VALUES ('337', '1', 'mst_agents', '21', 'insert', '2018-04-10 10:05:28');
INSERT INTO `project_activity_logs` VALUES ('338', '1', 'mst_agents', '22', 'insert', '2018-04-10 10:06:13');
INSERT INTO `project_activity_logs` VALUES ('339', '1', 'mst_agents', '23', 'insert', '2018-04-10 10:15:13');
INSERT INTO `project_activity_logs` VALUES ('340', '1', 'mst_agents', '23', 'update', '2018-04-10 10:15:15');
INSERT INTO `project_activity_logs` VALUES ('341', '1', 'mst_agents', '23', 'update', '2018-04-10 10:38:16');
INSERT INTO `project_activity_logs` VALUES ('342', '1', 'mst_agents', '24', 'insert', '2018-04-10 11:09:15');
INSERT INTO `project_activity_logs` VALUES ('343', '1', 'mst_agents', '25', 'insert', '2018-04-10 11:09:16');
INSERT INTO `project_activity_logs` VALUES ('344', '1', 'mst_agents', '26', 'insert', '2018-04-10 11:09:16');
INSERT INTO `project_activity_logs` VALUES ('345', '1', 'mst_agents', '27', 'insert', '2018-04-10 11:09:16');
INSERT INTO `project_activity_logs` VALUES ('346', '1', 'mst_parties', '3', 'insert', '2018-04-10 11:09:17');
INSERT INTO `project_activity_logs` VALUES ('347', '1', 'mst_parties', '4', 'insert', '2018-04-10 11:09:17');
INSERT INTO `project_activity_logs` VALUES ('348', '1', 'mst_parties', '5', 'insert', '2018-04-10 11:09:17');
INSERT INTO `project_activity_logs` VALUES ('349', '1', 'mst_parties', '6', 'insert', '2018-04-10 11:09:17');
INSERT INTO `project_activity_logs` VALUES ('350', '1', 'mst_parties', '7', 'insert', '2018-04-10 11:09:17');
INSERT INTO `project_activity_logs` VALUES ('351', '1', 'mst_parties', '8', 'insert', '2018-04-10 11:09:18');
INSERT INTO `project_activity_logs` VALUES ('352', '1', 'mst_brands', '4', 'insert', '2018-04-10 11:27:26');
INSERT INTO `project_activity_logs` VALUES ('353', '1', 'mst_nepali_month', '3', 'update', '2018-04-10 11:28:27');
INSERT INTO `project_activity_logs` VALUES ('354', '1', 'tbl_sales', '2', 'update', '2018-04-10 11:32:25');
INSERT INTO `project_activity_logs` VALUES ('355', '1', 'tbl_sales', '3', 'update', '2018-04-10 11:32:26');
INSERT INTO `project_activity_logs` VALUES ('356', '1', 'tbl_sales', '4', 'update', '2018-04-10 11:32:26');
INSERT INTO `project_activity_logs` VALUES ('357', '1', 'mst_agents', '28', 'insert', '2018-04-10 12:01:53');
INSERT INTO `project_activity_logs` VALUES ('358', '1', 'mst_agents', '29', 'insert', '2018-04-10 12:01:54');
INSERT INTO `project_activity_logs` VALUES ('359', '1', 'mst_parties', '9', 'insert', '2018-04-10 12:01:54');
INSERT INTO `project_activity_logs` VALUES ('360', '1', 'mst_parties', '10', 'insert', '2018-04-10 12:01:54');
INSERT INTO `project_activity_logs` VALUES ('361', '1', 'mst_parties', '11', 'insert', '2018-04-10 12:01:54');
INSERT INTO `project_activity_logs` VALUES ('362', '1', 'mst_agents', '30', 'insert', '2018-04-10 12:02:07');
INSERT INTO `project_activity_logs` VALUES ('363', '1', 'mst_agents', '31', 'insert', '2018-04-10 12:02:08');
INSERT INTO `project_activity_logs` VALUES ('364', '1', 'mst_parties', '12', 'insert', '2018-04-10 12:02:09');
INSERT INTO `project_activity_logs` VALUES ('365', '1', 'mst_parties', '13', 'insert', '2018-04-10 12:02:09');
INSERT INTO `project_activity_logs` VALUES ('366', '1', 'mst_parties', '14', 'insert', '2018-04-10 12:02:10');
INSERT INTO `project_activity_logs` VALUES ('367', '1', 'mst_agents', '32', 'insert', '2018-04-10 12:02:19');
INSERT INTO `project_activity_logs` VALUES ('368', '1', 'mst_agents', '33', 'insert', '2018-04-10 12:02:20');
INSERT INTO `project_activity_logs` VALUES ('369', '1', 'mst_parties', '15', 'insert', '2018-04-10 12:02:20');
INSERT INTO `project_activity_logs` VALUES ('370', '1', 'mst_parties', '16', 'insert', '2018-04-10 12:02:20');
INSERT INTO `project_activity_logs` VALUES ('371', '1', 'mst_parties', '17', 'insert', '2018-04-10 12:02:20');
INSERT INTO `project_activity_logs` VALUES ('372', '1', 'mst_parties', '18', 'insert', '2018-04-10 12:02:20');
INSERT INTO `project_activity_logs` VALUES ('373', '1', 'mst_agents', '1', 'insert', '2018-04-10 12:12:39');
INSERT INTO `project_activity_logs` VALUES ('374', '1', 'mst_agents', '1', 'insert', '2018-04-10 12:13:49');
INSERT INTO `project_activity_logs` VALUES ('375', '1', 'mst_agents', '2', 'insert', '2018-04-10 12:13:49');
INSERT INTO `project_activity_logs` VALUES ('376', '1', 'mst_agents', '3', 'insert', '2018-04-10 12:13:50');
INSERT INTO `project_activity_logs` VALUES ('377', '1', 'mst_agents', '4', 'insert', '2018-04-10 12:13:50');
INSERT INTO `project_activity_logs` VALUES ('378', '1', 'mst_agents', '5', 'insert', '2018-04-10 12:13:50');
INSERT INTO `project_activity_logs` VALUES ('379', '1', 'mst_parties', '1', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('380', '1', 'mst_parties', '2', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('381', '1', 'mst_parties', '3', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('382', '1', 'mst_parties', '4', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('383', '1', 'mst_parties', '5', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('384', '1', 'mst_parties', '6', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('385', '1', 'mst_parties', '7', 'insert', '2018-04-10 12:13:51');
INSERT INTO `project_activity_logs` VALUES ('386', '1', 'mst_agents', '6', 'insert', '2018-04-10 12:15:48');
INSERT INTO `project_activity_logs` VALUES ('387', '1', 'mst_agents', '7', 'insert', '2018-04-10 12:15:49');
INSERT INTO `project_activity_logs` VALUES ('388', '1', 'mst_agents', '8', 'insert', '2018-04-10 12:15:49');
INSERT INTO `project_activity_logs` VALUES ('389', '1', 'mst_agents', '9', 'insert', '2018-04-10 12:15:50');
INSERT INTO `project_activity_logs` VALUES ('390', '1', 'mst_parties', '8', 'insert', '2018-04-10 12:15:50');
INSERT INTO `project_activity_logs` VALUES ('391', '1', 'mst_parties', '9', 'insert', '2018-04-10 12:15:50');
INSERT INTO `project_activity_logs` VALUES ('392', '1', 'mst_parties', '10', 'insert', '2018-04-10 12:15:51');
INSERT INTO `project_activity_logs` VALUES ('393', '1', 'mst_parties', '11', 'insert', '2018-04-10 12:15:51');
INSERT INTO `project_activity_logs` VALUES ('394', '1', 'mst_parties', '12', 'insert', '2018-04-10 12:15:51');
INSERT INTO `project_activity_logs` VALUES ('395', '1', 'mst_parties', '13', 'insert', '2018-04-10 12:15:51');
INSERT INTO `project_activity_logs` VALUES ('396', '1', 'mst_parties', '14', 'insert', '2018-04-10 12:15:51');
INSERT INTO `project_activity_logs` VALUES ('397', '1', 'mst_monthly_target', '1', 'insert', '2018-04-10 12:18:16');
INSERT INTO `project_activity_logs` VALUES ('398', '1', 'mst_monthly_target', '2', 'insert', '2018-04-10 12:18:29');
INSERT INTO `project_activity_logs` VALUES ('399', '1', 'mst_monthly_target', '3', 'insert', '2018-04-10 12:18:41');
INSERT INTO `project_activity_logs` VALUES ('400', '1', 'mst_schemes', '1', 'insert', '2018-04-10 12:18:59');
INSERT INTO `project_activity_logs` VALUES ('401', '1', 'mst_schemes', '2', 'insert', '2018-04-10 12:19:21');
INSERT INTO `project_activity_logs` VALUES ('402', '1', 'mst_schemes', '3', 'insert', '2018-04-10 12:19:34');
INSERT INTO `project_activity_logs` VALUES ('403', '1', 'mst_schemes', '4', 'insert', '2018-04-10 12:23:03');
INSERT INTO `project_activity_logs` VALUES ('404', '1', 'mst_monthly_target', '4', 'insert', '2018-04-10 12:23:36');

-- ----------------------------
-- Table structure for project_audit_logs
-- ----------------------------
DROP TABLE IF EXISTS `project_audit_logs`;
CREATE TABLE `project_audit_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `table_pk` int(11) unsigned NOT NULL,
  `column_name` varchar(255) DEFAULT NULL,
  `old_value` varchar(1000) DEFAULT NULL,
  `new_value` varchar(1000) DEFAULT NULL,
  `action_dttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_audit_logs
-- ----------------------------
INSERT INTO `project_audit_logs` VALUES ('1', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:56:52');
INSERT INTO `project_audit_logs` VALUES ('2', '1', 'mst_agents', '1', 'phone', '9803163986', '01-4283184', '2018-03-26 11:56:52');
INSERT INTO `project_audit_logs` VALUES ('3', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:57:07');
INSERT INTO `project_audit_logs` VALUES ('4', '1', 'mst_agents', '1', 'email', 'sanish@pagodalabs.com', 'crazy.san8@gmail.com', '2018-03-26 11:57:07');
INSERT INTO `project_audit_logs` VALUES ('5', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:57:30');
INSERT INTO `project_audit_logs` VALUES ('6', '1', 'mst_agents', '1', 'email', 'crazy.san8@gmail.com', 'sanish@pagodalabs.com', '2018-03-26 11:57:30');
INSERT INTO `project_audit_logs` VALUES ('7', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:58:17');
INSERT INTO `project_audit_logs` VALUES ('8', '1', 'mst_agents', '1', 'email', 'sanish@pagodalabs.com', 'crazy.san8@gmail.com', '2018-03-26 11:58:17');
INSERT INTO `project_audit_logs` VALUES ('9', '1', 'mst_agents', '1', 'user_id', '0', null, '2018-03-26 14:14:21');
INSERT INTO `project_audit_logs` VALUES ('10', '1', 'mst_agents', '1', 'district', '47', null, '2018-03-26 14:14:21');
INSERT INTO `project_audit_logs` VALUES ('11', '1', 'mst_agents', '1', 'mun_vdc', '1623', null, '2018-03-26 14:14:21');
INSERT INTO `project_audit_logs` VALUES ('12', '1', 'mst_agents', '1', 'email', 'crazy.san8@gmail.com', 'sanish@pagodalabs.com', '2018-03-26 14:39:20');
INSERT INTO `project_audit_logs` VALUES ('13', '1', 'mst_agents', '1', 'district_id', null, '25', '2018-03-26 15:57:15');
INSERT INTO `project_audit_logs` VALUES ('14', '1', 'mst_agents', '1', 'mun_vdc_id', null, '1004', '2018-03-26 15:57:15');
INSERT INTO `project_audit_logs` VALUES ('15', '1', 'mst_agents', '1', 'province_id', null, '2', '2018-03-26 15:57:15');
INSERT INTO `project_audit_logs` VALUES ('16', '1', 'mst_nepali_month', '1', 'rank', '1', '2', '2018-03-27 17:41:10');
INSERT INTO `project_audit_logs` VALUES ('17', '1', 'mst_nepali_month', '1', 'rank', '2', '1', '2018-03-27 17:41:14');
INSERT INTO `project_audit_logs` VALUES ('18', '1', 'mst_agents', '44', 'district_id', null, '40', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('19', '1', 'mst_agents', '44', 'mun_vdc_id', null, '1923', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('20', '1', 'mst_agents', '44', 'city', null, 'Rautahat', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('21', '1', 'mst_agents', '44', 'address', null, 'Rautahat', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('22', '1', 'mst_agents', '44', 'phone', null, '9803163986', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('23', '1', 'mst_agents', '44', 'mobile', null, '9803163986', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('24', '1', 'mst_agents', '44', 'email', null, 'sanish@pagodalabs.com', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('25', '1', 'mst_agents', '44', 'province_id', null, '2', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('26', '1', 'tbl_sales', '12', 'is_published', '0', '1', '2018-04-02 16:54:51');
INSERT INTO `project_audit_logs` VALUES ('27', '1', 'tbl_sales', '9', 'is_published', '0', '1', '2018-04-02 16:55:18');
INSERT INTO `project_audit_logs` VALUES ('28', '1', 'tbl_sales', '10', 'is_published', '0', '1', '2018-04-02 16:56:54');
INSERT INTO `project_audit_logs` VALUES ('29', '1', 'tbl_sales', '11', 'is_published', '0', '1', '2018-04-02 16:56:54');
INSERT INTO `project_audit_logs` VALUES ('30', '1', 'tbl_sales', '12', 'is_published', '0', '1', '2018-04-02 16:56:54');
INSERT INTO `project_audit_logs` VALUES ('31', '1', 'tbl_sales', '7', 'is_published', '0', '1', '2018-04-02 16:57:31');
INSERT INTO `project_audit_logs` VALUES ('32', '1', 'tbl_sales', '8', 'is_published', '0', '1', '2018-04-02 16:57:31');
INSERT INTO `project_audit_logs` VALUES ('33', '1', 'tbl_sales', '9', 'is_published', '0', '1', '2018-04-02 16:57:31');
INSERT INTO `project_audit_logs` VALUES ('34', '1', 'tbl_sales', '10', 'is_published', '0', '1', '2018-04-02 16:57:54');
INSERT INTO `project_audit_logs` VALUES ('35', '1', 'tbl_sales', '11', 'is_published', '0', '1', '2018-04-02 16:57:54');
INSERT INTO `project_audit_logs` VALUES ('36', '1', 'tbl_sales', '12', 'is_published', '0', '1', '2018-04-02 16:57:55');
INSERT INTO `project_audit_logs` VALUES ('37', '1', 'tbl_sales', '7', 'is_published', '0', '1', '2018-04-02 16:58:36');
INSERT INTO `project_audit_logs` VALUES ('38', '1', 'tbl_sales', '8', 'is_published', '0', '1', '2018-04-02 16:58:36');
INSERT INTO `project_audit_logs` VALUES ('39', '1', 'tbl_sales', '9', 'is_published', '0', '1', '2018-04-02 16:58:36');
INSERT INTO `project_audit_logs` VALUES ('40', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 12:06:42');
INSERT INTO `project_audit_logs` VALUES ('41', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:51:45');
INSERT INTO `project_audit_logs` VALUES ('42', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:53:54');
INSERT INTO `project_audit_logs` VALUES ('43', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:56:33');
INSERT INTO `project_audit_logs` VALUES ('44', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:57:40');
INSERT INTO `project_audit_logs` VALUES ('45', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:59:03');
INSERT INTO `project_audit_logs` VALUES ('46', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 15:00:02');
INSERT INTO `project_audit_logs` VALUES ('47', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 15:01:40');
INSERT INTO `project_audit_logs` VALUES ('48', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 15:19:51');
INSERT INTO `project_audit_logs` VALUES ('49', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 15:19:51');
INSERT INTO `project_audit_logs` VALUES ('50', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 15:20:53');
INSERT INTO `project_audit_logs` VALUES ('51', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 15:20:53');
INSERT INTO `project_audit_logs` VALUES ('52', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 17:54:35');
INSERT INTO `project_audit_logs` VALUES ('53', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 17:54:36');
INSERT INTO `project_audit_logs` VALUES ('54', '1', 'tbl_sales', '3', 'is_published', '0', '1', '2018-04-04 17:54:36');
INSERT INTO `project_audit_logs` VALUES ('55', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 17:55:50');
INSERT INTO `project_audit_logs` VALUES ('56', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 17:55:50');
INSERT INTO `project_audit_logs` VALUES ('57', '1', 'tbl_sales', '3', 'is_published', '0', '1', '2018-04-04 17:55:50');
INSERT INTO `project_audit_logs` VALUES ('58', '1', 'tbl_sales_temporary', '46', 'quantity', '420', '200', '2018-04-05 12:23:07');
INSERT INTO `project_audit_logs` VALUES ('59', '1', 'tbl_sales_temporary', '47', 'quantity', '420', '300', '2018-04-05 12:23:57');
INSERT INTO `project_audit_logs` VALUES ('60', '1', 'tbl_sales_temporary', '48', 'quantity', '420', '20000', '2018-04-05 12:24:05');
INSERT INTO `project_audit_logs` VALUES ('61', '1', 'tbl_sales_temporary', '1', 'rate', '577.37', '400', '2018-04-05 12:26:08');
INSERT INTO `project_audit_logs` VALUES ('62', '1', 'tbl_sales_temporary', '1', 'quantity', '420', '300', '2018-04-05 12:28:12');
INSERT INTO `project_audit_logs` VALUES ('63', '1', 'tbl_sales_temporary', '12', 'quantity', '420', '1', '2018-04-05 14:32:35');
INSERT INTO `project_audit_logs` VALUES ('64', '1', 'tbl_sales_temporary', '11', 'quantity', '420', '20', '2018-04-05 14:32:40');
INSERT INTO `project_audit_logs` VALUES ('65', '1', 'tbl_sales_temporary', '13', 'quantity', '420', '1', '2018-04-05 14:34:16');
INSERT INTO `project_audit_logs` VALUES ('66', '1', 'tbl_sales_temporary', '14', 'quantity', '420', '1', '2018-04-05 14:34:19');
INSERT INTO `project_audit_logs` VALUES ('67', '1', 'tbl_sales_temporary', '15', 'quantity', '420', '1', '2018-04-05 14:34:23');
INSERT INTO `project_audit_logs` VALUES ('68', '1', 'tbl_sales_temporary', '16', 'quantity', '420', '1', '2018-04-05 14:34:26');
INSERT INTO `project_audit_logs` VALUES ('69', '1', 'tbl_sales_temporary', '17', 'quantity', '420', '2', '2018-04-05 14:34:30');
INSERT INTO `project_audit_logs` VALUES ('70', '1', 'tbl_sales_temporary', '18', 'quantity', '420', '2', '2018-04-05 14:34:34');
INSERT INTO `project_audit_logs` VALUES ('71', '1', 'tbl_sales_temporary', '21', 'quantity', '420', '1', '2018-04-05 14:36:24');
INSERT INTO `project_audit_logs` VALUES ('72', '1', 'tbl_sales_temporary', '20', 'quantity', '420', '1', '2018-04-05 14:36:27');
INSERT INTO `project_audit_logs` VALUES ('73', '1', 'tbl_sales_temporary', '19', 'quantity', '420', '1', '2018-04-05 14:36:30');
INSERT INTO `project_audit_logs` VALUES ('74', '1', 'tbl_sales_temporary', '24', 'quantity', '420', '1', '2018-04-05 14:37:24');
INSERT INTO `project_audit_logs` VALUES ('75', '1', 'tbl_sales_temporary', '23', 'quantity', '420', '1', '2018-04-05 14:37:29');
INSERT INTO `project_audit_logs` VALUES ('76', '1', 'tbl_sales_temporary', '22', 'quantity', '420', '1', '2018-04-05 14:37:31');
INSERT INTO `project_audit_logs` VALUES ('77', '1', 'tbl_sales_temporary', '27', 'quantity', '420', '1', '2018-04-05 14:38:22');
INSERT INTO `project_audit_logs` VALUES ('78', '1', 'tbl_sales_temporary', '27', 'discount', '30', '1', '2018-04-05 14:38:28');
INSERT INTO `project_audit_logs` VALUES ('79', '1', 'tbl_sales_temporary', '30', 'quantity', '420', '1', '2018-04-05 14:39:49');
INSERT INTO `project_audit_logs` VALUES ('80', '1', 'tbl_sales_temporary', '33', 'quantity', '420', '1', '2018-04-05 14:40:53');
INSERT INTO `project_audit_logs` VALUES ('81', '1', 'tbl_sales_temporary', '36', 'quantity', '420', '1', '2018-04-05 14:41:25');
INSERT INTO `project_audit_logs` VALUES ('82', '1', 'tbl_sales_temporary', '39', 'quantity', '420', '1', '2018-04-05 14:42:29');
INSERT INTO `project_audit_logs` VALUES ('83', '1', 'tbl_sales_temporary', '38', 'quantity', '420', '1', '2018-04-05 14:42:32');
INSERT INTO `project_audit_logs` VALUES ('84', '1', 'tbl_sales_temporary', '37', 'quantity', '420', '1', '2018-04-05 14:42:36');
INSERT INTO `project_audit_logs` VALUES ('85', '1', 'tbl_sales', '43', 'is_published', '0', '1', '2018-04-05 14:45:26');
INSERT INTO `project_audit_logs` VALUES ('86', '1', 'tbl_sales', '44', 'is_published', '0', '1', '2018-04-05 14:45:26');
INSERT INTO `project_audit_logs` VALUES ('87', '1', 'tbl_sales', '45', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('88', '1', 'tbl_sales', '46', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('89', '1', 'tbl_sales', '47', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('90', '1', 'tbl_sales', '48', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('91', '1', 'tbl_sales', '49', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('92', '1', 'tbl_sales', '50', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('93', '1', 'tbl_sales', '51', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('94', '1', 'tbl_sales', '52', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('95', '1', 'tbl_sales', '53', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('96', '1', 'tbl_sales', '54', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('97', '1', 'tbl_sales', '55', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('98', '1', 'tbl_sales', '56', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('99', '1', 'tbl_sales', '57', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('100', '1', 'tbl_sales', '58', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('101', '1', 'tbl_sales', '59', 'is_published', '0', '1', '2018-04-05 14:45:30');
INSERT INTO `project_audit_logs` VALUES ('102', '1', 'tbl_sales', '60', 'is_published', '0', '1', '2018-04-05 14:45:30');
INSERT INTO `project_audit_logs` VALUES ('103', '1', 'tbl_sales_temporary', '3', 'quantity', '420', '2', '2018-04-05 14:54:34');
INSERT INTO `project_audit_logs` VALUES ('104', '1', 'tbl_sales_temporary', '1', 'rate', '577.37', '2000', '2018-04-05 14:54:40');
INSERT INTO `project_audit_logs` VALUES ('105', '1', 'tbl_sales_temporary', '1', 'quantity', '420', '100', '2018-04-05 14:54:45');
INSERT INTO `project_audit_logs` VALUES ('106', '1', 'tbl_sales_temporary', '12', 'quantity', '420', '10', '2018-04-05 14:57:52');
INSERT INTO `project_audit_logs` VALUES ('107', '1', 'tbl_sales_temporary', '11', 'quantity', '420', '20', '2018-04-05 14:57:56');
INSERT INTO `project_audit_logs` VALUES ('108', '1', 'tbl_sales_temporary', '10', 'rate', '577.37', '100', '2018-04-05 14:57:59');
INSERT INTO `project_audit_logs` VALUES ('109', '1', 'tbl_sales_temporary', '1', 'quantity', '420', '100', '2018-04-05 15:05:30');
INSERT INTO `project_audit_logs` VALUES ('110', '1', 'tbl_sales_temporary', '14', 'quantity', '330', '200', '2018-04-05 15:15:26');
INSERT INTO `project_audit_logs` VALUES ('111', '1', 'tbl_sales_temporary', '1', 'quantity', '330', '10', '2018-04-05 15:17:50');
INSERT INTO `project_audit_logs` VALUES ('112', '1', 'tbl_sales_temporary', '2', 'quantity', '330', '100', '2018-04-05 15:18:38');
INSERT INTO `project_audit_logs` VALUES ('113', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-05 15:18:59');
INSERT INTO `project_audit_logs` VALUES ('114', '1', 'tbl_payment_temporary', '5', 'amount', '100000', '200', '2018-04-05 15:35:12');
INSERT INTO `project_audit_logs` VALUES ('115', '1', 'tbl_payment_temporary', '8', 'amount', '100000', '900', '2018-04-05 15:38:47');
INSERT INTO `project_audit_logs` VALUES ('116', '1', 'mst_schemes', '1', 'amount', '20000', '200000', '2018-04-09 15:52:54');
INSERT INTO `project_audit_logs` VALUES ('117', '1', 'mst_schemes', '1', 'scheme_type', 'Lump Sum', 'Bag Number', '2018-04-09 15:52:54');
INSERT INTO `project_audit_logs` VALUES ('118', '1', 'mst_schemes', '1', 'nepali_month_id', '2', '1', '2018-04-09 15:52:54');
INSERT INTO `project_audit_logs` VALUES ('119', '1', 'mst_schemes', '1', 'bag_count', '0', '100', '2018-04-09 15:52:54');
INSERT INTO `project_audit_logs` VALUES ('120', '1', 'mst_schemes', '1', 'price', '0', '2000', '2018-04-09 15:52:54');
INSERT INTO `project_audit_logs` VALUES ('121', '1', 'mst_schemes', '1', 'amount', '200000', '2', '2018-04-09 16:01:40');
INSERT INTO `project_audit_logs` VALUES ('122', '1', 'mst_schemes', '1', 'scheme_type', 'Bag Number', 'Lump Sum', '2018-04-09 16:01:40');
INSERT INTO `project_audit_logs` VALUES ('123', '1', 'mst_schemes', '2', 'amount', '200000', '1', '2018-04-09 16:10:38');
INSERT INTO `project_audit_logs` VALUES ('124', '1', 'mst_schemes', '2', 'scheme_type', 'Lump Sum', 'Bag Number', '2018-04-09 16:10:38');
INSERT INTO `project_audit_logs` VALUES ('125', '1', 'mst_schemes', '2', 'bag_count', '0', '1', '2018-04-09 16:10:38');
INSERT INTO `project_audit_logs` VALUES ('126', '1', 'mst_schemes', '2', 'price', '0', '1', '2018-04-09 16:10:38');
INSERT INTO `project_audit_logs` VALUES ('127', '1', 'tbl_sales_temporary', '1', 'quantity', '330', '100', '2018-04-09 17:05:27');
INSERT INTO `project_audit_logs` VALUES ('128', '1', 'tbl_sales_temporary', '1', 'quantity', '100', '10', '2018-04-09 17:20:40');
INSERT INTO `project_audit_logs` VALUES ('129', '1', 'tbl_sales_temporary', '1', 'quantity', '10', '20', '2018-04-09 17:20:55');
INSERT INTO `project_audit_logs` VALUES ('130', '1', 'tbl_sales_temporary', '1', 'quantity', '20', '10', '2018-04-09 17:21:21');
INSERT INTO `project_audit_logs` VALUES ('131', '1', 'tbl_sales', '1', 'quantity', '100', '20', '2018-04-09 17:22:51');
INSERT INTO `project_audit_logs` VALUES ('132', '1', 'tbl_sales', '1', 'quantity', '20', '70', '2018-04-09 17:25:00');
INSERT INTO `project_audit_logs` VALUES ('133', '1', 'tbl_sales', '1', 'rate', '1577.37', '20', '2018-04-09 17:25:10');
INSERT INTO `project_audit_logs` VALUES ('134', '1', 'tbl_sales', '1', 'quantity', '70', '10', '2018-04-09 17:27:24');
INSERT INTO `project_audit_logs` VALUES ('135', '1', 'tbl_sales', '1', 'quantity', '10', '20', '2018-04-09 17:35:23');
INSERT INTO `project_audit_logs` VALUES ('136', '1', 'tbl_sales', '1', 'gross_total', '157737', '400', '2018-04-09 17:35:23');
INSERT INTO `project_audit_logs` VALUES ('137', '1', 'tbl_sales', '1', 'rate', '20', '300', '2018-04-09 17:36:01');
INSERT INTO `project_audit_logs` VALUES ('138', '1', 'tbl_sales', '1', 'gross_total', '400', '6000', '2018-04-09 17:36:01');
INSERT INTO `project_audit_logs` VALUES ('139', '1', 'tbl_sales', '1', 'rate', '300', '30', '2018-04-09 17:36:24');
INSERT INTO `project_audit_logs` VALUES ('140', '1', 'tbl_sales', '1', 'gross_total', '6000', '600', '2018-04-09 17:36:24');
INSERT INTO `project_audit_logs` VALUES ('141', '1', 'tbl_sales', '1', 'discount', '20', '30', '2018-04-09 17:37:03');
INSERT INTO `project_audit_logs` VALUES ('142', '1', 'tbl_sales', '1', 'rate', '30', '100', '2018-04-09 17:37:43');
INSERT INTO `project_audit_logs` VALUES ('143', '1', 'tbl_sales', '1', 'gross_total', '600', '2000', '2018-04-09 17:37:43');
INSERT INTO `project_audit_logs` VALUES ('144', '1', 'tbl_sales', '1', 'discount', '30', '50', '2018-04-09 17:38:16');
INSERT INTO `project_audit_logs` VALUES ('145', '1', 'tbl_sales', '1', 'quantity', '20', '100', '2018-04-09 17:38:26');
INSERT INTO `project_audit_logs` VALUES ('146', '1', 'tbl_sales', '1', 'gross_total', '2000', '10000', '2018-04-09 17:38:26');
INSERT INTO `project_audit_logs` VALUES ('147', '1', 'tbl_payment_temporary', '1', 'amount', '100000', '20', '2018-04-09 17:48:29');
INSERT INTO `project_audit_logs` VALUES ('148', '1', 'tbl_payment', '1', 'amount', '20', '20000', '2018-04-09 17:52:59');
INSERT INTO `project_audit_logs` VALUES ('149', '1', 'tbl_payment', '1', 'amount', '20000', '100', '2018-04-09 18:18:21');
INSERT INTO `project_audit_logs` VALUES ('150', '1', 'mst_agents', '23', 'user_id', null, '116', '2018-04-10 10:15:15');
INSERT INTO `project_audit_logs` VALUES ('151', '1', 'mst_agents', '23', 'opening_amount', null, '200', '2018-04-10 10:38:16');
INSERT INTO `project_audit_logs` VALUES ('152', '1', 'mst_nepali_month', '3', 'name', 'Ashoj', 'Ashwin', '2018-04-10 11:28:27');
INSERT INTO `project_audit_logs` VALUES ('153', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-10 11:32:25');
INSERT INTO `project_audit_logs` VALUES ('154', '1', 'tbl_sales', '3', 'is_published', '0', '1', '2018-04-10 11:32:26');
INSERT INTO `project_audit_logs` VALUES ('155', '1', 'tbl_sales', '4', 'is_published', '0', '1', '2018-04-10 11:32:26');

-- ----------------------------
-- Table structure for project_migrations
-- ----------------------------
DROP TABLE IF EXISTS `project_migrations`;
CREATE TABLE `project_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_migrations
-- ----------------------------
INSERT INTO `project_migrations` VALUES ('24');

-- ----------------------------
-- Table structure for project_sessions
-- ----------------------------
DROP TABLE IF EXISTS `project_sessions`;
CREATE TABLE `project_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_sessions
-- ----------------------------
INSERT INTO `project_sessions` VALUES ('17aa1de7e47c872aa14922f1fe9752cad98b53d2', '::1', '1523330930', '__ci_last_regenerate|i:1523330928;');
INSERT INTO `project_sessions` VALUES ('18f14a8a1f7a42257ab0b1093797a355d7f25948', '::1', '1523339250', '__ci_last_regenerate|i:1523339201;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('21fb7f5553e5d40a9af033dd588872424edeb85b', '::1', '1523341051', '__ci_last_regenerate|i:1523340995;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('25c27fffabe68b007f51a0124b083bac6977d1d1', '::1', '1523361998', '__ci_last_regenerate|i:1523361713;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('261250906b38c89ca38d0178a92b552bb9120afc', '::1', '1523362584', '__ci_last_regenerate|i:1523362346;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('26c97da9db6f4db4f1ba8002421ee2985577c4ec', '::1', '1523360811', '__ci_last_regenerate|i:1523360512;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('27ca3e412e77dcf86d2b07693b633b6b98ccecec', '::1', '1523341981', '__ci_last_regenerate|i:1523341977;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('2a3fb44bfa2a57413f29cfc7b0d772d4e4313a78', '::1', '1523359823', '__ci_last_regenerate|i:1523359531;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('2af188cbbecad5b9dcc5d22b81c2d86585dd5c13', '::1', '1523334647', '__ci_last_regenerate|i:1523334010;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('2b309f458ab2640f90503088d225a0641fc7fc48', '::1', '1523334843', '__ci_last_regenerate|i:1523334683;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('3e0ea4c209b3b3d88ea2d0938d25e39f716aeb25', '::1', '1523361000', '__ci_last_regenerate|i:1523360932;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('3e5a59731293951fe7ce4a0b876ae945db601d9d', '::1', '1523341861', '__ci_last_regenerate|i:1523341631;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('489c290f586b977056b5ce4d7cea715409c56e9e', '::1', '1523338208', '__ci_last_regenerate|i:1523338204;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('48e01533e1341f4481ec75a9ea8893acb4583c4a', '::1', '1523335389', '__ci_last_regenerate|i:1523335198;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('4d2baeb4688b9caacaa65260b674b46bffb8e25a', '::1', '1523348546', '__ci_last_regenerate|i:1523348536;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('75467027e3cf65c53e7f2e57cb80f2e2bde88623', '::1', '1523359310', '__ci_last_regenerate|i:1523359082;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('7e5d77d7909746de3e9a1c44313f6ba6e428b5f6', '::1', '1523331103', '__ci_last_regenerate|i:1523330928;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('7e7d1c7543e1affa49be8ae301a4d88d9657283a', '::1', '1523360177', '__ci_last_regenerate|i:1523359890;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('85cdc52ae6f93ab6fd6a2846e190042e5bf16c5c', '::1', '1523332028', '__ci_last_regenerate|i:1523332026;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('90489e034d2226b682b14e66e1d6a3c133136043', '::1', '1523331936', '__ci_last_regenerate|i:1523331833;id|s:3:\"113\";username|s:12:\"sales.person\";email|s:17:\"test@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('9868210688533d6c45ac6be56eda05da4dbbace5', '::1', '1523342359', '__ci_last_regenerate|i:1523342356;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('9b2a2a00c48aabe9ecb98b0ecce922f557ab02c5', '::1', '1523333993', '__ci_last_regenerate|i:1523333688;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('a8a16e402d5ffbbf390de099cd40cae376440db7', '::1', '1523337858', '__ci_last_regenerate|i:1523337579;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('aff33eb5315f90632eeeae775f057eca7fb0babc', '::1', '1523358530', '__ci_last_regenerate|i:1523358440;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('b24c203706c95abdab5d3db84ee7c82ca46673dd', '::1', '1523342316', '__ci_last_regenerate|i:1523341975;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('bffc7e66ce3afc423478e756abbeb0a36e566a89', '::1', '1523339132', '__ci_last_regenerate|i:1523338891;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('d5fcaf19df76706bc63c8178169a03a1c7d36320', '::1', '1523358925', '__ci_last_regenerate|i:1523358776;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('e0e750cc5be368d3e8d06fd3a35771989cd87968', '::1', '1523362337', '__ci_last_regenerate|i:1523362037;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('e42c367ed4afc2ffed0d4609fc9874ff3930a6bf', '::1', '1523333667', '__ci_last_regenerate|i:1523332626;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('e5876edf0e8e1ad6caf2682d6a722ad7f689462f', '::1', '1523336008', '__ci_last_regenerate|i:1523335931;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('eacc4297e4dc6071a3cbc1dc1dc26b641c3e1549', '::1', '1523361575', '__ci_last_regenerate|i:1523361278;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('ef5a25526fa70a9b9a5dab090e609b3bf6024fcd', '::1', '1523360504', '__ci_last_regenerate|i:1523360204;id|s:3:\"101\";username|s:10:\"prashantji\";email|s:23:\"prashantji@reliance.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('f38a94f3034bbe5fac3fc1f258eaa15afa397c55', '::1', '1523331813', '__ci_last_regenerate|i:1523331301;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('f811990f00dd912a957ce97779e97c2ced0d78b0', '::1', '1523358759', '__ci_last_regenerate|i:1523358751;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');

-- ----------------------------
-- Table structure for tbl_payment
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payment`;
CREATE TABLE `tbl_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_date_np` varchar(255) DEFAULT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `amount` double(53,2) DEFAULT NULL,
  `is_published` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_payment
-- ----------------------------
INSERT INTO `tbl_payment` VALUES ('1', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-08-16', '2074-04-32', '1', 'Himalayan Bank Ltd-01904858550017', '1', '14', '300000.00', '0');
INSERT INTO `tbl_payment` VALUES ('2', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-08-16', '2074-04-32', '1', 'Himalayan Bank Ltd-01904858550017', '9', '6', '500000.00', '0');
INSERT INTO `tbl_payment` VALUES ('3', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-08-16', '2074-04-32', '1', 'Himalayan Bank Ltd-01904858550017', '8', '12', '150000.00', '0');
INSERT INTO `tbl_payment` VALUES ('4', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-08-15', '2074-04-31', '1', 'Global IME Bank Ltd. (7501010000996)', '7', '11', '50000.00', '0');
INSERT INTO `tbl_payment` VALUES ('5', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-08-15', '2074-04-31', '1', 'Global IME Bank Ltd. (7501010000996)', '7', '10', '30000.00', '0');
INSERT INTO `tbl_payment` VALUES ('6', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-07-16', '2074-04-01', '1', 'Nepal Bank Ltd-Ktm', '7', '9', '40000.00', '0');
INSERT INTO `tbl_payment` VALUES ('7', '1', '1', null, '2018-04-10 12:15:57', '2018-04-10 12:15:57', null, '1', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '6', '8', '100000.00', '0');

-- ----------------------------
-- Table structure for tbl_payment_temporary
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payment_temporary`;
CREATE TABLE `tbl_payment_temporary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_date_np` varchar(255) NOT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `bank_name` varchar(255) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `is_processed` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_payment_temporary
-- ----------------------------
INSERT INTO `tbl_payment_temporary` VALUES ('1', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '6', '8', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('2', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-07-16', '2074-04-01', '1', 'Nepal Bank Ltd-Ktm', '7', '9', '40000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('3', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-08-15', '2074-04-31', '1', 'Global IME Bank Ltd. (7501010000996)', '7', '10', '30000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('4', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-08-15', '2074-04-31', '1', 'Global IME Bank Ltd. (7501010000996)', '7', '11', '50000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('5', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-08-16', '2074-04-32', '1', 'Himalayan Bank Ltd-01904858550017', '8', '12', '150000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('6', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-08-16', '2074-04-32', '1', 'Himalayan Bank Ltd-01904858550017', '9', '13', '500000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('7', '1', '1', null, '2018-04-10 12:15:51', '2018-04-10 12:15:51', null, '1', '2017-08-16', '2074-04-32', '1', 'Himalayan Bank Ltd-01904858550017', '1', '14', '300000', '1');

-- ----------------------------
-- Table structure for tbl_sales
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sales`;
CREATE TABLE `tbl_sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_date_np` varchar(255) DEFAULT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `truck_number` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `gross_total` double(53,2) DEFAULT NULL,
  `is_published` int(11) DEFAULT '0',
  `agent_name` varchar(255) DEFAULT NULL,
  `total_pre_discount` double DEFAULT NULL,
  `total_discount` double DEFAULT NULL,
  `total_excise` double DEFAULT NULL,
  `total_vat` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sales
-- ----------------------------
INSERT INTO `tbl_sales` VALUES ('1', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.920', '2017-09-23', '2074-06-07', '3', '4', '7', 'Na6ta491', '1', '4', '40', '654.72', '0', '26188.80', '0', 'Ramesh Sah-Imlipatti', '26188.8', '0', '360', '3451.344', '30000.144');
INSERT INTO `tbl_sales` VALUES ('2', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.919', '2017-09-23', '2074-06-07', '3', '5', '6', 'Na6kh2802', '1', '4', '210', '667.02', '30', '140074.20', '0', 'Chandanji', '140074.2', '6300', '1890', '17636.346', '153300.546');
INSERT INTO `tbl_sales` VALUES ('3', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.919', '2017-09-23', '2074-06-07', '3', '5', '6', 'Na6kh2802', '2', '1', '210', '569.67', '30', '119630.70', '0', 'Chandanji', '119630.7', '6300', '1890', '14978.691', '130199.391');
INSERT INTO `tbl_sales` VALUES ('4', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.3', '2017-07-16', '2074-04-01', '1', '6', '8', 'Na5ta7685', '1', '2', '100', '684.72', '30', '68472.00', '0', 'Sureshji', '68472', '3000', '900', '8628.36', '75000.36');
INSERT INTO `tbl_sales` VALUES ('5', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.2', '2017-07-16', '2074-04-01', '1', '3', '4', 'Na5kh9167', '2', '1', '420', '587.37', '30', '246695.40', '0', 'Dineshji', '246695.4', '12600', '3780', '30923.802', '268799.202');
INSERT INTO `tbl_sales` VALUES ('6', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.1', '2017-07-16', '2074-04-01', '1', '2', '3', 'Na5kh3040', '2', '1', '420', '577.37', '20', '242495.40', '0', 'Vishwanathji', '242495.4', '8400', '3780', '30923.802', '268799.202');
INSERT INTO `tbl_sales` VALUES ('7', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.518', '2017-08-17', '2074-05-01', '2', '1', '2', 'Na5kh8581', '2', '1', '300', '568.52', '20', '170556.00', '0', 'Prashantji', '170556', '6000', '2700', '21743.28', '188999.28');
INSERT INTO `tbl_sales` VALUES ('8', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.518', '2017-08-17', '2074-05-01', '2', '1', '2', 'Na5kh8581', '1', '4', '120', '665.87', '20', '79904.40', '0', 'Prashantji', '79904.4', '2400', '1080', '10215.972', '88800.372');
INSERT INTO `tbl_sales` VALUES ('9', '1', '1', null, '2018-04-10 12:14:02', '2018-04-10 12:14:02', null, '1', 'INVOICE NO.516', '2017-08-17', '2074-05-01', '2', '1', '1', 'Na3kh9464', '1', '4', '23', '8500', '0', '195500.00', '0', 'Advance Marketing Pvt.Ltd.-Jeetpur', '191420', '0', '0', '24884.6', '191420');

-- ----------------------------
-- Table structure for tbl_sales_temporary
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sales_temporary`;
CREATE TABLE `tbl_sales_temporary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_date_np` varchar(255) DEFAULT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `truck_number` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `gross_total` double DEFAULT NULL,
  `is_processed` int(11) DEFAULT '0',
  `agent_name` varchar(255) DEFAULT NULL,
  `total_pre_discount` double DEFAULT NULL,
  `total_discount` double DEFAULT NULL,
  `total_excise` double DEFAULT NULL,
  `total_vat` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sales_temporary
-- ----------------------------
INSERT INTO `tbl_sales_temporary` VALUES ('1', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.516', '2017-08-17', '2074-05-01', '2', '1', '1', 'Na3kh9464', '1', '4', '23', '8500', '0', '216304.6', '1', 'Advance Marketing Pvt.Ltd.-Jeetpur', '191420', '0', '0', '24884.6', '191420');
INSERT INTO `tbl_sales_temporary` VALUES ('2', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.518', '2017-08-17', '2074-05-01', '2', '1', '2', 'Na5kh8581', '1', '4', '120', '665.87', '20', '88800.372', '1', 'Prashantji', '79904.4', '2400', '1080', '10215.972', '88800.372');
INSERT INTO `tbl_sales_temporary` VALUES ('3', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.518', '2017-08-17', '2074-05-01', '2', '1', '2', 'Na5kh8581', '2', '1', '300', '568.52', '20', '188999.28', '1', 'Prashantji', '170556', '6000', '2700', '21743.28', '188999.28');
INSERT INTO `tbl_sales_temporary` VALUES ('4', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.1', '2017-07-16', '2074-04-01', '1', '2', '3', 'Na5kh3040', '2', '1', '420', '577.37', '20', '268799.202', '1', 'Vishwanathji', '242495.4', '8400', '3780', '30923.802', '268799.202');
INSERT INTO `tbl_sales_temporary` VALUES ('5', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.2', '2017-07-16', '2074-04-01', '1', '3', '4', 'Na5kh9167', '2', '1', '420', '587.37', '30', '268799.202', '1', 'Dineshji', '246695.4', '12600', '3780', '30923.802', '268799.202');
INSERT INTO `tbl_sales_temporary` VALUES ('6', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.3', '2017-07-16', '2074-04-01', '1', '4', '5', 'Na5ta7685', '1', '2', '100', '684.72', '30', '75000.36', '1', 'Sureshji', '68472', '3000', '900', '8628.36', '75000.36');
INSERT INTO `tbl_sales_temporary` VALUES ('7', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.919', '2017-09-23', '2074-06-07', '3', '5', '6', 'Na6kh2802', '2', '1', '210', '569.67', '30', '130199.391', '1', 'Chandanji', '119630.7', '6300', '1890', '14978.691', '130199.391');
INSERT INTO `tbl_sales_temporary` VALUES ('8', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.919', '2017-09-23', '2074-06-07', '3', '5', '6', 'Na6kh2802', '1', '4', '210', '667.02', '30', '153300.546', '1', 'Chandanji', '140074.2', '6300', '1890', '17636.346', '153300.546');
INSERT INTO `tbl_sales_temporary` VALUES ('9', '1', '1', null, '2018-04-10 12:13:52', '2018-04-10 12:13:52', null, '1', 'INVOICE NO.920', '2017-09-23', '2074-06-07', '3', '4', '7', 'Na6ta491', '1', '4', '40', '654.72', '0', '30000.144', '1', 'Ramesh Sah-Imlipatti', '26188.8', '0', '360', '3451.344', '30000.144');

-- ----------------------------
-- View structure for view_agents
-- ----------------------------
DROP VIEW IF EXISTS `view_agents`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_agents` AS SELECT
mst_agents.id,
mst_agents.`name`,
mst_agents.mobile,
mst_agents.phone,
mst_agents.created_by,
mst_agents.updated_by,
mst_agents.deleted_by,
mst_agents.created_at,
mst_agents.updated_at,
mst_agents.deleted_at,
mst_agents.user_id,
aauth_users.fullname AS salesperson_name,
mst_agents.sales_person_id,
mst_agents.opening_amount
FROM
mst_agents
LEFT JOIN aauth_users ON mst_agents.sales_person_id = aauth_users.id ;

-- ----------------------------
-- View structure for view_group_permissions
-- ----------------------------
DROP VIEW IF EXISTS `view_group_permissions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_group_permissions` AS (
	SELECT
		gp.group_id AS group_id,
		gp.perm_id AS perm_id,
		perm.name AS permission,
		grp.name AS group_name
	FROM
		aauth_group_permissions gp
	INNER JOIN aauth_groups grp ON (grp.id = gp.group_id)
	INNER JOIN aauth_permissions perm ON (perm.id = gp.perm_id)			
) ;

-- ----------------------------
-- View structure for view_ledger
-- ----------------------------
DROP VIEW IF EXISTS `view_ledger`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_ledger` AS SELECT
	nepali_month_id,
	nepali_month,
	party_id,
	party_name,
	agent_id,
	agent_name,
	sum(amount) AS total_payment,
	0 AS total_sales
FROM
	view_payment
GROUP BY
	nepali_month_id,
	nepali_month,
	party_id,
	party_name,
	agent_id,
	agent_name
UNION
	SELECT
		nepali_month_id,
		nepali_month,
		party_id,
		party_name,
		agent_id,
		agent_name,
		0 AS total_payment,
		sum(gross_total) AS total_sales
	FROM
		view_sales
	GROUP BY
		nepali_month_id,
		nepali_month,
		party_id,
		party_name,
		agent_id,
		agent_name ;

-- ----------------------------
-- View structure for view_mst_monthly_target
-- ----------------------------
DROP VIEW IF EXISTS `view_mst_monthly_target`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_mst_monthly_target` AS SELECT
mst_monthly_target.id,
mst_monthly_target.created_by,
mst_monthly_target.updated_by,
mst_monthly_target.deleted_by,
mst_monthly_target.created_at,
mst_monthly_target.updated_at,
mst_monthly_target.deleted_at,
mst_monthly_target.agent_id,
mst_monthly_target.amount,
mst_monthly_target.nepali_month_id,
mst_agents.`name` AS agent_name,
mst_nepali_month.`name` AS nepali_month,
mst_nepali_month.rank AS nepali_month_rank
FROM
mst_monthly_target
INNER JOIN mst_agents ON mst_monthly_target.agent_id = mst_agents.id
INNER JOIN mst_nepali_month ON mst_monthly_target.nepali_month_id = mst_nepali_month.id ;

-- ----------------------------
-- View structure for view_partywise_sales
-- ----------------------------
DROP VIEW IF EXISTS `view_partywise_sales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_partywise_sales` AS SELECT 
nepali_month_id,
nepali_month,
party_id,
party_name,
agent_id,
agent_name,
max(total_payment) as total_payment,
max(total_sales) as total_sales
FROM view_ledger
 GROUP BY nepali_month,agent_id,party_id ;

-- ----------------------------
-- View structure for view_payment
-- ----------------------------
DROP VIEW IF EXISTS `view_payment`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_payment` AS SELECT
tbl_payment.id,
tbl_payment.created_by,
tbl_payment.updated_by,
tbl_payment.deleted_by,
tbl_payment.created_at,
tbl_payment.updated_at,
tbl_payment.deleted_at,
tbl_payment.payment_date,
tbl_payment.payment_date_np,
tbl_payment.nepali_month_id,
tbl_payment.bank_name,
tbl_payment.agent_id,
tbl_payment.party_id,
tbl_payment.amount,
tbl_payment.is_published,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS agent_name,
mst_parties.`name` AS party_name
FROM
tbl_payment
INNER JOIN mst_nepali_month ON tbl_payment.nepali_month_id = mst_nepali_month.id
INNER JOIN mst_agents ON tbl_payment.agent_id = mst_agents.id
INNER JOIN mst_parties ON tbl_payment.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_payment_temporary
-- ----------------------------
DROP VIEW IF EXISTS `view_payment_temporary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_payment_temporary` AS SELECT
tbl_payment_temporary.id,
tbl_payment_temporary.created_by,
tbl_payment_temporary.updated_by,
tbl_payment_temporary.deleted_by,
tbl_payment_temporary.created_at,
tbl_payment_temporary.updated_at,
tbl_payment_temporary.deleted_at,
tbl_payment_temporary.payment_date,
tbl_payment_temporary.payment_date_np,
tbl_payment_temporary.nepali_month_id,
tbl_payment_temporary.bank_name,
tbl_payment_temporary.agent_id,
tbl_payment_temporary.party_id,
tbl_payment_temporary.amount,
tbl_payment_temporary.is_processed,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS agent_name,
mst_parties.`name` AS party_name,
tbl_payment_temporary.fiscal_year_id
FROM
tbl_payment_temporary
INNER JOIN mst_nepali_month ON tbl_payment_temporary.nepali_month_id = mst_nepali_month.id
INNER JOIN mst_agents ON tbl_payment_temporary.agent_id = mst_agents.id
INNER JOIN mst_parties ON tbl_payment_temporary.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_report_overview
-- ----------------------------
DROP VIEW IF EXISTS `view_report_overview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_report_overview` AS SELECT
ag.id,
ag.`name`,
ag.opening_amount,
COALESCE((SELECT sum(tbl_sales.gross_total) from tbl_sales where tbl_sales.agent_id = ag.id),0) AS total_sales,
COALESCE((SELECT sum(tbl_payment.amount) from tbl_payment where tbl_payment.agent_id = ag.id),0) AS total_payment,
(COALESCE(ag.opening_amount,0) + COALESCE((SELECT sum(tbl_sales.gross_total) from tbl_sales where tbl_sales.agent_id = ag.id),0) - COALESCE((SELECT sum(tbl_payment.amount) from tbl_payment where tbl_payment.agent_id = ag.id),0) - COALESCE(mst_schemes.amount,0)) AS remaining_amount,
COALESCE(mst_schemes.amount,0) as scheme_amount
FROM
mst_agents AS ag
LEFT JOIN mst_schemes ON ag.id = mst_schemes.agent_id ;

-- ----------------------------
-- View structure for view_sales
-- ----------------------------
DROP VIEW IF EXISTS `view_sales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_sales` AS SELECT
tbl_sales.id,
tbl_sales.created_by,
tbl_sales.updated_by,
tbl_sales.deleted_by,
tbl_sales.created_at,
tbl_sales.updated_at,
tbl_sales.deleted_at,
tbl_sales.invoice_no,
tbl_sales.invoice_date,
tbl_sales.invoice_date_np,
tbl_sales.nepali_month_id,
tbl_sales.agent_id,
tbl_sales.party_id,
tbl_sales.truck_number,
tbl_sales.type_id,
tbl_sales.brand_id,
tbl_sales.quantity,
tbl_sales.rate,
tbl_sales.discount,
tbl_sales.gross_total,
tbl_sales.is_published,
tbl_sales.agent_name,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS actual_agent_name,
mst_parties.`name` AS party_name,
mst_brand_types.`name` AS brand_type,
mst_brands.`name` AS brand_name,
tbl_sales.total_pre_discount,
tbl_sales.total_discount,
tbl_sales.total_excise,
tbl_sales.total_vat,
tbl_sales.total
FROM
tbl_sales
LEFT JOIN mst_agents ON tbl_sales.agent_id = mst_agents.id
INNER JOIN mst_brand_types ON tbl_sales.type_id = mst_brand_types.id
INNER JOIN mst_brands ON tbl_sales.brand_id = mst_brands.id
INNER JOIN mst_nepali_month ON tbl_sales.nepali_month_id = mst_nepali_month.id
LEFT JOIN mst_parties ON tbl_sales.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_sales_temporary
-- ----------------------------
DROP VIEW IF EXISTS `view_sales_temporary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_sales_temporary` AS SELECT
tbl_sales_temporary.id,
tbl_sales_temporary.created_by,
tbl_sales_temporary.updated_by,
tbl_sales_temporary.deleted_by,
tbl_sales_temporary.created_at,
tbl_sales_temporary.updated_at,
tbl_sales_temporary.deleted_at,
tbl_sales_temporary.invoice_no,
tbl_sales_temporary.invoice_date,
tbl_sales_temporary.invoice_date_np,
tbl_sales_temporary.nepali_month_id,
tbl_sales_temporary.agent_id,
tbl_sales_temporary.party_id,
tbl_sales_temporary.truck_number,
tbl_sales_temporary.type_id,
tbl_sales_temporary.brand_id,
tbl_sales_temporary.quantity,
tbl_sales_temporary.rate,
tbl_sales_temporary.discount,
tbl_sales_temporary.gross_total,
tbl_sales_temporary.is_processed,
tbl_sales_temporary.agent_name,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS actual_agent_name,
mst_parties.`name` AS party_name,
mst_brand_types.`name` AS brand_type,
mst_brands.`name` AS brand_name,
tbl_sales_temporary.fiscal_year_id,
tbl_sales_temporary.total_pre_discount,
tbl_sales_temporary.total_discount,
tbl_sales_temporary.total_excise,
tbl_sales_temporary.total_vat,
tbl_sales_temporary.total
FROM
tbl_sales_temporary
LEFT JOIN mst_agents ON tbl_sales_temporary.agent_id = mst_agents.id
INNER JOIN mst_brand_types ON tbl_sales_temporary.type_id = mst_brand_types.id
INNER JOIN mst_brands ON tbl_sales_temporary.brand_id = mst_brands.id
INNER JOIN mst_nepali_month ON tbl_sales_temporary.nepali_month_id = mst_nepali_month.id
LEFT JOIN mst_parties ON tbl_sales_temporary.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_schemes
-- ----------------------------
DROP VIEW IF EXISTS `view_schemes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_schemes` AS SELECT
sch.id,
sch.created_by,
sch.updated_by,
sch.deleted_by,
sch.created_at,
sch.updated_at,
sch.deleted_at,
sch.fiscal_year_id,
sch.agent_id,
sch.amount,
age.`name` AS agent_name,
CONCAT(SUBSTR(mfy.nepali_start_date,1,5),SUBSTR(mfy.nepali_end_date,3,2)) AS fiscal_year,
sch.bag_count,
sch.price,
sch.nepali_month_id,
mst_nepali_month.`name` AS nepali_month,
mst_nepali_month.rank AS nepali_month_rank,
sch.scheme_type
FROM
mst_schemes AS sch
INNER JOIN mst_agents AS age ON sch.agent_id = age.id
INNER JOIN mst_fiscal_years AS mfy ON sch.fiscal_year_id = mfy.id
INNER JOIN mst_nepali_month ON sch.nepali_month_id = mst_nepali_month.id ;

-- ----------------------------
-- View structure for view_users
-- ----------------------------
DROP VIEW IF EXISTS `view_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_users` AS (
	SELECT
		u.id AS id, 
		u.email AS email, 
		u.pass AS pass, 
		u.username AS username, 
		u.fullname as fullname,
		u.banned AS banned, 
		u.last_login AS last_login, 
		u.last_activity AS last_activity, 
		u.date_created AS date_created, 
		u.forgot_exp AS forgot_exp, 
		u.remember_time AS remember_time, 
		u.remember_exp AS remember_exp, 
		u.verification_code AS verification_code, 
		u.totp_secret AS totp_secret, 
		u.ip_address AS ip_address
	FROM
		aauth_users u
) ;

-- ----------------------------
-- View structure for view_user_groups
-- ----------------------------
DROP VIEW IF EXISTS `view_user_groups`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_user_groups` AS (
	SELECT 
		ug.user_id AS user_id,
	    ug.group_id AS group_id,
	    g.name AS group_name,
	    u.username as username,
	    u.email as email,
	    u.fullname as fullname
   	FROM 
   		aauth_user_groups ug
    INNER JOIN aauth_users u ON (u.id = ug.user_id)
    INNER JOIN aauth_groups g ON (g.id = ug.group_id)
) ;

-- ----------------------------
-- View structure for view_user_permissions
-- ----------------------------
DROP VIEW IF EXISTS `view_user_permissions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_user_permissions` AS (
	SELECT
		up.user_id AS user_id,
		up.perm_id AS perm_id,
		perm.name AS permission,
		u.username AS username,
		u.email AS email,
		u.fullname as fullname
	FROM
		aauth_user_permissions up
	INNER JOIN aauth_users u ON (u.id = up.user_id)
	INNER JOIN aauth_permissions perm ON (perm.id = up.perm_id)
) ;
