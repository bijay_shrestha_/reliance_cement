/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance_cement

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2018-04-06 16:15:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for aauth_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_groups`;
CREATE TABLE `aauth_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_groups
-- ----------------------------
INSERT INTO `aauth_groups` VALUES ('1', 'Member', 'Member Default Access Group');
INSERT INTO `aauth_groups` VALUES ('2', 'Super Administrator', 'Super Administrator Access Group');
INSERT INTO `aauth_groups` VALUES ('100', 'Administrator', 'Administrator Access Group');
INSERT INTO `aauth_groups` VALUES ('500', 'Factory Staff', 'Login for factory staff');
INSERT INTO `aauth_groups` VALUES ('1000', 'Sells Person', 'Login for sells person\r\n');
INSERT INTO `aauth_groups` VALUES ('1500', 'Agent', 'Login for agent person');
INSERT INTO `aauth_groups` VALUES ('2000', 'Office Staff', 'Login for Office Staff');

-- ----------------------------
-- Table structure for aauth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_group_permissions`;
CREATE TABLE `aauth_group_permissions` (
  `perm_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_group_permissions
-- ----------------------------
INSERT INTO `aauth_group_permissions` VALUES ('1', '1');
INSERT INTO `aauth_group_permissions` VALUES ('7', '100');
INSERT INTO `aauth_group_permissions` VALUES ('8', '100');
INSERT INTO `aauth_group_permissions` VALUES ('13', '1500');
INSERT INTO `aauth_group_permissions` VALUES ('14', '1500');
INSERT INTO `aauth_group_permissions` VALUES ('15', '1500');

-- ----------------------------
-- Table structure for aauth_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `aauth_login_attempts`;
CREATE TABLE `aauth_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_permissions`;
CREATE TABLE `aauth_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_permissions
-- ----------------------------
INSERT INTO `aauth_permissions` VALUES ('1', 'Control Panel', 'Control Panel');
INSERT INTO `aauth_permissions` VALUES ('2', 'System', 'System');
INSERT INTO `aauth_permissions` VALUES ('3', 'Users', 'Users');
INSERT INTO `aauth_permissions` VALUES ('4', 'Groups', 'Groups');
INSERT INTO `aauth_permissions` VALUES ('5', 'Permissions', 'Permissions');
INSERT INTO `aauth_permissions` VALUES ('6', 'Brand Types', 'Brand Types');
INSERT INTO `aauth_permissions` VALUES ('7', 'Master', 'Permission for master data');
INSERT INTO `aauth_permissions` VALUES ('8', 'Brands', 'Brands');
INSERT INTO `aauth_permissions` VALUES ('9', 'Parties', 'Parties');
INSERT INTO `aauth_permissions` VALUES ('10', 'Nepali Months', 'Nepali Months');
INSERT INTO `aauth_permissions` VALUES ('11', 'Schemes', 'Schemes');
INSERT INTO `aauth_permissions` VALUES ('12', 'Fiscal Year', 'Fiscal Year');
INSERT INTO `aauth_permissions` VALUES ('13', 'Payments', 'Payments');
INSERT INTO `aauth_permissions` VALUES ('14', 'Sales', 'Sales');
INSERT INTO `aauth_permissions` VALUES ('15', 'Transactions', 'Transactions');

-- ----------------------------
-- Table structure for aauth_pms
-- ----------------------------
DROP TABLE IF EXISTS `aauth_pms`;
CREATE TABLE `aauth_pms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(11) NOT NULL DEFAULT '0',
  `pm_deleted_receiver` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_sender_id_receiver_id_date_read` (`id`,`sender_id`,`receiver_id`,`date_read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_pms
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_sub_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_sub_groups`;
CREATE TABLE `aauth_sub_groups` (
  `group_id` int(11) unsigned NOT NULL,
  `subgroup_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_sub_groups
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_users
-- ----------------------------
DROP TABLE IF EXISTS `aauth_users`;
CREATE TABLE `aauth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `banned` int(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_users
-- ----------------------------
INSERT INTO `aauth_users` VALUES ('1', 'superadmin@gmail.com', '8b235284a9f7a82364468e52dab386f33844421b481113794e0b4d634c86d0f3', 'superadmin', 'Super Administrator', '0', '2018-04-05 09:38:19', '2018-04-05 16:59:14', '2018-03-15 09:35:46', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('2', 'admin@no.com', '52b3a93aac36bd14b3a1c9e7118f79981d14d39c6fd5118884d7544e58232a8d', 'admin', 'Administrator', '0', '2018-03-27 10:03:43', '2018-03-27 10:04:43', '2018-03-15 09:35:46', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('3', 'anish@pagodalabs.com', 'b89bd1a246c9a6e66ca19346197364988b9ca62ddf12b2fcc3a4accff7211f6e', 'anish.cement.autoshop', 'Anish Cement Autoshop', '0', '2018-03-27 11:46:42', '2018-03-27 11:50:20', '2018-03-27 11:44:59', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('4', 'sanish@pagodalabs.com', '3a9b96b74ac928c5eb3386749b5389f743e322bdec53e0d58f079bc959b2168c', 'dinesh.pandit', 'Dinesh Pandit', '0', null, null, '2018-03-27 12:13:06', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('5', 'vishwanathji@reliance.com', 'e2586cacc809b010b229cb9846907b8558e1344fd258e9947d4bd649bb0960dd', 'vishwanathji', 'Vishwanathji', '0', null, null, '2018-04-04 18:08:55', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('6', 'dineshji@reliance.com', 'f1c3603ac332273d85910ff9db1d4ed245de33c69ca29c7d516d0fc7477f8ce5', 'dineshji', 'Dineshji', '0', null, null, '2018-04-04 18:09:26', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('7', 'rameshji@reliance.com', '9e047cea959e730db47bcdd02ff812864a8864ccbae22af035a8c8e29acfb6d2', 'rameshji', 'Rameshji', '0', null, null, '2018-04-04 18:09:27', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('8', 'ram.kumarji@reliance.com', 'e463594ab2f42d74b183163143fa197656536bca7cad9ee8843e76626170f2a9', 'ram.kumarji', 'Ram Kumarji', '0', null, null, '2018-04-05 15:04:22', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('9', 'nagendraji@reliance.com', '0f144452d56948c8bd97ba16cda90dea67481e2c2b45134e04d0298a2f2c4173', 'nagendraji', 'Nagendraji', '0', null, null, '2018-04-05 15:06:25', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('10', 'piyushji@reliance.com', 'ccee8c07f4d9a5047fd232b56f3017c9ed52c8fc5bb56b62bb0ac886fbee0a0f', 'piyushji', 'Piyushji', '0', null, null, '2018-04-05 15:07:07', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('11', 'jagdishji@reliance.com', '8b19f4baf97d6d6d56b42029662d89243148ce416789c21740820038d542605e', 'jagdishji', 'Jagdishji', '0', null, null, '2018-04-05 15:09:24', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('12', 'banglamukhi@reliance.com', 'ce1a7abd4930a0f13cb115855d6cd5dbd74a388eabd0d7da496a90e0f7a320d6', 'banglamukhi', 'Banglamukhi', '0', null, null, '2018-04-05 15:11:00', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('13', 'kishan.kedia@reliance.com', '8935066d5a77191ca2065d919fd3bfa82bbd9f357beb42c2d264b32eec16eb20', 'kishan.kedia', 'Kishan Kedia', '0', null, null, '2018-04-05 15:15:17', null, null, null, null, null, null);
INSERT INTO `aauth_users` VALUES ('14', 'sanish.maharjan@reliance.com', '472c960e0d25d4763604602acabd875ff5e314ebecd8c52419ef21ab1c1770b6', 'sanish.maharjan', 'Sanish Maharjan', '0', '2018-04-05 16:56:42', '2018-04-05 17:00:28', '2018-04-05 15:17:39', null, null, null, null, null, '::1');

-- ----------------------------
-- Table structure for aauth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_groups`;
CREATE TABLE `aauth_user_groups` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_groups
-- ----------------------------
INSERT INTO `aauth_user_groups` VALUES ('1', '1');
INSERT INTO `aauth_user_groups` VALUES ('1', '2');
INSERT INTO `aauth_user_groups` VALUES ('2', '1');
INSERT INTO `aauth_user_groups` VALUES ('2', '100');
INSERT INTO `aauth_user_groups` VALUES ('3', '1');
INSERT INTO `aauth_user_groups` VALUES ('3', '1500');
INSERT INTO `aauth_user_groups` VALUES ('4', '1');
INSERT INTO `aauth_user_groups` VALUES ('4', '1500');
INSERT INTO `aauth_user_groups` VALUES ('6', '1');
INSERT INTO `aauth_user_groups` VALUES ('6', '1500');
INSERT INTO `aauth_user_groups` VALUES ('7', '1');
INSERT INTO `aauth_user_groups` VALUES ('7', '1500');
INSERT INTO `aauth_user_groups` VALUES ('8', '1');
INSERT INTO `aauth_user_groups` VALUES ('8', '1500');
INSERT INTO `aauth_user_groups` VALUES ('9', '1');
INSERT INTO `aauth_user_groups` VALUES ('9', '1500');
INSERT INTO `aauth_user_groups` VALUES ('10', '1');
INSERT INTO `aauth_user_groups` VALUES ('10', '1500');
INSERT INTO `aauth_user_groups` VALUES ('11', '1');
INSERT INTO `aauth_user_groups` VALUES ('11', '1500');
INSERT INTO `aauth_user_groups` VALUES ('12', '1');
INSERT INTO `aauth_user_groups` VALUES ('12', '1500');
INSERT INTO `aauth_user_groups` VALUES ('13', '1');
INSERT INTO `aauth_user_groups` VALUES ('13', '1500');
INSERT INTO `aauth_user_groups` VALUES ('14', '1');
INSERT INTO `aauth_user_groups` VALUES ('14', '1500');

-- ----------------------------
-- Table structure for aauth_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_permissions`;
CREATE TABLE `aauth_user_permissions` (
  `perm_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_user_variables
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_variables`;
CREATE TABLE `aauth_user_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_variables
-- ----------------------------

-- ----------------------------
-- Table structure for mst_agents
-- ----------------------------
DROP TABLE IF EXISTS `mst_agents`;
CREATE TABLE `mst_agents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `mun_vdc_id` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_agents
-- ----------------------------
INSERT INTO `mst_agents` VALUES ('1', '1', '1', null, '2018-04-04 18:08:55', '2018-04-04 18:08:55', null, 'Vishwanathji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('2', '1', '1', null, '2018-04-04 18:09:26', '2018-04-04 18:09:26', null, 'Vishwanathji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('3', '1', '1', null, '2018-04-04 18:09:26', '2018-04-04 18:09:26', null, 'Dineshji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('4', '1', '1', null, '2018-04-04 18:09:27', '2018-04-04 18:09:27', null, 'Rameshji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('5', '1', '1', null, '2018-04-05 15:04:22', '2018-04-05 15:04:22', null, 'Ram Kumarji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('6', '1', '1', null, '2018-04-05 15:06:25', '2018-04-05 15:06:25', null, 'Nagendraji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('7', '1', '1', null, '2018-04-05 15:07:06', '2018-04-05 15:07:06', null, 'Piyushji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('8', '1', '1', null, '2018-04-05 15:09:23', '2018-04-05 15:09:23', null, 'Jagdishji', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('9', '1', '1', null, '2018-04-05 15:09:24', '2018-04-05 15:09:24', null, 'Gorkha Suppliers-Gorkha', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('10', '1', '1', null, '2018-04-05 15:10:59', '2018-04-05 15:10:59', null, 'Hari Shrestha, Gorkha', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('11', '1', '1', null, '2018-04-05 15:10:59', '2018-04-05 15:10:59', null, 'Banglamukhi', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('12', '1', '1', null, '2018-04-05 15:13:36', '2018-04-05 15:13:36', null, 'Rasuwa K.D. JV Pvt. Ltd.-Kathmandu', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('13', '1', '1', null, '2018-04-05 15:15:17', '2018-04-05 15:15:17', null, 'Kishan Kedia', null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_agents` VALUES ('14', '1', '1', null, '2018-04-05 15:17:39', '2018-04-05 15:17:39', null, 'Sanish Maharjan', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for mst_brands
-- ----------------------------
DROP TABLE IF EXISTS `mst_brands`;
CREATE TABLE `mst_brands` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_brands
-- ----------------------------
INSERT INTO `mst_brands` VALUES ('1', '1', '1', null, '2018-03-30 09:28:49', '2018-03-30 09:28:49', null, 'Dunlop', '1', null, null);
INSERT INTO `mst_brands` VALUES ('2', '1', '1', null, '2018-04-05 15:04:50', '2018-04-05 15:04:50', null, 'Dragon', '2', null, null);
INSERT INTO `mst_brands` VALUES ('3', '1', '1', null, '2018-04-05 15:05:03', '2018-04-05 15:05:03', null, 'Shivalik', '3', null, null);

-- ----------------------------
-- Table structure for mst_brand_types
-- ----------------------------
DROP TABLE IF EXISTS `mst_brand_types`;
CREATE TABLE `mst_brand_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_brand_types
-- ----------------------------
INSERT INTO `mst_brand_types` VALUES ('1', '1', '1', null, '2018-03-16 10:11:03', '2018-03-16 10:11:03', null, 'OPC', null, null);
INSERT INTO `mst_brand_types` VALUES ('2', '1', '1', null, '2018-03-16 10:11:10', '2018-03-16 10:11:10', null, 'PSC', null, null);

-- ----------------------------
-- Table structure for mst_fiscal_years
-- ----------------------------
DROP TABLE IF EXISTS `mst_fiscal_years`;
CREATE TABLE `mst_fiscal_years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nepali_start_date` varchar(255) DEFAULT NULL,
  `nepali_end_date` varchar(255) DEFAULT NULL,
  `english_start_date` date DEFAULT NULL,
  `english_end_date` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_fiscal_years
-- ----------------------------
INSERT INTO `mst_fiscal_years` VALUES ('1', '1', '1', null, '2018-04-04 17:18:26', null, null, '2073-04-01', '2074-03-32', '2016-07-16', '2017-07-16', '0');
INSERT INTO `mst_fiscal_years` VALUES ('2', '1', '1', null, '2018-04-04 17:18:26', '2018-04-04 17:18:26', null, '2074-04-01', '2075-03-32', '2017-07-16', '2018-07-16', '1');

-- ----------------------------
-- Table structure for mst_munvdc
-- ----------------------------
DROP TABLE IF EXISTS `mst_munvdc`;
CREATE TABLE `mst_munvdc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3798 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_munvdc
-- ----------------------------
INSERT INTO `mst_munvdc` VALUES ('1', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 1', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('2', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 2', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('3', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 3', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('4', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 4', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('5', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 5', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('6', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 6', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('7', '2', null, null, '2017-03-26 13:49:28', null, null, 'Province No. 7', '0', 'PROVINCE');
INSERT INTO `mst_munvdc` VALUES ('8', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhapa', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('9', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ilam', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('10', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchthar', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('11', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taplejung', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('12', '2', null, null, '2017-03-26 13:49:28', null, null, 'Morang', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('13', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunsari', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('14', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhojpur', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('15', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhankuta', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('16', '2', null, null, '2017-03-26 13:49:28', null, null, 'Terhathum', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('17', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankhuwasabha', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('18', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saptari', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('19', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siraha', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('20', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udayapur', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('21', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khotang', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('22', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okhaldhunga', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('23', '2', null, null, '2017-03-26 13:49:28', null, null, 'Solukhumbu', '1', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('24', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanusa', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('25', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahottari', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('26', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarlahi', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('27', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sindhuli', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('28', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramechhap', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('29', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dolakha', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('30', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhaktapur', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('31', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhading', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('32', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kathmandu', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('33', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhrepalanchok', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('34', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalitpur', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('35', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nuwakot', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('36', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rasuwa', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('37', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sindhupalchok', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('38', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bara', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('39', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsa', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('40', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rautahat', '2', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('41', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chitwan', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('42', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makwanpur', '3', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('43', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gorkha', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('44', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaski', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('45', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamjung', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('46', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syangja', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('47', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tanahu', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('48', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manang', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('49', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kapilvastu', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('50', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nawalpur', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('51', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parasi', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('52', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupandehi', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('53', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arghakhanchi', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('54', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gulmi', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('55', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palpa', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('56', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baglung', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('57', '2', null, null, '2017-03-26 13:49:28', null, null, 'Myagdi', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('58', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parbat', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('59', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mustang', '4', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('60', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dang', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('61', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pyuthan', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('62', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rolpa', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('63', '2', null, null, '2017-03-26 13:49:28', null, null, 'EasternRukum', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('64', '2', null, null, '2017-03-26 13:49:28', null, null, 'WesternRukum', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('65', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salyan', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('66', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dolpa', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('67', '2', null, null, '2017-03-26 13:49:28', null, null, 'Humla', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('68', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jumla', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('69', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikot', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('70', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mugu', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('71', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banke', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('72', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bardiya', '5', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('73', '2', null, null, '2017-03-26 13:49:28', null, null, 'Surkhet', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('74', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dailekh', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('75', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jajarkot', '6', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('76', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kailali', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('77', '2', null, null, '2017-03-26 13:49:28', null, null, 'Achham', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('78', '2', null, null, '2017-03-26 13:49:28', null, null, 'Doti', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('79', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajhang', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('80', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajura', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('81', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanchanpur', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('82', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadeldhura', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('83', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baitadi', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('84', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darchula', '7', 'DISTRICT');
INSERT INTO `mst_munvdc` VALUES ('85', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amchok', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('86', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajho', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('87', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barbote', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('88', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chamaita', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('89', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chisapani', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('90', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chulachuli', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('91', '2', null, null, '2017-03-26 13:49:28', null, null, 'Danabari', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('92', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ebhang', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('93', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ektappa', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('94', '2', null, null, '2017-03-26 13:49:28', null, null, 'Emang', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('95', '2', null, null, '2017-03-26 13:49:28', null, null, 'Erautar', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('96', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajurmukhi', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('97', '2', null, null, '2017-03-26 13:49:28', null, null, 'Godak', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('98', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gorkhe', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('99', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuna', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('100', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jirmale', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('101', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jitpur', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('102', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jogmai', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('103', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolbung', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('104', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('105', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lumbe', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('106', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mabu', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('107', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahamai', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('108', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maimajhuwa', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('109', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maipokhari', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('110', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namsaling', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('111', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naya Bazar', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('112', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pashupatinagar', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('113', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phakphok', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('114', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phuyatappa', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('115', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puwamajwa', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('116', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pyang', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('117', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakphara', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('118', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhejung', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('119', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samalpung', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('120', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangrumba', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('121', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shanti Danda', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('122', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shantipur', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('123', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddhithumka', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('124', '2', null, null, '2017-03-26 13:49:28', null, null, 'Soyak', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('125', '2', null, null, '2017-03-26 13:49:28', null, null, 'Soyang', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('126', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sri Antu', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('127', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sulubung', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('128', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sumbek ', '9', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('129', '2', null, null, '2017-03-26 13:49:28', null, null, 'Anarmani', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('130', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahundangi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('131', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baiagundhara', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('132', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balubari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('133', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baniyani', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('134', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhabare', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('135', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chakchaki', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('136', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandragadhi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('137', '2', null, null, '2017-03-26 13:49:28', null, null, 'Charpane', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('138', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangibari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('139', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhaijan', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('140', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharmpur', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('141', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duhagadhi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('142', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garamani', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('143', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gauriganj', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('144', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gherabari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('145', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goldhhap', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('146', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haldibari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('147', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalthal', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('148', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jyamirgadhi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('149', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kechana', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('150', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khajurgachhi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('151', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khudunabari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('152', '2', null, null, '2017-03-26 13:49:28', null, null, 'Korobari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('153', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kumarkhod', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('154', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakhanpur', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('155', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahabhara', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('156', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maheshpur', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('157', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchganchi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('158', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathabhari', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('159', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathariya', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('160', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prithivinagar', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('161', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajghadh', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('162', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shantinagar', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('163', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sharanamati', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('164', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taghanduba', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('165', '2', null, null, '2017-03-26 13:49:28', null, null, 'Topgachchi', '8', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('166', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranitar', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('167', '2', null, null, '2017-03-26 13:49:28', null, null, 'Luwamphu', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('168', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yangnam', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('169', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nangin', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('170', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lungrupa', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('171', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ambarpur', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('172', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchami', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('173', '2', null, null, '2017-03-26 13:49:28', null, null, 'Subhang', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('174', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharapa', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('175', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yasok', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('176', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rani Gaun', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('177', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mangjabung', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('178', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syabarumba', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('179', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aarubote', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('180', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarangdanda', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('181', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rabi', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('182', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kurumba', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('183', '2', null, null, '2017-03-26 13:49:28', null, null, 'Limba', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('184', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durdimba', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('185', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ektin', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('186', '2', null, null, '2017-03-26 13:49:28', null, null, 'Memeng', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('187', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prangbung', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('188', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yangnam', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('189', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sidin', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('190', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nawamidanda', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('191', '2', null, null, '2017-03-26 13:49:28', null, null, 'Imbung', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('192', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pauwa Sartap', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('193', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chilingdin', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('194', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aangsarang', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('195', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phaktep', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('196', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aangna', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('197', '2', null, null, '2017-03-26 13:49:28', null, null, 'Olane', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('198', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hangum', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('199', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mauwa', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('200', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyangthapu', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('201', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalaicha', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('202', '2', null, null, '2017-03-26 13:49:28', null, null, 'Oyam', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('203', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tharpu', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('204', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagi', '10', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('205', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ambegudin', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('206', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ankhop', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('207', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaksibote', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('208', '2', null, null, '2017-03-26 13:49:28', null, null, 'Change', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('209', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhungesaghu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('210', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dummrise', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('211', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ekhabu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('212', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hangdeva', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('213', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hangpang', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('214', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikhola', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('215', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khamlung', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('216', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khejenim', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('217', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khewang', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('218', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khokling', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('219', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lelep', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('220', '2', null, null, '2017-03-26 13:49:28', null, null, 'Limbudin', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('221', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lingtep', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('222', '2', null, null, '2017-03-26 13:49:28', null, null, 'Linkhim', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('223', '2', null, null, '2017-03-26 13:49:28', null, null, 'Liwang', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('224', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mamangkhe', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('225', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nalbu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('226', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nankholyang', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('227', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nidhuradin', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('228', '2', null, null, '2017-03-26 13:49:28', null, null, 'Olangchung Gola', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('229', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paidang', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('230', '2', null, null, '2017-03-26 13:49:28', null, null, 'Papung', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('231', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pedang', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('232', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phakumba', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('233', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phawakhola', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('234', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulbari', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('235', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phurumbu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('236', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sadewa', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('237', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('238', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santhakra', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('239', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sawa', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('240', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sawadin', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('241', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sawalakhu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('242', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikaicha', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('243', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sinam', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('244', '2', null, null, '2017-03-26 13:49:28', null, null, 'Surumakhim', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('245', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tapethok', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('246', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tellok', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('247', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thechambu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('248', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thinglabu', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('249', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thukima', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('250', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thumbedin', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('251', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tiringe', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('252', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yamphudin', '11', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('253', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aamtep', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('254', '2', null, null, '2017-03-26 13:49:28', null, null, 'Annapurna', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('255', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baikuntha', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('256', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basikhola', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('257', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basingtharpur', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('258', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bastim', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('259', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhubal', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('260', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhulke', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('261', '2', null, null, '2017-03-26 13:49:28', null, null, 'Boya', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('262', '2', null, null, '2017-03-26 13:49:28', null, null, 'Champe', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('263', '2', null, null, '2017-03-26 13:49:28', null, null, 'Changre', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('264', '2', null, null, '2017-03-26 13:49:28', null, null, 'Charambi', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('265', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaukidada', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('266', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhinamukh', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('267', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dalgaun', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('268', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('269', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dewantar', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('270', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhodalekhani', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('271', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dobhane', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('272', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dummana', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('273', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gogane', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('274', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gupteshwar', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('275', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hasanpur', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('276', '2', null, null, '2017-03-26 13:49:28', null, null, 'Helauchha', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('277', '2', null, null, '2017-03-26 13:49:28', null, null, 'Homtang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('278', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jarayotar', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('279', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khairang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('280', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khatamma', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('281', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khawa', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('282', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kot', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('283', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kudak Kaule', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('284', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kulunga', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('285', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekharka', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('286', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mane Bhanjyang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('287', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagi', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('288', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nepaledanda', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('289', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okhre', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('290', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pangcha', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('291', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patle Pani', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('292', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pawala', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('293', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pyauli', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('294', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranibas', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('295', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangpang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('296', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sano Dumba', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('297', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shyamsila', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('298', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddheshwar', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('299', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sindrang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('300', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syamsila', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('301', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thidingkha', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('302', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Dumba', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('303', '2', null, null, '2017-03-26 13:49:28', null, null, 'Timma', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('304', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tiwari Bhanjyang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('305', '2', null, null, '2017-03-26 13:49:28', null, null, 'Walangkha', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('306', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yaku', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('307', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yangpang', '14', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('308', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ahale', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('309', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ankhisalla', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('310', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arkhaule Jitpur', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('311', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantatar', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('312', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belhara', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('313', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhabare', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('314', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhirgaun', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('315', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bodhe', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('316', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhabare', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('317', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budi Morang', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('318', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chanuwa', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('319', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhintang', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('320', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chungmang', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('321', '2', null, null, '2017-03-26 13:49:28', null, null, 'Danda Bazar', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('322', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandagaun', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('323', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathikharka', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('324', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jitpur Arkhaule', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('325', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khoku', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('326', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khuwaphok', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('327', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuruletenupa', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('328', '2', null, null, '2017-03-26 13:49:28', null, null, 'Leguwa', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('329', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahabharat', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('330', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marek Katahare', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('331', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maunabuthuk', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('332', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudebas', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('333', '2', null, null, '2017-03-26 13:49:28', null, null, 'Murtidhunga', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('334', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parewadin', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('335', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phaksib', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('336', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raja Rani', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('337', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tankhuwa', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('338', '2', null, null, '2017-03-26 13:49:28', null, null, 'Telia', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('339', '2', null, null, '2017-03-26 13:49:28', null, null, 'Vedatar', '15', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('340', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amaibariyati', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('341', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amardaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('342', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babiya Birta', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('343', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahuni', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('344', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banigama', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('345', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baradanga', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('346', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bayarban', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('347', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhaudaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('348', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhanagar', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('349', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dainiya', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('350', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangihat', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('351', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangraha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('352', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darbairiya', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('353', '2', null, null, '2017-03-26 13:49:28', null, null, 'Drabesh', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('354', '2', null, null, '2017-03-26 13:49:28', null, null, 'Govindapur', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('355', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hasandaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('356', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathimudha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('357', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hoklabari', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('358', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itahara', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('359', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jante', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('360', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhapa Baijanathpur', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('361', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhorahat', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('362', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhurkiya', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('363', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kadamaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('364', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katahari', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('365', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kathamaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('366', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerabari', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('367', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerawan', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('368', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakhantari', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('369', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhumalla', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('370', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadeva', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('371', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhare', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('372', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matigachha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('373', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motipur', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('374', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nocha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('375', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patigaun', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('376', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhariya', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('377', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajghat', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('378', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramite Khola', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('379', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sidharaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('380', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sijuwa', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('381', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sinhadevi Sombare', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('382', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisabanibadahara', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('383', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisawanijahada', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('384', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sorabhaj', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('385', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tandi', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('386', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tankisinuwari', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('387', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tetariya', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('388', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thalaha', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('389', '2', null, null, '2017-03-26 13:49:28', null, null, 'Warangi', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('390', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yangshila', '12', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('391', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ankhibhui', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('392', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahrabise', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('393', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bala', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('394', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chepuwa', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('395', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhupu', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('396', '2', null, null, '2017-03-26 13:49:28', null, null, 'Diding', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('397', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatiya', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('398', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaljala', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('399', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kimathanka', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('400', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madi Mulkharka', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('401', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madi Rambeni', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('402', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makalu', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('403', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malta', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('404', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mamling', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('405', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manakamana', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('406', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mangtewa', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('407', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matsya Pokhari', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('408', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mawadin', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('409', '2', null, null, '2017-03-26 13:49:28', null, null, 'Num', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('410', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nundhaki', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('411', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pangma', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('412', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathibhara', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('413', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pawakhola', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('414', '2', null, null, '2017-03-26 13:49:28', null, null, 'Savapokhari', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('415', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisuwakhola', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('416', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitalpati', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('417', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syabun', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('418', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tamaphok', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('419', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tamku', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('420', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yaphu', '17', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('421', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aekamba', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('422', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amaduwa', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('423', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amahibelaha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('424', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aurabarni', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('425', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babiya', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('426', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bakalauri', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('427', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barahachhetra', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('428', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('429', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharaul', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('430', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhokraha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('431', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishnupaduka', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('432', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chadwela', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('433', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhitaha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('434', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chimdi', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('435', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dewanganj', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('436', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghuski', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('437', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumaraha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('438', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gautampur', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('439', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hanshpokha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('440', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harinagar', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('441', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haripur', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('442', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalpapur', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('443', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaptanganj', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('444', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khanar', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('445', '2', null, null, '2017-03-26 13:49:28', null, null, 'Laukahi', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('446', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madheli', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('447', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhesa', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('448', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhuwan', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('449', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhyeharsahi', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('450', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahendranagar', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('451', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narshinhatappu', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('452', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakali', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('453', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchakanya', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('454', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paschim Kasuha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('455', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prakashpur', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('456', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purbakushaha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('457', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramganj Belgachhi', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('458', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramganj Senuwari', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('459', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramnagar Bhutaha', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('460', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahebganj', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('461', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santerjhora', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('462', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simariya', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('463', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonapur', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('464', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sripurjabdi', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('465', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tanamuna', '13', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('466', '2', null, null, '2017-03-26 13:49:28', null, null, 'Angdim', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('467', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('468', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhate Dhunga', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('469', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chuhandanda', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('470', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangpa', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('471', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hamarjung', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('472', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hawaku', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('473', '2', null, null, '2017-03-26 13:49:28', null, null, 'Isibu', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('474', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaljale', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('475', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khamlalung', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('476', '2', null, null, '2017-03-26 13:49:28', null, null, 'Morahang', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('477', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okhare', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('478', '2', null, null, '2017-03-26 13:49:28', null, null, 'Oyakjung', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('479', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchakanya Pokhari', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('480', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phakchamara', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('481', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulek', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('482', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pauthak', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('483', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sabla', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('484', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samdu', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('485', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankranti Bazar', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('486', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simle', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('487', '2', null, null, '2017-03-26 13:49:28', null, null, 'Solma', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('488', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sri Jung', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('489', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sudap', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('490', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sungnam', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('491', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thoklung', '16', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('492', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ainselu Kharka', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('493', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arkhale', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('494', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badahare', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('495', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badka Dipali', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('496', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahunidanda', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('497', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bakachol', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('498', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baksila', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('499', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barahapokhari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('500', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baspani', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('501', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batase', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('502', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijaya Kharka', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('503', '2', null, null, '2017-03-26 13:49:28', null, null, 'Buipa', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('504', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhitapokhari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('505', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhorambu', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('506', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chipring', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('507', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chisapani', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('508', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyandanda', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('509', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyasmitar', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('510', '2', null, null, '2017-03-26 13:49:28', null, null, 'Damarkhu Shivalaya', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('511', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandagaun', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('512', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devisthan', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('513', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharapani', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('514', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhitung', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('515', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dikuwa', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('516', '2', null, null, '2017-03-26 13:49:28', null, null, 'Diplung', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('517', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dipsung', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('518', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dorpa Chiuridanda', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('519', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubekol', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('520', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumre Dharapani', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('521', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durchhim', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('522', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hanchaur', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('523', '2', null, null, '2017-03-26 13:49:28', null, null, 'Indrayani Pokhari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('524', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalapa', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('525', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jyamire', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('526', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaule', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('527', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharmi', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('528', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharpa', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('529', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khartamchha', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('530', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khidima', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('531', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khotang Bazar', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('532', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuvinde', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('533', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamidanda', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('534', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lichki Ramche', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('535', '2', null, null, '2017-03-26 13:49:28', null, null, 'Linkuwa Pokhari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('536', '2', null, null, '2017-03-26 13:49:28', null, null, 'Magpa', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('537', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevasthan', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('538', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mangaltar', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('539', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mattim Birta', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('540', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mauwabote', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('541', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nerpa', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('542', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nirmalidanda', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('543', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nunthala', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('544', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patheka', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('545', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pauwasera', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('546', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phaktang', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('547', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phedi', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('548', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajapani', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('549', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakha Bangdel', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('550', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakha Dipsung', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('551', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratancha Majhagaun', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('552', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ribdung Jaleshwari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('553', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ribdung Maheshwari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('554', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salle', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('555', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santeshwar Chhitapokhari', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('556', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sapteshwar', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('557', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saunechaur', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('558', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sawakatahare', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('559', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simpani', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('560', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sungdel', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('561', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suntale', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('562', '2', null, null, '2017-03-26 13:49:28', null, null, 'Woplukha', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('563', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wopung', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('564', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yamkhya', '21', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('565', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baksa', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('566', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balakhu', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('567', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baraneshwar', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('568', '2', null, null, '2017-03-26 13:49:28', null, null, 'Betini', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('569', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadaure', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('570', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhussinga', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('571', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bigutar', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('572', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bilandu', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('573', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyanam', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('574', '2', null, null, '2017-03-26 13:49:28', null, null, 'Diyale', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('575', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamnangtar', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('576', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harkapur', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('577', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jantarkhani', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('578', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikadevi', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('579', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaptigaun', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('580', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katunje', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('581', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ketuke', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('582', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khiji Chandeshwari', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('583', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khijiphalate', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('584', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuibhir', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('585', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuntadevi', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('586', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhavpur', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('587', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mamkha', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('588', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manebhanjyang', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('589', '2', null, null, '2017-03-26 13:49:28', null, null, 'Moli', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('590', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mulkharka', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('591', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narmedeshwar', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('592', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okhaldhunga', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('593', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palapu', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('594', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patle', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('595', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phediguth', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('596', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulbari', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('597', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhare', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('598', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokli', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('599', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prapchan', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('600', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ragani', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('601', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajadip', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('602', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raniban', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('603', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratmata', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('604', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rawadolu', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('605', '2', null, null, '2017-03-26 13:49:28', null, null, 'Serna', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('606', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srichaur', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('607', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singhadevi', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('608', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisneri', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('609', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taluwa', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('610', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarkerabari', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('611', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thakle', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('612', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thoksela', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('613', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulachhap', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('614', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ubu', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('615', '2', null, null, '2017-03-26 13:49:28', null, null, 'Vadaure', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('616', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yasam', '22', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('617', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arnaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('618', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aurahi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('619', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bainiya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('620', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bairawa', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('621', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bakdhauwa', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('622', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bamangamakatti', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('623', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banarjhula', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('624', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banaula', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('625', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banauli', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('626', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barhmapur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('627', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barsain', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('628', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basbiti', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('629', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bathnaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('630', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belhi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('631', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belhi Chapena', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('632', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawatpur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('633', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhardaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('634', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhutahi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('635', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birpur Barahi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('636', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishariya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('637', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budebarsaien', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('638', '2', null, null, '2017-03-26 13:49:28', null, null, 'Boriya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('639', '2', null, null, '2017-03-26 13:49:28', null, null, 'Brahmapur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('640', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhinnamasta', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('641', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dauda', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('642', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daulatpur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('643', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deuri', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('644', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurimaruwa', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('645', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanagadi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('646', '2', null, null, '2017-03-26 13:49:28', null, null, 'Didhawa', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('647', '2', null, null, '2017-03-26 13:49:28', null, null, 'Diman', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('648', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamhariya Parwaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('649', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goithi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('650', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hardiya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('651', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('652', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haripur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('653', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inarwa Phulbariya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('654', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itahari Bishnupur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('655', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuni Madhapura', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('656', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jandaul', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('657', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhutaki', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('658', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabilash', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('659', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kachan', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('660', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalyanpur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('661', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kataiya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('662', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadgapur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('663', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khojpur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('664', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ko. Madhepura', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('665', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kochabakhari', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('666', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koiladi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('667', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kushaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('668', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalapati', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('669', '2', null, null, '2017-03-26 13:49:28', null, null, 'Launiya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('670', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lohajara', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('671', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhawapur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('672', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhupati', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('673', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadeva', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('674', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maina Kaderi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('675', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maina Sahasrabahu', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('676', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malekpur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('677', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maleth', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('678', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malhanama', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('679', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malhaniya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('680', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manraja', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('681', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mauwaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('682', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nargho', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('683', '2', null, null, '2017-03-26 13:49:28', null, null, 'Negada', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('684', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakari', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('685', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pansera', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('686', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parasbani', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('687', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paterwa', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('688', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pato', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('689', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patthargada', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('690', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phakira', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('691', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pharseth', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('692', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulkahi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('693', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra (West)', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('694', '2', null, null, '2017-03-26 13:49:28', null, null, 'Portaha', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('695', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramnagar', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('696', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur Malhaniya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('697', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rautahat', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('698', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rayapur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('699', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankarpura', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('700', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarashwar', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('701', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simraha Sigiyaun', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('702', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siswa Beihi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('703', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitapur', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('704', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarahi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('705', '2', null, null, '2017-03-26 13:49:28', null, null, 'Terahota', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('706', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tikuliya', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('707', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilathi', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('708', '2', null, null, '2017-03-26 13:49:28', null, null, 'Trikola', '18', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('709', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arnama Lalpur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('710', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arnama Rampur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('711', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aurahi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('712', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badharamal', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('713', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barchhawa', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('714', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bariyarpatti', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('715', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basbita', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('716', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bastipur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('717', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belaha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('718', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadaiya', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('719', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawanpur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('720', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawatipur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('721', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawanpur Kalabanchar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('722', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhokraha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('723', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishnupur Pra. Ma.', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('724', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishnupur Pra. Ra.', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('725', '2', null, null, '2017-03-26 13:49:28', null, null, 'Brahmagaughadi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('726', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandra Ayodhyapur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('727', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chatari', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('728', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chikana', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('729', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devipur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('730', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhodhana', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('731', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumari', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('732', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durgapur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('733', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('734', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gauripur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('735', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gautari', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('736', '2', null, null, '2017-03-26 13:49:28', null, null, 'Govindapur Malahanama', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('737', '2', null, null, '2017-03-26 13:49:28', null, null, 'Govindpur Taregana', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('738', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hakpara', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('739', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hanuman Nagar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('740', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harakathi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('741', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inarwa', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('742', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itarhawa', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('743', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itari Parsahi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('744', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itatar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('745', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janakinagar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('746', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jighaul', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('747', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabilasi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('748', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kachanari', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('749', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalyanpur Jabadi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('750', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalyanpur Kalabanchar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('751', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karjanha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('752', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharukyanhi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('753', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khirauna', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('754', '2', null, null, '2017-03-26 13:49:28', null, null, 'Krishnapur Birta', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('755', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lagadi Gadiyani', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('756', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lagadigoth', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('757', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshminiya', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('758', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur (Pra. Ma.)', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('759', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur Patari', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('760', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('761', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadewa Portaha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('762', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahanaur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('763', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maheshpur Patari', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('764', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhauliya', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('765', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhaura', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('766', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makhanaha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('767', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malhaniya Gamharia', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('768', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mauwahi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('769', '2', null, null, '2017-03-26 13:49:28', null, null, 'Media', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('770', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nahara Rigaul', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('771', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naraha Balkawa', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('772', '2', null, null, '2017-03-26 13:49:28', null, null, 'Navarajpur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('773', '2', null, null, '2017-03-26 13:49:28', null, null, 'Padariya Tharutol', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('774', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra Pra. Dha.', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('775', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra Pra. Pi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('776', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokharbhinda', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('777', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajpur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('778', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhuwanankarkatti', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('779', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanhaitha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('780', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarashwar', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('781', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikron', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('782', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisawani', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('783', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonmati Majhaura', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('784', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sothayan', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('785', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukhachina', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('786', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tenuwapati', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('787', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thalaha Kataha', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('788', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thegahi', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('789', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tulsipur', '19', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('790', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baku', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('791', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bapha', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('792', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basa', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('793', '2', null, null, '2017-03-26 13:49:28', null, null, 'Beni', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('794', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhakanje', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('795', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bung', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('796', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaulakharka', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('797', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaurikharka', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('798', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chheskam', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('799', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deusa', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('800', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goli', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('801', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gorakhani', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('802', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gudel', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('803', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jubing', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('804', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jubu', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('805', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaku', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('806', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kangel', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('807', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerung', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('808', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khumjung', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('809', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lokhim', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('810', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mabe', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('811', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mukali', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('812', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namche', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('813', '2', null, null, '2017-03-26 13:49:28', null, null, 'Necha Batase', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('814', '2', null, null, '2017-03-26 13:49:28', null, null, 'Necha Bedghari', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('815', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nele', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('816', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchan', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('817', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salyan', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('818', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sautang', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('819', '2', null, null, '2017-03-26 13:49:28', null, null, 'Takasindu', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('820', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tapting', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('821', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tingla', '23', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('822', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aaptar', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('823', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balaltar', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('824', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baraha', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('825', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barai', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('826', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basabote', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('827', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumarashuwa', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('828', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuttar', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('829', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaudandi', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('830', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumre', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('831', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hadiya', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('832', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hardeni', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('833', '2', null, null, '2017-03-26 13:49:28', null, null, 'Iname', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('834', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalpachilaune', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('835', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janti', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('836', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jogidaha', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('837', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katunjebawala', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('838', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khanbu', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('839', '2', null, null, '2017-03-26 13:49:28', null, null, 'Laphagaun', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('840', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhani', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('841', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhgau', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('842', '2', null, null, '2017-03-26 13:49:28', null, null, 'Limpatar', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('843', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mainamiani', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('844', '2', null, null, '2017-03-26 13:49:28', null, null, 'Myakhu', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('845', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nametar', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('846', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okhale', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('847', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchawati', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('848', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhari', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('849', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rauta', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('850', '2', null, null, '2017-03-26 13:49:28', null, null, 'Risku', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('851', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupatar', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('852', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saune', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('853', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shorung Chabise', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('854', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirise', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('855', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sithdipur', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('856', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundarpur', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('857', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tamlichha', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('858', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tapeshwari', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('859', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tawasri', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('860', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thanagaun', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('861', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thoksila', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('862', '2', null, null, '2017-03-26 13:49:28', null, null, 'Valaya Danda', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('863', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yayankhu', '20', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('864', '2', null, null, '2017-03-26 13:49:28', null, null, 'Andupatti', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('865', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aurahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('866', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baphai', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('867', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagchaura', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('868', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baheda Bala', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('869', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahuarba', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('870', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatauliya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('871', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balabakhar', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('872', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balaha Kathal', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('873', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balaha Sadhara', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('874', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ballagoth', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('875', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baniniya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('876', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basahiya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('877', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basbitti', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('878', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bateshwar', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('879', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bega Shivapur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('880', '2', null, null, '2017-03-26 13:49:28', null, null, 'Begadawar', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('881', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuchakrapur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('882', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhutahi Paterwa', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('883', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bindhi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('884', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisarbhora', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('885', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chakkar', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('886', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chora Koilpur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('887', '2', null, null, '2017-03-26 13:49:28', null, null, 'Debadiha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('888', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deuri Parbaha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('889', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devpura Rupetha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('890', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhabauli', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('891', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanauji', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('892', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubarikot Hathalekha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('893', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duhabi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('894', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ekarahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('895', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghodghans', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('896', '2', null, null, '2017-03-26 13:49:28', null, null, 'Giddha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('897', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gopalpur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('898', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goth Kohelpur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('899', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansapur Kathpula', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('900', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harine', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('901', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathipur Harbara', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('902', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inarwa', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('903', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itaharwa', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('904', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhatiyahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('905', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhojhi Kataiya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('906', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kachuri Thera', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('907', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kajara Ramaul', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('908', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanakpatti', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('909', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khajuri Chanha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('910', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khariyani', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('911', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kurtha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('912', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lagmamdha Guthi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('913', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakhauri', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('914', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakkad', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('915', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshminibas', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('916', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur Bagewa', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('917', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lohana Bahbangama', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('918', '2', null, null, '2017-03-26 13:49:28', null, null, 'Machijhitakaiya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('919', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahuwa (Pra. Ko)', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('920', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahuwa (Pra. Khe)', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('921', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mansingpatti', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('922', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mithileshwar Nikash', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('923', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mithileshwar Mauwahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('924', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mukhiyapatti Mushargiya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('925', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagarain', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('926', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nauwakhor Prashahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('927', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nunpatti', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('928', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pachaharwa', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('929', '2', null, null, '2017-03-26 13:49:28', null, null, 'Papikleshwar', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('930', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patanuka', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('931', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paterwa', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('932', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paudeshwar', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('933', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulgama', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('934', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puspalpur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('935', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raghunathpur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('936', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur Birta', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('937', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sapahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('938', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shantipur', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('939', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('940', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singyahi Maidan', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('941', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sinurjoda', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('942', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonigama', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('943', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suga Madhukarahi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('944', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suganikash', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('945', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarapatti Sirsiya', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('946', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thadi Jhijha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('947', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tulsi Chauda', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('948', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tulsiyahi Nikas', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('949', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tulsiyani Jabdi', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('950', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yadukuha', '24', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('951', '2', null, null, '2017-03-26 13:49:28', null, null, 'Alampu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('952', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babare', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('953', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhedapu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('954', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhirkot', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('955', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhusapheda', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('956', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bigu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('957', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bocha', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('958', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bulung', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('959', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chankhu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('960', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhetrapa', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('961', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chilankha', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('962', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyama', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('963', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadhpokhari', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('964', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandakharka', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('965', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gairimudi', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('966', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaurishankar', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('967', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghang Sukathokar', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('968', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hawa', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('969', '2', null, null, '2017-03-26 13:49:28', null, null, 'Japhe', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('970', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhule', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('971', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhyaku', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('972', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jugu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('973', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhre', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('974', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalingchok', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('975', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katakuti', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('976', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khare', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('977', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khupachagu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('978', '2', null, null, '2017-03-26 13:49:28', null, null, 'Laduk', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('979', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakuri Danda', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('980', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamabagar', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('981', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamidanda', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('982', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lapilang', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('983', '2', null, null, '2017-03-26 13:49:28', null, null, 'Magapauwa', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('984', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makaibari', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('985', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('986', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marbu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('987', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mati', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('988', '2', null, null, '2017-03-26 13:49:28', null, null, 'Melung', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('989', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mirge', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('990', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namdu', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('991', '2', null, null, '2017-03-26 13:49:28', null, null, 'Orang', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('992', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pawati', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('993', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phasku', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('994', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahare', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('995', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shailungeshwar', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('996', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunakhani', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('997', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundrawati', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('998', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sureti', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('999', '2', null, null, '2017-03-26 13:49:28', null, null, 'Susma Chhemawati', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1000', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syama', '29', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1001', '2', null, null, '2017-03-26 13:49:28', null, null, 'Anakar', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1002', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aurahi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1003', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagada', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1004', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagiya Banchauri', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1005', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bairjiya Lakshminiya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1006', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balawa', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1007', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banauli Donauli', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1008', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banauta', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1009', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basabitti', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1010', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bathnaha', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1011', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belgachhi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1012', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharatpur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1013', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatauliya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1014', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijayalpura', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1015', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bramarpura', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1016', '2', null, null, '2017-03-26 13:49:28', null, null, 'Damhi Marai', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1017', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamaura', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1018', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharmapur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1019', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhirapur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1020', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ekadarabela', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1021', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ekarahiya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1022', '2', null, null, '2017-03-26 13:49:28', null, null, 'Etaharwakatti', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1023', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaidaha Bhelpur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1024', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gonarpura', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1025', '2', null, null, '2017-03-26 13:49:28', null, null, 'Halkhori', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1026', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur Harinagari', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1027', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathilet', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1028', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatisarwa', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1029', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khairbanni', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1030', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaya Mara', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1031', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khopi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1032', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khuttapipradhi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1033', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolhusa Bagaiya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1034', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshminiya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1035', '2', null, null, '2017-03-26 13:49:28', null, null, 'Loharpatti', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1036', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadaiyatapanpur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1037', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhora Bishnupur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1038', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manara', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1039', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matihani', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1040', '2', null, null, '2017-03-26 13:49:28', null, null, 'Meghanath Gorhanna', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1041', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nainhi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1042', '2', null, null, '2017-03-26 13:49:28', null, null, 'Padaul', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1043', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsa Pateli', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1044', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsadewadh', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1045', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pashupatinagar', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1046', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulahatta Parikauli', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1047', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulakaha', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1048', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pigauna', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1049', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1050', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokharibhinda Samgrampur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1051', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raghunathpur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1052', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramgopalpur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1053', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratauli', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1054', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahasaula', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1055', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahorawa', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1056', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samdha', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1057', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarpallo', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1058', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shamsi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1059', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sripur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1060', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simardahi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1061', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singyahi', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1062', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisawakataiya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1063', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonama', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1064', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonamar', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1065', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonaum', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1066', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suga Bhawani', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1067', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundarpur', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1068', '2', null, null, '2017-03-26 13:49:28', null, null, 'Vangaha Prariya', '25', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1069', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bamti Bhandar', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1070', '2', null, null, '2017-03-26 13:49:28', null, null, 'Betali', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1071', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bethan', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1072', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadaure', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1073', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhirpani', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1074', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuji', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1075', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijulikot', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1076', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chanakhu', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1077', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chuchure', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1078', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadhuwa', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1079', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daragaun', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1080', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1081', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhyaurali', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1082', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dimipokhari', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1083', '2', null, null, '2017-03-26 13:49:28', null, null, 'Doramba', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1084', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duragau', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1085', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gelu', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1086', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goshwara', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1087', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothgau', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1088', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumdel', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1089', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gunsi Bhadaure', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1090', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gupteshwar', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1091', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hiledevi', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1092', '2', null, null, '2017-03-26 13:49:28', null, null, 'Himganga', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1093', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khandadevi', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1094', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaniyapani', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1095', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khimti', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1096', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kubukasthali', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1097', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakhanpur', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1098', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majuwa', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1099', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makadum', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1100', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naga Daha', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1101', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namadi', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1102', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakarbas', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1103', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pharpu', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1104', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulasi', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1105', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piukhuri', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1106', '2', null, null, '2017-03-26 13:49:28', null, null, 'Priti', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1107', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puranagau', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1108', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakathum', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1109', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1110', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rasanalu', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1111', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saipu', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1112', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanghutar', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1113', '2', null, null, '2017-03-26 13:49:28', null, null, 'Those', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1114', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilpung', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1115', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tokarpur', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1116', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wapti', '28', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1117', '2', null, null, '2017-03-26 13:49:28', null, null, 'Achalgadh', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1118', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arnaha', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1119', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aurahi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1120', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babarganj', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1121', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagdaha', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1122', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahadurpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1123', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balara', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1124', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bara Udhoran', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1125', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1126', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batraul', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1127', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bela', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1128', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belhi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1129', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belwajabdi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1130', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadsar', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1131', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawatipur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1132', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawanipur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1133', '2', null, null, '2017-03-26 13:49:28', null, null, 'Brahmapuri', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1134', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandra Nagar', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1135', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhataul', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1136', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatona', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1137', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhana Palbhawari', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1138', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanakaul Purba', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1139', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhangada', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1140', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumariya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1141', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadahiyabairi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1142', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamhariya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1143', '2', null, null, '2017-03-26 13:49:28', null, null, 'Godeta', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1144', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaurishankar', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1145', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harakthawa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1146', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haripur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1147', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haripurwa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1148', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathiyol', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1149', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hempur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1150', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuniya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1151', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janaki Nagar', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1152', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jingadawa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1153', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabilasi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1154', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalinjor', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1155', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaiharwa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1156', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khoriya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1157', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kisanpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1158', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kodena', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1159', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur Kodraha', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1160', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur Su.', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1161', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhubangoth', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1162', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhubani', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1163', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahinathpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1164', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mailhi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1165', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1166', '2', null, null, '2017-03-26 13:49:28', null, null, 'Masaili', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1167', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mirjapur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1168', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mohanpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1169', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motipur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1170', '2', null, null, '2017-03-26 13:49:28', null, null, 'Musauli', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1171', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayan Khola', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1172', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayanpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1173', '2', null, null, '2017-03-26 13:49:28', null, null, 'Netraganj', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1174', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naukailawa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1175', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1176', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parwanipur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1177', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pharahadawa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1178', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulparasi', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1179', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pidari', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1180', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pidariya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1181', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipariya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1182', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramnagar Bahaur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1183', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranban', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1184', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raniganj', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1185', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rohuwa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1186', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakraula', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1187', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salempur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1188', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangrampur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1189', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shahorwa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1190', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shreepur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1191', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikhauna', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1192', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simara', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1193', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisautiya', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1194', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisaut', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1195', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shankarpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1196', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sohadawa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1197', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sudama', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1198', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundarpur', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1199', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundarpur Choharwa', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1200', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tribhuwannagar', '26', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1201', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amale', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1202', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arun Thakur', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1203', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahuntilpung', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1204', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balajor', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1205', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baseshwar', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1206', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bastipur', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1207', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belghari', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1208', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadrakali', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1209', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhiman', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1210', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bimeshwar', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1211', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimsthan', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1212', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuwaneshwar Gwaltar', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1213', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bitijor Bagaincha', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1214', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadiguranshe', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1215', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dudbhanjyang', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1216', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur Gadhi', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1217', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harsahi', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1218', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatpate', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1219', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalkanya', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1220', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jarayotar', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1221', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhangajholi Ratmati', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1222', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jinakhu', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1223', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kakur Thakur', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1224', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalpabriksha', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1225', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kapilakot', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1226', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khang Sang', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1227', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kholagaun', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1228', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kusheshwar Dumja', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1229', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kyaneshwar', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1230', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ladabhir', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1231', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lampantar', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1232', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevdada', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1233', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevsthan', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1234', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahendrajhayadi', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1235', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majuwa', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1236', '2', null, null, '2017-03-26 13:49:28', null, null, 'Netrakali', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1237', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nipane', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1238', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipalmadi', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1239', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purano Jhangajholi', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1240', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranibas', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1241', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranichauri', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1242', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratnachura', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1243', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratnawati', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1244', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shanteshwari', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1245', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddheshwari', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1246', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirthauli', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1247', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitalpati', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1248', '2', null, null, '2017-03-26 13:49:28', null, null, 'Solpathana', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1249', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunam Pokhari', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1250', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tamajor', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1251', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tandi', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1252', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tinkanya', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1253', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tosramkhola', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1254', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tribhuvan Ambote', '27', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1255', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bageshwari', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1256', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balkot', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1257', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balkumari', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1258', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bode', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1259', '2', null, null, '2017-03-26 13:49:28', null, null, 'Changunarayan', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1260', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chapacho', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1261', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhaling', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1262', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chittpol', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1263', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadhikot', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1264', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duwakot', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1265', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gundu', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1266', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhaukhel', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1267', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katunje', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1268', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lokanthali', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1269', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagadesh', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1270', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagarkot', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1271', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nankhel', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1272', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipadol', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1273', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirutar', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1274', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sudal', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1275', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tathali', '30', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1276', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aginchok', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1277', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baireni', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1278', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baseri', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1279', '2', null, null, '2017-03-26 13:49:28', null, null, 'Benighat', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1280', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumesthan', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1281', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhathum', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1282', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chainpur', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1283', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatre Dyaurali', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1284', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darkha', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1285', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhola', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1286', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhusha', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1287', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhuwakot', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1288', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajuri', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1289', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghussa', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1290', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goganpani', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1291', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumdi', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1292', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jiwanpur', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1293', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jharlang', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1294', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jogimara', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1295', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jyamaruk', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1296', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalleri', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1297', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katunje', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1298', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kebalpur', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1299', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khalte', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1300', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khari', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1301', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kiranchok', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1302', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kumpur', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1303', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lapa', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1304', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevsthan', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1305', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maidi', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1306', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marpak', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1307', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mulpani', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1308', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nalang', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1309', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naubise', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1310', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulkharka', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1311', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pida', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1312', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rigaun', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1313', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salang', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1314', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salyankot', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1315', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salyantar', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1316', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satyadevi', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1317', '2', null, null, '2017-03-26 13:49:28', null, null, 'Semjong', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1318', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirtung', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1319', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tasarphu', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1320', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thakre', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1321', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tipling', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1322', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tripureshwar', '31', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1323', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aalapot', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1324', '2', null, null, '2017-03-26 13:49:28', null, null, 'Band Bhanjyang', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1325', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajrayogini', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1326', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balambu', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1327', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baluwa', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1328', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadrabas', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1329', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimdhunga', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1330', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budanilkantha', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1331', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chalnakhel', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1332', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chapali Bhadrakali', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1333', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhaimale', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1334', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chobhar', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1335', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nepal', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1336', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chauketar Dahachok', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1337', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chunikhel', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1338', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daanchhi', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1339', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dakshinkali', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1340', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhapasi', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1341', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharmasthali', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1342', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gapalphedi', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1343', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gokarna', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1344', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goldhunga', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1345', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gongabu', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1346', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothatar', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1347', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ichankhu Narayan', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1348', '2', null, null, '2017-03-26 13:49:28', null, null, 'Indrayani', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1349', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhor Mahankal', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1350', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jitpurphedi', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1351', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jorpati', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1352', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhresthali', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1353', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kapan', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1354', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadka Bhadrakali', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1355', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kirtipur Chitubihar', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1356', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koteshwar', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1357', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lapsiphedi', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1358', '2', null, null, '2017-03-26 13:49:28', null, null, 'Machhegaun', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1359', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevsthan', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1360', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahankal', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1361', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manmaiju', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1362', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matatirtha', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1363', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mulpani', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1364', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nanglebhare', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1365', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naikap Naya Bhanjyang', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1366', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naikap Purano Bhanjyang', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1367', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nayapati', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1368', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phutung', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1369', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pukhulachhi', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1370', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramkot', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1371', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangla', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1372', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satungal', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1373', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syuchatar', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1374', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sheshnarayan', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1375', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitapaila', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1376', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sokhek', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1377', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundarijal', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1378', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suntol', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1379', '2', null, null, '2017-03-26 13:49:28', null, null, 'Talkududechaur', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1380', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thankot', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1381', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tinthana', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1382', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tokha Chandeshwari', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1383', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tokha Saraswati', '32', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1384', '2', null, null, '2017-03-26 13:49:28', null, null, 'Anekot', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1385', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balthali', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1386', '2', null, null, '2017-03-26 13:49:28', null, null, 'Walting', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1387', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baluwapatti Deupur', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1388', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banakhu Chaur', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1389', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batase', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1390', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bekhsimle', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1391', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimkhori', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1392', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumidanda', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1393', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumlungtar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1394', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birtadeurali', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1395', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bolde Phediche', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1396', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhakhani', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1397', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chalal Ganeshsthan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1398', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandeni Mandan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1399', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaubas', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1400', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyamrangbesi', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1401', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyasing Kharka', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1402', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandagaun', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1403', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dewabhumi Baluwa', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1404', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devitar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1405', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhungkharka Bahrabise', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1406', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhuseni Siwalaya', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1407', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dolalghat', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1408', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gairi Bisauna Deupur', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1409', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghartichhap', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1410', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gokule', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1411', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothpani', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1412', '2', null, null, '2017-03-26 13:49:28', null, null, 'Indreshwar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1413', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaisithok Mandan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1414', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janagal', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1415', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jyamdi Mandan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1416', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalati Bhumidanda', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1417', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanpur Kalapani', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1418', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kartike Deurali', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1419', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katunje Besi', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1420', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhre Nitya Chandeshwari', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1421', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khahare Pangu', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1422', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharelthok', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1423', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharpachok', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1424', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khopasi', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1425', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolati Bhumlu', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1426', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koshidekha', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1427', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kurubas Chapakhori', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1428', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kushadevi', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1429', '2', null, null, '2017-03-26 13:49:28', null, null, 'Machchhegaun', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1430', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madan Kundari', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1431', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevsthan Mandan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1432', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevtar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1433', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahankal Chaur', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1434', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahendra Jyoti', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1435', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhi Pheda', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1436', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malpi', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1437', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mangaltar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1438', '2', null, null, '2017-03-26 13:49:28', null, null, 'Milche', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1439', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagre Gagarche', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1440', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nala', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1441', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nasiksthan Sanga', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1442', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nayagaun Deupur', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1443', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalante Bhumlu', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1444', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalametar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1445', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phoksingtar', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1446', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhari Chaunri', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1447', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhari Narayansthan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1448', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ravi Opi', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1449', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rayale', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1450', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saldhara', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1451', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salle Bhumlu', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1452', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salmechakala', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1453', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankhu Patichaur', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1454', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanowangthali', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1455', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saping', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1456', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sharada Batase', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1457', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarmathali', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1458', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarasyunkharka', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1459', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sathighar Bhagawati', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1460', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shikar Ambote', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1461', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simalchaur Shyampati', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1462', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simthali', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1463', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipali Chilaune', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1464', '2', null, null, '2017-03-26 13:49:28', null, null, 'Subbagaun', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1465', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunthan', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1466', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thaukhal', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1467', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Parsal', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1468', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tukucha Nala', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1469', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ugratara Janagal', '33', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1470', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ashrang', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1471', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhardev', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1472', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhattedanda', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1473', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bukhel', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1474', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandanpur', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1475', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaughare', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1476', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dalchoki', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1477', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devichaur', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1478', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghusel', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1479', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durlung', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1480', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gimdi', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1481', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gotikhel', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1482', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ikudol', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1483', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaleshwar', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1484', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malta', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1485', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manikhel', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1486', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nallu', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1487', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pyutar', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1488', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankhu', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1489', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thuladurlung', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1490', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badikhel', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1491', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisankhunarayan', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1492', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bungamati', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1493', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhampi', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1494', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chapagaon', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1495', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhapakhel', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1496', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dukuchhap', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1497', '2', null, null, '2017-03-26 13:49:28', null, null, 'Godamchaur', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1498', '2', null, null, '2017-03-26 13:49:28', null, null, 'Godawari', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1499', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harisiddhi', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1500', '2', null, null, '2017-03-26 13:49:28', null, null, 'Imadol', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1501', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jharuwarasi', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1502', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khokana', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1503', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamatar', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1504', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lele', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1505', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lubhu', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1506', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sainbu', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1507', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddhipur', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1508', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunakothi', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1509', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thecho', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1510', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thaiba', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1511', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tikathali', '34', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1512', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bageshwari', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1513', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balkumari', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1514', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barsunchet', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1515', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belkot', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1516', '2', null, null, '2017-03-26 13:49:28', null, null, 'Beteni', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1517', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadratar', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1518', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhalche', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1519', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhasing', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1520', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bungtang', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1521', '2', null, null, '2017-03-26 13:49:28', null, null, 'Charghare', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1522', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaturale', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1523', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaughada', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1524', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chauthe', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1525', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhap', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1526', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangsing', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1527', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1528', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duipipal', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1529', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phikuri', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1530', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganeshsthan', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1531', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaunkharka', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1532', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gerkhu', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1533', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghyangphedi', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1534', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gorsyang', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1535', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jiling', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1536', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kakani', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1537', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalibas', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1538', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikahalde', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1539', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalyanpur', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1540', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaule', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1541', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadag Bhanjyang', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1542', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharanitar', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1543', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kholegaun Khanigaun', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1544', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kintang', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1545', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kumari', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1546', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lachyang', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1547', '2', null, null, '2017-03-26 13:49:28', null, null, 'Likhu', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1548', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madanpur', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1549', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahakali', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1550', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manakamana', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1551', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narjamandap', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1552', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okharpauwa', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1553', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchkanya', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1554', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ralukadevi', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1555', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratmate', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1556', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rautbesi', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1557', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salme', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1558', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samari', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1559', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samundradevi', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1560', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samundratar', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1561', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shikharbesi', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1562', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikre', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1563', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundaradevi', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1564', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunkhani', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1565', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suryamati', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1566', '2', null, null, '2017-03-26 13:49:28', null, null, 'Talakhu', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1567', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taruka', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1568', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thanapati', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1569', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thansing', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1570', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thaprek', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1571', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tupche', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1572', '2', null, null, '2017-03-26 13:49:28', null, null, 'Urleni', '35', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1573', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhorle', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1574', '2', null, null, '2017-03-26 13:49:28', null, null, 'Briddhim', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1575', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chilime', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1576', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandagaun', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1577', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhaibung', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1578', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gatlang', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1579', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goljung', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1580', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haku', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1581', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jibjibe', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1582', '2', null, null, '2017-03-26 13:49:28', null, null, 'Laharepauwa', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1583', '2', null, null, '2017-03-26 13:49:28', null, null, 'Langtang', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1584', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramche', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1585', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saramthali', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1586', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syaphru', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1587', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulogaun', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1588', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thuman', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1589', '2', null, null, '2017-03-26 13:49:28', null, null, 'Timure', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1590', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yarsa', '36', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1591', '2', null, null, '2017-03-26 13:49:28', null, null, 'Atarpur', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1592', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badegau', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1593', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banskharka', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1594', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baramchi', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1595', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barhabise', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1596', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baruwa', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1597', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batase', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1598', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimtar', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1599', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhote Namlang', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1600', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhotsiba', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1601', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaukati', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1602', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhumthang', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1603', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubarchaur', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1604', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gati', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1605', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghorthali', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1606', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghuskun', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1607', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gloche', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1608', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumba', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1609', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gunsakot', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1610', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hagam', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1611', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haibung', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1612', '2', null, null, '2017-03-26 13:49:28', null, null, 'Helumbu', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1613', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ichok', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1614', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ikhu Bhanjyang', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1615', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalbire', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1616', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jethal', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1617', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1618', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karkhali', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1619', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katambas', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1620', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kiul', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1621', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kunchok', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1622', '2', null, null, '2017-03-26 13:49:28', null, null, 'Langarche', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1623', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lisankhu', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1624', '2', null, null, '2017-03-26 13:49:28', null, null, 'Listikot', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1625', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahankal', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1626', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maneshwara', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1627', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mankha', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1628', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marming', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1629', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motang', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1630', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nawalpur', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1631', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pagretar', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1632', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palchok', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1633', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pangtang', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1634', '2', null, null, '2017-03-26 13:49:28', null, null, 'Petaku', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1635', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulping Katti', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1636', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulpingdanda', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1637', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulpingkot', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1638', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piskar', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1639', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramche', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1640', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangachok', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1641', '2', null, null, '2017-03-26 13:49:28', null, null, 'Selang', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1642', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipa Pokhare', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1643', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipal Kabhre', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1644', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunkhani', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1645', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syaule Bazar', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1646', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tatopani', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1647', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tauthali', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1648', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tekanpur', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1649', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thakani', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1650', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thampal Chhap', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1651', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thangpalkot', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1652', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thokarpa', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1653', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Dhading', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1654', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Pakhar', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1655', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Siruwari', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1656', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thum Pakhar', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1657', '2', null, null, '2017-03-26 13:49:28', null, null, 'Timpul Ghyangul', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1658', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yamanadanda', '37', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1659', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amarpatti', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1660', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amlekhganj', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1661', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amritganj', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1662', '2', null, null, '2017-03-26 13:49:28', null, null, 'Avab', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1663', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babuain', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1664', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bachhanpurwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1665', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badaki Phulbariya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1666', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagadi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1667', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahuari', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1668', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balirampur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1669', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bandhuwan', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1670', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banjariya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1671', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barainiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1672', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bariyarpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1673', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1674', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batara', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1675', '2', null, null, '2017-03-26 13:49:28', null, null, 'Beldari', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1676', '2', null, null, '2017-03-26 13:49:28', null, null, 'Benauli', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1677', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagwanpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1678', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhaluwai Arwaliya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1679', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatauda', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1680', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhaudaha', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1681', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuluhi Marwaliya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1682', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishnupur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1683', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishnupurwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1684', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishrampur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1685', '2', null, null, '2017-03-26 13:49:28', null, null, 'Biswambharpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1686', '2', null, null, '2017-03-26 13:49:28', null, null, 'Brahmapuri', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1687', '2', null, null, '2017-03-26 13:49:28', null, null, 'Buniyad', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1688', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatawa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1689', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dahiyar', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1690', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dewapur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1691', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharmanagar', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1692', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dohari', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1693', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadhahal', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1694', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganj Bhawanipur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1695', '2', null, null, '2017-03-26 13:49:28', null, null, 'Golaganj', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1696', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haraiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1697', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hardiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1698', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1699', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inarwamal', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1700', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inarwasira', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1701', '2', null, null, '2017-03-26 13:49:28', null, null, 'Itiyahi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1702', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhitakaiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1703', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jitpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1704', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabahigoth', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1705', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabahijabdi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1706', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kachorwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1707', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karaiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1708', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khopawa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1709', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khutwajabdi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1710', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kudawa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1711', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur Kotwali', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1712', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lipanimal', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1713', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhurijabdi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1714', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahendra Adarsha', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1715', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maheshpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1716', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maini', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1717', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhariya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1718', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manaharwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1719', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matiarwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1720', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motisar', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1721', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naktuwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1722', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narahi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1723', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakadiya Chikani', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1724', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsurampur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1725', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paterwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1726', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patharhati', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1727', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathora', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1728', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pheta', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1729', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piparpati Ek', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1730', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piparpati Dui', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1731', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piparpati Jabdi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1732', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piparpati Parchrauwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1733', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra Basantapur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1734', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piprabirta', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1735', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipradhi Goth', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1736', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prasauni', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1737', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prastoka', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1738', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purainiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1739', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raghunathpur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1740', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur Tokani', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1741', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampurwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1742', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rauwahi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1743', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srinagar Bairiya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1744', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sihorwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1745', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sinhasani', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1746', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisahaniya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1747', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tedhakatti', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1748', '2', null, null, '2017-03-26 13:49:28', null, null, 'Telkuwa', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1749', '2', null, null, '2017-03-26 13:49:28', null, null, 'Terariya', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1750', '2', null, null, '2017-03-26 13:49:28', null, null, 'Uchidiha', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1751', '2', null, null, '2017-03-26 13:49:28', null, null, 'Umarjan', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1752', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jitpur Bhawanipur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1753', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhata Pipra', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1754', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phattepur', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1755', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumbarwana', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1756', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratnapuri', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1757', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharatganj Singaul', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1758', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kakadi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1759', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolhabi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1760', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prasona', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1761', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sapahi', '38', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1762', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ayodhyapuri', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1763', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bachhayauli', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1764', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagauda', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1765', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhandara', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1766', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birendranagar', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1767', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandi Bhanjyang', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1768', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dahakhani', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1769', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darechok', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1770', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dibyanagar', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1771', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gardi', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1772', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gitanagar', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1773', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagatpur', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1774', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jutpani', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1775', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabilas', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1776', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kathar', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1777', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaule', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1778', '2', null, null, '2017-03-26 13:49:28', null, null, 'Korak', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1779', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lothar', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1780', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madi Kalyanpur', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1781', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mangalpur', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1782', '2', null, null, '2017-03-26 13:49:28', null, null, 'Meghauli', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1783', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayanpur', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1784', '2', null, null, '2017-03-26 13:49:28', null, null, 'Padampur', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1785', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pancha Kanya', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1786', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parbatipur', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1787', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patihani', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1788', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulbari', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1789', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piple', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1790', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pithuwa', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1791', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shaktikhor', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1792', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shivanagar', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1793', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddi', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1794', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shukranagar khairahani', '41', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1795', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ambhanjyang', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1796', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajrabarahi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1797', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basamadi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1798', '2', null, null, '2017-03-26 13:49:28', null, null, 'Betini', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1799', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhainse', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1800', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharta Pundyadevi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1801', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimphedi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1802', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhichaur', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1803', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatiwan', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1804', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chitlang', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1805', '2', null, null, '2017-03-26 13:49:28', null, null, 'Churiyamai', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1806', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daman', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1807', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandakharka', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1808', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhimal', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1809', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gogane', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1810', '2', null, null, '2017-03-26 13:49:28', null, null, 'Handikhola', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1811', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatiya', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1812', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hurnamadi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1813', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ipa Panchakanya', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1814', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikatar', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1815', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kankada', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1816', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khairang', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1817', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kogate', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1818', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kulekhani', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1819', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makwanpurgadhi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1820', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manahari', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1821', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manthali', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1822', '2', null, null, '2017-03-26 13:49:28', null, null, 'Markhu', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1823', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namtar', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1824', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nibuwatar', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1825', '2', null, null, '2017-03-26 13:49:28', null, null, 'Padma Pokhari', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1826', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palung', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1827', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phakhel', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1828', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phaparbadi', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1829', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raigaun', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1830', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raksirang', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1831', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarikhet Palase', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1832', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shikharpur', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1833', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisneri Mahadevsthan', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1834', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukaura', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1835', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thingan', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1836', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tistung Deurali', '42', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1837', '2', null, null, '2017-03-26 13:49:28', null, null, 'Alau', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1838', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amarpatti', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1839', '2', null, null, '2017-03-26 13:49:28', null, null, 'Auraha', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1840', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagahi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1841', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagbana', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1842', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bageshwari', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1843', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahauri Pidari', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1844', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahuarba Bhatha', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1845', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basadilwa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1846', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1847', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belwa Parsauni', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1848', '2', null, null, '2017-03-26 13:49:28', null, null, 'Beriya Birta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1849', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawanipur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1850', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhedihari', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1851', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhisawa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1852', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijbaniya', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1853', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bindyabasini', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1854', '2', null, null, '2017-03-26 13:49:28', null, null, 'Biranchibarba', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1855', '2', null, null, '2017-03-26 13:49:28', null, null, 'Biruwa Guthi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1856', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisrampur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1857', '2', null, null, '2017-03-26 13:49:28', null, null, 'Charani', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1858', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deukhana', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1859', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhaubini', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1860', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1861', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamhariya', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1862', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghoddauda Pipra', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1863', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghore', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1864', '2', null, null, '2017-03-26 13:49:28', null, null, 'Govindapur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1865', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1866', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur Birta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1867', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harapataganj', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1868', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harpur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1869', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagarnathpur Sira', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1870', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaimanglapur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1871', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janikatala', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1872', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhauwa Guthi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1873', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jitpur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1874', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kauwa Ban Kataiya', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1875', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lahawarthakari', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1876', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakhanpur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1877', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lal Parsa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1878', '2', null, null, '2017-03-26 13:49:28', null, null, 'Langadi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1879', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lipani Birta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1880', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhuban Mathaul', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1881', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevpatti', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1882', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahuwan', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1883', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mainiyari', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1884', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mainpur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1885', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mikhampur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1886', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mirjapur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1887', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mosihani', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1888', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudali', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1889', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagardaha', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1890', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nirchuta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1891', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nirmal Basti', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1892', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pancharukhi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1893', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsauni Birta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1894', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsauni Matha', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1895', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patbari Tola-Warwa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1896', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paterwa Sugauli', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1897', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pidariguthi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1898', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhariya', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1899', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prasurampur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1900', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramgadhawa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1901', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramnagari', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1902', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sabaithawa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1903', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhuwa Parsauni', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1904', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samjhauta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1905', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shankar Saraiya', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1906', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sapauli', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1907', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sedhawa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1908', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shiva Worga', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1909', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirsiya Khalwatola', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1910', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonbarsa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1911', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srisiya', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1912', '2', null, null, '2017-03-26 13:49:28', null, null, 'Subarnapur', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1913', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sugauli Birta', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1914', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sugauli Partewa', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1915', '2', null, null, '2017-03-26 13:49:28', null, null, 'Surjaha', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1916', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thori', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1917', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tulsi Barba', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1918', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udayapur Dhurmi', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1919', '2', null, null, '2017-03-26 13:49:28', null, null, 'Vauratar', '39', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1920', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ajagabi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1921', '2', null, null, '2017-03-26 13:49:28', null, null, 'Akolawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1922', '2', null, null, '2017-03-26 13:49:28', null, null, 'Auraiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1923', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badharwa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1924', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1925', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahuwa Madanpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1926', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bairiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1927', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banjaraha', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1928', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bariyarpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1929', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapatti', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1930', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1931', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basbiti Jingadiya ', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1932', '2', null, null, '2017-03-26 13:49:28', null, null, 'halohiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1933', '2', null, null, '2017-03-26 13:49:28', null, null, ' Bhediyahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1934', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birtipraskota', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1935', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bishrampur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1936', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisunpurwa Manpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1937', '2', null, null, '2017-03-26 13:49:28', null, null, 'Brahmapuri', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1938', '2', null, null, '2017-03-26 13:49:28', null, null, 'Debahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1939', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharampur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1940', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharhari', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1941', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dipahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1942', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumriya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1943', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balchanpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1944', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadhi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1945', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamhariya Birta', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1946', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamhariya Parsa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1947', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gangapipra', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1948', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garuda Bairiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1949', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gedahiguthi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1950', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gunahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1951', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hajminiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1952', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hardiya Paltuwa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1953', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harsaha', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1954', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathiyahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1955', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inarbari Jyutahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1956', '2', null, null, '2017-03-26 13:49:28', null, null, 'Inaruwa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1957', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jatahare', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1958', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jayanagar', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1959', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jethrahiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1960', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhunkhunma', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1961', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jingadawa Belbichhwa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1962', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jingadiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1963', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jowaha', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1964', '2', null, null, '2017-03-26 13:49:28', null, null, 'Judibela', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1965', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kakanpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1966', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karkach Karmaiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1967', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karuniya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1968', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katahariya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1969', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khesarhiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1970', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshminiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1971', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1972', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur Belbichhawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1973', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lokaha', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1974', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madanpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1975', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madhopur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1976', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahamadpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1977', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1978', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maryadpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1979', '2', null, null, '2017-03-26 13:49:28', null, null, 'Masedawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1980', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mathiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1981', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matsari', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1982', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mithuawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1983', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudwalawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1984', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narkatiya Guthi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1985', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pacharukhi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1986', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pataura', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1987', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathara Budharampur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1988', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paurai', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1989', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phatuha Maheshpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1990', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipariya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1991', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra Bhagwanpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1992', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra Pokhariya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1993', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipra Rajbara', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1994', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pothiyahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1995', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pratappur Paltuwa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1996', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prempur Gunahi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1997', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purainawma', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1998', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raghunathpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('1999', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajdevi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2000', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajpur Pharhadawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2001', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajpur Tulsi', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2002', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramoli Bairiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2003', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur Khap', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2004', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangapur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2005', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhuwa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2006', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhuwa Dhamaura', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2007', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samanpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2008', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sangrampur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2009', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santapur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2010', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santpur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2011', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarmujawa', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2012', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saruatha', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2013', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saunaraniya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2014', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sawagada', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2015', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shitalpur Bairgania', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2016', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simara Bhawanipur', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2017', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirsiya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2018', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tejapakar', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2019', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tengraha', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2020', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tikuliya', '40', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2021', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aanppipal', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2022', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aaru Arbang', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2023', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aaru Chanuate', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2024', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aarupokhari', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2025', '2', null, null, '2017-03-26 13:49:28', null, null, 'Asrang', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2026', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baguwa', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2027', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bakrang', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2028', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhirkot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2029', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumlichok', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2030', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bihi', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2031', '2', null, null, '2017-03-26 13:49:28', null, null, 'Borlang', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2032', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bunkot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2033', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhaikampar', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2034', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhoprak', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2035', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chumchet', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2036', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chyangli', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2037', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darbhung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2038', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2039', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhawa', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2040', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhuwakot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2041', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaikhur', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2042', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gakhu', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2043', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghairung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2044', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghyachok', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2045', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghyalchok', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2046', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gorakhkali', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2047', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumda', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2048', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansapur', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2049', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harbhi', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2050', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaubari', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2051', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kashigaun', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2052', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerabari', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2053', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerauja', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2054', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharibot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2055', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khoplang', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2056', '2', null, null, '2017-03-26 13:49:28', null, null, 'Laprak', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2057', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lapu', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2058', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lho', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2059', '2', null, null, '2017-03-26 13:49:28', null, null, 'Makaising', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2060', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manakamana', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2061', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manbu', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2062', '2', null, null, '2017-03-26 13:49:28', null, null, 'Masel', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2063', '2', null, null, '2017-03-26 13:49:28', null, null, 'Muchhok', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2064', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namjung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2065', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nareshwar', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2066', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palumtar', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2067', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchkhuwadeurali', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2068', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pandrung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2069', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phinam', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2070', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phujel', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2071', '2', null, null, '2017-03-26 13:49:28', null, null, 'Prok', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2072', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranishwara', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2073', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samagaun', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2074', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saurpani', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2075', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srinathkot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2076', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simjung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2077', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirdibas', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2078', '2', null, null, '2017-03-26 13:49:28', null, null, 'Swara', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2079', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taklung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2080', '2', null, null, '2017-03-26 13:49:28', null, null, 'Takukot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2081', '2', null, null, '2017-03-26 13:49:28', null, null, 'Takumajhalakuribot', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2082', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tandrang', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2083', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tanglichok', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2084', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taple', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2085', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tara Nagar', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2086', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thalajung', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2087', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thumi', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2088', '2', null, null, '2017-03-26 13:49:28', null, null, 'Uiya', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2089', '2', null, null, '2017-03-26 13:49:28', null, null, 'Warpak', '43', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2090', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arba Vijaya', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2091', '2', null, null, '2017-03-26 13:49:28', null, null, 'Armala', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2092', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garlang', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2093', '2', null, null, '2017-03-26 13:49:28', null, null, 'Begnas', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2094', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhachok', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2095', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadaure Tamagi', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2096', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharat Pokhari', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2097', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chapakot', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2098', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangsing', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2099', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2100', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhampus', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2101', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhikure Pokhari', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2102', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhital', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2103', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghachok', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2104', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghandruk', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2105', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansapur', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2106', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hemaja', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2107', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kahun', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2108', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2109', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaskikot', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2110', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kristinachnechaur', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2111', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lahachok', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2112', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamachaur', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2113', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lumle', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2114', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lwangghale', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2115', '2', null, null, '2017-03-26 13:49:28', null, null, 'Machhapuchchhre', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2116', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhthana', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2117', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mala', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2118', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mauja', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2119', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mijuredanda', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2120', '2', null, null, '2017-03-26 13:49:28', null, null, 'Namarjung', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2121', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nirmalpokhari', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2122', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parche', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2123', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pumdibhumdi', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2124', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puranchaur', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2125', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakhi', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2126', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ribhan', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2127', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupakot', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2128', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saimarang', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2129', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salyan', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2130', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarangkot', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2131', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sardikhola', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2132', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shisuwa', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2133', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddha', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2134', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sildujure', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2135', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thumakodanda', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2136', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thumki', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2137', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhalam', '44', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2138', '2', null, null, '2017-03-26 13:49:28', null, null, 'Archalbot', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2139', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahundanda', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2140', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajhakhet', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2141', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balungpani', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2142', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangre', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2143', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bansar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2144', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhalayakharka', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2145', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharte', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2146', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhoje', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2147', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhorletar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2148', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhotewodar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2149', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhujung', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2150', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhulbhule', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2151', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bichaur', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2152', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chakratirtha', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2153', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandreshwar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2154', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chiti', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2155', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamilikuwa', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2156', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhodeni', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2157', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhuseni', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2158', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dudhpokhari', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2159', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duradanda', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2160', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gauda', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2161', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghanpokhara', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2162', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghermu', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2163', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gilung', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2164', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hiletaksar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2165', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ilampokhari', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2166', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ishaneshwar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2167', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jita', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2168', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karapu', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2169', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khudi', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2170', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolki', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2171', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kunchha', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2172', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maling', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2173', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mohoriyakot', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2174', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nalma', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2175', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nauthar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2176', '2', null, null, '2017-03-26 13:49:28', null, null, 'Neta', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2177', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pachok', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2178', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parewadanda', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2179', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pasagaun', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2180', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phaleni', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2181', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puranokot', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2182', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pyarjung', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2183', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangha', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2184', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samibhanjyang', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2185', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srimanjyang', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2186', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simpani', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2187', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sindure', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2188', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundarbazar', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2189', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suryapal', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2190', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taghring', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2191', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tandrang', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2192', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarku', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2193', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarkughat', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2194', '2', null, null, '2017-03-26 13:49:28', null, null, 'Uttarkanya', '45', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2195', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagarchhap', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2196', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhakra', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2197', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chame', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2198', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharapani', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2199', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghyaru', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2200', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khangsar', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2201', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manang', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2202', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nar', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2203', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nyawal', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2204', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pisang', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2205', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phu', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2206', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tachi Bagarchhap', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2207', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tanki Manang', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2208', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thoche', '48', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2209', '2', null, null, '2017-03-26 13:49:28', null, null, 'Almadevi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2210', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arjun Chaupari', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2211', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aruchaur', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2212', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arukharka', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2213', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagephatake', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2214', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahakot', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2215', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banethok Deurali', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2216', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatkhola', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2217', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bichari Chautara', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2218', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birgha Archale', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2219', '2', null, null, '2017-03-26 13:49:28', null, null, 'Biruwa Archale', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2220', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandi Bhanjyang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2221', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandikalika', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2222', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhangchhangdi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2223', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chilaunebas', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2224', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chimnebas', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2225', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chisapani', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2226', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chitre Bhanjyang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2227', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darsing Dahathum', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2228', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanubase', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2229', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhapuk Simal Bhanjyang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2230', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganeshpur', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2231', '2', null, null, '2017-03-26 13:49:28', null, null, 'Iladi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2232', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagat Bhanjyang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2233', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagatradevi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2234', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikakot', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2235', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karendanda', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2236', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolma Barahachaur', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2237', '2', null, null, '2017-03-26 13:49:28', null, null, 'Keware Bhanjyang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2238', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khilung Deurali', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2239', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kichnas', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2240', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kyakami', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2241', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhakot Sivalaya', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2242', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malengkot', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2243', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manakamana', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2244', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nibuwakharka', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2245', '2', null, null, '2017-03-26 13:49:28', null, null, 'Oraste', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2246', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakbadi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2247', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchamul', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2248', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pauwegaude', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2249', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pekhuwa Baghakhor', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2250', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pelakot', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2251', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pelkachaur', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2252', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phaparthum', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2253', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phedikhola', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2254', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pindikhola', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2255', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangvang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2256', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rapakot', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2257', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhar', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2258', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daraun', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2259', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satupasal', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2260', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sekham', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2261', '2', null, null, '2017-03-26 13:49:28', null, null, 'Setidobhan', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2262', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srikrishna Gandaki', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2263', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirsekot', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2264', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sorek', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2265', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taksar', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2266', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thuladihi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2267', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thumpokhara', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2268', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tindobate', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2269', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tulsibhanjyang', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2270', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wangsing Deurali', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2271', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yaladi', '46', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2272', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ambukhaireni', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2273', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arunodaya', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2274', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baidi', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2275', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barbhanjyang', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2276', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2277', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhanu', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2278', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhanumati', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2279', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimad', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2280', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhirkot', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2281', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhirlung', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2282', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhang', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2283', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhimkeshwari', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2284', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhipchhipe', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2285', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chok Chisapani', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2286', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2287', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devghat', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2288', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharampani', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2289', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajarkot', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2290', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghansikuwa', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2291', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamune Bhanjyang', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2292', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabilas', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2293', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kahu Shivapur', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2294', '2', null, null, '2017-03-26 13:49:28', null, null, 'Keshavtar', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2295', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kihun', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2296', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kota', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2297', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotdarbar', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2298', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kyamin', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2299', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhakot', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2300', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manpang', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2301', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phirphire', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2302', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhari Bhanjyang', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2303', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purkot', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2304', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raipur', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2305', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramjakot', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2306', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranipokhari', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2307', '2', null, null, '2017-03-26 13:49:28', null, null, 'Risti', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2308', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupakot', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2309', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samungbhagawati', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2310', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satiswara', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2311', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sundhara', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2312', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syamgha', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2313', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tanahunsur', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2314', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thaprek', '47', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2315', '2', null, null, '2017-03-26 13:49:28', null, null, 'Adguri', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2316', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arghatos', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2317', '2', null, null, '2017-03-26 13:49:28', null, null, 'Asurkot', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2318', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balkot', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2319', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangi', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2320', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawati', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2321', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatraganj', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2322', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chidika', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2323', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhakawang', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2324', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanchaur', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2325', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharapani', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2326', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhatiwang', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2327', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhikura', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2328', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gorkhunga', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2329', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansapur', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2330', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jukena', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2331', '2', null, null, '2017-03-26 13:49:28', null, null, 'Juluke', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2332', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerunga', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2333', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khan', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2334', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khandaha', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2335', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khidim', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2336', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khilji', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2337', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maidan', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2338', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mareng', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2339', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nuwakot', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2340', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pali', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2341', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panena', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2342', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathauti', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2343', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pathona', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2344', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokharathok', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2345', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddhara', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2346', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simalapani', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2347', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitapur', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2348', '2', null, null, '2017-03-26 13:49:28', null, null, 'Subarnakhal', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2349', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thada', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2350', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Pokhara', '53', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2351', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aaglung', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2352', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amar Abathok', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2353', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amarpur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2354', '2', null, null, '2017-03-26 13:49:28', null, null, 'Apchaur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2355', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arbani', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2356', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arje', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2357', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arkhawang', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2358', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arlangkot', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2359', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aslewa', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2360', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badagaun', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2361', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajhketeri', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2362', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baletaksar', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2363', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balithum', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2364', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bamgha', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2365', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bami', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2366', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bastu', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2367', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhanbhane', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2368', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhangari', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2369', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharse', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2370', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhurmung', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2371', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birbas', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2372', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisukharka', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2373', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhapahile', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2374', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dalamchaur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2375', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darbar Devisthan', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2376', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darling', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2377', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daungha', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2378', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamir', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2379', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhurkot Bastu', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2380', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhurkot Bhanbhane', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2381', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhurkot Nayagaun', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2382', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhurkot Rajasthal', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2383', '2', null, null, '2017-03-26 13:49:28', null, null, 'Digam', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2384', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dirbung', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2385', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dohali', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2386', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubichaur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2387', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dusma Rajasthal', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2388', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaidakot', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2389', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gurukot Rajasthal', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2390', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gwadha', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2391', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gwadi', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2392', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hadahade', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2393', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hadinete', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2394', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansara', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2395', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harewa', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2396', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harmichaur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2397', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harrachaur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2398', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hasara', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2399', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hastichaur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2400', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hawangdi', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2401', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hunga', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2402', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaisithok', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2403', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jayakhani', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2404', '2', null, null, '2017-03-26 13:49:28', null, null, 'Johang', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2405', '2', null, null, '2017-03-26 13:49:28', null, null, 'Juniya', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2406', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jubhung', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2407', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadgakot', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2408', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharjyang', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2409', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kurgha', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2410', '2', null, null, '2017-03-26 13:49:28', null, null, 'Limgha', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2411', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malagiri', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2412', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marbhung', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2413', '2', null, null, '2017-03-26 13:49:28', null, null, 'Musikot', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2414', '2', null, null, '2017-03-26 13:49:28', null, null, 'Myal Pokhari', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2415', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nayagaun', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2416', '2', null, null, '2017-03-26 13:49:28', null, null, 'Neta', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2417', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palkikot', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2418', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paralmi', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2419', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paudi Amarahi', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2420', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipaldhara', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2421', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phoksing', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2422', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purkot Daha', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2423', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purtighat', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2424', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rimuwa', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2425', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupakot', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2426', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ruru', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2427', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shantipur', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2428', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siseni', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2429', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thanpati', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2430', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Lumpek', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2431', '2', null, null, '2017-03-26 13:49:28', null, null, 'Turang', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2432', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wagla', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2433', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wamitaksar', '54', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2434', '2', null, null, '2017-03-26 13:49:28', null, null, 'Abhirawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2435', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ajingara', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2436', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahadurganj', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2437', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuni', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2438', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukharampur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2439', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balarampur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2440', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baluhawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2441', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangai', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2442', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banganga', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2443', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baraipur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2444', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barakulpur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2445', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2446', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baskhaur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2447', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bedauli', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2448', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawanpur Choti', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2449', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhalabari', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2450', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijuwa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2451', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bithuwa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2452', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhi', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2453', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhankauli', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2454', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharampaniya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2455', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dohani', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2456', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubiya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2457', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumara', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2458', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajehada', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2459', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganeshpur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2460', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gauri', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2461', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gotihawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2462', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gugauli', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2463', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haranampur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2464', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hardauna', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2465', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2466', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathausa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2467', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hathihawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2468', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jahadi', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2469', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jayanagar', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2470', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kajarhawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2471', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khurhuriya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2472', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kopawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2473', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kushawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2474', '2', null, null, '2017-03-26 13:49:28', null, null, 'Labani', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2475', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalpur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2476', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maharajganj', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2477', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahendrakot', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2478', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahuwa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2479', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malwar', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2480', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manpur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2481', '2', null, null, '2017-03-26 13:49:28', null, null, 'Milmi', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2482', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motipur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2483', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nanda Nagar', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2484', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nigalihawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2485', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakadi', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2486', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsohiya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2487', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patariya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2488', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patna', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2489', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patthardaihiya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2490', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulika', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2491', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipari', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2492', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purusottampur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2493', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajpur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2494', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramghat', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2495', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramnagar', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2496', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangapur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2497', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sauraha', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2498', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shivagadhi', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2499', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singhkhor', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2500', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2501', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shivapur Palta', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2502', '2', null, null, '2017-03-26 13:49:28', null, null, 'Somdiha', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2503', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taulihawa', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2504', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thunhiya', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2505', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilaurakot', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2506', '2', null, null, '2017-03-26 13:49:28', null, null, 'Titirkhi', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2507', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udayapur', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2508', '2', null, null, '2017-03-26 13:49:28', null, null, 'Vidhya Nagar', '49', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2509', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amarapuri', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2510', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amraut', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2511', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badahara Dubauliya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2512', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baidauli', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2513', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banjariya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2514', '2', null, null, '2017-03-26 13:49:28', null, null, 'Benimanipur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2515', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bharatipur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2516', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhujhawa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2517', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bulingtar', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2518', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadajheri Tadi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2519', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dawanne Devi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2520', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dedgaun', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2521', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2522', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devagawa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2523', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhobadi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2524', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhurkot', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2525', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumkibas', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2526', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gairami', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2527', '2', null, null, '2017-03-26 13:49:28', null, null, 'Guthi Parsauni', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2528', '2', null, null, '2017-03-26 13:49:28', null, null, 'Guthisuryapura', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2529', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hakui', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2530', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harpur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2531', '2', null, null, '2017-03-26 13:49:28', null, null, 'Humsekot', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2532', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jahada', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2533', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuniya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2534', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuwad', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2535', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaubari', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2536', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolhuwa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2537', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotathar', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2538', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kudiya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2539', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kumarwarti', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2540', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kusma', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2541', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mainaghat', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2542', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manari', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2543', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manjhariya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2544', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mithukaram', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2545', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mukundapur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2546', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naram', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2547', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narsahi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2548', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naya Belhani', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2549', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakalihawa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2550', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palhi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2551', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parasi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2552', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsauni', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2553', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pratappur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2554', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajahar', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2555', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakachuli', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2556', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakuwa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2557', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramnagar', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2558', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur Khadauna', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2559', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampurwa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2560', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratnapur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2561', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ruchang', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2562', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupauliya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2563', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanai', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2564', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarawal', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2565', '2', null, null, '2017-03-26 13:49:28', null, null, 'Somani', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2566', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukrauli', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2567', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suryapura', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2568', '2', null, null, '2017-03-26 13:49:28', null, null, 'Swathi', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2569', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tamasariya', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2570', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thulo Khairatawa', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2571', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilakpur', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2572', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tribenisusta', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2573', '2', null, null, '2017-03-26 13:49:28', null, null, 'Unwach', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2574', '2', null, null, '2017-03-26 13:49:28', null, null, 'Upallo Arkhale', '50', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2575', '2', null, null, '2017-03-26 13:49:28', null, null, 'Archale', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2576', '2', null, null, '2017-03-26 13:49:28', null, null, 'Argali', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2577', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahadurpur', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2578', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baldengadhi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2579', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bandi Pokhara', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2580', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barandi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2581', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhairabsthan', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2582', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuwan Pokhari', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2583', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birkot', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2584', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bodhapokharathok', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2585', '2', null, null, '2017-03-26 13:49:28', null, null, 'Boudhagumba', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2586', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chappani', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2587', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhahara', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2588', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chidipani', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2589', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chirtungdhara', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2590', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darchha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2591', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darlamdanda', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2592', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2593', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devinagar', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2594', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dobhan', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2595', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadakot', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2596', '2', null, null, '2017-03-26 13:49:28', null, null, 'Galgha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2597', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gegha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2598', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothadi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2599', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haklang', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2600', '2', null, null, '2017-03-26 13:49:28', null, null, 'Humin', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2601', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hungi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2602', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jalpa', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2603', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhadewa', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2604', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhirubas', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2605', '2', null, null, '2017-03-26 13:49:28', null, null, 'Juthapauwa', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2606', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jyamire', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2607', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kachal', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2608', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaseni', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2609', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaliban', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2610', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaniban', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2611', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khanichhap', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2612', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khanigau', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2613', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khasyoli', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2614', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khyaha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2615', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koldada', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2616', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kusumkhola', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2617', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madanpokhara', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2618', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mainadi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2619', '2', null, null, '2017-03-26 13:49:28', null, null, 'Masyam', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2620', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mityal', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2621', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mujhung', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2622', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayanmatales', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2623', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palung Mainadi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2624', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phek', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2625', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phoksingkot', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2626', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipaldada', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2627', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokharathok', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2628', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rahabasy', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2629', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ringneraha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2630', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupse', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2631', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahalkot', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2632', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satyawati', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2633', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddheshwar', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2634', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siluwa', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2635', '2', null, null, '2017-03-26 13:49:28', null, null, 'Somadi', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2636', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tahu', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2637', '2', null, null, '2017-03-26 13:49:28', null, null, 'Telgha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2638', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thu', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2639', '2', null, null, '2017-03-26 13:49:28', null, null, 'Timurekha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2640', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wakamalang', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2641', '2', null, null, '2017-03-26 13:49:28', null, null, 'Yangha', '55', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2642', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aama', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2643', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amari', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2644', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amawa Marchawar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2645', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amuwa Paschim', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2646', '2', null, null, '2017-03-26 13:49:28', null, null, 'Asurena', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2647', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babhani', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2648', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagaha', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2649', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagauli', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2650', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bairghat', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2651', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balarampur', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2652', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangai', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2653', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangai Marchwar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2654', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baragadewa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2655', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barsauli', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2656', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2657', '2', null, null, '2017-03-26 13:49:28', null, null, 'Betakuiya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2658', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisunpura', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2659', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bodabar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2660', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bogadi', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2661', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhipagada', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2662', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhotaki Ramnaga', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2663', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chilhiya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2664', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dayanagar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2665', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhakadhai', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2666', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamauli', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2667', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dudharakchhe', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2668', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajedi', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2669', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gangoliya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2670', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gonaha', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2671', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hanaiya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2672', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hati Bangai', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2673', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hati Pharsatika', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2674', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jogada', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2675', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kamahariya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2676', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karauta', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2677', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karmahawa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2678', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kataya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2679', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kerbani', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2680', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadawa Bangai', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2681', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mainahiya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2682', '2', null, null, '2017-03-26 13:49:28', null, null, 'Man Materiya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2683', '2', null, null, '2017-03-26 13:49:28', null, null, 'Man Pakadi', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2684', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maryadpur', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2685', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motipur', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2686', '2', null, null, '2017-03-26 13:49:28', null, null, 'Padsari', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2687', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pajarkatli', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2688', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakadi Sakron', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2689', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parroha', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2690', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2691', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patekhauli', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2692', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pharena', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2693', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pharsatikar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2694', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pharsatikarhati', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2695', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piprahawa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2696', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokharvindi', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2697', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rayapur', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2698', '2', null, null, '2017-03-26 13:49:28', null, null, 'Roinihawa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2699', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rudrapur', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2700', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sadi', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2701', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saljhundi', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2702', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samera Marchwar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2703', '2', null, null, '2017-03-26 13:49:28', null, null, 'Semalar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2704', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikatahan', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2705', '2', null, null, '2017-03-26 13:49:28', null, null, 'Silautiya', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2706', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipawa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2707', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sauraha Pharsatikar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2708', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suryapura', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2709', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tama Nagar', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2710', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarkulaha', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2711', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tharki', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2712', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thumhawa Piprahawa', '52', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2713', '2', null, null, '2017-03-26 13:49:28', null, null, 'Adhikarichaur', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2714', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amalachaur', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2715', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amarbhumi', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2716', '2', null, null, '2017-03-26 13:49:28', null, null, 'Argal', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2717', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arjewa', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2718', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baskot', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2719', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batakachaur', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2720', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhakunde', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2721', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimgithe', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2722', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhimpokhara', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2723', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bihunkot', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2724', '2', null, null, '2017-03-26 13:49:28', null, null, 'Binamare', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2725', '2', null, null, '2017-03-26 13:49:28', null, null, 'Boharagaun', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2726', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bowang', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2727', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bungadovan', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2728', '2', null, null, '2017-03-26 13:49:28', null, null, 'Burtibang', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2729', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhisti', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2730', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daga Tundada', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2731', '2', null, null, '2017-03-26 13:49:28', null, null, 'Damek', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2732', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darling', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2733', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devisthan', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2734', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamja', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2735', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baglung Jaidi', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2736', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhullubaskot', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2737', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dudilavati', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2738', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gwalichaur-Harichaur', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2739', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatiya', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2740', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hila', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2741', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hudgishir', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2742', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaljala', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2743', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kandebas', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2744', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khungkhani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2745', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khunga', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2746', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kusmishera', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2747', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2748', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malika', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2749', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malma', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2750', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mulpani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2751', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayansthan', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2752', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narethanti', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2753', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nisi', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2754', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paiyunthanthap', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2755', '2', null, null, '2017-03-26 13:49:28', null, null, 'Palakot', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2756', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pandavkhani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2757', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paiyunpata', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2758', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajkut', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2759', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranasingkiteni', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2760', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangkhani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2761', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rayadanda', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2762', '2', null, null, '2017-03-26 13:49:28', null, null, 'Resha', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2763', '2', null, null, '2017-03-26 13:49:28', null, null, 'Righa', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2764', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salyan', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2765', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarkuwa', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2766', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singana', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2767', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisakhani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2768', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukhura', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2769', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunkhani', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2770', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taman', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2771', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tangram', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2772', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tara', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2773', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tityang', '56', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2774', '2', null, null, '2017-03-26 13:49:28', null, null, 'Charang', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2775', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhonhup', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2776', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhoser', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2777', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhusang', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2778', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhami', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2779', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jomsom', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2780', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhong', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2781', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kagbeni', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2782', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kowang', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2783', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kunjo', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2784', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lete', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2785', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lo Manthang', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2786', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marpha', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2787', '2', null, null, '2017-03-26 13:49:28', null, null, 'Muktinath', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2788', '2', null, null, '2017-03-26 13:49:28', null, null, 'Surkhang', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2789', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tukuche', '59', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2790', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arman', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2791', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arthunge', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2792', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babiyachaur', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2793', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baranja', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2794', '2', null, null, '2017-03-26 13:49:28', null, null, 'Begkhola', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2795', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhakilmi', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2796', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bima', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2797', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chimkhola', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2798', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dagnam', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2799', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dana', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2800', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darwang', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2801', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devisthan', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2802', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhatan', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2803', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dowa', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2804', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gurja Khani', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2805', '2', null, null, '2017-03-26 13:49:28', null, null, 'Histhan Mandali', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2806', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhin', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2807', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jyamrukot', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2808', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuhunkot', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2809', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuinemangale', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2810', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lulang', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2811', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malkwang', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2812', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marang', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2813', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudi', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2814', '2', null, null, '2017-03-26 13:49:28', null, null, 'Muna', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2815', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narchyang', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2816', '2', null, null, '2017-03-26 13:49:28', null, null, 'Niskot', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2817', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okharbot', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2818', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakhapani', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2819', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patlekhet', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2820', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pulachaur', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2821', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakhu Bhagawati', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2822', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakhupiple', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2823', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramche', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2824', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratnechaur', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2825', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rumaga', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2826', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shikha', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2827', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singa', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2828', '2', null, null, '2017-03-26 13:49:28', null, null, 'Takam', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2829', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tatopani', '57', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2830', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arthar Dadakharka', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2831', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bachchha', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2832', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bahaki Thanti', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2833', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajung', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2834', '2', null, null, '2017-03-26 13:49:28', null, null, 'Balakot', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2835', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banau', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2836', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baskharka', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2837', '2', null, null, '2017-03-26 13:49:28', null, null, 'Behulibans', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2838', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhangara', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2839', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhoksing', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2840', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhorle', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2841', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuk Deurali', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2842', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuktangle', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2843', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bihadi Barachaur', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2844', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bihadi Ranipani', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2845', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bitalawa Pipaltari', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2846', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chitre', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2847', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deupurkot', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2848', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deurali', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2849', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devisthan', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2850', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhairing', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2851', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hosrangdi', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2852', '2', null, null, '2017-03-26 13:49:28', null, null, 'Huwas', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2853', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karkineta', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2854', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khola Lakuri', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2855', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kurgha', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2856', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kyang', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2857', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhphant', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2858', '2', null, null, '2017-03-26 13:49:28', null, null, 'Limithana', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2859', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahashila Gaupalika', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2860', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mallaj Majhfant', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2861', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudikuwa', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2862', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagliwang', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2863', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakhapani', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2864', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pangrang', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2865', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalamkhani', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2866', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalebas Devisthan', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2867', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalebas Khanigaun', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2868', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramja Deurali', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2869', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saligram', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2870', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salija', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2871', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saraukhola', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2872', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shankar Pokhari', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2873', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taklak', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2874', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tanglekot', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2875', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thana Maulo', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2876', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thapathana', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2877', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thuli Pokhari', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2878', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilahar', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2879', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tribeni', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2880', '2', null, null, '2017-03-26 13:49:28', null, null, 'Urampokhara', '58', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2881', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baghmare', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2882', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bela', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2883', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijauri', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2884', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chailahi', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2885', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanauri', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2886', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharna', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2887', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhikpur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2888', '2', null, null, '2017-03-26 13:49:28', null, null, 'Diruwa', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2889', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadhawa', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2890', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gangapraspur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2891', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gobardiya', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2892', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goltakuri', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2893', '2', null, null, '2017-03-26 13:49:28', null, null, 'Halwar', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2894', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansipur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2895', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hapur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2896', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hekuli', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2897', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhre', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2898', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koilabas', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2899', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalmitiya', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2900', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2901', '2', null, null, '2017-03-26 13:49:28', null, null, 'Loharpani', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2902', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manpur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2903', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayanpur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2904', '2', null, null, '2017-03-26 13:49:28', null, null, 'Panchakule', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2905', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pawan Nagar', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2906', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulbari', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2907', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purandhara', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2908', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajpur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2909', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rampur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2910', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saidha', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2911', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satbariya', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2912', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saudiyar', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2913', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shantinagar', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2914', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srigaun', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2915', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisahaniya', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2916', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonpur', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2917', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syuja', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2918', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarigaun', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2919', '2', null, null, '2017-03-26 13:49:28', null, null, 'Urahari', '60', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2920', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arkha', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2921', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badikot', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2922', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangemarkot', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2923', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bangesal', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2924', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baraula', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2925', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barjibang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2926', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belbas', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2927', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhingri', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2928', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijaya Nagar', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2929', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijuwar', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2930', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijuli', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2931', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chuja', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2932', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dakhanwadi', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2933', '2', null, null, '2017-03-26 13:49:28', null, null, 'Damri', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2934', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangbang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2935', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharamawati', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2936', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharampani', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2937', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhobaghat', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2938', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhubang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2939', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dungegadi', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2940', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothibang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2941', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hansapur', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2942', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jumrikanda', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2943', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaira', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2944', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khabang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2945', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khung', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2946', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kochibang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2947', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ligha', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2948', '2', null, null, '2017-03-26 13:49:28', null, null, 'Libang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2949', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lung', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2950', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhakot', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2951', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maranthana', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2952', '2', null, null, '2017-03-26 13:49:28', null, null, 'Markabang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2953', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narikot', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2954', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naya Gaun', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2955', '2', null, null, '2017-03-26 13:49:28', null, null, 'Okharkot', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2956', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakala', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2957', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phopli', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2958', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puja', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2959', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pythan', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2960', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajbara', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2961', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramdi', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2962', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ruspur Kot', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2963', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sari', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2964', '2', null, null, '2017-03-26 13:49:28', null, null, 'Swargadwarikhal', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2965', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syaulibang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2966', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tarwang', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2967', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tiram', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2968', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tusara', '61', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2969', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aresh', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2970', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2971', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mirul', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2972', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budagaun', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2973', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2974', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubidanda', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2975', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dubring', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2976', '2', null, null, '2017-03-26 13:49:28', null, null, 'Eriwang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2977', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phagam', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2978', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gam', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2979', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajul', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2980', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaurigaun', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2981', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gharti Gaun', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2982', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghodagaun', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2983', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumchal', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2984', '2', null, null, '2017-03-26 13:49:28', null, null, 'Harjang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2985', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jailwang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2986', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaimakasala', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2987', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jankot', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2988', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jauli Pokhari', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2989', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jedwang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2990', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhenam', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2991', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jinawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2992', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jungar', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2993', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karchawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2994', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kareti', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2995', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khumel', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2996', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khungri', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2997', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotgaun', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2998', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kureli', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('2999', '2', null, null, '2017-03-26 13:49:28', null, null, 'Liwang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3000', '2', null, null, '2017-03-26 13:49:28', null, null, 'Masina', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3001', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mijhing', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3002', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nuwagaun', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3003', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pachhawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3004', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakhapani', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3005', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3006', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangkot', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3007', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangsi', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3008', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rank', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3009', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakhi', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3010', '2', null, null, '2017-03-26 13:49:28', null, null, 'Seram', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3011', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirpa', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3012', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siuri', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3013', '2', null, null, '2017-03-26 13:49:28', null, null, 'Talawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3014', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tewang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3015', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thawang', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3016', '2', null, null, '2017-03-26 13:49:28', null, null, 'Uwa', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3017', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wadachaur', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3018', '2', null, null, '2017-03-26 13:49:28', null, null, 'Whama', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3019', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wot', '62', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3020', '2', null, null, '2017-03-26 13:49:28', null, null, 'Aathbis Kot', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3021', '2', null, null, '2017-03-26 13:49:28', null, null, 'Arma', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3022', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bapsekot', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3023', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhalakachha', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3024', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaurjahari', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3025', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhiwang', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3026', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chokhawang', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3027', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chunwang', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3028', '2', null, null, '2017-03-26 13:49:28', null, null, 'Duli', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3029', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garayala', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3030', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gautamkot', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3031', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghetma', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3032', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hukam', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3033', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jang', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3034', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhula', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3035', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanda', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3036', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kankri', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3037', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khara', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3038', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kholagaun', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3039', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kol', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3040', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotjahari', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3041', '2', null, null, '2017-03-26 13:49:28', null, null, 'Magma', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3042', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahat', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3043', '2', null, null, '2017-03-26 13:49:28', null, null, 'Morawang', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3044', '2', null, null, '2017-03-26 13:49:28', null, null, 'Muru', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3045', '2', null, null, '2017-03-26 13:49:28', null, null, 'Musikot Khalanga', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3046', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nuwakot', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3047', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipal', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3048', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhara', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3049', '2', null, null, '2017-03-26 13:49:28', null, null, 'Purtim Kanda', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3050', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pwang', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3051', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pyaugha', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3052', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rangsi', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3053', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranmamekot', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3054', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rugha', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3055', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simli', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3056', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sisne', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3057', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sobha', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3058', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syalagadi', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3059', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syalapakha', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3060', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taksera', '63', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3061', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badagaun', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3062', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baphukhola', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3063', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajh Kanda', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3064', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bame Banghad', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3065', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhalchaur', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3066', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chande', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3067', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhayachhetra', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3068', '2', null, null, '2017-03-26 13:49:28', null, null, 'Damachaur', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3069', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darmakot', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3070', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devisthal', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3071', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhagari Pipal', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3072', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhakadam', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3073', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanwang', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3074', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhimpe', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3075', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jimali', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3076', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhrechaur', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3077', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalagaun', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3078', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalimati Kalche', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3079', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalimati Rampur', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3080', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kabhra', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3081', '2', null, null, '2017-03-26 13:49:28', null, null, 'Korbang Jhimpe', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3082', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotbara', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3083', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotmala', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3084', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kubhinde', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3085', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmipur', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3086', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhpokhara', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3087', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majh Khanda', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3088', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marmaparikhanda', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3089', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mulkhola', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3090', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nigalchula', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3091', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phalawang', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3092', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipal Neta', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3093', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rim', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3094', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarpani Garpa', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3095', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sibaratha', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3096', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddheshwar', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3097', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sinwang', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3098', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suikot', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3099', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tharmare', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3100', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tribeni', '65', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3101', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhijer', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3102', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chharka', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3103', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dho', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3104', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dunai', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3105', '2', null, null, '2017-03-26 13:49:28', null, null, 'Juphal', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3106', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaigaun', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3107', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3108', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadang', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3109', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lawan', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3110', '2', null, null, '2017-03-26 13:49:28', null, null, 'Likhu', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3111', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhphal', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3112', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mukot', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3113', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narku', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3114', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pahada', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3115', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phoksundo', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3116', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raha', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3117', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rimi', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3118', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahartara', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3119', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saldang', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3120', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarmi', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3121', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunhu', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3122', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tinje', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3123', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tripurakot', '66', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3124', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barai', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3125', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bargaun', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3126', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhipra', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3127', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darma', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3128', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothi', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3129', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hepka', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3130', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaira', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3131', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3132', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khagalgaun', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3133', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharpunath', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3134', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lali', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3135', '2', null, null, '2017-03-26 13:49:28', null, null, 'Limi', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3136', '2', null, null, '2017-03-26 13:49:28', null, null, 'Madana', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3137', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maila', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3138', '2', null, null, '2017-03-26 13:49:28', null, null, 'Melchham', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3139', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mimi', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3140', '2', null, null, '2017-03-26 13:49:28', null, null, 'Muchu', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3141', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raya', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3142', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rodikot', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3143', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarkideu', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3144', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saya', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3145', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srinagar', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3146', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srimastha', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3147', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simikot', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3148', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syada', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3149', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thehe', '67', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3150', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badki', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3151', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birat', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3152', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bamramadichaur', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3153', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhumchaur', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3154', '2', null, null, '2017-03-26 13:49:28', null, null, 'Depalgaun', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3155', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhapa', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3156', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dillichaur', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3157', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garjyangkot', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3158', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghode Mahadev', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3159', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gothichaur', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3160', '2', null, null, '2017-03-26 13:49:28', null, null, 'Haku', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3161', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikakhetu', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3162', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanakasundari', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3163', '2', null, null, '2017-03-26 13:49:28', null, null, 'Labhra', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3164', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lihi', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3165', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahabe Pattharkhola', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3166', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadev', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3167', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malika Bota', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3168', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malikathota', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3169', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narakot', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3170', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pandawagupha', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3171', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patarasi', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3172', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patmara', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3173', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanigaun', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3174', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tamti', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3175', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tatopani', '68', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3176', '2', null, null, '2017-03-26 13:49:28', null, null, 'Badalkot', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3177', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhapre', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3178', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chilkhaya', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3179', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daha', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3180', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dholagohe', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3181', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gela', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3182', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jubika', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3183', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khin', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3184', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotbada', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3185', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kumalgaun', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3186', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalu', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3187', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marta', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3188', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mehal Madi', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3189', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mugraha', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3190', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mumra', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3191', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nanikot', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3192', '2', null, null, '2017-03-26 13:49:28', null, null, 'Odanku', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3193', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pakha', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3194', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phoi Mahadev', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3195', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phukot', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3196', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raku', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3197', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramanakot', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3198', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranchuli', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3199', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupsa', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3200', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipkhana', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3201', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siuna', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3202', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukitaya', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3203', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thirpu', '69', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3204', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhiyi', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3205', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhainakot', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3206', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dolphu', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3207', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghaina', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3208', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumtha', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3209', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hyanglung', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3210', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jima', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3211', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kale', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3212', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karkibada', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3213', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kimari', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3214', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotdanda', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3215', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mangri', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3216', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mihi', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3217', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mugu', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3218', '2', null, null, '2017-03-26 13:49:28', null, null, 'Natharpu', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3219', '2', null, null, '2017-03-26 13:49:28', null, null, 'Photu', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3220', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pina', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3221', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pulu', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3222', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rara', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3223', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rara Kalai', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3224', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rowa', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3225', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ruga', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3226', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rumale', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3227', '2', null, null, '2017-03-26 13:49:28', null, null, 'Seri', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3228', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srikot', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3229', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srinagar', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3230', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sukhadhik', '70', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3231', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bageshwari', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3232', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banakatawa', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3233', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banakatti', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3234', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basudevapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3235', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bejapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3236', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belahari', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3237', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belbhar', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3238', '2', null, null, '2017-03-26 13:49:28', null, null, 'Betahani', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3239', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawaniyapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3240', '2', null, null, '2017-03-26 13:49:28', null, null, 'Binauna', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3241', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chisapani', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3242', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3243', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gangapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3244', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hirminiya', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3245', '2', null, null, '2017-03-26 13:49:28', null, null, 'Holiya', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3246', '2', null, null, '2017-03-26 13:49:28', null, null, 'Indrapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3247', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jaispur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3248', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalaphanta', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3249', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kamdi', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3250', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanchanpur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3251', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kathkuiya', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3252', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khajura Khurda', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3253', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaskarkando', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3254', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaskusma', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3255', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshmanpur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3256', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevpuri', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3257', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manikapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3258', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matahiya', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3259', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narainapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3260', '2', null, null, '2017-03-26 13:49:28', null, null, 'Naubasta', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3261', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parsapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3262', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phattepur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3263', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piparhawa', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3264', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puraina', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3265', '2', null, null, '2017-03-26 13:49:28', null, null, 'Puraini', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3266', '2', null, null, '2017-03-26 13:49:28', null, null, 'Radhapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3267', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajhena', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3268', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raniyapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3269', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saigaun', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3270', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samserganj', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3271', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3272', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sonapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3273', '2', null, null, '2017-03-26 13:49:28', null, null, 'Titahiriya', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3274', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udarapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3275', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udayapur', '71', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3276', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baganaha', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3277', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baniyabhar', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3278', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belawa', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3279', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deudakala', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3280', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhadhawar', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3281', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhodhari', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3282', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gola', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3283', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jamuni', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3284', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3285', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khairapur', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3286', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khairi Chandanpur', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3287', '2', null, null, '2017-03-26 13:49:28', null, null, 'Magaragadi', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3288', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahamadpur', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3289', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manau', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3290', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manpur Mainapokhar', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3291', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manpur Tapara', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3292', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mathurahardwar', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3293', '2', null, null, '2017-03-26 13:49:28', null, null, 'Motipur', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3294', '2', null, null, '2017-03-26 13:49:28', null, null, 'Neulapur', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3295', '2', null, null, '2017-03-26 13:49:28', null, null, 'Padanaha', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3296', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pashupatinagar', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3297', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patabhar', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3298', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanesri', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3299', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shivapur', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3300', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sorhawa', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3301', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suryapatawa', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3302', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taratal', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3303', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thakudwara', '72', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3304', '2', null, null, '2017-03-26 13:49:28', null, null, 'Awal Parajul', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3305', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bada Bhairab', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3306', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bada Khola', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3307', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baluwatar', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3308', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bansi', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3309', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baraha', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3310', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantamala', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3311', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belaspur', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3312', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belpata', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3313', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawani', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3314', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bindhyabasini', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3315', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bisalla', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3316', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chamunda', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3317', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chauratha', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3318', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dada Parajul', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3319', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gamaudi', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3320', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gauri', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3321', '2', null, null, '2017-03-26 13:49:28', null, null, 'Goganpani', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3322', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagannath', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3323', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jambukandh', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3324', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kal Bhairab', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3325', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3326', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kasikandh', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3327', '2', null, null, '2017-03-26 13:49:28', null, null, 'Katti', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3328', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khadkawada', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3329', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharigera', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3330', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kusapani', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3331', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakhandra', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3332', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakuri', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3333', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalikhanda', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3334', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lyati Bindraseni', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3335', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mairi Kalikathum', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3336', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malika', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3337', '2', null, null, '2017-03-26 13:49:28', null, null, 'Moheltoli', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3338', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nomule', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3339', '2', null, null, '2017-03-26 13:49:28', null, null, 'Odhari', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3340', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pagnath', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3341', '2', null, null, '2017-03-26 13:49:28', null, null, 'Piladi', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3342', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipalkot', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3343', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakam Karnali', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3344', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raniban', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3345', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rawalkot', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3346', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rum', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3347', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salleri', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3348', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santalla', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3349', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saraswati', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3350', '2', null, null, '2017-03-26 13:49:28', null, null, 'Seri', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3351', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sigaudi', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3352', '2', null, null, '2017-03-26 13:49:28', null, null, 'Singasain', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3353', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilepata', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3354', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tilijaisi', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3355', '2', null, null, '2017-03-26 13:49:28', null, null, 'Toli', '74', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3356', '2', null, null, '2017-03-26 13:49:28', null, null, 'Archhani', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3357', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawati Tol', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3358', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhur', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3359', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daha', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3360', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandagaun', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3361', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dasera', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3362', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhime', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3363', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garkhakot', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3364', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagatipur', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3365', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhapra', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3366', '2', null, null, '2017-03-26 13:49:28', null, null, 'Junga Thapachaur', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3367', '2', null, null, '2017-03-26 13:49:28', null, null, 'Karkigaun', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3368', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khagenkot', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3369', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khalanga', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3370', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kortrang', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3371', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lahai', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3372', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhkot', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3373', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nayakwada', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3374', '2', null, null, '2017-03-26 13:49:28', null, null, 'Paink', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3375', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pajaru', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3376', '2', null, null, '2017-03-26 13:49:28', null, null, 'Punama', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3377', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ragda', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3378', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramidanda', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3379', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rokayagaun', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3380', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakala', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3381', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salma', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3382', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sima', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3383', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suwanauli', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3384', '2', null, null, '2017-03-26 13:49:28', null, null, 'Talegaun', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3385', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thala Raikar', '75', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3386', '2', null, null, '2017-03-26 13:49:28', null, null, 'Agragaun', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3387', '2', null, null, '2017-03-26 13:49:28', null, null, 'Awalaching', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3388', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bajedichaur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3389', '2', null, null, '2017-03-26 13:49:28', null, null, 'Betan', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3390', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bidyapur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3391', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijaura', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3392', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chapre', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3393', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhinchu', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3394', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dabiyachaur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3395', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dahachaur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3396', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dandakhali', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3397', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dasarathpur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3398', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharapani', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3399', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadi Bayalkada', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3400', '2', null, null, '2017-03-26 13:49:28', null, null, 'Garpan', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3401', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghatgaun', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3402', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghoreta', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3403', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghumkhahare', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3404', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gumi', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3405', '2', null, null, '2017-03-26 13:49:28', null, null, 'Guthu', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3406', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hariharpur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3407', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jarbuta', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3408', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaphalkot', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3409', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalyan', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3410', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaprichaur', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3411', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khanikhola', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3412', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kunathari', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3413', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lagam', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3414', '2', null, null, '2017-03-26 13:49:28', null, null, 'Latikoili', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3415', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhpharsa', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3416', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhgaun', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3417', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhparajul', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3418', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maintada', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3419', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malarani', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3420', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matela', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3421', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mehelkuna', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3422', '2', null, null, '2017-03-26 13:49:28', null, null, 'Neta', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3423', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pamka', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3424', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokharikanda', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3425', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rajeni', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3426', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rakam', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3427', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramghat', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3428', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranibas', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3429', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratu', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3430', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahare', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3431', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salkot', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3432', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satokhani', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3433', '2', null, null, '2017-03-26 13:49:28', null, null, 'Taranga', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3434', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tatopani', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3435', '2', null, null, '2017-03-26 13:49:28', null, null, 'Uttarganga', '73', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3436', '2', null, null, '2017-03-26 13:49:28', null, null, 'Babala', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3437', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bannatoli', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3438', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baradadivi', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3439', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basti', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3440', '2', null, null, '2017-03-26 13:49:28', null, null, 'Batulasen', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3441', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bayala', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3442', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhairavsthan', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3443', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatakatiya', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3444', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhuli', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3445', '2', null, null, '2017-03-26 13:49:28', null, null, 'Binayak', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3446', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bindhyawasini', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3447', '2', null, null, '2017-03-26 13:49:28', null, null, 'Birpath', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3448', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhakot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3449', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chalsa', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3450', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chapamandau', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3451', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatara', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3452', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darna', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3453', '2', null, null, '2017-03-26 13:49:28', null, null, 'Devisthan', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3454', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhakari', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3455', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhaku', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3456', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamali', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3457', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dharaki', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3458', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhodasain', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3459', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhudharukot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3460', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhungachalna', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3461', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dumi', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3462', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajara', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3463', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatikot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3464', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hichma', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3465', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janalikot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3466', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalagaun', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3467', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalekanda', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3468', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalika', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3469', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikasthan', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3470', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khaptad', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3471', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khodasadevi', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3472', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuika', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3473', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kushkot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3474', '2', null, null, '2017-03-26 13:49:28', null, null, 'Layati', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3475', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lungra', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3476', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malatikot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3477', '2', null, null, '2017-03-26 13:49:28', null, null, 'Marku', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3478', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mashtanamdali', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3479', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nada', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3480', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nandegata', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3481', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patalkot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3482', '2', null, null, '2017-03-26 13:49:28', null, null, 'Payal', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3483', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pulletala', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3484', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rahaph', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3485', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramarosan', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3486', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raniban', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3487', '2', null, null, '2017-03-26 13:49:28', null, null, 'Risidaha', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3488', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakot', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3489', '2', null, null, '2017-03-26 13:49:28', null, null, 'Santada', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3490', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sera', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3491', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siudi', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3492', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sutar', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3493', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tadigaira', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3494', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thanti', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3495', '2', null, null, '2017-03-26 13:49:28', null, null, 'Timilsain', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3496', '2', null, null, '2017-03-26 13:49:28', null, null, 'Toli', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3497', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tosi', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3498', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tumarkhad', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3499', '2', null, null, '2017-03-26 13:49:28', null, null, 'Walant', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3500', '2', null, null, '2017-03-26 13:49:28', null, null, 'Warla', '77', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3501', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banjh', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3502', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhairab Nath', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3503', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhamchaur', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3504', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatekhola', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3505', '2', null, null, '2017-03-26 13:49:28', null, null, 'Byasi', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3506', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaudhari', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3507', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dahabagar', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3508', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dangaji', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3509', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dantola', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3510', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daulichaur', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3511', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deulekh', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3512', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deulikot', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3513', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhamena', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3514', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadaraya', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3515', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kadel', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3516', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kailash', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3517', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalukheti', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3518', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanda', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3519', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaphalaseri', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3520', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khiratadi', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3521', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koiralakot', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3522', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kot Bhairab', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3523', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotdewal', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3524', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamatola', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3525', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lekhgaun', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3526', '2', null, null, '2017-03-26 13:49:28', null, null, 'Majhigaun', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3527', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malumela', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3528', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mashdev', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3529', '2', null, null, '2017-03-26 13:49:28', null, null, 'Matela', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3530', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maulali', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3531', '2', null, null, '2017-03-26 13:49:28', null, null, 'Melbisauni', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3532', '2', null, null, '2017-03-26 13:49:28', null, null, 'Parakatne', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3533', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patadewal', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3534', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pauwagadhi', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3535', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipalkot', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3536', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rayal', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3537', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rilu', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3538', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sainpasela', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3539', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunikot', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3540', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunkuda', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3541', '2', null, null, '2017-03-26 13:49:28', null, null, 'Surma', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3542', '2', null, null, '2017-03-26 13:49:28', null, null, 'Syandi', '79', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3543', '2', null, null, '2017-03-26 13:49:28', null, null, 'Atichaur', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3544', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baddhu', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3545', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bai', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3546', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barhabise', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3547', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bichhiya', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3548', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bramhatola', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3549', '2', null, null, '2017-03-26 13:49:28', null, null, 'Budhiganga', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3550', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatara', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3551', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dahakot', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3552', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dogadi', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3553', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gotri', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3554', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gudukhati', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3555', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jagannath', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3556', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jayabageshwari', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3557', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jugada', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3558', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kailashmandau', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3559', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanda', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3560', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kolti', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3561', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotila', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3562', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuldeumandau', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3563', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manakot', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3564', '2', null, null, '2017-03-26 13:49:28', null, null, 'Martadi', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3565', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pandusain', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3566', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rugin', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3567', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sappata', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3568', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tolodewal', '80', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3569', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banalek', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3570', '2', null, null, '2017-03-26 13:49:28', null, null, 'Banja Kakani', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3571', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barchhen', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3572', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basudevi', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3573', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhawardanda', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3574', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadhegaun', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3575', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumirajmandau', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3576', '2', null, null, '2017-03-26 13:49:28', null, null, 'Changra', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3577', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhapali', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3578', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chhatiwan', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3579', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dahakalikasthan', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3580', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daud', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3581', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhanglagaun', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3582', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhirkamandau', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3583', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durgamandau', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3584', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadasera', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3585', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaguda', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3586', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gaihragaun', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3587', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganjari', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3588', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghanteshwar', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3589', '2', null, null, '2017-03-26 13:49:28', null, null, 'Girichauka', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3590', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jijodamandau', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3591', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kadamandau', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3592', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalena', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3593', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kalikasthan', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3594', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kanachaur', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3595', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kapalleki', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3596', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kedar Akhada', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3597', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khatiwada', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3598', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khirsain', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3599', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ladagada', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3600', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lamikhal', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3601', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lana Kedareshwar', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3602', '2', null, null, '2017-03-26 13:49:28', null, null, 'Latamandau', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3603', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lakshminagar', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3604', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevsthan', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3605', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mannakapadi', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3606', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudabhara', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3607', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mudhegaun', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3608', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nirauli', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3609', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pachanali', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3610', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pokhari', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3611', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranagaun', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3612', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sanagaun', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3613', '2', null, null, '2017-03-26 13:49:28', null, null, 'Saraswatinagar', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3614', '2', null, null, '2017-03-26 13:49:28', null, null, 'Satphari', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3615', '2', null, null, '2017-03-26 13:49:28', null, null, 'Simchaur', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3616', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tijali', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3617', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tikha', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3618', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tikhatar', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3619', '2', null, null, '2017-03-26 13:49:28', null, null, 'Toleni', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3620', '2', null, null, '2017-03-26 13:49:28', null, null, 'Wagalek', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3621', '2', null, null, '2017-03-26 13:49:28', null, null, 'Warpata', '78', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3622', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basauti', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3623', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhajani', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3624', '2', null, null, '2017-03-26 13:49:28', null, null, 'Boniya', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3625', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaumala', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3626', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dansinhapur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3627', '2', null, null, '2017-03-26 13:49:28', null, null, 'Darakh', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3628', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dododhara', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3629', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durgauli', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3630', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gadariya', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3631', '2', null, null, '2017-03-26 13:49:28', null, null, 'Godawari', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3632', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hasuliya', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3633', '2', null, null, '2017-03-26 13:49:28', null, null, 'Janakinagar', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3634', '2', null, null, '2017-03-26 13:49:28', null, null, 'Joshipur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3635', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khailad', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3636', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khairala', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3637', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kota Tulsipur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3638', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lalbojhi', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3639', '2', null, null, '2017-03-26 13:49:28', null, null, 'Masuriya', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3640', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mohanyal', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3641', '2', null, null, '2017-03-26 13:49:28', null, null, 'Munuwa', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3642', '2', null, null, '2017-03-26 13:49:28', null, null, 'Narayanpur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3643', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nigali', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3644', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pahalmanpur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3645', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pandaun', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3646', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pawera', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3647', '2', null, null, '2017-03-26 13:49:28', null, null, 'Phulbari', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3648', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pratapur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3649', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ramsikhar Jhala', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3650', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ratnapur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3651', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sadepani', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3652', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahajpur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3653', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sugarkhal', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3654', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thapapur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3655', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udasipur', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3656', '2', null, null, '2017-03-26 13:49:28', null, null, 'Urma', '76', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3657', '2', null, null, '2017-03-26 13:49:28', null, null, 'Amchaur', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3658', '2', null, null, '2017-03-26 13:49:28', null, null, 'Barakot', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3659', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basantapur', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3660', '2', null, null, '2017-03-26 13:49:28', null, null, 'Basuling', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3661', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhatana', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3662', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumeshwar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3663', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bijayapur', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3664', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bilaspur', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3665', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bumiraj', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3666', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chadeu', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3667', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chaukham', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3668', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dehimandau', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3669', '2', null, null, '2017-03-26 13:49:28', null, null, 'Deulek', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3670', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhikarim', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3671', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhikasintad', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3672', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhungad', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3673', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dilasaini', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3674', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durga Bhabani', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3675', '2', null, null, '2017-03-26 13:49:28', null, null, 'Durgasthan', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3676', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gajari', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3677', '2', null, null, '2017-03-26 13:49:28', null, null, 'Giregada', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3678', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gokuleshwar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3679', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gujar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3680', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gurukhola', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3681', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gwallek', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3682', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hat', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3683', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hatraj', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3684', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jogannath', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3685', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kaipal', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3686', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kataujpani', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3687', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khalanga', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3688', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotila', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3689', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kotpetara', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3690', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kulau', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3691', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kuwakot', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3692', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahadevsthan', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3693', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mahakali', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3694', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maharudra', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3695', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malladehi', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3696', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mathraj', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3697', '2', null, null, '2017-03-26 13:49:28', null, null, 'Maunali', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3698', '2', null, null, '2017-03-26 13:49:28', null, null, 'Melauli', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3699', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nagarjun', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3700', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nwadeu', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3701', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nwali', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3702', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pancheshwar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3703', '2', null, null, '2017-03-26 13:49:28', null, null, 'Patan', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3704', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raudidewal', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3705', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rauleshwar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3706', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rudreshwar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3707', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sakar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3708', '2', null, null, '2017-03-26 13:49:28', null, null, 'Salena', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3709', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3710', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sarmali', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3711', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shibanath', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3712', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shikharpur', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3713', '2', null, null, '2017-03-26 13:49:28', null, null, 'Shivaling', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3714', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srikedar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3715', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddhapur', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3716', '2', null, null, '2017-03-26 13:49:28', null, null, 'Siddheshwar', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3717', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikash', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3718', '2', null, null, '2017-03-26 13:49:28', null, null, 'Silanga', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3719', '2', null, null, '2017-03-26 13:49:28', null, null, 'Srikot', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3720', '2', null, null, '2017-03-26 13:49:28', null, null, 'Talladehi', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3721', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thalakanda', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3722', '2', null, null, '2017-03-26 13:49:28', null, null, 'Thalegada', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3723', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tripurasundari', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3724', '2', null, null, '2017-03-26 13:49:28', null, null, 'Udayadeb', '83', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3725', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ajayameru', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3726', '2', null, null, '2017-03-26 13:49:28', null, null, 'Alital', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3727', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ashigram', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3728', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bagarkot', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3729', '2', null, null, '2017-03-26 13:49:28', null, null, 'Belapur', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3730', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhadrapur', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3731', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhageshwar', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3732', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhumiraj', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3733', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chipur', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3734', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dewal Dibyapur', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3735', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhatal', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3736', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ganeshpur', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3737', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gankhet', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3738', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jogbuda', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3739', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kailapalmandau', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3740', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khalanga', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3741', '2', null, null, '2017-03-26 13:49:28', null, null, 'Koteli', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3742', '2', null, null, '2017-03-26 13:49:28', null, null, 'Manilek', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3743', '2', null, null, '2017-03-26 13:49:28', null, null, 'Mashtamandau', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3744', '2', null, null, '2017-03-26 13:49:28', null, null, 'Nawadurga', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3745', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rupal', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3746', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sahastralinga', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3747', '2', null, null, '2017-03-26 13:49:28', null, null, 'Samaiji', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3748', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sirsha', '82', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3749', '2', null, null, '2017-03-26 13:49:28', null, null, 'Bhagawati', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3750', '2', null, null, '2017-03-26 13:49:28', null, null, 'Boharigau', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3751', '2', null, null, '2017-03-26 13:49:28', null, null, 'Byans', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3752', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dadakot', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3753', '2', null, null, '2017-03-26 13:49:28', null, null, 'Datu', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3754', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dethala', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3755', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhari', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3756', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhaulakot', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3757', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dhuligada', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3758', '2', null, null, '2017-03-26 13:49:28', null, null, 'Eyarkot', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3759', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ghusa', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3760', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gokuleshwar', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3761', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gulijar', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3762', '2', null, null, '2017-03-26 13:49:28', null, null, 'Gwami', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3763', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hikila', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3764', '2', null, null, '2017-03-26 13:49:28', null, null, 'Hunainath', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3765', '2', null, null, '2017-03-26 13:49:28', null, null, 'Huti', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3766', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khandeshwari', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3767', '2', null, null, '2017-03-26 13:49:28', null, null, 'Khar', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3768', '2', null, null, '2017-03-26 13:49:28', null, null, 'Kharkada', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3769', '2', null, null, '2017-03-26 13:49:28', null, null, 'Lali', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3770', '2', null, null, '2017-03-26 13:49:28', null, null, 'Latinath', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3771', '2', null, null, '2017-03-26 13:49:28', null, null, 'Malikarjun', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3772', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipalchauri', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3773', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ralpa', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3774', '2', null, null, '2017-03-26 13:49:28', null, null, 'Ranisikhar', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3775', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rithachaupata', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3776', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankarpur', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3777', '2', null, null, '2017-03-26 13:49:28', null, null, 'Seri', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3778', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sharmauli', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3779', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sikhar', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3780', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sipti', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3781', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sitaula', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3782', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sunsera', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3783', '2', null, null, '2017-03-26 13:49:28', null, null, 'Tapoban', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3784', '2', null, null, '2017-03-26 13:49:28', null, null, 'Uku', '84', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3785', '2', null, null, '2017-03-26 13:49:28', null, null, 'Baise Bichawa', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3786', '2', null, null, '2017-03-26 13:49:28', null, null, 'Beldandi', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3787', '2', null, null, '2017-03-26 13:49:28', null, null, 'Chandani', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3788', '2', null, null, '2017-03-26 13:49:28', null, null, 'Daiji', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3789', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dekhatbhuli', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3790', '2', null, null, '2017-03-26 13:49:28', null, null, 'Dodhara', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3791', '2', null, null, '2017-03-26 13:49:28', null, null, 'Jhalari', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3792', '2', null, null, '2017-03-26 13:49:28', null, null, 'Krishnapur', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3793', '2', null, null, '2017-03-26 13:49:28', null, null, 'Pipaladi', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3794', '2', null, null, '2017-03-26 13:49:28', null, null, 'Raikawar Bichawa', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3795', '2', null, null, '2017-03-26 13:49:28', null, null, 'Rauteli Bichawa', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3796', '2', null, null, '2017-03-26 13:49:28', null, null, 'Sankarpur', '81', 'MUN/VDC');
INSERT INTO `mst_munvdc` VALUES ('3797', '2', null, null, '2017-03-26 13:49:28', null, null, 'Suda', '81', 'MUN/VDC');

-- ----------------------------
-- Table structure for mst_nepali_month
-- ----------------------------
DROP TABLE IF EXISTS `mst_nepali_month`;
CREATE TABLE `mst_nepali_month` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `rank` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_nepali_month
-- ----------------------------
INSERT INTO `mst_nepali_month` VALUES ('1', '1', '1', null, '2018-03-27 17:40:52', '2018-03-27 17:41:14', null, 'Shrawan', '1');

-- ----------------------------
-- Table structure for mst_parties
-- ----------------------------
DROP TABLE IF EXISTS `mst_parties`;
CREATE TABLE `mst_parties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `mun_vdc_id` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_parties
-- ----------------------------
INSERT INTO `mst_parties` VALUES ('1', '1', '1', null, '2018-04-04 18:09:27', '2018-04-04 18:09:27', null, 'Shiv Kripa Traders-Mirchaiya', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('2', '1', '1', null, '2018-04-04 18:09:27', '2018-04-04 18:09:27', null, 'Jagdamba International-Birganj', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('3', '1', '1', null, '2018-04-05 15:04:23', '2018-04-05 15:04:23', null, 'Kushwaha Enterprises-Mahuwan', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('4', '1', '1', null, '2018-04-05 15:04:23', '2018-04-05 15:04:23', null, 'Anmol Traders-Malangwa', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('5', '1', '1', null, '2018-04-05 15:06:26', '2018-04-05 15:06:26', null, 'Prashant Hardware-Kabilasi,Gair', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('6', '1', '1', null, '2018-04-05 15:09:24', '2018-04-05 15:09:24', null, 'Nav Durga Hardware Suppliers-Itahari', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('7', '1', '1', null, '2018-04-05 15:09:25', '2018-04-05 15:09:25', null, 'Gorkha Suppliers-Gorkha', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('8', '1', '1', null, '2018-04-05 15:11:00', '2018-04-05 15:11:00', null, 'Roshani Suppliers-Chainesdhara,Gorkha', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('9', '1', '1', null, '2018-04-05 15:11:00', '2018-04-05 15:11:00', null, 'E.G.Infra Pvt Ltd.-Kathmandu', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('10', '1', '1', null, '2018-04-05 15:13:37', '2018-04-05 15:13:37', null, 'Rasuwa K.D. JV Pvt. Ltd.-Kathmandu', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('11', '1', '1', null, '2018-04-05 15:13:37', '2018-04-05 15:13:37', null, 'Kamla Suppliers-Ithari', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('12', '1', '1', null, '2018-04-05 15:15:17', '2018-04-05 15:15:17', null, 'Jai Sri Hardware Pvt.Ltd.-Biratnagar', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `mst_parties` VALUES ('13', '1', '1', null, '2018-04-05 15:17:40', '2018-04-05 15:17:40', null, 'Jai Hari Siddhi.-Biratnagar', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for mst_schemes
-- ----------------------------
DROP TABLE IF EXISTS `mst_schemes`;
CREATE TABLE `mst_schemes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_schemes
-- ----------------------------
INSERT INTO `mst_schemes` VALUES ('1', '1', '1', null, '2018-04-04 17:26:01', '2018-04-04 17:26:01', null, '1', '2', '20000');
INSERT INTO `mst_schemes` VALUES ('2', '1', '1', null, '2018-04-04 17:37:49', '2018-04-04 17:37:49', null, '1', '3', '10000');
INSERT INTO `mst_schemes` VALUES ('3', '1', '1', null, '2018-04-04 17:38:23', '2018-04-04 17:38:23', null, '2', '2', '15000');
INSERT INTO `mst_schemes` VALUES ('4', '1', '1', null, '2018-04-04 17:38:54', '2018-04-04 17:38:54', null, '2', '1', '25000');
INSERT INTO `mst_schemes` VALUES ('5', '1', '1', null, '2018-04-04 17:41:43', '2018-04-04 17:41:43', null, '2', '3', '200000');

-- ----------------------------
-- Table structure for project_activity_logs
-- ----------------------------
DROP TABLE IF EXISTS `project_activity_logs`;
CREATE TABLE `project_activity_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `table_pk` int(11) unsigned NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_dttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_activity_logs
-- ----------------------------
INSERT INTO `project_activity_logs` VALUES ('1', '1', 'mst_brand_types', '1', 'insert', '2018-03-16 10:11:03');
INSERT INTO `project_activity_logs` VALUES ('2', '1', 'mst_brand_types', '2', 'insert', '2018-03-16 10:11:10');
INSERT INTO `project_activity_logs` VALUES ('3', '1', 'mst_agents', '1', 'insert', '2018-03-26 11:55:19');
INSERT INTO `project_activity_logs` VALUES ('4', '1', 'mst_agents', '1', 'update', '2018-03-26 11:56:52');
INSERT INTO `project_activity_logs` VALUES ('5', '1', 'mst_agents', '1', 'update', '2018-03-26 11:57:07');
INSERT INTO `project_activity_logs` VALUES ('6', '1', 'mst_agents', '1', 'update', '2018-03-26 11:57:30');
INSERT INTO `project_activity_logs` VALUES ('7', '1', 'mst_agents', '1', 'update', '2018-03-26 11:58:17');
INSERT INTO `project_activity_logs` VALUES ('8', '1', 'mst_agents', '1', 'update', '2018-03-26 14:14:21');
INSERT INTO `project_activity_logs` VALUES ('9', '1', 'mst_agents', '1', 'update', '2018-03-26 14:39:20');
INSERT INTO `project_activity_logs` VALUES ('10', '1', 'mst_agents', '1', 'update', '2018-03-26 15:57:15');
INSERT INTO `project_activity_logs` VALUES ('11', '1', 'mst_agents', '2', 'insert', '2018-03-26 17:33:29');
INSERT INTO `project_activity_logs` VALUES ('12', '1', 'mst_agents', '3', 'insert', '2018-03-27 11:22:16');
INSERT INTO `project_activity_logs` VALUES ('13', '1', 'mst_agents', '4', 'insert', '2018-03-27 11:23:45');
INSERT INTO `project_activity_logs` VALUES ('14', '1', 'mst_agents', '5', 'insert', '2018-03-27 11:24:08');
INSERT INTO `project_activity_logs` VALUES ('15', '1', 'mst_parties', '1', 'insert', '2018-03-27 12:09:23');
INSERT INTO `project_activity_logs` VALUES ('16', '1', 'mst_agents', '6', 'insert', '2018-03-27 12:13:05');
INSERT INTO `project_activity_logs` VALUES ('17', '1', 'mst_agents', '7', 'insert', '2018-03-27 12:30:57');
INSERT INTO `project_activity_logs` VALUES ('18', '1', 'mst_nepali_month', '1', 'insert', '2018-03-27 17:40:52');
INSERT INTO `project_activity_logs` VALUES ('19', '1', 'mst_nepali_month', '1', 'update', '2018-03-27 17:41:10');
INSERT INTO `project_activity_logs` VALUES ('20', '1', 'mst_nepali_month', '1', 'update', '2018-03-27 17:41:14');
INSERT INTO `project_activity_logs` VALUES ('21', '1', 'mst_agents', '8', 'insert', '2018-03-29 18:05:35');
INSERT INTO `project_activity_logs` VALUES ('22', '1', 'mst_agents', '9', 'insert', '2018-03-29 18:05:36');
INSERT INTO `project_activity_logs` VALUES ('23', '1', 'mst_agents', '1', 'insert', '2018-03-30 09:23:36');
INSERT INTO `project_activity_logs` VALUES ('24', '1', 'mst_agents', '2', 'insert', '2018-03-30 09:23:36');
INSERT INTO `project_activity_logs` VALUES ('25', '1', 'mst_agents', '3', 'insert', '2018-03-30 09:23:36');
INSERT INTO `project_activity_logs` VALUES ('26', '1', 'mst_agents', '4', 'insert', '2018-03-30 09:23:53');
INSERT INTO `project_activity_logs` VALUES ('27', '1', 'mst_agents', '5', 'insert', '2018-03-30 09:23:53');
INSERT INTO `project_activity_logs` VALUES ('28', '1', 'mst_agents', '6', 'insert', '2018-03-30 09:23:54');
INSERT INTO `project_activity_logs` VALUES ('29', '1', 'mst_agents', '7', 'insert', '2018-03-30 09:24:47');
INSERT INTO `project_activity_logs` VALUES ('30', '1', 'mst_agents', '8', 'insert', '2018-03-30 09:24:47');
INSERT INTO `project_activity_logs` VALUES ('31', '1', 'mst_agents', '9', 'insert', '2018-03-30 09:24:47');
INSERT INTO `project_activity_logs` VALUES ('32', '1', 'mst_agents', '10', 'insert', '2018-03-30 09:25:32');
INSERT INTO `project_activity_logs` VALUES ('33', '1', 'mst_agents', '11', 'insert', '2018-03-30 09:25:33');
INSERT INTO `project_activity_logs` VALUES ('34', '1', 'mst_agents', '12', 'insert', '2018-03-30 09:25:33');
INSERT INTO `project_activity_logs` VALUES ('35', '1', 'mst_brands', '1', 'insert', '2018-03-30 09:28:49');
INSERT INTO `project_activity_logs` VALUES ('36', '1', 'mst_agents', '13', 'insert', '2018-03-30 09:36:49');
INSERT INTO `project_activity_logs` VALUES ('37', '1', 'mst_agents', '14', 'insert', '2018-03-30 09:36:49');
INSERT INTO `project_activity_logs` VALUES ('38', '1', 'mst_agents', '15', 'insert', '2018-03-30 09:36:50');
INSERT INTO `project_activity_logs` VALUES ('39', '1', 'mst_agents', '16', 'insert', '2018-03-30 09:37:56');
INSERT INTO `project_activity_logs` VALUES ('40', '1', 'mst_agents', '17', 'insert', '2018-03-30 09:37:56');
INSERT INTO `project_activity_logs` VALUES ('41', '1', 'mst_agents', '18', 'insert', '2018-03-30 09:37:56');
INSERT INTO `project_activity_logs` VALUES ('42', '1', 'mst_agents', '19', 'insert', '2018-03-30 09:38:20');
INSERT INTO `project_activity_logs` VALUES ('43', '1', 'mst_agents', '20', 'insert', '2018-03-30 09:38:20');
INSERT INTO `project_activity_logs` VALUES ('44', '1', 'mst_agents', '21', 'insert', '2018-03-30 09:38:20');
INSERT INTO `project_activity_logs` VALUES ('45', '1', 'mst_agents', '22', 'insert', '2018-03-30 09:38:45');
INSERT INTO `project_activity_logs` VALUES ('46', '1', 'mst_agents', '23', 'insert', '2018-03-30 09:38:46');
INSERT INTO `project_activity_logs` VALUES ('47', '1', 'mst_agents', '24', 'insert', '2018-03-30 09:38:46');
INSERT INTO `project_activity_logs` VALUES ('48', '1', 'mst_agents', '25', 'insert', '2018-03-30 09:39:26');
INSERT INTO `project_activity_logs` VALUES ('49', '1', 'mst_agents', '26', 'insert', '2018-03-30 09:39:26');
INSERT INTO `project_activity_logs` VALUES ('50', '1', 'mst_agents', '27', 'insert', '2018-03-30 09:39:26');
INSERT INTO `project_activity_logs` VALUES ('51', '1', 'mst_agents', '28', 'insert', '2018-03-30 09:40:54');
INSERT INTO `project_activity_logs` VALUES ('52', '1', 'mst_agents', '29', 'insert', '2018-03-30 09:40:54');
INSERT INTO `project_activity_logs` VALUES ('53', '1', 'mst_agents', '30', 'insert', '2018-03-30 09:40:55');
INSERT INTO `project_activity_logs` VALUES ('54', '1', 'mst_agents', '31', 'insert', '2018-03-30 09:42:07');
INSERT INTO `project_activity_logs` VALUES ('55', '1', 'mst_agents', '32', 'insert', '2018-03-30 09:42:08');
INSERT INTO `project_activity_logs` VALUES ('56', '1', 'mst_agents', '33', 'insert', '2018-03-30 09:42:08');
INSERT INTO `project_activity_logs` VALUES ('57', '1', 'mst_agents', '34', 'insert', '2018-03-30 09:51:34');
INSERT INTO `project_activity_logs` VALUES ('58', '1', 'mst_agents', '35', 'insert', '2018-03-30 09:51:35');
INSERT INTO `project_activity_logs` VALUES ('59', '1', 'mst_agents', '36', 'insert', '2018-03-30 09:53:38');
INSERT INTO `project_activity_logs` VALUES ('60', '1', 'mst_agents', '37', 'insert', '2018-03-30 09:53:38');
INSERT INTO `project_activity_logs` VALUES ('61', '1', 'mst_agents', '38', 'insert', '2018-03-30 09:53:39');
INSERT INTO `project_activity_logs` VALUES ('62', '1', 'mst_agents', '39', 'insert', '2018-03-30 09:53:39');
INSERT INTO `project_activity_logs` VALUES ('63', '1', 'mst_agents', '40', 'insert', '2018-03-30 09:54:31');
INSERT INTO `project_activity_logs` VALUES ('64', '1', 'mst_agents', '41', 'insert', '2018-03-30 09:54:31');
INSERT INTO `project_activity_logs` VALUES ('65', '1', 'mst_agents', '42', 'insert', '2018-03-30 09:56:06');
INSERT INTO `project_activity_logs` VALUES ('66', '1', 'mst_agents', '43', 'insert', '2018-03-30 09:56:06');
INSERT INTO `project_activity_logs` VALUES ('67', '1', 'mst_agents', '44', 'insert', '2018-03-30 09:58:21');
INSERT INTO `project_activity_logs` VALUES ('68', '1', 'mst_agents', '44', 'update', '2018-03-30 11:06:43');
INSERT INTO `project_activity_logs` VALUES ('69', '1', 'mst_agents', '45', 'insert', '2018-03-30 12:56:40');
INSERT INTO `project_activity_logs` VALUES ('70', '1', 'mst_agents', '46', 'insert', '2018-03-30 12:56:40');
INSERT INTO `project_activity_logs` VALUES ('71', '1', 'mst_agents', '47', 'insert', '2018-03-30 12:56:40');
INSERT INTO `project_activity_logs` VALUES ('72', '1', 'mst_agents', '48', 'insert', '2018-03-30 12:58:43');
INSERT INTO `project_activity_logs` VALUES ('73', '1', 'mst_agents', '49', 'insert', '2018-03-30 12:58:43');
INSERT INTO `project_activity_logs` VALUES ('74', '1', 'mst_agents', '50', 'insert', '2018-03-30 12:58:44');
INSERT INTO `project_activity_logs` VALUES ('75', '1', 'mst_agents', '51', 'insert', '2018-03-30 13:08:47');
INSERT INTO `project_activity_logs` VALUES ('76', '1', 'mst_agents', '52', 'insert', '2018-03-30 13:08:47');
INSERT INTO `project_activity_logs` VALUES ('77', '1', 'mst_agents', '53', 'insert', '2018-03-30 13:08:47');
INSERT INTO `project_activity_logs` VALUES ('78', '1', 'mst_parties', '2', 'insert', '2018-03-30 13:08:48');
INSERT INTO `project_activity_logs` VALUES ('79', '1', 'mst_parties', '3', 'insert', '2018-03-30 13:08:48');
INSERT INTO `project_activity_logs` VALUES ('80', '1', 'mst_agents', '1', 'insert', '2018-03-30 14:32:43');
INSERT INTO `project_activity_logs` VALUES ('81', '1', 'mst_agents', '2', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('82', '1', 'mst_agents', '3', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('83', '1', 'mst_agents', '4', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('84', '1', 'mst_agents', '5', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('85', '1', 'mst_agents', '6', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('86', '1', 'mst_agents', '7', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('87', '1', 'mst_agents', '8', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('88', '1', 'mst_agents', '9', 'insert', '2018-03-30 14:32:44');
INSERT INTO `project_activity_logs` VALUES ('89', '1', 'mst_agents', '10', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('90', '1', 'mst_agents', '11', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('91', '1', 'mst_agents', '12', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('92', '1', 'mst_agents', '13', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('93', '1', 'mst_agents', '14', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('94', '1', 'mst_agents', '15', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('95', '1', 'mst_agents', '16', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('96', '1', 'mst_agents', '17', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('97', '1', 'mst_agents', '18', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('98', '1', 'mst_agents', '19', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('99', '1', 'mst_agents', '20', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('100', '1', 'mst_agents', '21', 'insert', '2018-03-30 14:32:45');
INSERT INTO `project_activity_logs` VALUES ('101', '1', 'mst_agents', '22', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('102', '1', 'mst_agents', '23', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('103', '1', 'mst_agents', '24', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('104', '1', 'mst_parties', '4', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('105', '1', 'mst_parties', '5', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('106', '1', 'mst_parties', '6', 'insert', '2018-03-30 14:32:46');
INSERT INTO `project_activity_logs` VALUES ('107', '1', 'mst_parties', '7', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('108', '1', 'mst_parties', '8', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('109', '1', 'mst_parties', '9', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('110', '1', 'mst_parties', '10', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('111', '1', 'mst_parties', '11', 'insert', '2018-03-30 14:32:47');
INSERT INTO `project_activity_logs` VALUES ('112', '1', 'mst_parties', '12', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('113', '1', 'mst_parties', '13', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('114', '1', 'mst_parties', '14', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('115', '1', 'mst_parties', '15', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('116', '1', 'mst_parties', '16', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('117', '1', 'mst_parties', '17', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('118', '1', 'mst_parties', '18', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('119', '1', 'mst_parties', '19', 'insert', '2018-03-30 14:32:48');
INSERT INTO `project_activity_logs` VALUES ('120', '1', 'mst_parties', '20', 'insert', '2018-03-30 14:32:49');
INSERT INTO `project_activity_logs` VALUES ('121', '1', 'mst_parties', '21', 'insert', '2018-03-30 14:32:49');
INSERT INTO `project_activity_logs` VALUES ('122', '1', 'mst_parties', '22', 'insert', '2018-03-30 14:32:49');
INSERT INTO `project_activity_logs` VALUES ('123', '1', 'mst_agents', '25', 'insert', '2018-04-02 15:09:09');
INSERT INTO `project_activity_logs` VALUES ('124', '1', 'tbl_sales', '12', 'update', '2018-04-02 16:54:51');
INSERT INTO `project_activity_logs` VALUES ('125', '1', 'tbl_sales', '9', 'update', '2018-04-02 16:55:18');
INSERT INTO `project_activity_logs` VALUES ('126', '1', 'tbl_sales', '10', 'update', '2018-04-02 16:56:54');
INSERT INTO `project_activity_logs` VALUES ('127', '1', 'tbl_sales', '11', 'update', '2018-04-02 16:56:54');
INSERT INTO `project_activity_logs` VALUES ('128', '1', 'tbl_sales', '12', 'update', '2018-04-02 16:56:54');
INSERT INTO `project_activity_logs` VALUES ('129', '1', 'tbl_sales', '7', 'update', '2018-04-02 16:57:31');
INSERT INTO `project_activity_logs` VALUES ('130', '1', 'tbl_sales', '8', 'update', '2018-04-02 16:57:31');
INSERT INTO `project_activity_logs` VALUES ('131', '1', 'tbl_sales', '9', 'update', '2018-04-02 16:57:31');
INSERT INTO `project_activity_logs` VALUES ('132', '1', 'tbl_sales', '10', 'update', '2018-04-02 16:57:54');
INSERT INTO `project_activity_logs` VALUES ('133', '1', 'tbl_sales', '11', 'update', '2018-04-02 16:57:54');
INSERT INTO `project_activity_logs` VALUES ('134', '1', 'tbl_sales', '12', 'update', '2018-04-02 16:57:55');
INSERT INTO `project_activity_logs` VALUES ('135', '1', 'tbl_sales', '7', 'update', '2018-04-02 16:58:36');
INSERT INTO `project_activity_logs` VALUES ('136', '1', 'tbl_sales', '8', 'update', '2018-04-02 16:58:36');
INSERT INTO `project_activity_logs` VALUES ('137', '1', 'tbl_sales', '9', 'update', '2018-04-02 16:58:36');
INSERT INTO `project_activity_logs` VALUES ('138', '1', 'tbl_payment', '1', 'update', '2018-04-03 12:06:42');
INSERT INTO `project_activity_logs` VALUES ('139', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:51:45');
INSERT INTO `project_activity_logs` VALUES ('140', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:53:54');
INSERT INTO `project_activity_logs` VALUES ('141', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:56:33');
INSERT INTO `project_activity_logs` VALUES ('142', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:57:40');
INSERT INTO `project_activity_logs` VALUES ('143', '1', 'tbl_payment', '1', 'update', '2018-04-03 14:59:03');
INSERT INTO `project_activity_logs` VALUES ('144', '1', 'tbl_payment', '1', 'update', '2018-04-03 15:00:02');
INSERT INTO `project_activity_logs` VALUES ('145', '1', 'tbl_payment', '1', 'update', '2018-04-03 15:01:40');
INSERT INTO `project_activity_logs` VALUES ('146', '1', 'mst_agents', '1', 'insert', '2018-04-04 14:57:29');
INSERT INTO `project_activity_logs` VALUES ('147', '1', 'mst_agents', '2', 'insert', '2018-04-04 14:58:07');
INSERT INTO `project_activity_logs` VALUES ('148', '1', 'mst_agents', '3', 'insert', '2018-04-04 14:59:37');
INSERT INTO `project_activity_logs` VALUES ('149', '1', 'mst_agents', '4', 'insert', '2018-04-04 14:59:37');
INSERT INTO `project_activity_logs` VALUES ('150', '1', 'mst_agents', '5', 'insert', '2018-04-04 14:59:38');
INSERT INTO `project_activity_logs` VALUES ('151', '1', 'mst_parties', '1', 'insert', '2018-04-04 14:59:38');
INSERT INTO `project_activity_logs` VALUES ('152', '1', 'mst_parties', '2', 'insert', '2018-04-04 14:59:38');
INSERT INTO `project_activity_logs` VALUES ('153', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:01:31');
INSERT INTO `project_activity_logs` VALUES ('154', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:01:32');
INSERT INTO `project_activity_logs` VALUES ('155', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:01:32');
INSERT INTO `project_activity_logs` VALUES ('156', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:03:19');
INSERT INTO `project_activity_logs` VALUES ('157', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:03:20');
INSERT INTO `project_activity_logs` VALUES ('158', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:03:20');
INSERT INTO `project_activity_logs` VALUES ('159', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:04:17');
INSERT INTO `project_activity_logs` VALUES ('160', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:04:18');
INSERT INTO `project_activity_logs` VALUES ('161', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:04:19');
INSERT INTO `project_activity_logs` VALUES ('162', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:07:08');
INSERT INTO `project_activity_logs` VALUES ('163', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:07:08');
INSERT INTO `project_activity_logs` VALUES ('164', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:07:08');
INSERT INTO `project_activity_logs` VALUES ('165', '1', 'mst_agents', '1', 'insert', '2018-04-04 15:08:15');
INSERT INTO `project_activity_logs` VALUES ('166', '1', 'mst_agents', '2', 'insert', '2018-04-04 15:08:16');
INSERT INTO `project_activity_logs` VALUES ('167', '1', 'mst_agents', '3', 'insert', '2018-04-04 15:08:16');
INSERT INTO `project_activity_logs` VALUES ('168', '1', 'tbl_sales', '1', 'update', '2018-04-04 15:19:51');
INSERT INTO `project_activity_logs` VALUES ('169', '1', 'tbl_sales', '2', 'update', '2018-04-04 15:19:51');
INSERT INTO `project_activity_logs` VALUES ('170', '1', 'tbl_sales', '1', 'update', '2018-04-04 15:20:53');
INSERT INTO `project_activity_logs` VALUES ('171', '1', 'tbl_sales', '2', 'update', '2018-04-04 15:20:53');
INSERT INTO `project_activity_logs` VALUES ('172', '1', 'mst_fiscal_years', '1', 'insert', '2018-04-04 16:32:00');
INSERT INTO `project_activity_logs` VALUES ('173', '1', 'mst_fiscal_years', '2', 'insert', '2018-04-04 16:32:14');
INSERT INTO `project_activity_logs` VALUES ('174', '1', 'mst_fiscal_years', '3', 'insert', '2018-04-04 16:49:45');
INSERT INTO `project_activity_logs` VALUES ('175', '1', 'mst_fiscal_years', '4', 'insert', '2018-04-04 16:51:59');
INSERT INTO `project_activity_logs` VALUES ('176', '1', 'mst_fiscal_years', '5', 'insert', '2018-04-04 16:53:46');
INSERT INTO `project_activity_logs` VALUES ('177', '1', 'mst_fiscal_years', '6', 'insert', '2018-04-04 16:54:20');
INSERT INTO `project_activity_logs` VALUES ('178', '1', 'mst_fiscal_years', '1', 'insert', '2018-04-04 17:02:47');
INSERT INTO `project_activity_logs` VALUES ('179', '1', 'mst_fiscal_years', '2', 'insert', '2018-04-04 17:18:26');
INSERT INTO `project_activity_logs` VALUES ('180', '1', 'mst_schemes', '1', 'insert', '2018-04-04 17:26:01');
INSERT INTO `project_activity_logs` VALUES ('181', '1', 'mst_schemes', '2', 'insert', '2018-04-04 17:37:49');
INSERT INTO `project_activity_logs` VALUES ('182', '1', 'mst_schemes', '3', 'insert', '2018-04-04 17:38:23');
INSERT INTO `project_activity_logs` VALUES ('183', '1', 'mst_schemes', '4', 'insert', '2018-04-04 17:38:54');
INSERT INTO `project_activity_logs` VALUES ('184', '1', 'mst_schemes', '5', 'insert', '2018-04-04 17:41:43');
INSERT INTO `project_activity_logs` VALUES ('185', '1', 'tbl_sales', '1', 'update', '2018-04-04 17:54:35');
INSERT INTO `project_activity_logs` VALUES ('186', '1', 'tbl_sales', '2', 'update', '2018-04-04 17:54:36');
INSERT INTO `project_activity_logs` VALUES ('187', '1', 'tbl_sales', '3', 'update', '2018-04-04 17:54:36');
INSERT INTO `project_activity_logs` VALUES ('188', '1', 'tbl_sales', '1', 'update', '2018-04-04 17:55:50');
INSERT INTO `project_activity_logs` VALUES ('189', '1', 'tbl_sales', '2', 'update', '2018-04-04 17:55:50');
INSERT INTO `project_activity_logs` VALUES ('190', '1', 'tbl_sales', '3', 'update', '2018-04-04 17:55:50');
INSERT INTO `project_activity_logs` VALUES ('191', '1', 'mst_agents', '1', 'insert', '2018-04-04 18:08:55');
INSERT INTO `project_activity_logs` VALUES ('192', '1', 'mst_agents', '2', 'insert', '2018-04-04 18:09:26');
INSERT INTO `project_activity_logs` VALUES ('193', '1', 'mst_agents', '3', 'insert', '2018-04-04 18:09:26');
INSERT INTO `project_activity_logs` VALUES ('194', '1', 'mst_agents', '4', 'insert', '2018-04-04 18:09:27');
INSERT INTO `project_activity_logs` VALUES ('195', '1', 'mst_parties', '1', 'insert', '2018-04-04 18:09:27');
INSERT INTO `project_activity_logs` VALUES ('196', '1', 'mst_parties', '2', 'insert', '2018-04-04 18:09:27');
INSERT INTO `project_activity_logs` VALUES ('197', '1', 'tbl_sales_temporary', '46', 'update', '2018-04-05 12:23:07');
INSERT INTO `project_activity_logs` VALUES ('198', '1', 'tbl_sales_temporary', '47', 'update', '2018-04-05 12:23:57');
INSERT INTO `project_activity_logs` VALUES ('199', '1', 'tbl_sales_temporary', '48', 'update', '2018-04-05 12:24:05');
INSERT INTO `project_activity_logs` VALUES ('200', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 12:26:08');
INSERT INTO `project_activity_logs` VALUES ('201', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 12:28:12');
INSERT INTO `project_activity_logs` VALUES ('202', '1', 'tbl_sales_temporary', '12', 'update', '2018-04-05 14:32:35');
INSERT INTO `project_activity_logs` VALUES ('203', '1', 'tbl_sales_temporary', '11', 'update', '2018-04-05 14:32:40');
INSERT INTO `project_activity_logs` VALUES ('204', '1', 'tbl_sales_temporary', '13', 'update', '2018-04-05 14:34:16');
INSERT INTO `project_activity_logs` VALUES ('205', '1', 'tbl_sales_temporary', '14', 'update', '2018-04-05 14:34:19');
INSERT INTO `project_activity_logs` VALUES ('206', '1', 'tbl_sales_temporary', '15', 'update', '2018-04-05 14:34:23');
INSERT INTO `project_activity_logs` VALUES ('207', '1', 'tbl_sales_temporary', '16', 'update', '2018-04-05 14:34:26');
INSERT INTO `project_activity_logs` VALUES ('208', '1', 'tbl_sales_temporary', '17', 'update', '2018-04-05 14:34:30');
INSERT INTO `project_activity_logs` VALUES ('209', '1', 'tbl_sales_temporary', '18', 'update', '2018-04-05 14:34:34');
INSERT INTO `project_activity_logs` VALUES ('210', '1', 'tbl_sales_temporary', '21', 'update', '2018-04-05 14:36:24');
INSERT INTO `project_activity_logs` VALUES ('211', '1', 'tbl_sales_temporary', '20', 'update', '2018-04-05 14:36:27');
INSERT INTO `project_activity_logs` VALUES ('212', '1', 'tbl_sales_temporary', '19', 'update', '2018-04-05 14:36:30');
INSERT INTO `project_activity_logs` VALUES ('213', '1', 'tbl_sales_temporary', '24', 'update', '2018-04-05 14:37:24');
INSERT INTO `project_activity_logs` VALUES ('214', '1', 'tbl_sales_temporary', '23', 'update', '2018-04-05 14:37:29');
INSERT INTO `project_activity_logs` VALUES ('215', '1', 'tbl_sales_temporary', '22', 'update', '2018-04-05 14:37:31');
INSERT INTO `project_activity_logs` VALUES ('216', '1', 'tbl_sales_temporary', '27', 'update', '2018-04-05 14:38:22');
INSERT INTO `project_activity_logs` VALUES ('217', '1', 'tbl_sales_temporary', '27', 'update', '2018-04-05 14:38:28');
INSERT INTO `project_activity_logs` VALUES ('218', '1', 'tbl_sales_temporary', '30', 'update', '2018-04-05 14:39:49');
INSERT INTO `project_activity_logs` VALUES ('219', '1', 'tbl_sales_temporary', '33', 'update', '2018-04-05 14:40:53');
INSERT INTO `project_activity_logs` VALUES ('220', '1', 'tbl_sales_temporary', '36', 'update', '2018-04-05 14:41:25');
INSERT INTO `project_activity_logs` VALUES ('221', '1', 'tbl_sales_temporary', '39', 'update', '2018-04-05 14:42:29');
INSERT INTO `project_activity_logs` VALUES ('222', '1', 'tbl_sales_temporary', '38', 'update', '2018-04-05 14:42:32');
INSERT INTO `project_activity_logs` VALUES ('223', '1', 'tbl_sales_temporary', '37', 'update', '2018-04-05 14:42:36');
INSERT INTO `project_activity_logs` VALUES ('224', '1', 'tbl_sales', '43', 'update', '2018-04-05 14:45:26');
INSERT INTO `project_activity_logs` VALUES ('225', '1', 'tbl_sales', '44', 'update', '2018-04-05 14:45:26');
INSERT INTO `project_activity_logs` VALUES ('226', '1', 'tbl_sales', '45', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('227', '1', 'tbl_sales', '46', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('228', '1', 'tbl_sales', '47', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('229', '1', 'tbl_sales', '48', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('230', '1', 'tbl_sales', '49', 'update', '2018-04-05 14:45:27');
INSERT INTO `project_activity_logs` VALUES ('231', '1', 'tbl_sales', '50', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('232', '1', 'tbl_sales', '51', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('233', '1', 'tbl_sales', '52', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('234', '1', 'tbl_sales', '53', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('235', '1', 'tbl_sales', '54', 'update', '2018-04-05 14:45:28');
INSERT INTO `project_activity_logs` VALUES ('236', '1', 'tbl_sales', '55', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('237', '1', 'tbl_sales', '56', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('238', '1', 'tbl_sales', '57', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('239', '1', 'tbl_sales', '58', 'update', '2018-04-05 14:45:29');
INSERT INTO `project_activity_logs` VALUES ('240', '1', 'tbl_sales', '59', 'update', '2018-04-05 14:45:30');
INSERT INTO `project_activity_logs` VALUES ('241', '1', 'tbl_sales', '60', 'update', '2018-04-05 14:45:30');
INSERT INTO `project_activity_logs` VALUES ('242', '1', 'tbl_sales_temporary', '3', 'update', '2018-04-05 14:54:34');
INSERT INTO `project_activity_logs` VALUES ('243', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 14:54:40');
INSERT INTO `project_activity_logs` VALUES ('244', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 14:54:45');
INSERT INTO `project_activity_logs` VALUES ('245', '1', 'tbl_sales_temporary', '12', 'update', '2018-04-05 14:57:52');
INSERT INTO `project_activity_logs` VALUES ('246', '1', 'tbl_sales_temporary', '11', 'update', '2018-04-05 14:57:56');
INSERT INTO `project_activity_logs` VALUES ('247', '1', 'tbl_sales_temporary', '10', 'update', '2018-04-05 14:57:59');
INSERT INTO `project_activity_logs` VALUES ('248', '1', 'mst_agents', '5', 'insert', '2018-04-05 15:04:22');
INSERT INTO `project_activity_logs` VALUES ('249', '1', 'mst_parties', '3', 'insert', '2018-04-05 15:04:23');
INSERT INTO `project_activity_logs` VALUES ('250', '1', 'mst_parties', '4', 'insert', '2018-04-05 15:04:23');
INSERT INTO `project_activity_logs` VALUES ('251', '1', 'mst_brands', '2', 'insert', '2018-04-05 15:04:50');
INSERT INTO `project_activity_logs` VALUES ('252', '1', 'mst_brands', '3', 'insert', '2018-04-05 15:05:03');
INSERT INTO `project_activity_logs` VALUES ('253', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 15:05:30');
INSERT INTO `project_activity_logs` VALUES ('254', '1', 'mst_agents', '6', 'insert', '2018-04-05 15:06:25');
INSERT INTO `project_activity_logs` VALUES ('255', '1', 'mst_parties', '5', 'insert', '2018-04-05 15:06:26');
INSERT INTO `project_activity_logs` VALUES ('256', '1', 'mst_agents', '7', 'insert', '2018-04-05 15:07:06');
INSERT INTO `project_activity_logs` VALUES ('257', '1', 'mst_agents', '8', 'insert', '2018-04-05 15:09:23');
INSERT INTO `project_activity_logs` VALUES ('258', '1', 'mst_agents', '9', 'insert', '2018-04-05 15:09:24');
INSERT INTO `project_activity_logs` VALUES ('259', '1', 'mst_parties', '6', 'insert', '2018-04-05 15:09:24');
INSERT INTO `project_activity_logs` VALUES ('260', '1', 'mst_parties', '7', 'insert', '2018-04-05 15:09:25');
INSERT INTO `project_activity_logs` VALUES ('261', '1', 'mst_agents', '10', 'insert', '2018-04-05 15:10:59');
INSERT INTO `project_activity_logs` VALUES ('262', '1', 'mst_agents', '11', 'insert', '2018-04-05 15:10:59');
INSERT INTO `project_activity_logs` VALUES ('263', '1', 'mst_parties', '8', 'insert', '2018-04-05 15:11:00');
INSERT INTO `project_activity_logs` VALUES ('264', '1', 'mst_parties', '9', 'insert', '2018-04-05 15:11:00');
INSERT INTO `project_activity_logs` VALUES ('265', '1', 'mst_agents', '12', 'insert', '2018-04-05 15:13:36');
INSERT INTO `project_activity_logs` VALUES ('266', '1', 'mst_parties', '10', 'insert', '2018-04-05 15:13:37');
INSERT INTO `project_activity_logs` VALUES ('267', '1', 'mst_parties', '11', 'insert', '2018-04-05 15:13:37');
INSERT INTO `project_activity_logs` VALUES ('268', '1', 'mst_agents', '13', 'insert', '2018-04-05 15:15:17');
INSERT INTO `project_activity_logs` VALUES ('269', '1', 'mst_parties', '12', 'insert', '2018-04-05 15:15:17');
INSERT INTO `project_activity_logs` VALUES ('270', '1', 'tbl_sales_temporary', '14', 'update', '2018-04-05 15:15:26');
INSERT INTO `project_activity_logs` VALUES ('271', '1', 'mst_agents', '14', 'insert', '2018-04-05 15:17:39');
INSERT INTO `project_activity_logs` VALUES ('272', '1', 'mst_parties', '13', 'insert', '2018-04-05 15:17:40');
INSERT INTO `project_activity_logs` VALUES ('273', '1', 'tbl_sales_temporary', '1', 'update', '2018-04-05 15:17:50');
INSERT INTO `project_activity_logs` VALUES ('274', '1', 'tbl_sales_temporary', '2', 'update', '2018-04-05 15:18:38');
INSERT INTO `project_activity_logs` VALUES ('275', '1', 'tbl_sales', '2', 'update', '2018-04-05 15:18:59');
INSERT INTO `project_activity_logs` VALUES ('276', '1', 'tbl_payment_temporary', '5', 'update', '2018-04-05 15:35:12');
INSERT INTO `project_activity_logs` VALUES ('277', '1', 'tbl_payment_temporary', '8', 'update', '2018-04-05 15:38:47');

-- ----------------------------
-- Table structure for project_audit_logs
-- ----------------------------
DROP TABLE IF EXISTS `project_audit_logs`;
CREATE TABLE `project_audit_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `table_pk` int(11) unsigned NOT NULL,
  `column_name` varchar(255) DEFAULT NULL,
  `old_value` varchar(1000) DEFAULT NULL,
  `new_value` varchar(1000) DEFAULT NULL,
  `action_dttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_audit_logs
-- ----------------------------
INSERT INTO `project_audit_logs` VALUES ('1', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:56:52');
INSERT INTO `project_audit_logs` VALUES ('2', '1', 'mst_agents', '1', 'phone', '9803163986', '01-4283184', '2018-03-26 11:56:52');
INSERT INTO `project_audit_logs` VALUES ('3', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:57:07');
INSERT INTO `project_audit_logs` VALUES ('4', '1', 'mst_agents', '1', 'email', 'sanish@pagodalabs.com', 'crazy.san8@gmail.com', '2018-03-26 11:57:07');
INSERT INTO `project_audit_logs` VALUES ('5', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:57:30');
INSERT INTO `project_audit_logs` VALUES ('6', '1', 'mst_agents', '1', 'email', 'crazy.san8@gmail.com', 'sanish@pagodalabs.com', '2018-03-26 11:57:30');
INSERT INTO `project_audit_logs` VALUES ('7', '1', 'mst_agents', '1', 'user_id', '0', '', '2018-03-26 11:58:17');
INSERT INTO `project_audit_logs` VALUES ('8', '1', 'mst_agents', '1', 'email', 'sanish@pagodalabs.com', 'crazy.san8@gmail.com', '2018-03-26 11:58:17');
INSERT INTO `project_audit_logs` VALUES ('9', '1', 'mst_agents', '1', 'user_id', '0', null, '2018-03-26 14:14:21');
INSERT INTO `project_audit_logs` VALUES ('10', '1', 'mst_agents', '1', 'district', '47', null, '2018-03-26 14:14:21');
INSERT INTO `project_audit_logs` VALUES ('11', '1', 'mst_agents', '1', 'mun_vdc', '1623', null, '2018-03-26 14:14:21');
INSERT INTO `project_audit_logs` VALUES ('12', '1', 'mst_agents', '1', 'email', 'crazy.san8@gmail.com', 'sanish@pagodalabs.com', '2018-03-26 14:39:20');
INSERT INTO `project_audit_logs` VALUES ('13', '1', 'mst_agents', '1', 'district_id', null, '25', '2018-03-26 15:57:15');
INSERT INTO `project_audit_logs` VALUES ('14', '1', 'mst_agents', '1', 'mun_vdc_id', null, '1004', '2018-03-26 15:57:15');
INSERT INTO `project_audit_logs` VALUES ('15', '1', 'mst_agents', '1', 'province_id', null, '2', '2018-03-26 15:57:15');
INSERT INTO `project_audit_logs` VALUES ('16', '1', 'mst_nepali_month', '1', 'rank', '1', '2', '2018-03-27 17:41:10');
INSERT INTO `project_audit_logs` VALUES ('17', '1', 'mst_nepali_month', '1', 'rank', '2', '1', '2018-03-27 17:41:14');
INSERT INTO `project_audit_logs` VALUES ('18', '1', 'mst_agents', '44', 'district_id', null, '40', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('19', '1', 'mst_agents', '44', 'mun_vdc_id', null, '1923', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('20', '1', 'mst_agents', '44', 'city', null, 'Rautahat', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('21', '1', 'mst_agents', '44', 'address', null, 'Rautahat', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('22', '1', 'mst_agents', '44', 'phone', null, '9803163986', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('23', '1', 'mst_agents', '44', 'mobile', null, '9803163986', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('24', '1', 'mst_agents', '44', 'email', null, 'sanish@pagodalabs.com', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('25', '1', 'mst_agents', '44', 'province_id', null, '2', '2018-03-30 11:06:43');
INSERT INTO `project_audit_logs` VALUES ('26', '1', 'tbl_sales', '12', 'is_published', '0', '1', '2018-04-02 16:54:51');
INSERT INTO `project_audit_logs` VALUES ('27', '1', 'tbl_sales', '9', 'is_published', '0', '1', '2018-04-02 16:55:18');
INSERT INTO `project_audit_logs` VALUES ('28', '1', 'tbl_sales', '10', 'is_published', '0', '1', '2018-04-02 16:56:54');
INSERT INTO `project_audit_logs` VALUES ('29', '1', 'tbl_sales', '11', 'is_published', '0', '1', '2018-04-02 16:56:54');
INSERT INTO `project_audit_logs` VALUES ('30', '1', 'tbl_sales', '12', 'is_published', '0', '1', '2018-04-02 16:56:54');
INSERT INTO `project_audit_logs` VALUES ('31', '1', 'tbl_sales', '7', 'is_published', '0', '1', '2018-04-02 16:57:31');
INSERT INTO `project_audit_logs` VALUES ('32', '1', 'tbl_sales', '8', 'is_published', '0', '1', '2018-04-02 16:57:31');
INSERT INTO `project_audit_logs` VALUES ('33', '1', 'tbl_sales', '9', 'is_published', '0', '1', '2018-04-02 16:57:31');
INSERT INTO `project_audit_logs` VALUES ('34', '1', 'tbl_sales', '10', 'is_published', '0', '1', '2018-04-02 16:57:54');
INSERT INTO `project_audit_logs` VALUES ('35', '1', 'tbl_sales', '11', 'is_published', '0', '1', '2018-04-02 16:57:54');
INSERT INTO `project_audit_logs` VALUES ('36', '1', 'tbl_sales', '12', 'is_published', '0', '1', '2018-04-02 16:57:55');
INSERT INTO `project_audit_logs` VALUES ('37', '1', 'tbl_sales', '7', 'is_published', '0', '1', '2018-04-02 16:58:36');
INSERT INTO `project_audit_logs` VALUES ('38', '1', 'tbl_sales', '8', 'is_published', '0', '1', '2018-04-02 16:58:36');
INSERT INTO `project_audit_logs` VALUES ('39', '1', 'tbl_sales', '9', 'is_published', '0', '1', '2018-04-02 16:58:36');
INSERT INTO `project_audit_logs` VALUES ('40', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 12:06:42');
INSERT INTO `project_audit_logs` VALUES ('41', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:51:45');
INSERT INTO `project_audit_logs` VALUES ('42', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:53:54');
INSERT INTO `project_audit_logs` VALUES ('43', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:56:33');
INSERT INTO `project_audit_logs` VALUES ('44', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:57:40');
INSERT INTO `project_audit_logs` VALUES ('45', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 14:59:03');
INSERT INTO `project_audit_logs` VALUES ('46', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 15:00:02');
INSERT INTO `project_audit_logs` VALUES ('47', '1', 'tbl_payment', '1', 'is_published', '0', '1', '2018-04-03 15:01:40');
INSERT INTO `project_audit_logs` VALUES ('48', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 15:19:51');
INSERT INTO `project_audit_logs` VALUES ('49', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 15:19:51');
INSERT INTO `project_audit_logs` VALUES ('50', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 15:20:53');
INSERT INTO `project_audit_logs` VALUES ('51', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 15:20:53');
INSERT INTO `project_audit_logs` VALUES ('52', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 17:54:35');
INSERT INTO `project_audit_logs` VALUES ('53', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 17:54:36');
INSERT INTO `project_audit_logs` VALUES ('54', '1', 'tbl_sales', '3', 'is_published', '0', '1', '2018-04-04 17:54:36');
INSERT INTO `project_audit_logs` VALUES ('55', '1', 'tbl_sales', '1', 'is_published', '0', '1', '2018-04-04 17:55:50');
INSERT INTO `project_audit_logs` VALUES ('56', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-04 17:55:50');
INSERT INTO `project_audit_logs` VALUES ('57', '1', 'tbl_sales', '3', 'is_published', '0', '1', '2018-04-04 17:55:50');
INSERT INTO `project_audit_logs` VALUES ('58', '1', 'tbl_sales_temporary', '46', 'quantity', '420', '200', '2018-04-05 12:23:07');
INSERT INTO `project_audit_logs` VALUES ('59', '1', 'tbl_sales_temporary', '47', 'quantity', '420', '300', '2018-04-05 12:23:57');
INSERT INTO `project_audit_logs` VALUES ('60', '1', 'tbl_sales_temporary', '48', 'quantity', '420', '20000', '2018-04-05 12:24:05');
INSERT INTO `project_audit_logs` VALUES ('61', '1', 'tbl_sales_temporary', '1', 'rate', '577.37', '400', '2018-04-05 12:26:08');
INSERT INTO `project_audit_logs` VALUES ('62', '1', 'tbl_sales_temporary', '1', 'quantity', '420', '300', '2018-04-05 12:28:12');
INSERT INTO `project_audit_logs` VALUES ('63', '1', 'tbl_sales_temporary', '12', 'quantity', '420', '1', '2018-04-05 14:32:35');
INSERT INTO `project_audit_logs` VALUES ('64', '1', 'tbl_sales_temporary', '11', 'quantity', '420', '20', '2018-04-05 14:32:40');
INSERT INTO `project_audit_logs` VALUES ('65', '1', 'tbl_sales_temporary', '13', 'quantity', '420', '1', '2018-04-05 14:34:16');
INSERT INTO `project_audit_logs` VALUES ('66', '1', 'tbl_sales_temporary', '14', 'quantity', '420', '1', '2018-04-05 14:34:19');
INSERT INTO `project_audit_logs` VALUES ('67', '1', 'tbl_sales_temporary', '15', 'quantity', '420', '1', '2018-04-05 14:34:23');
INSERT INTO `project_audit_logs` VALUES ('68', '1', 'tbl_sales_temporary', '16', 'quantity', '420', '1', '2018-04-05 14:34:26');
INSERT INTO `project_audit_logs` VALUES ('69', '1', 'tbl_sales_temporary', '17', 'quantity', '420', '2', '2018-04-05 14:34:30');
INSERT INTO `project_audit_logs` VALUES ('70', '1', 'tbl_sales_temporary', '18', 'quantity', '420', '2', '2018-04-05 14:34:34');
INSERT INTO `project_audit_logs` VALUES ('71', '1', 'tbl_sales_temporary', '21', 'quantity', '420', '1', '2018-04-05 14:36:24');
INSERT INTO `project_audit_logs` VALUES ('72', '1', 'tbl_sales_temporary', '20', 'quantity', '420', '1', '2018-04-05 14:36:27');
INSERT INTO `project_audit_logs` VALUES ('73', '1', 'tbl_sales_temporary', '19', 'quantity', '420', '1', '2018-04-05 14:36:30');
INSERT INTO `project_audit_logs` VALUES ('74', '1', 'tbl_sales_temporary', '24', 'quantity', '420', '1', '2018-04-05 14:37:24');
INSERT INTO `project_audit_logs` VALUES ('75', '1', 'tbl_sales_temporary', '23', 'quantity', '420', '1', '2018-04-05 14:37:29');
INSERT INTO `project_audit_logs` VALUES ('76', '1', 'tbl_sales_temporary', '22', 'quantity', '420', '1', '2018-04-05 14:37:31');
INSERT INTO `project_audit_logs` VALUES ('77', '1', 'tbl_sales_temporary', '27', 'quantity', '420', '1', '2018-04-05 14:38:22');
INSERT INTO `project_audit_logs` VALUES ('78', '1', 'tbl_sales_temporary', '27', 'discount', '30', '1', '2018-04-05 14:38:28');
INSERT INTO `project_audit_logs` VALUES ('79', '1', 'tbl_sales_temporary', '30', 'quantity', '420', '1', '2018-04-05 14:39:49');
INSERT INTO `project_audit_logs` VALUES ('80', '1', 'tbl_sales_temporary', '33', 'quantity', '420', '1', '2018-04-05 14:40:53');
INSERT INTO `project_audit_logs` VALUES ('81', '1', 'tbl_sales_temporary', '36', 'quantity', '420', '1', '2018-04-05 14:41:25');
INSERT INTO `project_audit_logs` VALUES ('82', '1', 'tbl_sales_temporary', '39', 'quantity', '420', '1', '2018-04-05 14:42:29');
INSERT INTO `project_audit_logs` VALUES ('83', '1', 'tbl_sales_temporary', '38', 'quantity', '420', '1', '2018-04-05 14:42:32');
INSERT INTO `project_audit_logs` VALUES ('84', '1', 'tbl_sales_temporary', '37', 'quantity', '420', '1', '2018-04-05 14:42:36');
INSERT INTO `project_audit_logs` VALUES ('85', '1', 'tbl_sales', '43', 'is_published', '0', '1', '2018-04-05 14:45:26');
INSERT INTO `project_audit_logs` VALUES ('86', '1', 'tbl_sales', '44', 'is_published', '0', '1', '2018-04-05 14:45:26');
INSERT INTO `project_audit_logs` VALUES ('87', '1', 'tbl_sales', '45', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('88', '1', 'tbl_sales', '46', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('89', '1', 'tbl_sales', '47', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('90', '1', 'tbl_sales', '48', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('91', '1', 'tbl_sales', '49', 'is_published', '0', '1', '2018-04-05 14:45:27');
INSERT INTO `project_audit_logs` VALUES ('92', '1', 'tbl_sales', '50', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('93', '1', 'tbl_sales', '51', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('94', '1', 'tbl_sales', '52', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('95', '1', 'tbl_sales', '53', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('96', '1', 'tbl_sales', '54', 'is_published', '0', '1', '2018-04-05 14:45:28');
INSERT INTO `project_audit_logs` VALUES ('97', '1', 'tbl_sales', '55', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('98', '1', 'tbl_sales', '56', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('99', '1', 'tbl_sales', '57', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('100', '1', 'tbl_sales', '58', 'is_published', '0', '1', '2018-04-05 14:45:29');
INSERT INTO `project_audit_logs` VALUES ('101', '1', 'tbl_sales', '59', 'is_published', '0', '1', '2018-04-05 14:45:30');
INSERT INTO `project_audit_logs` VALUES ('102', '1', 'tbl_sales', '60', 'is_published', '0', '1', '2018-04-05 14:45:30');
INSERT INTO `project_audit_logs` VALUES ('103', '1', 'tbl_sales_temporary', '3', 'quantity', '420', '2', '2018-04-05 14:54:34');
INSERT INTO `project_audit_logs` VALUES ('104', '1', 'tbl_sales_temporary', '1', 'rate', '577.37', '2000', '2018-04-05 14:54:40');
INSERT INTO `project_audit_logs` VALUES ('105', '1', 'tbl_sales_temporary', '1', 'quantity', '420', '100', '2018-04-05 14:54:45');
INSERT INTO `project_audit_logs` VALUES ('106', '1', 'tbl_sales_temporary', '12', 'quantity', '420', '10', '2018-04-05 14:57:52');
INSERT INTO `project_audit_logs` VALUES ('107', '1', 'tbl_sales_temporary', '11', 'quantity', '420', '20', '2018-04-05 14:57:56');
INSERT INTO `project_audit_logs` VALUES ('108', '1', 'tbl_sales_temporary', '10', 'rate', '577.37', '100', '2018-04-05 14:57:59');
INSERT INTO `project_audit_logs` VALUES ('109', '1', 'tbl_sales_temporary', '1', 'quantity', '420', '100', '2018-04-05 15:05:30');
INSERT INTO `project_audit_logs` VALUES ('110', '1', 'tbl_sales_temporary', '14', 'quantity', '330', '200', '2018-04-05 15:15:26');
INSERT INTO `project_audit_logs` VALUES ('111', '1', 'tbl_sales_temporary', '1', 'quantity', '330', '10', '2018-04-05 15:17:50');
INSERT INTO `project_audit_logs` VALUES ('112', '1', 'tbl_sales_temporary', '2', 'quantity', '330', '100', '2018-04-05 15:18:38');
INSERT INTO `project_audit_logs` VALUES ('113', '1', 'tbl_sales', '2', 'is_published', '0', '1', '2018-04-05 15:18:59');
INSERT INTO `project_audit_logs` VALUES ('114', '1', 'tbl_payment_temporary', '5', 'amount', '100000', '200', '2018-04-05 15:35:12');
INSERT INTO `project_audit_logs` VALUES ('115', '1', 'tbl_payment_temporary', '8', 'amount', '100000', '900', '2018-04-05 15:38:47');

-- ----------------------------
-- Table structure for project_migrations
-- ----------------------------
DROP TABLE IF EXISTS `project_migrations`;
CREATE TABLE `project_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_migrations
-- ----------------------------
INSERT INTO `project_migrations` VALUES ('23');

-- ----------------------------
-- Table structure for project_sessions
-- ----------------------------
DROP TABLE IF EXISTS `project_sessions`;
CREATE TABLE `project_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_sessions
-- ----------------------------
INSERT INTO `project_sessions` VALUES ('18dec203bfdd411e356cfece2036842ee5141484', '::1', '1522922317', '__ci_last_regenerate|i:1522922165;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('235123d031497443374090657a29408ff335241d', '::1', '1522918835', '__ci_last_regenerate|i:1522918627;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('3445ca0a2723ef89d86d50d459b395b091daf090', '::1', '1522919594', '__ci_last_regenerate|i:1522919406;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('48bd40852b88d37ad667537888ab49b40f977c66', '::1', '1522920638', '__ci_last_regenerate|i:1522920341;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('56f963a9397aa02f13626edc3b04270a488d26db', '::1', '1522926854', '__ci_last_regenerate|i:1522926583;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('600bddf2cd559d16ac311a7e0d22619beba79173', '::1', '1522911320', '__ci_last_regenerate|i:1522911307;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('6447cd47a691d3dd17c09598fb4bc511ba75982c', '::1', '1522922768', '__ci_last_regenerate|i:1522922510;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('761b9b5df4260b39578db8e57a0cf67b1e7d4379', '::1', '1522922041', '__ci_last_regenerate|i:1522921764;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('7ca7b111cbb942c0e0bb2c768bcd5e42fb9b079a', '::1', '1522918294', '__ci_last_regenerate|i:1522917982;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('8a766fc2a0a9bd160760e9c9eb7e6ffe74694227', '::1', '1522920013', '__ci_last_regenerate|i:1522919718;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('a80e71c82fe1ff5f258fa508b370b3fa7b1dd49b', '::1', '1522922955', '__ci_last_regenerate|i:1522922822;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('d562bf798fe20d32b29910accc9ade6accc60c46', '::1', '1522918588', '__ci_last_regenerate|i:1522918302;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('d69db41417bd56f35980d2064861e21373a4e505', '::1', '1522919401', '__ci_last_regenerate|i:1522919105;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('d7aeb4543a231adc632b614158d04d8e19765ad9', '::1', '1522926656', '__ci_last_regenerate|i:1522926654;');
INSERT INTO `project_sessions` VALUES ('e2ae5c472cfa4b70d15cec6e6f3fdf5bf642a403', '::1', '1522926947', '__ci_last_regenerate|i:1522926947;');
INSERT INTO `project_sessions` VALUES ('ead8b23753e39032705cb609349ab9ef29fb1fc7', '::1', '1522921725', '__ci_last_regenerate|i:1522921358;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('ec610d75c49157efd0d99ac81ef2644f298624af', '::1', '1522920315', '__ci_last_regenerate|i:1522920020;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('f765355b03633408ef05273b764eb04d38af68ab', '::1', '1522920862', '__ci_last_regenerate|i:1522920713;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('fca78fab23cd1f800866b97645b8816a608fde7c', '::1', '1522926928', '__ci_last_regenerate|i:1522926656;id|s:2:\"14\";username|s:15:\"sanish.maharjan\";email|s:28:\"sanish.maharjan@reliance.com\";loggedin|b:1;');

-- ----------------------------
-- Table structure for tbl_payment
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payment`;
CREATE TABLE `tbl_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_date_np` varchar(255) NOT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `bank_name` varchar(255) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `is_published` int(11) DEFAULT '0',
  `fiscal_year_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_payment
-- ----------------------------
INSERT INTO `tbl_payment` VALUES ('1', '1', '1', '1', '2018-04-05 15:33:44', '2018-04-05 15:33:44', '2018-04-05 15:50:43', '0000-00-00', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '20000', '0', null);
INSERT INTO `tbl_payment` VALUES ('2', '1', '1', '1', '2018-04-05 15:33:44', '2018-04-05 15:33:44', '2018-04-05 15:50:43', '0000-00-00', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', null);
INSERT INTO `tbl_payment` VALUES ('3', '1', '1', '1', '2018-04-05 15:33:44', '2018-04-05 15:33:44', '2018-04-05 15:50:43', '0000-00-00', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', null);
INSERT INTO `tbl_payment` VALUES ('4', '1', '1', '1', '2018-04-05 15:35:29', '2018-04-05 15:35:29', '2018-04-05 15:50:43', '0000-00-00', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '200', '0', '2');
INSERT INTO `tbl_payment` VALUES ('5', '1', '1', '1', '2018-04-05 15:37:26', '2018-04-05 15:37:26', '2018-04-05 15:50:43', '0000-00-00', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', '2');
INSERT INTO `tbl_payment` VALUES ('6', '1', '1', '1', '2018-04-05 15:37:51', '2018-04-05 15:37:51', '2018-04-05 15:50:43', '0000-00-00', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', '2');
INSERT INTO `tbl_payment` VALUES ('7', '1', '1', '1', '2018-04-05 15:38:54', '2018-04-05 15:38:54', '2018-04-05 15:50:12', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '900', '0', '2');
INSERT INTO `tbl_payment` VALUES ('8', '1', '1', '1', '2018-04-05 15:41:25', '2018-04-05 15:41:25', '2018-04-05 15:50:12', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', '2');
INSERT INTO `tbl_payment` VALUES ('9', '1', '1', '1', '2018-04-05 15:42:39', '2018-04-05 15:42:39', '2018-04-05 15:50:12', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', '2');
INSERT INTO `tbl_payment` VALUES ('10', '1', '1', '1', '2018-04-05 15:52:24', '2018-04-05 15:52:24', '2018-04-05 15:52:39', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '0', '2');

-- ----------------------------
-- Table structure for tbl_payment_temporary
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payment_temporary`;
CREATE TABLE `tbl_payment_temporary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_date_np` varchar(255) NOT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `bank_name` varchar(255) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `is_processed` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_payment_temporary
-- ----------------------------
INSERT INTO `tbl_payment_temporary` VALUES ('1', '1', '1', null, '2018-04-05 15:27:57', '2018-04-05 15:27:57', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('2', '1', '1', null, '2018-04-05 15:29:37', '2018-04-05 15:29:37', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('3', '1', '1', null, '2018-04-05 15:31:09', '2018-04-05 15:31:09', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('4', '1', '1', null, '2018-04-05 15:32:02', '2018-04-05 15:32:02', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('5', '1', '1', null, '2018-04-05 15:35:04', '2018-04-05 15:35:12', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '200', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('6', '1', '1', null, '2018-04-05 15:36:58', '2018-04-05 15:36:58', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('7', '1', '1', null, '2018-04-05 15:37:45', '2018-04-05 15:37:45', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('8', '1', '1', null, '2018-04-05 15:38:40', '2018-04-05 15:38:47', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '900', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('9', '1', '1', null, '2018-04-05 15:41:21', '2018-04-05 15:41:21', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('10', '1', '1', null, '2018-04-05 15:42:35', '2018-04-05 15:42:35', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');
INSERT INTO `tbl_payment_temporary` VALUES ('11', '1', '1', null, '2018-04-05 15:52:17', '2018-04-05 15:52:17', null, '2', '2017-07-16', '2074-04-01', '1', 'Nepal SBI Bank Ltd-Teku', '1', '2', '100000', '1');

-- ----------------------------
-- Table structure for tbl_sales
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sales`;
CREATE TABLE `tbl_sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_date_np` varchar(255) DEFAULT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `truck_number` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `gross_total` double DEFAULT NULL,
  `is_published` int(11) DEFAULT '0',
  `agent_name` varchar(255) DEFAULT NULL,
  `batch_no` int(11) DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sales
-- ----------------------------
INSERT INTO `tbl_sales` VALUES ('1', '1', '1', '1', '2018-04-05 15:17:54', '2018-04-05 15:17:54', '2018-04-05 15:19:15', 'INVOICE NO.17', '2017-07-26', '2074-04-11', '1', '14', '13', 'Na4kh8332', '2', '3', '10', '577.37', '20', '5773.7', '0', 'Sanish Maharjan', null, '2');
INSERT INTO `tbl_sales` VALUES ('2', '1', '1', null, '2018-04-05 15:18:41', '2018-04-05 15:18:59', null, 'INVOICE NO.18', '2017-07-29', '2074-04-14', '1', '14', '13', 'Na4kh8332', '2', '3', '100', '1577.37', '20', '157737', '1', 'Sanish Maharjan', null, '2');
INSERT INTO `tbl_sales` VALUES ('3', '1', '1', '1', '2018-04-05 15:53:29', '2018-04-05 15:53:29', '2018-04-05 15:53:44', 'INVOICE NO.18', '2017-07-29', '2074-04-14', '1', '14', '13', 'Na4kh8332', '2', '3', '330', '1577.37', '20', '520532.1', '0', 'Sanish Maharjan', null, '2');

-- ----------------------------
-- Table structure for tbl_sales_temporary
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sales_temporary`;
CREATE TABLE `tbl_sales_temporary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fiscal_year_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_date_np` varchar(255) NOT NULL,
  `nepali_month_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `truck_number` varchar(255) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `gross_total` double DEFAULT NULL,
  `is_processed` int(11) DEFAULT '0',
  `agent_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sales_temporary
-- ----------------------------
INSERT INTO `tbl_sales_temporary` VALUES ('1', '1', '1', null, '2018-04-05 15:17:41', '2018-04-05 15:17:50', null, '2', 'INVOICE NO.17', '2017-07-26', '2074-04-11', '1', '14', '13', 'Na4kh8332', '2', '3', '10', '577.37', '20', '190532.1', '1', 'Sanish Maharjan');
INSERT INTO `tbl_sales_temporary` VALUES ('2', '1', '1', null, '2018-04-05 15:18:29', '2018-04-05 15:18:38', null, '2', 'INVOICE NO.18', '2017-07-29', '2074-04-14', '1', '14', '13', 'Na4kh8332', '2', '3', '100', '1577.37', '20', '190532.1', '1', 'Sanish Maharjan');
INSERT INTO `tbl_sales_temporary` VALUES ('3', '1', '1', null, '2018-04-05 15:53:24', '2018-04-05 15:53:24', null, '2', 'INVOICE NO.18', '2017-07-29', '2074-04-14', '1', '14', '13', 'Na4kh8332', '2', '3', '330', '1577.37', '20', '190532.1', '1', 'Sanish Maharjan');

-- ----------------------------
-- View structure for view_agents
-- ----------------------------
DROP VIEW IF EXISTS `view_agents`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_agents` AS SELECT
mst_agents.id,
mst_agents.created_by,
mst_agents.updated_by,
mst_agents.deleted_by,
mst_agents.created_at,
mst_agents.updated_at,
mst_agents.deleted_at,
mst_agents.`name`,
mst_agents.district_id,
mst_agents.mun_vdc_id,
mst_agents.city,
mst_agents.address,
mst_agents.phone,
mst_agents.mobile,
mst_agents.email,
mst_agents.province_id,
prov_mun.`name` AS province_name,
dist_mun.`name` AS district_name,
vdc_mun.`name` AS mun_name
FROM
mst_agents
INNER JOIN mst_munvdc AS prov_mun ON mst_agents.province_id = prov_mun.id
INNER JOIN mst_munvdc AS dist_mun ON mst_agents.district_id = dist_mun.id
INNER JOIN mst_munvdc AS vdc_mun ON mst_agents.mun_vdc_id = vdc_mun.id ;

-- ----------------------------
-- View structure for view_group_permissions
-- ----------------------------
DROP VIEW IF EXISTS `view_group_permissions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_group_permissions` AS (
	SELECT
		gp.group_id AS group_id,
		gp.perm_id AS perm_id,
		perm.name AS permission,
		grp.name AS group_name
	FROM
		aauth_group_permissions gp
	INNER JOIN aauth_groups grp ON (grp.id = gp.group_id)
	INNER JOIN aauth_permissions perm ON (perm.id = gp.perm_id)			
) ;

-- ----------------------------
-- View structure for view_payment
-- ----------------------------
DROP VIEW IF EXISTS `view_payment`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_payment` AS SELECT
tbl_payment.id,
tbl_payment.created_by,
tbl_payment.updated_by,
tbl_payment.deleted_by,
tbl_payment.created_at,
tbl_payment.updated_at,
tbl_payment.deleted_at,
tbl_payment.payment_date,
tbl_payment.payment_date_np,
tbl_payment.nepali_month_id,
tbl_payment.bank_name,
tbl_payment.agent_id,
tbl_payment.party_id,
tbl_payment.amount,
tbl_payment.is_published,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS agent_name,
mst_parties.`name` AS party_name
FROM
tbl_payment
INNER JOIN mst_nepali_month ON tbl_payment.nepali_month_id = mst_nepali_month.id
INNER JOIN mst_agents ON tbl_payment.agent_id = mst_agents.id
INNER JOIN mst_parties ON tbl_payment.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_payment_temporary
-- ----------------------------
DROP VIEW IF EXISTS `view_payment_temporary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_payment_temporary` AS SELECT
tbl_payment_temporary.id,
tbl_payment_temporary.created_by,
tbl_payment_temporary.updated_by,
tbl_payment_temporary.deleted_by,
tbl_payment_temporary.created_at,
tbl_payment_temporary.updated_at,
tbl_payment_temporary.deleted_at,
tbl_payment_temporary.payment_date,
tbl_payment_temporary.payment_date_np,
tbl_payment_temporary.nepali_month_id,
tbl_payment_temporary.bank_name,
tbl_payment_temporary.agent_id,
tbl_payment_temporary.party_id,
tbl_payment_temporary.amount,
tbl_payment_temporary.is_processed,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS agent_name,
mst_parties.`name` AS party_name,
tbl_payment_temporary.fiscal_year_id
FROM
tbl_payment_temporary
INNER JOIN mst_nepali_month ON tbl_payment_temporary.nepali_month_id = mst_nepali_month.id
INNER JOIN mst_agents ON tbl_payment_temporary.agent_id = mst_agents.id
INNER JOIN mst_parties ON tbl_payment_temporary.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_sales
-- ----------------------------
DROP VIEW IF EXISTS `view_sales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_sales` AS SELECT
tbl_sales.id,
tbl_sales.created_by,
tbl_sales.updated_by,
tbl_sales.deleted_by,
tbl_sales.created_at,
tbl_sales.updated_at,
tbl_sales.deleted_at,
tbl_sales.invoice_no,
tbl_sales.invoice_date,
tbl_sales.invoice_date_np,
tbl_sales.nepali_month_id,
tbl_sales.agent_id,
tbl_sales.party_id,
tbl_sales.truck_number,
tbl_sales.type_id,
tbl_sales.brand_id,
tbl_sales.quantity,
tbl_sales.rate,
tbl_sales.discount,
tbl_sales.gross_total,
tbl_sales.is_published,
tbl_sales.agent_name,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS actual_agent_name,
mst_parties.`name` AS party_name,
mst_brand_types.`name` AS brand_type,
mst_brands.`name` AS brand_name
FROM
tbl_sales
LEFT JOIN mst_agents ON tbl_sales.agent_id = mst_agents.id
INNER JOIN mst_brand_types ON tbl_sales.type_id = mst_brand_types.id
INNER JOIN mst_brands ON tbl_sales.brand_id = mst_brands.id
INNER JOIN mst_nepali_month ON tbl_sales.nepali_month_id = mst_nepali_month.id
LEFT JOIN mst_parties ON tbl_sales.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_sales_temporary
-- ----------------------------
DROP VIEW IF EXISTS `view_sales_temporary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_sales_temporary` AS SELECT
tbl_sales_temporary.id,
tbl_sales_temporary.created_by,
tbl_sales_temporary.updated_by,
tbl_sales_temporary.deleted_by,
tbl_sales_temporary.created_at,
tbl_sales_temporary.updated_at,
tbl_sales_temporary.deleted_at,
tbl_sales_temporary.invoice_no,
tbl_sales_temporary.invoice_date,
tbl_sales_temporary.invoice_date_np,
tbl_sales_temporary.nepali_month_id,
tbl_sales_temporary.agent_id,
tbl_sales_temporary.party_id,
tbl_sales_temporary.truck_number,
tbl_sales_temporary.type_id,
tbl_sales_temporary.brand_id,
tbl_sales_temporary.quantity,
tbl_sales_temporary.rate,
tbl_sales_temporary.discount,
tbl_sales_temporary.gross_total,
tbl_sales_temporary.is_processed,
tbl_sales_temporary.agent_name,
mst_nepali_month.`name` AS nepali_month,
mst_agents.`name` AS actual_agent_name,
mst_parties.`name` AS party_name,
mst_brand_types.`name` AS brand_type,
mst_brands.`name` AS brand_name,
tbl_sales_temporary.fiscal_year_id
FROM
tbl_sales_temporary
LEFT JOIN mst_agents ON tbl_sales_temporary.agent_id = mst_agents.id
INNER JOIN mst_brand_types ON tbl_sales_temporary.type_id = mst_brand_types.id
INNER JOIN mst_brands ON tbl_sales_temporary.brand_id = mst_brands.id
INNER JOIN mst_nepali_month ON tbl_sales_temporary.nepali_month_id = mst_nepali_month.id
LEFT JOIN mst_parties ON tbl_sales_temporary.party_id = mst_parties.id ;

-- ----------------------------
-- View structure for view_schemes
-- ----------------------------
DROP VIEW IF EXISTS `view_schemes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_schemes` AS SELECT
sch.id,
sch.created_by,
sch.updated_by,
sch.deleted_by,
sch.created_at,
sch.updated_at,
sch.deleted_at,
sch.fiscal_year_id,
sch.agent_id,
sch.amount,
age.`name` AS agent_name,
CONCAT(SUBSTR(mfy.nepali_start_date,1,5),SUBSTR(mfy.nepali_end_date,3,2)) AS fiscal_year
FROM
mst_schemes AS sch
INNER JOIN mst_agents AS age ON sch.agent_id = age.id
INNER JOIN mst_fiscal_years AS mfy ON sch.fiscal_year_id = mfy.id ;

-- ----------------------------
-- View structure for view_users
-- ----------------------------
DROP VIEW IF EXISTS `view_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_users` AS (
	SELECT
		u.id AS id, 
		u.email AS email, 
		u.pass AS pass, 
		u.username AS username, 
		u.fullname as fullname,
		u.banned AS banned, 
		u.last_login AS last_login, 
		u.last_activity AS last_activity, 
		u.date_created AS date_created, 
		u.forgot_exp AS forgot_exp, 
		u.remember_time AS remember_time, 
		u.remember_exp AS remember_exp, 
		u.verification_code AS verification_code, 
		u.totp_secret AS totp_secret, 
		u.ip_address AS ip_address
	FROM
		aauth_users u
) ;

-- ----------------------------
-- View structure for view_user_groups
-- ----------------------------
DROP VIEW IF EXISTS `view_user_groups`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_user_groups` AS (
	SELECT 
		ug.user_id AS user_id,
	    ug.group_id AS group_id,
	    g.name AS group_name,
	    u.username as username,
	    u.email as email,
	    u.fullname as fullname
   	FROM 
   		aauth_user_groups ug
    INNER JOIN aauth_users u ON (u.id = ug.user_id)
    INNER JOIN aauth_groups g ON (g.id = ug.group_id)
) ;

-- ----------------------------
-- View structure for view_user_permissions
-- ----------------------------
DROP VIEW IF EXISTS `view_user_permissions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_user_permissions` AS (
	SELECT
		up.user_id AS user_id,
		up.perm_id AS perm_id,
		perm.name AS permission,
		u.username AS username,
		u.email AS email,
		u.fullname as fullname
	FROM
		aauth_user_permissions up
	INNER JOIN aauth_users u ON (u.id = up.user_id)
	INNER JOIN aauth_permissions perm ON (perm.id = up.perm_id)
) ;
