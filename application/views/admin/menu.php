<?php $uri = explode("/", $this->uri->uri_string()); ?>
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <ul class="nav side-menu">
        <?php $css = (!isset($uri[1])) ? 'class="active"' : ''; ?> 
        <li <?php echo $css;?>>
            <a href="<?php echo site_url('admin'); ?>">
                <i class="fa fa-home"></i> <span><?php echo lang('menu_dashboard'); ?></span>
            </a>
        </li>

        <?php if(control('System', FALSE)):?>
            <?php $css = (isset($uri[1]) && in_array($uri[1], array('users', 'groups', 'permissions'))) ? 'active' : ''; ?>
            <li class="<?php echo $css; ?>">
                <a><i class="fa fa-edit"></i> <?php echo lang('menu_system'); ?> <span class="fa fa-chevron-down"></span></a>
                <!-- <a href="javascript:void(0)">
                    <i class="fa fa-gear"></i>
                    <span><?php echo lang('menu_system'); ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a> -->
                <ul class="nav child_menu">
                    <?php if(control('Permissions', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'permissions') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Permissions')?>"><?php echo lang('menu_permissions');?></a></li>
                    <?php endif;?>
                    <?php if(control('Groups', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'groups') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Groups')?>"><?php echo lang('menu_groups');?></a></li>
                    <?php endif;?>
                    <?php if(control('Users', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'users') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Users')?>"><?php echo lang('menu_users');?></a></li>
                    <?php endif;?>
                </ul>
            </li>
        <?php endif;?>
        <?php if(control('Master', FALSE)):?>
            <?php $css = (isset($uri[1]) && in_array($uri[1], array('fiscal_years','brands', 'brand_types','agents','parties','nepali_months','schemes','monthly_targets'))) ? 'active' : ''; ?>
            <li class="<?php echo $css; ?>">
                <a><i class="fa fa-hdd-o"></i> <?php echo lang('menu_master'); ?> <span class="fa fa-chevron-down"></span></a>
                <!-- <a href="javascript:void(0)">
                    <i class="fa fa-gear"></i>
                    <span><?php echo lang('menu_system'); ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a> -->
                <ul class="nav child_menu">
                     <?php if(control('Fiscal Year', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'fiscal_years') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Fiscal_years')?>"><?php echo lang('menu_fiscal_years');?></a></li>
                    <?php endif;?>
                    <?php if(control('Brand Types', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'brand_types') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Brand_types')?>"><?php echo lang('menu_brand_types');?></a></li>
                    <?php endif;?>
                    <?php if(control('Brands', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'brands') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Brands')?>"><?php echo lang('menu_brands');?></a></li>
                    <?php endif;?>
                    <?php if(control('Agents', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'agents') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Agents')?>"><?php echo lang('menu_agents');?></a></li>
                    <?php endif;?>
                    <?php if(control('Parties', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'parties') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Parties')?>"><?php echo lang('menu_parties');?></a></li>
                    <?php endif;?>
                    <?php if(control('Nepali Months', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'nepali_months') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Nepali_months')?>"><?php echo lang('menu_nepali_months');?></a></li>
                    <?php endif;?>
                    <?php if(control('Monthly Targets', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'monthly_targets') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Monthly_targets')?>"><?php echo lang('menu_monthly_targets');?></a></li>
                    <?php endif;?>
                    <?php if(control('Schemes', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'schemes') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Schemes')?>"><?php echo lang('menu_schemes');?></a></li>
                    <?php endif;?>
                </ul>
            </li>
        <?php endif;?>
        <?php if(control('Sales', FALSE)):?>
            <?php $css = (isset($uri[1]) && in_array($uri[1], array('sales','payments'))) ? 'active' : ''; ?>
            <li class="<?php echo $css; ?>">
                <a><i class="fa fa-hdd-o"></i> <?php echo lang('menu_transactions'); ?> <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <?php if(control('Sales', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'sales') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Sales')?>"><?php echo lang('menu_sales');?></a></li>
                    <?php endif;?>
                    <?php if(control('Payments', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'payments') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/Payments')?>"><?php echo lang('menu_payments');?></a></li>
                    <?php endif;?>
                </ul>
            </li>
        <?php endif;?> 
    </ul>
</div>

</div>
<!-- /sidebar menu -->