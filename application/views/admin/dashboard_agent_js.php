<script type="text/javascript">
	$(function(){

		var dealer_salesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'party_name', type: 'string' },
			{ name: 'nepali_month', type: 'string' },
			{ name: 'total_sales', type: 'number' },
			{ name: 'total_payment', type: 'number' },
			{ name: 'difference', type: 'number' },
			{ name: 'opc_qty', type: 'number' },
			{ name: 'ppc_qty', type: 'number' },
			{ name: 'total_qty', type: 'number' },
			],
			url: '<?php echo site_url("admin/Sales/dashboard_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
		};
		
		$("#jqxGrid_dashboard").jqxGrid({
			width: '100%',
			height: gridHeight,
			source: dealer_salesDataSource,
			sortable: true,
			filterable: true,
			filtermode: 'excel',
			columnsresize: true,
			autoshowfiltericon: false,
			showstatusbar: true,
			showaggregates: true,
			rendertoolbar: function (toolbar) {
				var container = $("<div style='margin: 5px; height:50px'></div>");
				container.append($('#jqxGridDealer_saleToolbar').html());
				toolbar.append(container);
			},
			columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{ text: '<?php echo "Party Name" ?>',datafield: 'party_name',width: 300,filterable: false,renderer: gridColumnsRenderer },
			{ text: '<?php echo "Month" ?>',datafield: 'nepali_month',width: 140 },
			{ text: '<?php echo "Total Sales" ?>',datafield: 'total_sales',width: 230,filterable: false,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
			function (row, columnfield, value, defaulthtml, columnproperties) {
				return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
			},
			aggregatesrenderer: function (aggregates, column, element) {
				var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
				return renderstring;
			} },
			{ text: '<?php echo "Total Payment" ?>',datafield: 'total_payment',width: 230,filterable: false,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
			function (row, columnfield, value, defaulthtml, columnproperties) {
				return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
			},
			aggregatesrenderer: function (aggregates, column, element) {
				var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
				return renderstring;
			} },
			{ text: '<?php echo "Difference" ?>',datafield: 'difference',width: 230,filterable: false,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
			function (row, columnfield, value, defaulthtml, columnproperties) {
				return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
			},
			aggregatesrenderer: function (aggregates, column, element) {
				var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
				return renderstring;
			} },

			{ text: '<?php echo "OPC" ?>',datafield: 'opc_qty',width: 230,filterable: false,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
			function (row, columnfield, value, defaulthtml, columnproperties) {
				return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
			},
			aggregatesrenderer: function (aggregates, column, element) {
				var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
				return renderstring;
			} },
			

			{ text: '<?php echo "PPC" ?>',datafield: 'ppc_qty',width: 230,filterable: false,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
			function (row, columnfield, value, defaulthtml, columnproperties) {
				return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
			},
			aggregatesrenderer: function (aggregates, column, element) {
				var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
				return renderstring;
			} },

			{ text: '<?php echo "Total" ?>',datafield: 'total_qty',width: 230,filterable: false,cellsalign:'right', renderer: gridColumnsRenderer,aggregates: ["sum"],cellsrenderer: 
			function (row, columnfield, value, defaulthtml, columnproperties) {
				return '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + (value?value.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
			},
			aggregatesrenderer: function (aggregates, column, element) {
				var renderstring = '<div style="position: relative; margin-top: 4px; margin-right:5px; text-align: right; overflow: hidden;">' + "Total" + ': ' + (aggregates.sum?aggregates.sum.toLocaleString('en-US', { maximumFractionDigits: 2, minimunFractionDigits:2 }):0) + '</div>';
				return renderstring;
			} },
			],
			rendergridrows: function (result) {
				return result.data;
			}
		});

		$("[data-toggle='offcanvas']").click(function(e) {
			e.preventDefault();
			setTimeout(function() {$("#jqxGrid_dashboard").jqxGrid('refresh');}, 500);
		});

		$(document).on('click','#jqxGridDealer_saleFilterClear', function () { 
			$('#jqxGrid_dashboard').jqxGrid('clearfilters');
		});

	});
</script>