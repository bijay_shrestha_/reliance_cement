<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3>OVERVIEW REPORT</h3>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php print displayStatus();?>
		<!-- top tiles -->
		<div class="row tile_count">
			<div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
				<span class="count_top"><i class="fa fa-money"></i> Opening Balance</span>
				<div class="count blue"><?php echo number_format($overview->opening_amount,2); ?></div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
				<span class="count_top"><i class="fa fa-money"></i> Total Sales</span>
				<div class="count blue"><?php echo number_format($overview->total_sales,2); ?></div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
				<span class="count_top"><i class="fa fa-money"></i> Total Payment</span>
				<div class="count blue"><?php echo number_format($overview->total_payment,2); ?></div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
				<span class="count_top"><i class="fa fa-money"></i> Total Scheme</span>
				<div class="count blue"><?php echo number_format($overview->scheme_amount,2); ?></div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
				<span class="count_top"><i class="fa fa-money"></i> Remaining Balace</span>
				<div class="count blue"><?php echo preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format($overview->remaining_amount,2,'.',',') ); ?></div>
			</div>
		</div>
	</section>
	<section class="content-header">
		<h3>MONTHLY SALES REPORT</h3>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row top_tiles">
			<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<!-- <div class="icon"><i class="fa fa-caret-square-o-right"></i></div> -->
					<div class="count <?php echo (monthly_summary_report('OPC','quantity') > previous_month_sales('OPC')?'green':'red'); ?>">OPC</div>
					<h3><?php echo number_format(monthly_summary_report('OPC','quantity'));?></h3>
				</div>
			</div>
			<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<!-- <div class="icon"><i class="fa fa-comments-o"></i></div> -->
					<div class="count <?php echo (monthly_summary_report('PSC','quantity') > previous_month_sales('PSC')?'green':'red'); ?>">PSC</div>
					<h3><?php echo number_format(monthly_summary_report('PSC', 'quantity'));?></h3>
				</div>
			</div>
			<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<!-- <div class="icon"><i class="fa fa-sort-amount-desc"></i></div> -->
					<div class="count  <?php echo (monthly_summary_report('all','quantity') > previous_month_sales()?'green':'red'); ?>">TOTAL</div>
					<h3><?php echo number_format(monthly_summary_report('all','quantity'));?></h3>
				</div>
			</div>
			<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<!-- <div class="icon"><i class="fa fa-check-square-o"></i></div> -->
					<div class="count">TARGET</div>
					<h3><?php echo (monthly_target() ? number_format(monthly_target()):'Target Not Set') ; ?></h3>
				</div>
			</div>
			<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<!-- <div class="icon"><i class="fa fa-check-square-o"></i></div> -->
					<div class="count <?php echo (((monthly_target() ? monthly_target(): 0) - monthly_summary_report()) > 0 ?'red':'green') ; ?>">DIFFERENCE</div>
					<h3><?php echo preg_replace('/(-)([\d\.\,]+)/ui', '($2)', number_format(((monthly_target() ? monthly_target(): 0) - (monthly_summary_report('all','quantity'))),0,'.',',') ); ?></h3>
				</div>
			</div>
		</div>
	</section>
	<section class="content-header">
		<h3>PARTYWISE SALES REPORT</h3>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row top_tiles">
			<div class="col-xs-12 connectedSortable">
				<div id="jqxGrid_dashboard"></div>
			</div><!-- /.col --> 
		</div>
	</section>
</div>


<?php echo $this->load->view($this->config->item('template_admin') .'dashboard_agent_js.php');?>
