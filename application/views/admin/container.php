<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $header . ' | ' . config_item('site_name');?></title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/iCheck/skins/flat/green.css" rel="stylesheet">
    
    <!-- bootstrap-progressbar -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/build/custom.min.css" rel="stylesheet">

    <link href="<?php echo site_url("assets/css/ionicons/css/ionicons.min.css")?>" rel="stylesheet">
    <link href="<?php echo site_url("assets/css/jqwidgets/jqx.base.css")?>" rel="stylesheet">
    <link href="<?php echo site_url("assets/css/jqwidgets/jqx.base.extended.css")?>" rel="stylesheet">
    <link href="<?php echo site_url("assets/css/jqwidgets/jqx.bootstrap.css")?>" rel="stylesheet">
    <link href="<?php echo site_url("assets/css/jqwidgets/jqx.office.css")?>" rel="stylesheet">
    <link href="<?php echo site_url("assets/css/partial-project.css")?>" rel="stylesheet">


    <script type="text/javascript">
        <!--
        var base_url = '<?php echo base_url();?>';
        var index_page = "";
        // -->
    </script>

    <!-- jQuery -->
    <script src="<?php echo site_url()?>assets/js/gentelella/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo site_url()?>assets/js/gentelella/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?php echo site_url()?>assets/js/jquery.cookie.js"></script>
    <script src="<?php echo site_url()?>assets/js/date.js"></script>
    <script src="<?php echo site_url()?>assets/js/jqwidgets/jqx-all.js"></script>
    <script src="<?php echo site_url()?>assets/js/jqwidgets/globalization/globalize.js"></script>
    <script src="<?php echo site_url()?>assets/js/jquery.blockUI.js"></script>
    <!-- partial project -->
    <script src="<?php echo site_url()?>assets/js/partial-project.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <!-- menu profile quick info -->
            <?php print $this->load->view($this->config->item('template_admin') . 'header');?>
            

            <br />

            <!-- sidebar menu -->
            <?php print $this->load->view($this->config->item('template_admin') . 'menu');?>
            
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a> -->
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url('logout')?>">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo site_url('assets/images/avatar5.png');?>" alt=""><?php echo $this->session->userdata('username')?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo site_url('admin/account/profile'); ?>"> Profile</a></li>
                    <!-- <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li> -->
                    <li><a href="<?php echo site_url('admin/account/change_password'); ?>">Change Password</a></li>
                    <li><a href="<?php echo site_url('logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <!-- top tiles -->
            <?php print $this->load->view($this->config->item('template_admin') . 'content');?>
            
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <?php print $this->load->view($this->config->item('template_admin') . 'footer');?>
          
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- FastClick -->    
    <script src="<?php echo site_url()?>assets/js/gentelella/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo site_url()?>assets/js/gentelella/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo site_url()?>assets/js/gentelella/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo site_url()?>assets/js/gentelella/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo site_url()?>assets/js/gentelella/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo site_url()?>assets/js/gentelella/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo site_url()?>assets/js/gentelella/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo site_url()?>assets/js/gentelella/Flot/jquery.flot.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo site_url()?>assets/js/gentelella/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo site_url()?>assets/js/gentelella/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo site_url()?>assets/js/gentelella/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo site_url()?>assets/js/gentelella/moment/min/moment.min.js"></script>
    <script src="<?php echo site_url()?>assets/js/gentelella/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo site_url()?>assets/js/gentelella/build/custom.min.js"></script>
    
  </body>
</html>
