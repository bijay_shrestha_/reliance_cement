<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Home
 *
 * Extends the MX_Controller class
 * 
 */
class Home extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('dashboard');
	}

	public function index()
	{
		if(is_agent())
		{
			$viewpage = 'dashboard_agent';
			
			$this->db->where('user_id',$this->session->userdata('id'));
			$agent = $this->db->get('mst_agents')->row();

			$this->db->where('id',$agent->id);
			$data['overview'] = $this->db->get('view_report_overview')->row();
		}
		else
		{
			$viewpage = 'home';
		}

		// Display Page
		$data['header'] = lang('menu_home');

		$data['page'] = $this->config->item('template_admin') . $viewpage;
		$this->load->view($this->_container, $data);
	}
}
