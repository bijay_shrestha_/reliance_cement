<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


if ( ! function_exists('monthly_summary_report'))
{
	function monthly_summary_report($brand_type = 'all', $requested_value = 'gross_total')
	{
		$CI = &get_instance();

        $CI->db->where('user_id',$CI->session->userdata('id'));
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_agent = NULL;

        $is_agent = (is_agent()) ? TRUE : NULL; 

        if ($is_agent) {
            $whereCondition[] = " ( agent_id = " . $agent_id. ")";
        }

        // ACCESS LEVEL CHECK ENDS

        if ($brand_type != 'all')
        {
        	$whereCondition[] = "brand_type = '{$brand_type}' ";	
        }

        $current_date = explode('-',get_nepali_date(date('Y-m-d'),'nep'));

        $whereCondition[] = " ( nepali_month_id = " . $current_date[1]. " AND is_published = 1)";

        $fields = array();

        $fields[] = "COALESCE(SUM($requested_value),0) AS total";

        $CI->db->select($fields);

        $CI->db->from('view_sales');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
    }
}

if ( ! function_exists('previous_month_sales'))
{
    function previous_month_sales($brand_type = 'all')
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$CI->session->userdata('id'));
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_agent = NULL;

        $is_agent = (is_agent()) ? TRUE : NULL; 

        if ($is_agent) {
            $whereCondition[] = " ( agent_id = " . $agent_id. ")";
        }

        // ACCESS LEVEL CHECK ENDS

        if ($brand_type != 'all')
        {
            $whereCondition[] = "brand_type = '{$brand_type}' ";    
        }

        $current_date = explode('-',get_nepali_date(date('Y-m-d'),'nep'));

        if($current_date[1] == 1)
        {
            $previous_month = 12;   
        }
        else
        {
            $previous_month = $current_date[1] - 1;
        }


        $whereCondition[] = " ( nepali_month_id = " . $previous_month. " AND is_published = 1 )";

        $fields = array();

        $fields[] = 'COALESCE(SUM(gross_total),0) AS total';

        $CI->db->select($fields);

        $CI->db->from('view_sales');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['total'];
    }
}

if ( ! function_exists('monthly_target'))
{
    function monthly_target()
    {
        $CI = &get_instance();

        $CI->db->where('user_id',$CI->session->userdata('id'));
        $agent = $CI->db->get('mst_agents')->row();
        $agent_id = $agent->id;

        $whereCondition = array();

        // ACCESS LEVEL CHECK STARTS
        $is_agent = NULL;

        $is_agent = (is_agent()) ? TRUE : NULL; 

        if ($is_agent) {
            $whereCondition[] = " ( agent_id = " . $agent_id. ")";
        }

        // ACCESS LEVEL CHECK ENDS

        $current_date = explode('-',get_nepali_date(date('Y-m-d'),'nep'));

        $whereCondition[] = " ( nepali_month_id = " . $current_date[1]. ")";

        $fields = array();

        $fields[] = 'amount';

        $CI->db->select($fields);

        $CI->db->from('mst_monthly_target');
        
        if (count($whereCondition) > 0) {
            $CI->db->where(implode(" AND " , $whereCondition));
        }
        
        $result = $CI->db->get()->row_array();

        return $result['amount'];
    }
}


/* End of file dashboard_helper.php */
/* Location: ./application/helpers/dashboard_helper.php */