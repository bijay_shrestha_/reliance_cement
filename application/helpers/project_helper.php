<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


if ( ! function_exists('paging'))
{
	function paging($default, $order = 'desc')
	{
		$CI = &get_instance();

		$input = $CI->input->get();
		
		$pagenum  = (isset($input['pagenum'])) ? $input['pagenum'] : 0;
		
		$pagesize  = (isset($input['pagesize'])) ? $input['pagesize'] : 100;
		
		$offset = $pagenum * $pagesize;

		$CI->db->limit($pagesize, $offset);

		//sorting
		if (isset($input['sortdatafield'])) {
			$sortdatafield = $input['sortdatafield'];
			$sortorder = (isset($input['sortorder'])) ? $input['sortorder'] :'asc';
			$CI->db->order_by($sortdatafield, $sortorder); 
		} else {
			$CI->db->order_by($default, $order);
		}
		
	}
}

if ( ! function_exists('search_params'))
{
	function search_params()
	{
		$CI = &get_instance();

		$input = $CI->input->get();

		if (isset($input['filterscount'])) {
			$filtersCount = $input['filterscount'];
			if ($filtersCount > 0) {
				for ($i=0; $i < $filtersCount; $i++) {
					// get the filter's column.
					$filterDatafield 	= $input['filterdatafield' . $i];

					// get the filter's value.
					$filterValue 		=  $input['filtervalue' 	. $i];

					// get the filter's condition.
					$filterCondition 	= $input['filtercondition' . $i];

					// get the filter's operator.
					$filterOperator 	= $input['filteroperator' 	. $i];

					$operatorLike = 'LIKE';

					switch($filterCondition) {
						case "CONTAINS":
						if (strtoupper($filterValue) == 'BLANK') {
                                // $CI->db->where("{$filterDatafield} IS EMPTY", null, false);
						} else if(strtoupper($filterValue) == 'NULL') {
							$CI->db->where("{$filterDatafield} IS NULL", null, false);
						} else {
							$CI->db->like($filterDatafield, $filterValue);
						}
						break;
						case "DOES_NOT_CONTAIN":
						$CI->db->inot_like($filterDatafield, $filterValue);
						break;
						case "EQUAL":
						$CI->db->where($filterDatafield, $filterValue);
						break;
						case "GREATER_THAN":
						$CI->db->where($filterDatafield . ' >', $filterValue);
						break;
						case "LESS_THAN":
						$CI->db->where($filterDatafield . ' <', $filterValue);
						break;
						case "GREATER_THAN_OR_EQUAL":
						$CI->db->where($filterDatafield . ' >=', $filterValue);
						break;
						case "LESS_THAN_OR_EQUAL":
						$CI->db->where($filterDatafield . ' <=', $filterValue);
						break;
						case "STARTS_WITH":
						$CI->db->like($filterDatafield, $filterValue, 'after'); 
						break;
						case "ENDS_WITH":
						$CI->db->like($filterDatafield, $filterValue, 'before'); 
						break;
					}
				}
			}
		}
	}

	if ( ! function_exists('get_english_date'))
	{
		function get_english_date($nepali_date = null, $retType = 'json')
		{
			$CI = &get_instance();

			$date = null;
			$success = false;

			if ($nepali_date == null) {
				echo json_encode(array('success' => $success, 'date' => $date));
				exit;
			}

			$CI->load->library('nepali_calendar');

			list($y,$m,$d) = explode('-', $nepali_date);

			$converted_date = $CI->nepali_calendar->BS_to_AD($y,$m,$d);

			$date = date('Y-m-d',mktime(0,0,0, $converted_date['month'], $converted_date['date'], $converted_date['year']));

			if ($retType == 'json') {
				$success = true;
				echo json_encode(array('success' => $success, 'date' => $date));
			} else {
				return $date;
			}
		}

	}

	if ( ! function_exists('get_nepali_date'))
	{
		function get_nepali_date($english_date = null, $retType = 'json')
		{
			$CI = &get_instance();

			$date = null;
			$success = false;

			if ($english_date == null) {
				echo json_encode(array('success' => $success, 'date' => $date));
				exit;
			}

			$CI->load->library('nepali_calendar');

			list($y,$m,$d) = explode('-', $english_date);

			$converted_date = $CI->nepali_calendar->AD_to_BS($y,$m,$d,'array');

			$date = $converted_date['year'] . '-' . sprintf("%02d", $converted_date['month'] ) . '-' . sprintf("%02d", $converted_date['date'] );

			if ($retType == 'json') {
				$success = true;
				echo json_encode(array('success' => $success, 'date' => $date));
			} else {
				return $date;
			}

		}

	}
	if ( ! function_exists('is_agent'))
	{
		function is_agent()
		{
			$CI = &get_instance();
			$CI->load->model('users/user_group_model');

			$CI->db->where('user_id',  $CI->session->userdata('id'));
			$CI->db->where('group_id', AGENT_GROUP_ID);

			return ($CI->user_group_model->find_count() == 1) ? TRUE : FALSE;
		}
	}

	if ( ! function_exists('is_sales_person'))
	{
		function is_sales_person()
		{
			$CI = &get_instance();
			$CI->load->model('users/user_group_model');

			$CI->db->where('user_id',  $CI->session->userdata('id'));
			$CI->db->where('group_id', SALES_PERSON_GROUP_ID);

			return ($CI->user_group_model->find_count() == 1) ? TRUE : FALSE;
		}
	}


	if ( ! function_exists('get_current_calendar_year'))
	{
		function get_current_calendar_year()
		{
			$CI = &get_instance();

			$CI->load->model('calendar_years/calendar_year_model');
			$id= 0;
			$row=$CI->calendar_year_model->get_by('active', TRUE);

			if($row){
				$id = $row->id;
			}

			return $id;
		}
	}

	
	if ( ! function_exists('get_current_fiscal_year'))
	{
		function get_current_fiscal_year()
		{
			$id 		 = null;
			$fiscal_year = null;

			$CI = &get_instance();
			$CI->load->model('fiscal_years/fiscal_year_model');
			$CI->db->where('CURDATE() >= english_start_date');
			$CI->db->where('CURDATE() <= english_end_date');

			$fields = array();
			$fields[] = 'id';
			$fields[] = "CONCAT(SUBSTR(nepali_start_date,1,5),SUBSTR(nepali_end_date,3,2)) as fiscal_year";
			$fields[] = 'english_start_date';
			$fields[] = 'english_end_date';

			$record = $CI->fiscal_year_model->findAll(null, $fields);

			if ($record){
				$id	= $record[0]->id;
				$fiscal_year = $record[0]->fiscal_year;	
				$english_start_date = $record[0]->english_start_date;	
				$english_end_date = $record[0]->english_end_date;	
			}
			return array($id, $fiscal_year,$english_start_date,$english_end_date);
		}
	}

	if (! function_exists('moneyFormat'))
	{
		function moneyFormat($num){

			$explrestunits = "" ;
			$num=preg_replace('/,+/', '', $num);
			$words = explode(".", $num);
			$des="00";
			if(count($words)<=2){
				$num=$words[0];
				if(count($words)>=2){$des=$words[1];}
				if(strlen($des)<2){$des="$des0";}else{$des=substr($des,0,2);}
			}
			if(strlen($num)>3){
				$lastthree = substr($num, strlen($num)-3, strlen($num));
				$restunits = substr($num, 0, strlen($num)-3);  //extracts the last three digits
				$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits;  //explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
				$expunit = str_split($restunits, 2);
				for($i=0; $i<sizeof($expunit); $i++){
					// creates each of the 2's group and adds a comma to the end
					if($i==0)
					{
						$explrestunits .= (int)$expunit[$i].",";  //if is first value , convert into integer
					}else{
						$explrestunits .= $expunit[$i].",";
					}
				}
				$thecash = $explrestunits.$lastthree;
			} else {
				$thecash = $num;
			}

			/*if($des == '00')
			{
				return "$thecash";
			}*/
			return "$thecash.$des";  //writes the final format where $currency is the currency symbol.
		}
	}
}

/* End of file project_helper.php */
/* Location: ./application/helpers/project_helper.php */