<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * PACKAGE DESCRIPTION
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

$lang['menu_home']		 		= 'Home';

$lang['menu_dashboard'] 		= 'Dashboard';
$lang['menu_system'] 			= 'System ';
$lang['menu_users'] 			= 'Users';
$lang['menu_groups'] 			= 'Groups';
$lang['menu_permissions'] 		= 'Permissions';
$lang['menu_group_permissions'] = 'Group Permissions';
$lang['menu_user_permissions'] 	= 'User Permissions';

$lang['menu_master'] 			= 'Masters';
$lang['menu_brand_types'] 		= 'Brand Types';
$lang['menu_brands'] 			= 'Brands';
$lang['menu_agents'] 			= 'Agents';
$lang['menu_parties'] 			= 'Parties';
$lang['menu_nepali_months'] 	= 'Nepali Months';
$lang['menu_sales'] 			= 'Sales';
$lang['menu_payments'] 			= 'Payments';
$lang['menu_fiscal_years'] 			= 'Fiscal Year';
$lang['menu_schemes'] 			= 'Schemes';
$lang['menu_transactions'] 			= 'Transactions';
$lang['menu_monthly_targets'] 			= 'Monthly Target';