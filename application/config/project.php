<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| APP CONFIG
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'RELIANCE';

// Public Template
$config['template_public'] = "public/";

// Admin Template
$config['template_admin'] = "admin/";

// Default Login
$config['default_login_action'] = 'home';

// Admin Login Actin
$config['admin_login_action'] = 'admin';

// Default Logout Action
$config['default_logout_action'] = '';


/*
|--------------------------------------------------------------------------
| APP CONSTANTS
|--------------------------------------------------------------------------
*/
defined('DEFAULT_PASSWORD') OR define('DEFAULT_PASSWORD', 'password');

defined('CACHE_PATH') OR define('CACHE_PATH', APPPATH .'cache'. DIRECTORY_SEPARATOR);

defined('AGENT_GROUP_ID') OR define('AGENT_GROUP_ID', 1500);

defined('SALES_PERSON_GROUP_ID') OR define('SALES_PERSON_GROUP_ID', 1000);

defined('SCHEME_TYPE_BAG') OR define('SCHEME_TYPE_BAG', 'Bag Number');
defined('SCHEME_TYPE_LUMP_SUM') OR define('SCHEME_TYPE_LUMP_SUM', 'Lump Sum');

