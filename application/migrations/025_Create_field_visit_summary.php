<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_field_visit_summary
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_field_visit_summary extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('field_visit_summary'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'agent_id'              => array('type' => 'int',           'constraint' => 11),
                'date_en'               => array('type' => 'date'),               
                'date_np'               => array('type' => 'varchar',       'constraint' => 256),               
                'latitude'              => array('type' => 'varchar',       'constraint' => 256),               
                'longitude'             => array('type' => 'varchar',       'constraint' => 256),               
                'city'                  => array('type' => 'varchar',       'constraint' => 256),               
                'province'              => array('type' => 'varchar',       'constraint' => 256),               
                'nepali_month_id'       => array('type' => 'int',           'constraint' => 11),
                'summary'               => array('type' => 'text'),
                'opc_price'             => array('type' => 'float',         'constraint' => 44),
                'ppc_price'             => array('type' => 'float',         'constraint' => 44),
             ));

            $this->dbforge->create_table('field_visit_summary', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('field_visit_summary');
    }
}