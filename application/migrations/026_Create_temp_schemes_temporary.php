<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_temp_schemes_temporary
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_temp_schemes_temporary extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('temp_schemes_temporary'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'fiscal_year_id'        => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'nepali_moth_id'        => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'agent_id'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'bag_count'             => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'price'                 => array('type' => 'float',         'constraint' => 32,     'null' => TRUE),
                'amount'                => array('type' => 'float',         'constraint' => 32,     'null' => TRUE),
                'scheme_type'           => array('type' => 'varchar',       'constraint' => 255,    'null' => TRUE),
                'is_processed'          => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,   'default'=> 0),
            ));

            $this->dbforge->create_table('temp_schemes_temporary', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('temp_schemes_temporary');
    }
}