<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_tbl_sales_temporary
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_tbl_sales_temporary extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('tbl_sales_temporary'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'fiscal_year_id'        => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'invoice_no'            => array('type' => 'varchar',       'constraint' => 255),
                'invoice_date'          => array('type' => 'date',          'null' => TRUE),
                'invoice_date_np'       => array('type' => 'varchar',       'constraint' => 255),
                'nepali_month_id'       => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'agent_id'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'party_id'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'truck_number'          => array('type' => 'varchar',       'constraint' => 255),
                'type_id'               => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'brand_id'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'quantity'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'rate'                  => array('type' => 'float',         'constraint' => 32,     'null' => TRUE),
                'discount'              => array('type' => 'float',         'constraint' => 32,     'null' => TRUE),
                'gross_total'           => array('type' => 'float',         'constraint' => 32,     'null' => TRUE),
                'is_processed'          => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,   'default'=> 0),
                'agent_name'            => array('type' => 'varchar',       'constraint' => 255),
            ));

            $this->dbforge->create_table('tbl_sales_temporary', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('tbl_sales_temporary');
    }
}