<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* PACKAGE DESCRIPTION
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/** @defgroup module_project_controller Project Controller Module
* \brief Master Module for Project Controller
* \details This module is used for managing data as master records for taxable and non taxable item of different project controller
*/

/**
* @addtogroup module_project_controller 
* @{
*/

/**
* \class Project Controller
* \brief Controller Class for managing master items of different project controller 
*/

/**
*@}
*/

class Project_Controller extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('munvdcs/munvdc_model');
    }


    public function _get_search_param()
    {
    //search_params helper (project_helper);
        search_params();
    }

    public function get_groups_combo_json() 
    {
        $this->load->model('groups/group_model');

    // $this->db->where_not_in('id', array(1,2));
        $current_user_groups = $this->aauth->get_user_groups();
        if(isset($current_user_groups[1])) {
            $this->db->where('id >', $current_user_groups[1]->group_id);
        }

        $this->group_model->order_by('group_name asc');

        $rows=$this->group_model->findAll(null, array('id','group_name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select Group'));

        echo json_encode($rows);
        exit;
    }

    public function check_duplicate() 
    {
        list($module, $model) = explode("/", $this->input->post('model'));
        $field = $this->input->post('field');
        $value = $this->input->post('value');

        $this->db->where($field, $value);

        $this->load->model($this->input->post('model'));

        if ($this->input->post('id')) {
            $this->db->where('id <>', $this->input->post('id'));
        }

        $total=$this->$model->find_count();

        if ($total == 0) 
            echo json_encode(array('success' => true));
        else
            echo json_encode(array('success' => false));
    }

    public function check_duplicate_scheme() 
    {
        list($fiscal_year_id, $fiscal_year) = get_current_fiscal_year();
        list($module, $model) = explode("/", $this->input->post('model'));
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $nepali_month_id = $this->input->post('nepali_month_id');

        $this->load->model($this->input->post('model'));

        if ($this->input->post('id')) {
            $this->db->where('id <>', $this->input->post('id'));
        }
        
        $this->db->where($field, $value);
        $this->db->where('fiscal_year_id',$fiscal_year_id);
        $this->db->where('nepali_month_id',$nepali_month_id);
        

        $total=$this->$model->find_count();

        // echo $this->db->last_query();
        
        if ($total == 0) 
            echo json_encode(array('success' => true));
        else
            echo json_encode(array('success' => false));
    }

    public function check_duplicate_monthly_target() 
    {
        list($module, $model) = explode("/", $this->input->post('model'));
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $nepali_month_id = $this->input->post('nepali_month_id');

        $this->load->model($this->input->post('model'));

        if ($this->input->post('id')) {
            $this->db->where('id <>', $this->input->post('id'));
        }
        
        $this->db->where($field, $value);
        $this->db->where('nepali_month_id',$nepali_month_id);

        $total=$this->$model->find_count();
        
        if ($total == 0) 
            echo json_encode(array('success' => true));
        else
            echo json_encode(array('success' => false));
    }

    public function get_provinces_combo_json() 
    {
        $this->db->where('type', 'PROVINCE');

        $this->munvdc_model->order_by('name asc');

        $rows=$this->munvdc_model->findAll(null, array('id','name'));

        echo json_encode($rows);

    }

    public function get_districts_combo_json() 
    {
        $parent_id = $this->input->get('parent_id');

        $this->db->where('type', 'DISTRICT');
        $this->db->where('parent_id', $parent_id);

        $this->munvdc_model->order_by('name asc');

        $rows=$this->munvdc_model->findAll(null, array('id','name'));

        echo json_encode($rows);

    }

    public function get_mun_vdcs_combo_json() 
    {
        $parent_id = $this->input->get('parent_id');

        $this->db->where('type', 'MUN/VDC');

        $this->db->where('parent_id', $parent_id);

        $this->munvdc_model->order_by('name asc');

        $rows=$this->munvdc_model->findAll(null, array('id','name'));

        echo json_encode($rows);
    }

    public function get_agents_combo_json() 
    {
        $this->load->model('agents/agent_model');
        
        $this->agent_model->order_by('name asc');

        $rows=$this->agent_model->findAll(null, array('id','name'));

        echo json_encode($rows);
    }

    public function get_nepali_month_combo_json() 
    {
        $this->load->model('nepali_months/nepali_month_model');
        
        $this->nepali_month_model->order_by('name asc');

        $rows=$this->nepali_month_model->findAll(null, array('id','name'));

        echo json_encode($rows);
    }

    public function get_sales_person_combo_json() 
    {
        $this->load->model('users/user_model');
        
        $this->user_model->_table = "view_user_groups";

        $this->user_model->order_by('fullname asc');

        $this->db->where('group_id <>', 1);

        $this->db->where('group_id', SALES_PERSON_GROUP_ID);

        $rows=$this->user_model->findAll(null, array('user_id','fullname'));

        echo json_encode($rows);
    }
}