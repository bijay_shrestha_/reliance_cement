<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	public function __construct()
    {
    	parent::__construct();
        $this->load->library('Aauth');
    }

    public function auth_get()
    {

    }

    public function login_post()
    {
        $email = $this->input->post('email');
        $pass  = $this->input->post('pass');

        $result = $this->aauth->login($email, $pass);

        if($result)
        {
            $user_id = $this->session->userdata('id');
            $token = $this->_generate_token();
            $data = array('access_token'=>$token);
            $this->db->where('id',$user_id);
            $this->db->update('aauth_users',$data);

            $this->db->where('user_id',$user_id);
            $data  = $this->db->get('view_agent_user')->row();
        }
        else
        {
            $data = 'Incorrect Username or Password';
        }

        echo json_encode($data);
    }

    public function auth_delete()
    {

    }

    private function _generate_token()
    {
        return md5(uniqid(rand(10000000, 99999999), true));
    }

    public function change_password_post()
    {
        $this->load->library('form_validation');

        $max_length = $this->config->item('max', 'aauth');
        $min_length = $this->config->item('min', 'aauth');

        $this->form_validation->set_rules('new_password', 'New Confirmation', 'required|min_length['.$min_length.']|max_length['.$max_length.']');
        $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required|min_length['.$min_length.']|max_length['.$max_length.']|matches[new_password]');

        if ($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(validation_errors());
        }
        else
        {
            $user_id = getallheaders()['user_id'];
            $pass=$this->input->post('new_password');
            
            if ( !$this->aauth->update_user($user_id, FALSE, $pass, FALSE)){
                echo json_encode('Something went wrong. Please try again');
            } 

            $row = $this->aauth->get_user($user_id);

            if ( !is_object($row)){
                echo json_encode('Something went wrong. Please try again');
            } 

            echo json_encode('You have successfully changed your password. Please login again.');
        }
    }
}