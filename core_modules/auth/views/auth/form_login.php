<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url('assets/css/gentelella/nprogress/nprogress.css')?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo site_url('assets/css/gentelella/animate.css/animate.min.css')?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo site_url("assets/css/gentelella")?>/build/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php print form_open('auth')?>
              <h1>Login Form</h1>
              <?php print displayStatus();?>
              <div>
                <!-- <input type="text" class="form-control" placeholder="Username" required="" /> -->
                <input type="text" name="email" id="email" class="form-control" autocomplete="off" placeholder="<?php echo lang('general_username');?>" value="<?php echo set_value('email');?>" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div>
                <!-- <input type="password" class="form-control" placeholder="Password" required="" /> -->
                <input type="password" id="pass" name="pass" class="form-control" placeholder="<?php echo lang('general_password');?>"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
                <div class="form-group has-feedback">
                    <?php echo generate_recaptcha_field(); ?>
                </div>
              <div>
                <!-- <a class="btn btn-default submit" href="index.html">Log in</a> -->
                <button type="submit" class="btn btn-default">Login</button>  
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
                <a href="<?php echo site_url('forgot_password');?>" class="reset_pass">Forgot Password</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p> -->

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1> <?php echo config_item('site_name');?></h1>
                  <p>©2018 All Rights Reserved <a href="http://reliancecement.com.np/" target="new" style="margin-right: 0px">Reliance Cement</a> | <a href="http://pagodalabs.com" target="new">Pagodalabs</a></p>
                </div>
              </div>
            <?php print form_close()?>
          </section>
        </div>

      </div>
    </div>
    <script type="text/javascript" src="<?php echo site_url('assets/js/project.min.js');?>"></script>
  </body>
</html>



<?php /*
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo config_item('site_name'); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="shortcut icon" href="<?php echo site_url("assets/icons/favicon.ico");?> " type="image/x-icon">

        <script type="text/javascript">
        <!--
        var base_url = '<?php echo site_url();?>';
        var index_page = "";
        // -->
        </script>

        <link href="<?php echo site_url('assets/css/project.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-2.1.4.min.js');?>"></script>
    </head>
    
    <body class="login-page">

    <div class="login-box">
        <?php print form_open('auth')?>
        <div class="login-logo">
            <a href="<?php echo site_url('admin');?>"><?php echo config_item('site_name');?></a>
        </div><!-- /.login-logo -->

        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <?php print displayStatus();?>
            <div class="form-group has-feedback">
                <input type="text" name="email" id="email" class="form-control" autocomplete="off" placeholder="<?php echo lang('general_username');?>" value="<?php echo set_value('email');?>" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="pass" name="pass" class="form-control" placeholder="<?php echo lang('general_password');?>"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?php echo generate_recaptcha_field(); ?>
            </div>
            <div class="row">
                <div class="col-xs-8">    
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>  
                </div><!-- /.col -->
            </div>
        <?php print form_close()?>
        <a href="<?php echo site_url('forgot_password');?>">Forgot Password</a><br>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script type="text/javascript" src="<?php echo site_url('assets/js/project.min.js');?>"></script>
</body>
</html> */?>